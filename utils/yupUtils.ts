import * as Y from "yup";
import { notNumber, positiveNumber, invalidNumber } from "./formikUtils";

const compose = <R>(fn1: (a: R) => R, ...fns: Array<(a: R) => R>) =>
  fns.reduce((prevFn, nextFn) => (value) => nextFn(prevFn(value)), fn1);

export const oneOfEnum = <T>(enumObject: { [s: string]: T } | ArrayLike<T>) =>
  Y.mixed<T>().oneOf(Object.values(enumObject));

export const isNumber = (schema: Y.StringSchema<string>) => {
  return schema.matches(/^(-)?(([1-9][0-9]*)|(0))(?:\.[0-9]+)?$/, notNumber);
};

export const isPositive = (schema: Y.StringSchema<string>) => {
  return schema.matches(/^(([1-9][0-9]*)|(0))(?:\.[0-9]+)?$/, positiveNumber);
};

export const isMoney = (schema: Y.StringSchema<string>) => {
  return schema.matches(
    /^(([1-9][0-9]*)|(0))(?:\.[0-9]{1,2})?$/,
    invalidNumber
  );
};

export const positiveInteger = Y.number()
  .typeError(notNumber)
  .integer(invalidNumber)
  .min(0, positiveNumber);

export const numberIn2Decimal = compose(
  isNumber,
  isPositive,
  isMoney
)(Y.string());

export const numberWithDecimal = compose(isNumber, isPositive)(Y.string());

export const nullSchema = Y.mixed().nullable();

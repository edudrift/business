import {
  addDays,
  differenceInCalendarWeeks,
  format,
  subDays,
  parseISO,
  differenceInCalendarDays,
  isEqual,
} from "date-fns";

export const formatDateWithFullDateAndTime = (date: Date) => {
  return format(date, "d MMMM yyyy hh:mm a");
};

export const formatDateWithDateAndDayOfWeek = (date: Date) => {
  return format(date, 'd-M eee');
}

export const formatDateWithFullDate = (date: Date | string) => {
  let confirmedDate: Date;
  if (typeof date === "string") {
    confirmedDate = new Date(date);
  } else {
    confirmedDate = date;
  }

  return format(confirmedDate, "d MMMM yyyy");
};

export const formatDateWithDayAndMonth = (date: Date) => {
  return format(date, "d.M");
};

export const getNumberOfWeeksBetweenTwoDates = (
  startDate: Date,
  endDate: Date
) => {
  return differenceInCalendarWeeks(endDate, startDate);
};

export const differenceInDays = (startDate: Date, endDate: Date) => {
  return differenceInCalendarDays(endDate, startDate);
};

export const addDaysToDate = (date: Date, numberOfDays: number) => {
  return addDays(date, numberOfDays);
};

export const minusDaysFromDate = (date: Date, numberOfDays: number) => {
  return subDays(date, numberOfDays);
};

export const parseISOString = (date: string) => {
  return parseISO(date);
}

export const isSameDate = (dateLeft: Date, dateRight: Date) => {
  return isEqual(dateLeft, dateRight);
}
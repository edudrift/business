export const requiredText = "This is required.";

export const notNumber = "Not a number";

export const positiveNumber = "Number must be positive";

export const invalidNumber = "Number is invalid";

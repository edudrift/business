import { ICreditCardDetails } from "./ICreditCardDetails";
import { IProgramTypeEligibility } from "./IProgramTypeEligibility";

export interface IBusinessProfile {
  id: string;
  name: string;
  verified: boolean;
  website: string;
  country: string;

  programTypeEligibility: IProgramTypeEligibility[];
  cardDetails: ICreditCardDetails;
  stripeAccountConnectId: string;

  managerName: string;
  managerEmail: string;
  managerTitle: string;

  address: string;
  addressLineTwo: string;
  addressPostalCode: string;

  primaryContactName: string;
  primaryContactEmail: string;
  primaryContactMobileNumber: string;
  primaryContactOfficeNumber: string;
  primaryContactWhatsappNumber: string;
  primaryContactWeChatNumber: string;
  primaryContactLineNumber: string;
  primaryContactKakaoTalkNumber: string;

  secondaryContactName: string;
  secondaryContactEmail: string;
  secondaryContactMobileNumber: string;
  secondaryContactOfficeNumber: string;
  secondaryContactWhatsappNumber: string;
  secondaryContactWeChatNumber: string;
  secondaryContactLineNumber: string;
  secondaryContactKakaoTalkNumber: string;

  entityId: string;
  entityType: string;
  entityProofDocumentName: string;
  entityProofLink: string;
}

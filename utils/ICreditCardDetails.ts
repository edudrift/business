export interface ICreditCardDetails {
  lastFourDigitsCardNumber: number;
  cvc: number;
  expiryMonth: string;
  expiryYear: string;
  nameOnCard: string;
}

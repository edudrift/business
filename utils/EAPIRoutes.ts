export enum EProgramAPIRoutes {
  CREATE_PROGRAM_FROM_PROGRAM_TYPE = "/api/programs/b",
  GET_APPROVED_PROGRAM_TYPES = "/api/programs/b/eligibility/program-types",
  GET_SINGLE_PROGRAM = "/api/programs/b/:programId",
  GET_ALL_PROGRAMS = "/api/programs/b",
}

export enum EProgramProviderAPIRoutes {
  INIT_PROFILE = "/api/program-providers/init",
}

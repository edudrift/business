import { EProgramType } from "../modules/Programs/types/Programs.types";

export interface IProgramTypeEligibilityStatus {
  PENDING: "PENDING";
  APPROVED: "APPROVED";
  REJECTED: "REJECTED";
}

export interface IProgramTypeEligibility {
  id: string;
  status: IProgramTypeEligibilityStatus;
  businessId: string;
  programType: EProgramType;
}

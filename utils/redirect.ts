import { NextPageContext } from "next";
import Router from "next/router";

export default function redirect(
  url: string,
  httpCode?: number,
  ctx?: NextPageContext
) {
  if (ctx?.res && httpCode) {
    ctx.res.writeHead(httpCode, {
      Location: url,
      "Content-Type": "text/html; charset=utf-8"
    });
    ctx.res.end();
    return;
  } else {
    Router.replace(url);
  }
}

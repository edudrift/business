import { NextPageContext } from "next";
import { FairPackage } from "~/types/FairPackages.types";
import { FairPackageSchemaAsType } from "../components/Forms/Fairs/schema";
import TokenService from "../services/Token.service";
import { FairDetailsSchemaAsType } from "./../components/Forms/Fairs/schema";
import { Fair } from "./../types/Fairs.types";
import EHttpMethods from "./EHttpMethods";
import isofetch from "./isofetch";

export enum EFairsAPIRoutes {
  GET_ALL = "/api/admin/fairs",
  GET_ALL_PUBLISHED = "/api/fairs",
  CREATE_FAIR = "/api/admin/fairs/init",
  GET_ONE = "/api/admin/fairs/:id",
  INIT_FAIR_PACKAGE = "/api/admin/fairs/:id/init/packages",
  UPDATE_FAIR_PACKAGE = "/api/admin/fairs/:id/packages/:packageId",
  UPDATE_FAIR = "/api/admin/fairs/:id",
  SET_PUBLISH = "/api/admin/fairs/:id/publish",
  GET_FROM_SLUG = "/api/fairs/:slug",
  GET_FAIR_PACKAGE = "/api/fairs/packages/:packageId"
}

export default class FairsService {
  static async getFairPackageFromId(packageId: string) {
    const url = EFairsAPIRoutes.GET_FAIR_PACKAGE.replace(
      ":packageId",
      packageId
    );

    const response = await isofetch<FairPackage>(url, EHttpMethods.GET);
    if (response.status === 200 && response.data) {
      return response.data;
    } else {
      console.log(response);
      throw new Error("Something went wrong while fetching fair package");
    }
  }

  static async getOneFairFromSlug(slug: string) {
    const response = await isofetch<Fair>(
      EFairsAPIRoutes.GET_FROM_SLUG.replace(":slug", slug),
      EHttpMethods.GET
    );

    if (response.status === 200 && response.data) {
      return response.data;
    } else if (response.status === 404) {
      throw new ResourceNotFoundError();
    } else {
      console.log(response);
      throw new Error("Something went wrong while fetching fair package");
    }
  }

  static async publish(fair: Fair) {
    const response = await isofetch<Fair>(
      EFairsAPIRoutes.SET_PUBLISH.replace(":id", fair.id),
      EHttpMethods.POST,
      TokenService.getToken(),
      {
        publish: true
      }
    );

    if (response.status === 200 && response.data) {
      return response.data;
    } else if (response.status === 404) {
      throw new ResourceNotFoundError();
    }
  }

  static async unpublish(fair: Fair) {
    const response = await isofetch<Fair>(
      EFairsAPIRoutes.SET_PUBLISH.replace(":id", fair.id),
      EHttpMethods.POST,
      TokenService.getToken(),
      {
        publish: false
      }
    );

    if (response.status === 200 && response.data) {
      return response.data;
    }
  }

  static async getAll(ctx: NextPageContext) {
    const response = await isofetch<Fair[]>(
      EFairsAPIRoutes.GET_ALL,
      EHttpMethods.GET,
      TokenService.getToken(ctx)
    );
    if (response.status === 200 && response.data) {
      return response.data;
    } else {
      throw new GetAllFairsError();
    }
  }

  static async getAllPublished() {
    const response = await isofetch<Fair[]>(
      EFairsAPIRoutes.GET_ALL_PUBLISHED,
      EHttpMethods.GET
    );

    if (response.status === 200 && response.data) {
      return response.data;
    }
  }

  static async tryToCreateFair(values: FairDetailsSchemaAsType) {
    const response = await isofetch<Fair>(
      EFairsAPIRoutes.CREATE_FAIR,
      EHttpMethods.POST,
      TokenService.getToken(),
      values
    );

    if (response.status === 201 && response.data) {
      return response.data;
    } else {
      throw new CreateFairsError();
    }
  }

  static async getFair(ctx: NextPageContext, fairId: string) {
    const response = await isofetch<Fair>(
      EFairsAPIRoutes.GET_ONE.replace(":id", fairId),
      EHttpMethods.GET,
      TokenService.getToken(ctx)
    );

    if (response.status === 200 && response.data) {
      return response.data;
    } else {
      console.log(response);
      throw new FairNotFound();
    }
  }

  static async initialiseNewPackage(fair: Fair): Promise<FairPackage> {
    const response = await isofetch<FairPackage>(
      EFairsAPIRoutes.INIT_FAIR_PACKAGE.replace(":id", fair.id),
      EHttpMethods.POST,
      TokenService.getToken()
    );

    if (response.status === 201 && response.data) {
      return response.data;
    } else {
      console.log(response);
      throw new Error("Problem initiating a new fair package.");
    }
  }

  static async updateFairPackage(
    fairId: string,
    fairPackageId: string,
    fairPackage: FairPackageSchemaAsType
  ) {
    const response = await isofetch<FairPackage>(
      EFairsAPIRoutes.UPDATE_FAIR_PACKAGE.replace(":id", fairId).replace(
        ":packageId",
        fairPackageId
      ),
      EHttpMethods.PUT,
      TokenService.getToken(),
      fairPackage
    );

    if (response.status === 204 && response.data) {
      return response.data;
    }
  }

  static async updateFairDetails(fair: Fair, values: FairDetailsSchemaAsType) {
    const response = await isofetch<Fair>(
      EFairsAPIRoutes.UPDATE_FAIR.replace(":id", fair.id),
      EHttpMethods.PUT,
      TokenService.getToken(),
      values
    );

    if (response.status === 204 && response.data) {
      return response.data;
    }
  }
}

// ERRORS

export class GetAllFairsError extends Error {}

export class CreateFairsError extends Error {}

export class FairNotFound extends Error {
  statusCode = 403;
}

export class ResourceNotFoundError extends Error {
  statusCode = 404;
}

import fetch from "isomorphic-unfetch";
import EHttpMethods from "./EHttpMethods";

interface IHeaders {
  Accept: string;
  "Content-Type"?: string;
  Authorization?: string;
}

export async function isofetchMultipart(
  url: string,
  formData: FormData,
  token: string
) {
  try {
    const response = await fetch(`${process.env.API_URL}${url}`, {
      body: formData,
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${token}`,
      },
      method: EHttpMethods.POST,
    });

    try {
      const data = await response.json();

      return {
        status: response.status,
        data,
      };
    } catch (e) {
      return {
        status: response.status,
        data: undefined,
      };
    }
  } catch (e) {
    console.log("Error making the request");
    console.log(e);
    throw e;
  }
}

export async function isofetch<T = undefined>(
  url: string,
  httpMethod: EHttpMethods,
  token?: string,
  payload?: object,
  fileUpload?: boolean
): Promise<{ status: number; data: T | undefined }> {
  try {
    const body =
      httpMethod === EHttpMethods.GET ? null : JSON.stringify({ ...payload });

    const headers: IHeaders = {
      Accept: "application/json",
      "Content-Type": "application/json",
    };

    if (fileUpload) {
      delete headers["Content-Type"];
    }

    if (token) headers.Authorization = `Bearer ${token}`;

    const rawResponse = await fetch(`${process.env.API_URL}${url}`, {
      body,
      headers: (headers as unknown) as Record<string, string>,
      method: httpMethod,
    });

    try {
      const data = await rawResponse.json();
      return {
        status: rawResponse.status,
        data,
      };
    } catch (e) {
      return {
        status: rawResponse.status,
        data: undefined,
      };
    }
  } catch (e) {
    console.log(e);
    throw e;
  }
}

export const ensureStatus200WithData = (response: {
  status: number;
  data?: any;
}) => {
  if (response.status === 200 && response.data) {
    return response.data;
  } else {
    // todo: better error here
    console.error(response);
    throw new Error(response.data.message);
  }
};

export default isofetch;

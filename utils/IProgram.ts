import { EProgramType } from "../modules/Programs/types/Programs.types";

export interface IProgram {
  id: string;
  businessId: string;
  type: EProgramType;
}

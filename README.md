# EduDrift Platform Version 1

## Getting started

### Running the project

We are using `Yarn` here, get it installed.

1. `yarn install` on the root of this project
2. `yarn start:dev`

Our application would run on port 5000 with the development environment configuration, on [localhost:5000](http://localhost:5000)

### Hosting the project

The project is automatically deployed by `Vercel` when the application is pushed
to the staging branch. The changes would become live on the website [edudrift.dev](https://edudrift.dev).

## Development Guide

### Architecture

Overall _architecture diagram_ for our EduDrift Platform can be seen below:

![Architecture](./docs/images/architecture.svg)

In `Next.js`, the files in the `/pages` directory are associated with a route based on its filename.

When the user first visits a `page`:

1. The `page` would render the React Components in the `UI` and run the javascript inside the `Logic`.
2. If initial data is required to be populated, the application would communicate
with the backend application [EduDrift's API](https://gitlab.com/edudrift/api) by the javascript `Logic`.
3. The data would be passed as props to React Components. A `Component` can 
have many children `Component` inside the same folder or include a `SharedComponent`
4. A `Component` that has any user-interaction, _e.g. MouseClick, onChange_ events would be handled by `Logic`. `Logic` would handle the manipulation of state, data, and communication with the backend application.

### Communication with backend

The `Logic` handles the communication with the backend application [EduDrift's API](https://gitlab.com/edudrift/api):

1. The javascript `Logic` would first call a `Service` specific to the `UI`'s `Component`.
3. To authenticate the current user, the application would query the `TokenService` to get the user's token.
4. A payload would then be generated consisting of:
    * user's token (if any)
    * data
5. The `FetchService` would then sent the resulting payload to the `API_URL` of [EduDrift's API](https://gitlab.com/edudrift/api) or the `API_URL` locally at [localhost:3000](http://localhost:3000)

### Routing

Our application extends `NextApp`, to allow us to create a custom `MyApp`. Thus, our `MyApp` is able to change the `Page` to a new `XYZPage`.

![Routing](./docs/images/routing.svg)

### Frontend Page Component

A particular page component passed as a prop to our custom `MyApp` has the following structure:

![Frontend Page Component](./docs/images/component.svg)

A particular page component _e.g. `XYZPage`_ consists of:

* Children Components _e.g. `XYZComponent1`, `XYZComponent2`, `XYZComponent3`_
* `Services`
* `DataEnum`
    * Enumerable for our data
* `DataType`
    * Strict typing for our data

### Features

#### User Verification Loop

When the user first visits the EduDrift Platform, it would need to login or register. 

![User Verification Loop Use Case](./docs/images/userusecase.svg)

A user can:
* login
* register
* reset his email
* verify his email address

A user going logging in to his own account or registering a new account would have to go throught the following user workflow. The activity diagram for the user verification loop can be seen below:

![User Verification Loop](./docs/images/userverificationloop.svg)

When registering for a new user account:
1. User signs up and fills up account information
2. System would send a verification email
3. User receives the verification email
4. User verifies their email address
5. Email Verification success
6. User is authenticated

When user logins:
1. User logins with his login credentials
2. If user forgots his password, he can reset his password
    1. System would send user a reset password email
    2. User resets a new password
    3. Password change successful
    4. System sends user a email that password was changed successfully
    5. User is authenticated
3. If user did not verify their account
    1. System would redirect user to verify their email
    2. System resends user a new verification email
    3. User verified theirs email address
    4. Email verification success
4. User is authenticated

#### Business Verification Loop

A Business would have to complete the business profile and its onboarding before it can proceed to create programs.  The activity diagram for the business verification loop can be seen below:

![Business Verification Loop](./docs/images/businessverificationloop.svg)

## [Contributing](CONTRIBUTING.md)

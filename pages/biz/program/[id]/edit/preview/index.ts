import ViewProgramPage from "../../../../../../modules/Programs/components/ViewProgramPage";
import { NextPageContext } from "next";
import { ProgramsService } from "../../../../../../modules/Programs/services/ProgramsService";
import { getUserProfileOrRedirect } from "../../../../../../services/Auth.functions";
import redirect from "../../../../../../utils/redirect";
import withError from "../../../../../../components/HOCs/withError";

ViewProgramPage.getInitialProps = async (ctx: NextPageContext) => {
  try {
    const user = await getUserProfileOrRedirect(ctx);
    const program = await ProgramsService.getOne(ctx.query.id as string);
    if (!program.startDate || !program.endDate) {
      redirect(`/biz/program/${program.id}/edit`, 302, ctx);
    }

    const isProgramFromUser = program.provider.id === user?.programProvider.id;
    if (!isProgramFromUser) {
      redirect(`/login?goTo=/biz/programs`, 301, ctx);
    }

    return { user, program, canEdit: isProgramFromUser };
  } catch (e) {
    throw e;
  }
};

export default withError(ViewProgramPage);

import NextApp from "next/app";
import Head from "next/head";
import * as React from "react";

import CssBaseline from "@material-ui/core/CssBaseline";
import { ThemeProvider } from "@material-ui/core/styles";

import DateFnsUtils from "@date-io/date-fns";
import { MuiPickersUtilsProvider } from "@material-ui/pickers";

import { AuthProvider } from "../services/Auth.context";
import { GlobalMessagingProvider } from "../services/GlobalMessaging.context";

import { AcceptCookieBanner } from "../modules/Shared/components/AcceptCookieBanner";
import theme from "../theme";

class MyApp extends NextApp {
  componentDidMount() {
    // const jssStyles = document.querySelector("#jss-server-side");
    // if (jssStyles && jssStyles.parentNode) {
    //   jssStyles.parentNode.removeChild(jssStyles);
    // }
  }

  render() {
    const { Component, pageProps } = this.props;

    return (
      <React.Fragment>
        <Head>
          <title>EduDrift</title>
          <link rel="icon" href="/static/favicon.ico" />
          <meta
            name="viewport"
            content="minimum-scale=1, initial-scale=1, width=device-width"
          />
        </Head>

        <ThemeProvider theme={theme}>
          <AuthProvider>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <GlobalMessagingProvider>
                <CssBaseline />
                <Component {...pageProps} />

                <AcceptCookieBanner />
              </GlobalMessagingProvider>
            </MuiPickersUtilsProvider>
          </AuthProvider>
        </ThemeProvider>
      </React.Fragment>
    );
  }
}

export default MyApp;

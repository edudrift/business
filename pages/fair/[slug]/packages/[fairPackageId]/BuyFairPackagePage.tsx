import React from "react";

import { Button, Grid } from "@material-ui/core";
import { loadStripe } from "@stripe/stripe-js";
import { NextPage } from "next";
import getConfig from "next/config";

import FairPackagePurchaseOverview from "../../../../../components/FairPackagePurchaseOverview";
import GenericLayout from "../../../../../components/GenericLayout/GenericLayout";
import withError from "../../../../../components/HOCs/withError";
import { Nextcrumbs } from "../../../../../components/Nextcrumbs/Nextcrumbs";
import { PaddedPaper } from "../../../../../modules/Shared/components/PaddedPaper";
import { getUserProfileOrRedirect } from "../../../../../services/Auth.functions";
import { FairPackageService } from "../../../../../services/FairPackage.service";
import FairPackage from "../../../../../types/FairPackages.types";
import Fair from "../../../../../types/Fairs.types";
import User from "../../../../../types/user.types";
import FairsService from "../../../../../utils/Fairs.service";

const BuyFairPackagePage: NextPage<Props> = (props) => {
  const [checkingOut, setCheckingOut] = React.useState(false);

  const proceedToCheckout = async () => {
    setCheckingOut(true);
    // todo: put in try/catch for service throws
    const sessionId = await FairPackageService.checkout(props.fairPackage.id);
    const { STRIPE_KEY } = getConfig().publicRuntimeConfig;

    const stripe = await loadStripe(STRIPE_KEY);

    if (stripe && sessionId) {
      const { error } = await stripe.redirectToCheckout({
        // Make the id field from the Checkout Session creation API response
        // available to this file, so you can provide it as parameter here
        // instead of the {{CHECKOUT_SESSION_ID}} placeholder.
        sessionId,
      });

      if (error) {
        alert(error);
        setCheckingOut(false);
      }
    } else {
      alert("Cannot load stripe");
    }
  };

  return (
    <GenericLayout user={props.user}>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Nextcrumbs
            crumbs={[
              { title: "Home", href: "/" },
              {
                title: props.fair.name,
                href: "/fair/[slug]",
                as: `/fair/${props.fair.slug}`,
              },
              {
                title: "Packages",
                href: "/fair/[slug]/packages",
                as: `/fair/${props.fair.slug}/packages`,
              },
              {
                title: `Confirm purchase of ${props.fairPackage.title} package`,
              },
            ]}
          />
        </Grid>
        <Grid item xs={12} md={6} container spacing={2}>
          <Grid item>
            <FairPackagePurchaseOverview
              fair={props.fair}
              package={props.fairPackage}
              alertOptions={{
                severity: "info",
                title: "Take note!",
                body:
                  "After purchasing, you will be instructed to complete your participation details.",
              }}
            />
          </Grid>

          <Grid item xs={12}>
            <PaddedPaper fullWidth>
              <Grid container spacing={2}>
                <Grid item xs container justify="center">
                  <img src="/powered_by_stripe@2x.png" height="45px" />
                </Grid>
                <Grid item xs container justify="center">
                  <Button
                    fullWidth
                    size="large"
                    variant="contained"
                    color="primary"
                    disabled={checkingOut}
                    onClick={proceedToCheckout}
                  >
                    {checkingOut ? "Preparing link for you..." : "Checkout now"}
                  </Button>
                </Grid>
              </Grid>
            </PaddedPaper>
          </Grid>
        </Grid>
      </Grid>
    </GenericLayout>
  );
};

type Props = {
  user?: User;
  fairPackage: FairPackage;
  fair: Fair;
};

BuyFairPackagePage.getInitialProps = async (ctx): Promise<Props> => {
  console.log("Buying fair package");
  const user = await getUserProfileOrRedirect(ctx);
  const fair = await FairsService.getOneFairFromSlug(ctx.query.slug as string);

  const fairPackage = await FairsService.getFairPackageFromId(
    ctx.query.fairPackageId as string
  );

  return {
    user,
    fairPackage,
    fair,
  };
};

export default withError(BuyFairPackagePage);

import { Box, Card, CardContent, Grid, Typography } from "@material-ui/core";
import { NextPage } from "next";

import React from "react";
import GenericLayout from "../../../../components/GenericLayout/GenericLayout";

import ButtonLink from "../../../../components/ButtonLink";
import { getUserOrReturnUndefined } from "../../../../services/Auth.functions";
import theme from "../../../../theme";
import Fair from "../../../../types/Fairs.types";
import User from "../../../../types/user.types";
import FairsService from "../../../../utils/Fairs.service";
import { Nextcrumbs } from "../../../../components/Nextcrumbs/Nextcrumbs";

const CardHeaderBlock: React.FC<{ title: string }> = ({ title }) => (
  <Grid
    container
    justify="center"
    alignItems="center"
    style={{
      background: theme.palette.primary.main,
      height: "40px",
    }}
  >
    <Grid item>
      <Typography variant="h6" align="center" style={{ color: "white" }}>
        {title}
      </Typography>
    </Grid>
  </Grid>
);

const SingleFairPackagesPage: NextPage<Props> = (props) => {
  return (
    <GenericLayout user={props.user}>
      <Nextcrumbs
        crumbs={[
          { title: "Home", href: "/" },
          {
            title: props.fair.name,
            href: "/fair/[slug]",
            as: `/fair/${props.fair.slug}`,
          },
          { title: "Packages" },
        ]}
      />
      <Grid
        container
        style={{
          paddingTop: theme.spacing(6),
          paddingBottom: theme.spacing(6),
        }}
      >
        <Grid item xs>
          <Typography component="h1" variant="h3" align="center" gutterBottom>
            Pricing Packages
          </Typography>
        </Grid>
      </Grid>
      <Grid container spacing={3} justify="center">
        {props.fair.packages.map((fairPackage) => (
          <Grid item xs={4} key={fairPackage.id}>
            <Card raised>
              <CardHeaderBlock title={fairPackage.title} />
              <Box
                style={{
                  background: theme.palette.grey[100],
                  padding: theme.spacing(4),
                }}
              >
                <CardContent>
                  <Typography align="center" variant="h3" component="p">
                    ${fairPackage.price}
                  </Typography>
                </CardContent>
              </Box>

              <CardContent>
                <Grid container spacing={2}>
                  <Grid item xs={12}>
                    <Typography variant="h5" component="p" align="center">
                      <strong>{fairPackage.maxPhotos} photos</strong>
                    </Typography>
                  </Grid>

                  <Grid item xs={12}>
                    <Typography variant="h5" component="p" align="center">
                      <strong>{fairPackage.maxBrochures} brochures</strong>
                    </Typography>
                  </Grid>

                  <Grid item xs={12}>
                    <Typography variant="h5" component="p" align="center">
                      <strong>{fairPackage.maxConsultants} consultants</strong>
                    </Typography>
                  </Grid>
                </Grid>
              </CardContent>

              <CardContent>
                <Grid container justify="center">
                  <Grid item>
                    <ButtonLink
                      href="/fair/[slug]/packages/[fairPackageId]"
                      as={`/fair/${props.fair.slug}/packages/${fairPackage.id}`}
                      size="large"
                      variant="outlined"
                      color="primary"
                    >
                      Buy this
                    </ButtonLink>
                  </Grid>
                </Grid>
              </CardContent>
            </Card>
          </Grid>
        ))}
      </Grid>
    </GenericLayout>
  );
};

type Props = {
  fair: Fair;
  user: User;
};

SingleFairPackagesPage.getInitialProps = async (ctx) => {
  try {
    const user = await getUserOrReturnUndefined(ctx);
    const fair = await FairsService.getOneFairFromSlug(
      ctx.query.slug as string
    );

    return {
      fair,
      user,
    } as Props;
  } catch (e) {
    throw e;
  }
};

export default SingleFairPackagesPage;

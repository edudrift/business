import React from "react";

import { Grid, Typography } from "@material-ui/core";
import { NextPage, NextPageContext } from "next";
import ButtonLink from "../../../components/ButtonLink";
import GenericLayout from "../../../components/GenericLayout/GenericLayout";
import withError from "../../../components/HOCs/withError";
import { Nextcrumbs } from "../../../components/Nextcrumbs/Nextcrumbs";
import { getUserOrReturnUndefined } from "../../../services/Auth.functions";
import Fair from "../../../types/Fairs.types";
import User from "../../../types/user.types";
import FairsService from "../../../utils/Fairs.service";
import { PaddedPaper } from "../../../modules/Shared/components/PaddedPaper";

const SingleFairPage: NextPage<Props> = ({ fair, user }) => {
  console.log(fair);
  return (
    <GenericLayout user={user}>
      <PaddedPaper fullWidth>
        <Grid container spacing={2}>
          <Grid item>
            <Nextcrumbs
              crumbs={[
                {
                  title: "Home",
                  href: "/",
                },
                { title: fair.name },
              ]}
            />
          </Grid>
          <Grid item container justify="space-between">
            <Grid item container justify="space-between">
              <Grid item>
                <Typography variant="h4" component="h1">
                  {fair.name}
                </Typography>
              </Grid>

              <Grid item>
                <ButtonLink
                  href="/fair/[slug]/packages"
                  as={`/fair/${fair.slug}/packages`}
                  variant="outlined"
                  color="primary"
                >
                  View packages
                </ButtonLink>
              </Grid>
            </Grid>
          </Grid>

          <Grid item xs={12}>
            <Typography variant="body1" style={{ whiteSpace: "pre-line" }}>
              {fair.description}
            </Typography>
          </Grid>
        </Grid>
      </PaddedPaper>
    </GenericLayout>
  );
};

type Props = {
  fair: Fair;
  user: User;
};

SingleFairPage.getInitialProps = async (ctx: NextPageContext) => {
  const user = await getUserOrReturnUndefined(ctx);
  const fair = await FairsService.getOneFairFromSlug(ctx.query.slug as string);

  return {
    user,
    fair,
  } as Props;
};

export default withError(SingleFairPage);

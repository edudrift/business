import React from "react";

import { Button, Grid, Link } from "@material-ui/core";
import { Form, Formik, FormikHelpers } from "formik";
import NextLink from "next/link";
import * as Yup from "yup";

import { FormikTextField } from "../components/Forms/components/FormikTexField";
import { AuthService } from "../modules/Authentication/AuthService";
import {
  EmailTakenError,
  ProgramProviderNameTakenError,
} from "../modules/Authentication/errors";
import { FullHeightCenteredFormWithHeaderLayout } from "../modules/Shared/components/FullHeightCenteredFormWithHeaderLayout";
import { TypographyWithPxFontAndColor } from "../modules/Shared/components/TypographyWithPxFontAndColor";
import theme from "../theme";
import VerifyYourEmailMessage from "../modules/Authentication/components/VerifyYourEmailMessage";

const SignupSchema = Yup.object().shape({
  programProviderName: Yup.string().required("Please enter a valid name."),
  email: Yup.string()
    .email("Please enter a valid email address.")
    .required("Please enter a valid email address."),
  password: Yup.string()
    .required("Your password must be minimum 8 characters long.")
    .min(8, "Your password must be minimum 8 characters long."),
  confirmPassword: Yup.string().when("password", {
    is: (val) => val && val.length > 0,
    then: Yup.string()
      .oneOf(
        [Yup.ref("password")],
        "Your passwords did not match. Please try again."
      )
      .required("Your passwords did not match. Please try again."),
  }),
});

export default () => {
  const [
    showVerificationCodeMessage,
    setShowVerificationCodeMessage,
  ] = React.useState(false);
  const [email, setEmail] = React.useState<string>();

  const onSubmit = async (
    values: Yup.InferType<typeof SignupSchema>,
    { setFieldError, setSubmitting }: FormikHelpers<typeof values>
  ) => {
    setSubmitting(true);
    try {
      await AuthService.signUp(values);
      setShowVerificationCodeMessage(true);
      setEmail(values.email);
    } catch (e) {
      setSubmitting(false);
      if (e instanceof EmailTakenError) {
        setFieldError(
          "email",
          "This email is a found in the system, would you like to log in instead."
        );
      } else if (e instanceof ProgramProviderNameTakenError) {
        setFieldError(
          "programProviderName",
          "This name has been taken. Please try something else."
        );
      }
    }
  };

  const ErrorMessageWithLink = (() =>
    (<React.Fragment>
        This email is a found in the system, would you like to{" "}
          <NextLink href="login" passHref>
            <Link color="inherit">
            <strong>log in</strong>
            </Link>
        </NextLink>{" "}
        instead
      </React.Fragment>
    )
  );

  if (showVerificationCodeMessage) {
    return (<VerifyYourEmailMessage email={email!}/>);
  }

  return (
    <FullHeightCenteredFormWithHeaderLayout
      header="Create an account"
      renderBelowPaper={() => (
        <TypographyWithPxFontAndColor sizeInPx={12} color="#fff">
          Already have an account?{" "}
          <NextLink href="login" passHref>
            <Link color="inherit">
              <strong>Sign in</strong>
            </Link>
          </NextLink>{" "}
          instead
        </TypographyWithPxFontAndColor>
      )}
    >
      <Formik
        validationSchema={SignupSchema}
        initialValues={{
          programProviderName: "",
          email: "",
          password: "",
          confirmPassword: "",
        }}
        onSubmit={onSubmit}
      >
        {({ errors, touched, isSubmitting }) => {
          return (
            <Form>
              <Grid container spacing={6}>
                <Grid item container spacing={2} direction="column">
                  <Grid item xs>
                    <FormikTextField
                      name="programProviderName"
                      label="Business/Organisation Name"
                      errors={errors}
                      touched={touched}
                    />
                  </Grid>

                  <Grid item xs>
                    <FormikTextField
                      name="email"
                      label="Email address"
                      errors={errors}
                      touched={touched}
                      ErrorMessageComponent={ErrorMessageWithLink}
                    />
                  </Grid>

                  <Grid item xs>
                    <FormikTextField
                      name="password"
                      label="Password"
                      errors={errors}
                      touched={touched}
                      type="password"
                    />
                  </Grid>

                  <Grid item xs>
                    <FormikTextField
                      name="confirmPassword"
                      label="Confirm password"
                      errors={errors}
                      touched={touched}
                      type="password"
                    />
                  </Grid>
                </Grid>

                <Grid item container spacing={1} direction="column">
                  <Grid item xs>
                    <TypographyWithPxFontAndColor
                      sizeInPx={10}
                      color={theme.palette.primary.main}
                    >
                      By registering an EduDrift account, I acknowledge that I
                      have read and agree to EduDrift's{" "}
                      <NextLink href="/terms" passHref>
                        <Link>
                          <strong>Terms & Conditions</strong>
                        </Link>
                      </NextLink>{" "}
                      and{" "}
                      <NextLink href="/privacy" passHref>
                        <Link>
                          <strong>Privacy Policy</strong>
                        </Link>
                      </NextLink>
                      .
                    </TypographyWithPxFontAndColor>
                  </Grid>

                  <Grid item xs>
                    <Button
                      variant="contained"
                      color="primary"
                      type="submit"
                      disableElevation
                      fullWidth
                      size="large"
                      disabled={isSubmitting}
                    >
                      Let's go
                    </Button>
                  </Grid>
                </Grid>
              </Grid>
            </Form>
          );
        }}
      </Formik>
    </FullHeightCenteredFormWithHeaderLayout>
  );
};

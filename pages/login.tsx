import React from "react";

import { Button, Grid, Link } from "@material-ui/core";
import { Form, Formik, FormikHelpers } from "formik";
import NextLink from "next/link";
import { useRouter } from "next/router";
import * as Yup from "yup";

import { FormikTextField } from "../components/Forms/components/FormikTexField";
import {
  EmailNotFoundError,
  WrongPasswordError,
} from "../modules/Authentication/components/LoginForm/errors";
import { loginOrThrow } from "../modules/Authentication/components/LoginForm/functions";
import { FullHeightCenteredFormWithHeaderLayout } from "../modules/Shared/components/FullHeightCenteredFormWithHeaderLayout";
import { TypographyWithPxFontAndColor } from "../modules/Shared/components/TypographyWithPxFontAndColor";
import { EProgramProviderStatus } from "../types/ProgramProvider.types";

const LoginSchema = Yup.object().shape({
  password: Yup.string()
    .required("Your password must be minimum 8 characters long.")
    .min(8, "Your password must be minimum 8 characters long."),
  email: Yup.string()
    .email("Please enter a valid email address.")
    .required("Please enter a valid email address."),
});

export default () => {
  const router = useRouter();

  const onSubmit = async (
    values: Yup.InferType<typeof LoginSchema>,
    { setFieldError, setSubmitting }: FormikHelpers<typeof values>
  ) => {
    setSubmitting(true);
    try {
      const userInformation = await loginOrThrow(values.email, values.password);
      if (!userInformation?.isEmailVerified) {
        router.push("/verify/email/alert");
      } else if (
        userInformation?.programProviderStatus === EProgramProviderStatus.NEW
      ) {
        router.push("/biz/profile");
      } else {
        router.query.goTo
          ? router.push(router.query.goTo as string)
          : router.push("/biz");
      }
    } catch (e) {
      setSubmitting(false);
      if (e instanceof EmailNotFoundError) {
        setFieldError(
          "email",
          "Email not found. Please enter a valid email address."
        );
      } else if (e instanceof WrongPasswordError) {
        setFieldError(
          "password",
          "Wrong password. Try again or click Forgot Password to reset it."
        );
      }
    }
  };

  return (
    <FullHeightCenteredFormWithHeaderLayout
      header="Sign in to your account"
      renderBelowPaper={() => (
        <TypographyWithPxFontAndColor sizeInPx={12} color="#fff">
          Don't have an account?{" "}
          <NextLink href="register" passHref>
            <Link color="inherit">
              <strong>Sign up</strong>
            </Link>
          </NextLink>
        </TypographyWithPxFontAndColor>
      )}
    >
      <Formik
        validationSchema={LoginSchema}
        initialValues={{
          email: "",
          password: "",
        }}
        onSubmit={onSubmit}
      >
        {({ errors, touched, isSubmitting }) => {
          return (
            <Form>
              <Grid container spacing={10}>
                <Grid item container spacing={2} direction="column">
                  <Grid item xs>
                    <FormikTextField
                      name="email"
                      label="Email address"
                      errors={errors}
                      touched={touched}
                    />
                  </Grid>

                  <Grid item xs>
                    <FormikTextField
                      name="password"
                      label="Password"
                      errors={errors}
                      touched={touched}
                      type="password"
                    />
                  </Grid>
                </Grid>

                <Grid
                  item
                  container
                  justify="space-between"
                  alignItems="center"
                >
                  <Grid item>
                    <NextLink href="/forgot-password" passHref>
                      <Link color="inherit">Forgot password?</Link>
                    </NextLink>
                  </Grid>

                  <Grid item>
                    <Button
                      variant="contained"
                      color="primary"
                      type="submit"
                      disableElevation
                      fullWidth
                      size="large"
                      disabled={isSubmitting}
                    >
                      Next
                    </Button>
                  </Grid>
                </Grid>
              </Grid>
            </Form>
          );
        }}
      </Formik>
    </FullHeightCenteredFormWithHeaderLayout>
  );
};

import React from "react";

import { Button, Grid, Link } from "@material-ui/core";
import { Form, Formik, FormikHelpers } from "formik";
import NextLink from "next/link";
import * as Yup from "yup";

import { FormikTextField } from "../components/Forms/components/FormikTexField";
import { AuthService } from "../modules/Authentication/AuthService";
import { AccountWithEmailNotFound } from "../modules/Authentication/errors";
import { FullHeightCenteredFormWithHeaderLayout } from "../modules/Shared/components/FullHeightCenteredFormWithHeaderLayout";
import { TypographyWithPxFontAndColor } from "../modules/Shared/components/TypographyWithPxFontAndColor";
import theme from "../theme";

const ForgotPasswordSchema = Yup.object().shape({
  email: Yup.string()
    .email("Please enter a valid email address.")
    .required("Please enter a valid email address."),
});

export default () => {
  const [linkSent, setLinkSent] = React.useState(false);
  const [email, setEmail] = React.useState<string>();
  const [linkResent, setLinkResent] = React.useState(false);
  const [resending, setResending] = React.useState(false);

  const onSubmit = async (
    values: Yup.InferType<typeof ForgotPasswordSchema>,
    { setFieldError }: FormikHelpers<typeof values>
  ) => {
    setLinkSent(false);

    try {
      await AuthService.sendResetPasswordLink(values.email);
      setLinkSent(true);
      setEmail(values.email);
    } catch (e) {
      if (e instanceof AccountWithEmailNotFound) {
        setFieldError(
          "email",
          "We couldn't find that email. Please try again."
        );
      } else {
        console.log(e);
        setFieldError("email", "Unknown error, check logs.");
      }
    }
  };

  const resendLink = async () => {
    setResending(true);
    try {
      await AuthService.sendResetPasswordLink(email!);
      setLinkResent(true);
      setResending(false);
    } catch (e) {
      setLinkResent(false);
      setResending(true);

      alert("Unhandled error in resending of password reset link");
      console.error(e);
    }
  };

  if (linkSent) {
    return (
      <FullHeightCenteredFormWithHeaderLayout
        header="Reset your password"
        renderBelowPaper={() => (
          <TypographyWithPxFontAndColor sizeInPx={12} color="#fff">
            Don't have an account?{" "}
            <NextLink href="register" passHref>
              <Link color="inherit">
                <strong>Sign up</strong>
              </Link>
            </NextLink>
          </TypographyWithPxFontAndColor>
        )}
      >
        <Grid container spacing={10}>
          <Grid item>
            <TypographyWithPxFontAndColor
              sizeInPx={16}
              color="rgba(0, 0, 0, 0.5)"
            >
              Thanks, check your email for instructions to reset your password.
            </TypographyWithPxFontAndColor>
          </Grid>

          <Grid item container spacing={1} direction="column">
            <Grid item>
              <TypographyWithPxFontAndColor
                sizeInPx={10}
                color={theme.palette.primary.main}
                centerAlign
              >
                Didn't get the email? Check your spam folder or resend the
                email.
              </TypographyWithPxFontAndColor>
            </Grid>

            <Grid item>
              <Button
                variant="contained"
                color="primary"
                type="submit"
                disableElevation
                fullWidth
                size="large"
                onClick={resendLink}
                disabled={linkResent || resending}
              >
                {linkResent ? "New link has been sent" : "Resend"}
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </FullHeightCenteredFormWithHeaderLayout>
    );
  }

  return (
    <FullHeightCenteredFormWithHeaderLayout
      header="Reset your password"
      renderBelowPaper={() => (
        <TypographyWithPxFontAndColor sizeInPx={12} color="#fff">
          Don't have an account?{" "}
          <NextLink href="register" passHref>
            <Link color="inherit">
              <strong>Sign up</strong>
            </Link>
          </NextLink>
        </TypographyWithPxFontAndColor>
      )}
    >
      <Formik
        validationSchema={ForgotPasswordSchema}
        initialValues={{
          email: "",
        }}
        onSubmit={onSubmit}
      >
        {({ errors, touched, isSubmitting }) => {
          return (
            <Form>
              <Grid container spacing={10}>
                <Grid item container spacing={2} direction="column">
                  <Grid item xs>
                    <TypographyWithPxFontAndColor
                      sizeInPx={14}
                      color="rgba(0, 0, 0, 0.5)"
                    >
                      Enter the email address associated with your account and
                      we'll send you a link to reset your password.
                    </TypographyWithPxFontAndColor>
                  </Grid>
                  <Grid item xs>
                    <FormikTextField
                      name="email"
                      label="Email address"
                      errors={errors}
                      touched={touched}
                    />
                  </Grid>
                </Grid>

                <Grid item xs>
                  <Button
                    variant="contained"
                    color="primary"
                    type="submit"
                    disableElevation
                    fullWidth
                    size="large"
                    disabled={isSubmitting}
                  >
                    Reset password
                  </Button>
                </Grid>
              </Grid>
            </Form>
          );
        }}
      </Formik>
    </FullHeightCenteredFormWithHeaderLayout>
  );
};

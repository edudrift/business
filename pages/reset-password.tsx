import React from "react";

import { Button, Grid, Link } from "@material-ui/core";
import { Form, Formik, FormikHelpers } from "formik";
import NextLink from "next/link";
import * as Yup from "yup";

import { FormikTextField } from "../components/Forms/components/FormikTexField";

import { useRouter } from "next/router";
import ButtonLink from "../components/ButtonLink";
import { AuthService } from "../modules/Authentication/AuthService";
import {
  ResetPasswordTokenExpired,
  ResetPasswordTokenInvalid,
  ResetPasswordTokenUsed,
} from "../modules/Authentication/errors";
import { FullHeightCenteredFormWithHeaderLayout } from "../modules/Shared/components/FullHeightCenteredFormWithHeaderLayout";
import { TypographyWithPxFontAndColor } from "../modules/Shared/components/TypographyWithPxFontAndColor";
import theme from "../theme";
import { EProgramProviderStatus } from "../types/ProgramProvider.types";

const ResetPasswordSchema = Yup.object().shape({
  password: Yup.string()
    .required("Your password must be minimum 8 characters long.")
    .min(8, "Your password must be minimum 8 characters long."),
  confirmPassword: Yup.string().when("password", {
    is: (val) => val && val.length > 0,
    then: Yup.string()
      .oneOf(
        [Yup.ref("password")],
        "Your passwords did not match. Please try again."
      )
      .required("Your passwords did not match. Please try again."),
  }),
});

export default () => {
  const router = useRouter();
  const [email, setEmail] = React.useState<string>();
  const [successfulReset, setSuccessfulReset] = React.useState(false);

  const [expiredToken, setExpiredToken] = React.useState(false);
  const [invalidToken, setInvalidToken] = React.useState(false);

  const [programProviderStatus, setProgramProviderStatus] = React.useState(
    EProgramProviderStatus.NEW
  );

  React.useEffect(() => {
    const token = router.query.token as string;
    if (!token) return;

    const { email: emailInToken } = AuthService.decodeToken(token);
    setEmail(emailInToken);
  }, [router.query.token]);

  const onSubmit = async (
    values: Yup.InferType<typeof ResetPasswordSchema>,
    { setFieldError }: FormikHelpers<typeof values>
  ) => {
    const token = router.query.token as string;
    if (!token) {
      setFieldError("password", "Token not found.");
    } else {
      try {
        const resetPasswordResponse = await AuthService.resetPassword(
          values.password,
          token
        );
        setSuccessfulReset(true);
        if (resetPasswordResponse) {
          setProgramProviderStatus(resetPasswordResponse.programProviderStatus);
        }
      } catch (e) {
        if (e instanceof ResetPasswordTokenExpired) {
          setExpiredToken(true);
        } else if (e instanceof ResetPasswordTokenInvalid) {
          setInvalidToken(true);
        }
        if (e instanceof ResetPasswordTokenUsed) {
          setInvalidToken(true);
        }
      }
    }
  };

  const redirectTo = () => {
    switch (programProviderStatus) {
      case EProgramProviderStatus.PENDING:
        return "/biz";
      case EProgramProviderStatus.VERIFIED:
        return "/biz";
      default:
        return "/biz/profile";
    }
  };

  if (expiredToken || invalidToken) {
    return (
      <FullHeightCenteredFormWithHeaderLayout
        header={expiredToken ? "Link expired" : "Password reset failed"}
        renderBelowPaper={() => (
          <TypographyWithPxFontAndColor sizeInPx={12} color="#fff">
            Don't have an account?{" "}
            <NextLink href="register" passHref>
              <Link color="inherit">
                <strong>Sign up</strong>
              </Link>
            </NextLink>
          </TypographyWithPxFontAndColor>
        )}
      >
        <Grid container spacing={10}>
          <Grid item xs={12}>
            <TypographyWithPxFontAndColor sizeInPx={16} color="rgba(0,0,0,0.5)">
              {expiredToken
                ? "This password reset link has expired. Try resetting your password again."
                : "Your password reset request couldn't be processed. Make sure cookies are enabled in your browser and try again."}
            </TypographyWithPxFontAndColor>
          </Grid>

          <Grid item xs container spacing={1} direction="column">
            <Grid item>
              <TypographyWithPxFontAndColor
                sizeInPx={10}
                color={theme.palette.primary.main}
                centerAlign
              >
                Note that the link will only be valid for 15 minutes.
              </TypographyWithPxFontAndColor>
            </Grid>
            <Grid item>
              <ButtonLink
                href="/forgot-password"
                variant="contained"
                fullWidth
                disableElevation
                color="primary"
                size="large"
              >
                Reset password
              </ButtonLink>
            </Grid>
          </Grid>
        </Grid>
      </FullHeightCenteredFormWithHeaderLayout>
    );
  }

  if (successfulReset) {
    return (
      <FullHeightCenteredFormWithHeaderLayout
        header="Password changed"
        renderBelowPaper={() => (
          <TypographyWithPxFontAndColor sizeInPx={12} color="#fff">
            Don't have an account?{" "}
            <NextLink href="register" passHref>
              <Link color="inherit">
                <strong>Sign up</strong>
              </Link>
            </NextLink>
          </TypographyWithPxFontAndColor>
        )}
      >
        <Grid container spacing={10}>
          <Grid item xs={12}>
            <TypographyWithPxFontAndColor sizeInPx={16} color="rgba(0,0,0,0.5)">
              You've successfully changed your password.
            </TypographyWithPxFontAndColor>
          </Grid>

          <Grid item xs>
            <ButtonLink
              href={redirectTo()}
              variant="contained"
              fullWidth
              disableElevation
              color="primary"
              size="large"
            >
              Continue to dashboard
            </ButtonLink>
          </Grid>
        </Grid>
      </FullHeightCenteredFormWithHeaderLayout>
    );
  }

  return (
    <FullHeightCenteredFormWithHeaderLayout
      header="Reset your password"
      renderBelowPaper={() => (
        <TypographyWithPxFontAndColor sizeInPx={12} color="#fff">
          Don't have an account?{" "}
          <NextLink href="register" passHref>
            <Link color="inherit">
              <strong>Sign up</strong>
            </Link>
          </NextLink>
        </TypographyWithPxFontAndColor>
      )}
    >
      <Formik
        validationSchema={ResetPasswordSchema}
        initialValues={{
          password: "",
          confirmPassword: "",
        }}
        onSubmit={onSubmit}
      >
        {({ errors, touched, isSubmitting }) => {
          return (
            <Form>
              <Grid container spacing={10}>
                <Grid item container spacing={4} direction="column">
                  <Grid item container spacing={1} direction="column">
                    <Grid item>
                      <TypographyWithPxFontAndColor
                        sizeInPx={16}
                        color="rgba(0, 0, 0, 0.5)"
                      >
                        Create a new password for the account associated with:
                      </TypographyWithPxFontAndColor>
                    </Grid>

                    <Grid item>
                      <strong>{email}</strong>
                    </Grid>
                  </Grid>

                  <Grid item container spacing={2} direction="column">
                    <Grid item xs>
                      <FormikTextField
                        name="password"
                        label="New password"
                        errors={errors}
                        touched={touched}
                        type="password"
                      />
                    </Grid>

                    <Grid item xs>
                      <FormikTextField
                        name="confirmPassword"
                        label="Confirm password"
                        errors={errors}
                        touched={touched}
                        type="password"
                      />
                    </Grid>
                  </Grid>
                </Grid>

                <Grid item xs>
                  <Button
                    variant="contained"
                    color="primary"
                    type="submit"
                    disableElevation
                    fullWidth
                    size="large"
                    disabled={isSubmitting}
                  >
                    Save password
                  </Button>
                </Grid>
              </Grid>
            </Form>
          );
        }}
      </Formik>
    </FullHeightCenteredFormWithHeaderLayout>
  );
};

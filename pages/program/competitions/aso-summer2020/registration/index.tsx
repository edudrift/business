//@ts-ignore
import HubspotForm from "react-hubspot-form";
import { Grid, CircularProgress } from "@material-ui/core";
import withError from "../../../../../components/HOCs/withError";
import { getUserOrReturnUndefined } from "../../../../../services/Auth.functions";
import User from "../../../../../types/user.types";
import { NextPageContext } from "next";
import HeaderLayoutWithFooter from "../../../../../modules/Home/HeaderLayoutWithFooter";

const AsiaSchoolsOpenPage = (props: Props) => (
    <HeaderLayoutWithFooter user={props.user}>
      <HubspotForm
        portalId="7573354"
        formId="9067eff2-d961-4a75-8c6a-00ef12fae3c2"
        loading={
          <Grid item container direction="column" alignItems="center">
            <Grid item justify="center">
              <h2>Loading...</h2>
            </Grid>
            <Grid item justify="center">
              <CircularProgress />
            </Grid>
          </Grid>
        }
        onSubmit={() => alert("Form has been submitted.")}
        onReady={() => console.log("Form ready!")}
      />
    </HeaderLayoutWithFooter>
);

type Props = {
  user: User;
};

AsiaSchoolsOpenPage.getInitialProps = async (ctx: NextPageContext) => {
  try {
    const user = await getUserOrReturnUndefined(ctx);

    return { user } as Props;
  } catch (e) {
    throw e;
  }
};

export default withError(AsiaSchoolsOpenPage);

import React from "react";
import { Grid, Box, Typography } from "@material-ui/core";
import withError from "../../../../../../components/HOCs/withError";
import { getUserOrReturnUndefined } from "../../../../../../services/Auth.functions";
import HeaderLayoutWithFooter from "../../../../../../modules/Home/HeaderLayoutWithFooter";
import User from "../../../../../../types/user.types";
import { NextPageContext } from "next";

const AsiaSchoolsOpenCheckoutPage = (props: Props) => (
  <HeaderLayoutWithFooter
    user={props.user}
    banner={
      <img
        src="/asa-summer2020.png"
        style={{ width: "100%", objectFit: "cover" }}
      />
    }
  >
    <Grid item container spacing={2} justify="center">
      <Grid item>
        <Box
          py={4}
          width="100%"
          justifyContent="center"
          alignItems="center"
          style={{ textAlign: "center" }}
        >
          <p>
            <h1>Thank you! Your payment to EduDrift was successful! </h1>
            <Typography>
              Please forward this proof-of-payment with your booking reference
              number to
              <br />
              <h3>events@edudrift.com</h3>
              <br />
              to secure your reservation.
            </Typography>
          </p>
        </Box>
      </Grid>
    </Grid>
  </HeaderLayoutWithFooter>
);

type Props = {
  user: User;
};

AsiaSchoolsOpenCheckoutPage.getInitialProps = async (ctx: NextPageContext) => {
  try {
    const user = await getUserOrReturnUndefined(ctx);

    return { user } as Props;
  } catch (e) {
    throw e;
  }
};

export default withError(AsiaSchoolsOpenCheckoutPage);

import React from "react";

import { Box, Button, Grid, MenuItem } from "@material-ui/core";
import { loadStripe, RedirectToCheckoutClientOptions } from "@stripe/stripe-js";
import { Formik, Form, Field } from "formik";
import { Select } from "formik-material-ui";
import { isEmpty, range } from "lodash";
import * as Y from "yup";
import CheckBoxOutlinedIcon from "@material-ui/icons/CheckBoxOutlined";
import PersonIcon from "@material-ui/icons/Person";
import GroupIcon from "@material-ui/icons/Group";

import withError from "../../../../../components/HOCs/withError";
import { getUserOrReturnUndefined } from "../../../../../services/Auth.functions";
import HeaderLayoutWithFooter from "../../../../../modules/Home/HeaderLayoutWithFooter";
import User from "../../../../../types/user.types";
import { NextPageContext } from "next";
import { requiredText } from "../../../../../utils/formikUtils";
import CardWithTitleDivider from "../../../../../modules/Shared/components/CardWithTitleDivider";
import { FormSubheaderWithSubtitle } from "../../../../../modules/Shared/components/FormSubheaderWithSubtitle";
import { TypographyWithPxFontAndColor } from "../../../../../modules/Shared/components/TypographyWithPxFontAndColor";

const stripePromise = loadStripe("pk_live_FLCUg3eiVnGaM8qv1ahRdcvY00a6C2XWkW");

const handleSubmit = async (value: { debate: number; observers: number }) => {
  const stripe = await stripePromise;
  if (stripe) {
    const lineItems = [
      {
        price: "price_1H2VbcAmC5oPbsuDUdUN24CA",
        quantity: value.debate,
      },
      {
        price: "price_1H2VcSAmC5oPbsuDBpwCZUAX",
        quantity: value.observers,
      },
    ].filter((i) => i.quantity > 0);

    if (!isEmpty(lineItems)) {
      const result = await stripe.redirectToCheckout(({
        lineItems,
        mode: "payment",
        successUrl:
          "https://edudrift.com/program/competitions/aso-summer2020/checkout/success",
        cancelUrl:
          "https://edudrift.com/program/competitions/aso-summer2020/checkout",
      } as unknown) as RedirectToCheckoutClientOptions);

      if (result.error) {
        alert(result.error.message);
      }
    } else {
      alert("Please select at least one ticket");
    }
  }
};

const validationSchema = Y.object().shape({
  debate: Y.string()
    .matches(/^[0-9]+$/, requiredText)
    .required(requiredText),
  observers: Y.string()
    .matches(/^[0-9]+$/, requiredText)
    .required(requiredText),
});

const RowTitleAndInfo = (props: { title: string; info: string }) => {
  return (
    <Grid item container spacing={2}>
      <Grid item md={3} xs={3}>
        <strong>{props.title}</strong>
      </Grid>
      <Grid item md={9} xs={9}>
        <Box whiteSpace="pre-wrap">{props.info}</Box>
      </Grid>
    </Grid>
  );
};

const NumberedListTitleAndInfo = (props: {
  title: string;
  info: string;
  number: number;
}) => {
  return (
    <Grid item container direction="column">
      <Grid item container xs={12}>
        <Box width="20px">
          <strong>{props.number}</strong>
        </Box>
        <Box width="calc(100% - 20px)">
          <strong>{props.title}</strong>
        </Box>
      </Grid>
      <Grid item container xs={12}>
        <Box width="20px" />
        <Box whiteSpace="pre-wrap" width="calc(100% - 20px)">
          {props.info}
        </Box>
      </Grid>
    </Grid>
  );
};

const ticketDetails = [
  { title: "Program", info: "Asia Schools Open-Summer 2020" },
  { title: "Location", info: "Singapore (GMT +8)" },
  { title: "Start Date", info: "25 July 2020" },
  { title: "End Date", info: "26 July 2020" },
  { title: "Organizing Entity", info: "EduDrift" },
  { title: "Ticket Types", info: "Debate Team; 2 Participants     SGD 200" },
  { title: "", info: "Observer/Parent                        SGD 20" },
  { title: "Extra Charges", info: "3.5% Transaction Fee for Online Payment" },
];

const feeTicketConditions = [
  {
    title: "Validity",
    info:
      "This ticket purchased is only valid for participation at this program.\
      \nThis ticket is only valid when full payment has been made.",
  },
  {
    title: "Participant name change",
    info:
      "Change of participant name(s) is permitted up to 24 hours before program commencement.\
      \nChange of participant type is permitted. Unused amount of the fee will not be refunded.",
  },
  {
    title: "Cancellation / Refund",
    info:
      "Tickets purchased after 01 July 2020 are non-refundable.\
      \nDisqualifications due to ineligibility are non-refundable,",
  },
  {
    title: "Other conditions",
    info:
      "Prices are not inclusive of transportation, accomodation and meals.\
      \nPrices are not inclusive of payment related costs unless otherwise stated.\
      \nInternational payments made must bear both sender and recipient transaction changes.\
      \nIncomplete payment may result in restricted participation.\
      \nEduDrift prefers TransferWise as a payment method",
  },
];

const TicketOptionsRow = (props: {
  type: React.ReactNode;
  participants: React.ReactNode;
  price: React.ReactNode;
  quantity: React.ReactNode;
}) => (
  <Grid item container spacing={2} direction="row">
    <Grid item sm={6} xs={6}>
      {props.type}
    </Grid>
    <Grid sm={2} xs="auto">
      <Box display={{ xs: "none", sm: "flex" }} p={1}>
        {props.participants}
      </Box>
    </Grid>
    <Grid item sm={2} xs={3}>
      {props.price}
    </Grid>
    <Grid item sm={2} xs={3}>
      {props.quantity}
    </Grid>
  </Grid>
);

const debateTeamDetails = [
  "5 Prelim Rounds",
  "Fireside Chat with Chief Adjuicators",
  "Personal Assessment Report",
  "Demonstration Videos",
];

const observerDetails = [
  "Access to all rounds",
  "Networking session with Invited Judges",
  "Demonstration Videos",
];

const CheckBoxWithDetail = (props: { detail: string }) => (
  <Box py={1} display="flex" alignItems="center">
    <Box width="24px" height="24px">
      <CheckBoxOutlinedIcon />
    </Box>
    <Box width="calc(100% - 36px)" px={1}>
      {props.detail}
    </Box>
  </Box>
);

const AsiaSchoolsOpenCheckoutPage = (props: Props) => {
  return (
    <HeaderLayoutWithFooter
      user={props.user}
      banner={
        <img
          src="/asa-summer2020.png"
          style={{ width: "100%", objectFit: "cover" }}
        />
      }
    >
      <Formik
        initialValues={{ debate: 0, observers: 0 }}
        onSubmit={handleSubmit}
        validationSchema={validationSchema}
      >
        {({}) => (
          <Form>
            <Grid
              item
              container
              spacing={2}
              direction="column"
              style={{ fontFamily: "Helvetica", lineHeight: "2em" }}
            >
              <Grid item container>
                <CardWithTitleDivider title="Ticket details">
                  {ticketDetails.map((d) => (
                    <RowTitleAndInfo
                      key={d.title}
                      title={d.title}
                      info={d.info}
                    />
                  ))}
                </CardWithTitleDivider>
              </Grid>

              <Grid item container>
                <CardWithTitleDivider title="Ticket options">
                  <TicketOptionsRow
                    type="Type"
                    participants="Participants"
                    price="Price/Ticket"
                    quantity="Quantity"
                  />
                  <TicketOptionsRow
                    type={
                      <Box pt="4px">
                        <FormSubheaderWithSubtitle title="Debate Team" />
                      </Box>
                    }
                    participants={
                      <Box px={3} py={1}>
                        <GroupIcon />
                      </Box>
                    }
                    price={
                      <Box py={1}>
                        <TypographyWithPxFontAndColor
                          color="black"
                          sizeInPx={16}
                        >
                          200.00
                        </TypographyWithPxFontAndColor>
                      </Box>
                    }
                    quantity={
                      <Box pt="2px" display="flex">
                        <Grid md={6} xs={12}>
                          <Field
                            component={Select}
                            label="Debate Teams"
                            name="debate"
                            type="number"
                            variant="outlined"
                            SelectDisplayProps={{
                              style: {
                                paddingTop: "8.5px",
                                paddingBottom: "8.5px",
                                width: "40px",
                              },
                            }}
                          >
                            {range(21).map((i) => (
                              <MenuItem key={i} value={i}>
                                {i}
                              </MenuItem>
                            ))}
                          </Field>
                        </Grid>
                      </Box>
                    }
                  />
                  <Grid item>
                    {debateTeamDetails.map((d, i) => (
                      <CheckBoxWithDetail detail={d} key={i} />
                    ))}
                  </Grid>
                  <TicketOptionsRow
                    type={
                      <Box pt="4px">
                        <FormSubheaderWithSubtitle title="Observer / Parent" />
                      </Box>
                    }
                    participants={
                      <Box px={3} py={1}>
                        <PersonIcon />
                      </Box>
                    }
                    price={
                      <Box py={1}>
                        <TypographyWithPxFontAndColor
                          color="black"
                          sizeInPx={16}
                        >
                          20.00
                        </TypographyWithPxFontAndColor>
                      </Box>
                    }
                    quantity={
                      <Box pt="2px" display="flex">
                        <Field
                          component={Select}
                          label="Observers"
                          name="observers"
                          type="number"
                          variant="outlined"
                          SelectDisplayProps={{
                            style: {
                              paddingTop: "8.5px",
                              paddingBottom: "8.5px",
                              width: "40px",
                            },
                          }}
                        >
                          {range(21).map((i) => (
                            <MenuItem key={i} value={i}>
                              {i}
                            </MenuItem>
                          ))}
                        </Field>
                      </Box>
                    }
                  />
                  <Grid item>
                    {observerDetails.map((d, i) => (
                      <CheckBoxWithDetail detail={d} key={i} />
                    ))}
                  </Grid>
                  <Grid item container spacing={2}>
                    <Grid item sm={8} xs={6} />
                    <Grid item sm={3} xs={6}>
                      <Button
                        fullWidth
                        variant="contained"
                        color="primary"
                        type="submit"
                        style={{
                          justifySelf: "center",
                          color: "#FFF",
                          border: 0,
                          borderRadius: "4px",
                          textTransform: "none",
                        }}
                      >
                        Pay Now
                      </Button>
                    </Grid>
                    <Grid sm={1} xs="auto" />
                  </Grid>
                </CardWithTitleDivider>
              </Grid>

              <Grid item container>
                <CardWithTitleDivider title="Fee / ticket conditions">
                  {feeTicketConditions.map((d, i) => (
                    <NumberedListTitleAndInfo
                      key={d.title}
                      title={d.title}
                      info={d.info}
                      number={i + 1}
                    />
                  ))}
                </CardWithTitleDivider>
              </Grid>
            </Grid>
          </Form>
        )}
      </Formik>
    </HeaderLayoutWithFooter>
  );
};

type Props = {
  user: User;
};

AsiaSchoolsOpenCheckoutPage.getInitialProps = async (ctx: NextPageContext) => {
  try {
    const user = await getUserOrReturnUndefined(ctx);

    return { user } as Props;
  } catch (e) {
    throw e;
  }
};

export default withError(AsiaSchoolsOpenCheckoutPage);

import ViewProgramPage from "../../../modules/Programs/components/ViewProgramPage";
import { NextPageContext } from "next";
import { ProgramsService } from "../../../modules/Programs/services/ProgramsService";
import { getUserOrReturnUndefined } from "../../../services/Auth.functions";
import redirect from "../../../utils/redirect";
import withError from "../../../components/HOCs/withError";

ViewProgramPage.getInitialProps = async (ctx: NextPageContext) => {
  try {
    const user = await getUserOrReturnUndefined(ctx);
    const program = await ProgramsService.getOne(ctx.query.id as string);
    if (!program.startDate || !program.endDate) {
      redirect(`/biz/program/${program.id}/edit`, 302, ctx);
    }

    const canEdit = program.provider.id === user?.programProvider.id;

    return { user, program, canEdit };
  } catch (e) {
    throw e;
  }
};

export default withError(ViewProgramPage);

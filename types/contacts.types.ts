type Contact = {
  id?: string;
  type: EContactTypes;

  firstName: string;
  lastName: string;
  email: string;

  title: string;
  department: string;

  office: string;
  mobile: string;
};

export enum EContactTypes {
  PRIMARY = "PRIMARY",
  SECONDARY = "SECONDARY",
}

export default Contact;

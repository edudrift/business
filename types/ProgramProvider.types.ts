import * as Y from "yup";
import Business from "../modules/ProgramProviders/types/business.types";
import { requiredText } from "../utils/formikUtils";
import { ProgramTypeEligibility } from "./ProgramEligiblity.types";
import User from "./user.types";

type ProgramProvider = {
  id: string;
  name: string;
  website: string;

  type: EProgramProviderTypes;

  manager: User;
  managerId: string;
  managerTitle: string;

  business: Business;
  businessId: string;

  programTypes: ProgramTypeEligibility[];

  stripeConnectStateKey?: string;
  stripeAccountConnectId?: string;

  status: EProgramProviderStatus;
  
  introductions: string[];
  logoImages: string[];
  imageTitles: string[];
  images: string[];
};

export enum EProgramProviderStatus {
  NEW = "NEW",
  PENDING = "PENDING",
  VERIFIED = "VERIFIED",
}

export enum EProgramProviderTypes {
  BUSINESS = "BUSINESS",
  STUDENT_ORGANISATION = "STUDENT_ORGANISATION",
}

export enum EProgramProviderTypeLabels {
  BUSINESS = "Business",
  STUDENT_ORGANISATION = "Student Organisation",
}

export const ProgramProviderSchema = Y.object().shape({
  name: Y.string().required(requiredText),
  introductions: Y.array().of(Y.string().required(requiredText)),
  imageTitles: Y.array().of(Y.string().required(requiredText)),
});

export type ProgramProviderSchemaAsType = Y.InferType<
  typeof ProgramProviderSchema
>;

export default ProgramProvider;


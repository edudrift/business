import Business from "../modules/ProgramProviders/types/business.types";
import { EProgramType } from "../modules/Programs/types/Programs.types";

export enum EProgramTypeEligibilityStatus {
  PENDING = "PENDING",
  REJECTED = "REJECTED",
  APPROVED = "APPROVED",
}

export type ProgramTypeEligibility = {
  id: string;
  businessId: string;
  business: Business;
  programType: EProgramType;
  status: EProgramTypeEligibilityStatus;
};

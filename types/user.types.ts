import ProgramProvider from "./ProgramProvider.types";

type User = {
  id: string;
  name: string;
  email: string;
  isEmailVerified: boolean;

  programProvider: ProgramProvider;
  programProviderId: string;

  role: EUserRoles;
};

export enum EUserRoles {
  BASIC = "BASIC",
  BUSINESS = "BUSINESS",
  ADMIN = "ADMIN",
}

export default User;

import FairPackage from "./FairPackages.types";
import User from "./user.types";

export enum EFairPackagePurchaseStatus {
  PENDING = "PENDING",
  PAID = "PAID",
  CANCELLED = "CANCELLED",
}

export enum EFairPackagePublishStatus {
  PENDING = "PENDING",
  REQUESTED = "REQUESTED",
  REVIEWING = "REVIEWING",
  PUBLISHED = "PUBLISHED",
}

export type FairPackagePurchase = {
  id: string;
  purchasedBy: User;
  fairPackage: FairPackage;
  status: EFairPackagePurchaseStatus;
  publishStatus: EFairPackagePublishStatus;
  stripeSessionId: string;
};

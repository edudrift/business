import { NextPageContext } from "next";

export enum EGlobalMessageSeverity {
  ERROR = "error",
  WARNING = "warning",
  INFO = "info",
  SUCCESS = "success"
}

export interface IGlobalMessagePayload {
  message: string | null;
  severity?: EGlobalMessageSeverity;
}

export interface IAppContext {
  Component: any;
  ctx: NextPageContext;
}

export interface IRedirectOptions {
  ctx: NextPageContext;
  status: number;
}

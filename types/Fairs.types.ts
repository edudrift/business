import FairPackage from "./FairPackages.types";

export enum EFairStatus {
  DRAFT = "DRAFT",
  PUBLISHED = "PUBLISHED"
}

export type Fair = {
  id: string;

  name: string;
  description: string;
  slug: string;

  startDateTime: string;
  endDateTime: string;

  status: EFairStatus;

  packages: FairPackage[];
};

export default Fair;

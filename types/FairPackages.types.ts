import Fair from "./Fairs.types";

export type FairPackage = {
  id: string;

  title: string;
  price: number;
  capacity: number;

  maxPhotos: number;
  maxBrochures: number;
  maxConsultants: number;

  additionalBenefits: string;

  fair: Fair;
  fairId: string;
};

export default FairPackage;

import { NextPage, NextPageContext } from "next";
import React from "react";

import { CreateFairForm } from "~/components/Forms/Fairs";
import AdminLayout from "../../../components/Layouts/AdminLayout";
import { getUserAndEnsureAdmin } from "../../../services/Auth.functions";
import User from "../../../types/user.types";

const CreateFairPage: NextPage<Props> = () => {
  return (
    <AdminLayout>
      <CreateFairForm />
    </AdminLayout>
  );
};

type Props = PromiseResult<ReturnType<typeof getInitialProps>>;

const getInitialProps = async (ctx: NextPageContext) => {
  try {
    const user = getUserAndEnsureAdmin(ctx);

    return {
      user,
    };
  } catch (e) {
    return {
      user: {} as Promise<User>,
    };
  }
};

CreateFairPage.getInitialProps = getInitialProps;

export default CreateFairPage;

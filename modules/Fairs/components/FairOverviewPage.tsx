import React from "react";

import { NextPage, NextPageContext } from "next";
import FairOverview from "~/components/FairOverview";
import AdminLayout from "~/components/Layouts/AdminLayout";
import { getUserAndEnsureAdmin } from "~/services/Auth.functions";
import FairsService from "~/utils/Fairs.service";
import withError from "../../../components/HOCs/withError";

const FairOverviewPage: NextPage<Props> = (props) => {
  return (
    <AdminLayout>
      <FairOverview fair={props.fair} />
    </AdminLayout>
  );
};

type Props = PromiseResult<ReturnType<typeof getInitialProps>>;

const getInitialProps = async (ctx: NextPageContext) => {
  const user = await getUserAndEnsureAdmin(ctx);
  const fair = await FairsService.getFair(ctx, ctx.query.id as string);

  return {
    user,
    fair,
  };
};

FairOverviewPage.getInitialProps = getInitialProps;

export default withError(FairOverviewPage);

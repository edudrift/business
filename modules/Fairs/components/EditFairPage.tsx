import React from "react";
import AdminLayout from "../../../components/Layouts/AdminLayout";

import { NextPage, NextPageContext } from "next";
import EditFairForm from "~/components/Forms/Fairs/EditFairForm";
import FairsService from "~/utils/Fairs.service";
import redirect from "~/utils/redirect";
import { getUserAndEnsureAdmin } from "../../../services/Auth.functions";

const EditFairPage: NextPage<Props> = (props) => {
  return (
    <AdminLayout>
      <EditFairForm fair={props.fair} startAtPackage={props.startAtPackage} />
    </AdminLayout>
  );
};

type Props = PromiseResult<ReturnType<typeof getInitialProps>>;

const getInitialProps = async (ctx: NextPageContext) => {
  try {
    const user = await getUserAndEnsureAdmin(ctx);
    const fair = await FairsService.getFair(ctx, ctx.query.id as string);

    let startAtPackage = false;
    if (ctx.query.startAtPackage) {
      startAtPackage = true;
    }

    return {
      user,
      fair,
      startAtPackage,
    };
  } catch (e) {
    redirect("/", 301, ctx);
  }
};

EditFairPage.getInitialProps = getInitialProps;

export default EditFairPage;

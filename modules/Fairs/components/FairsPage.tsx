import { Grid, Typography } from "@material-ui/core";
import { NextPage, NextPageContext } from "next";
import React from "react";

import AdminLayout from "../../../components/Layouts/AdminLayout";
import { PaddedPaper } from "../../Shared/components/PaddedPaper";
import FairsTable from "../../../components/Tables/FairsTable";
import { getUserAndEnsureAdmin } from "../../../services/Auth.functions";

import ButtonLink from "../../../components/ButtonLink";
import withError from "../../../components/HOCs/withError";
import FairsService from "../../../utils/Fairs.service";

const FairsPage: NextPage<Props> = (props) => {
  return (
    <AdminLayout>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <PaddedPaper fullWidth>
            <Grid item container spacing={2} justify="space-between">
              <Grid item style={{ display: "flex", alignItems: "center" }}>
                <Typography>List of all Fairs</Typography>
              </Grid>
              <Grid item>
                <ButtonLink
                  style={{ borderRadius: "30px" }}
                  disableElevation
                  variant="contained"
                  color="primary"
                  href="/admin/fairs/create"
                >
                  New Fair
                </ButtonLink>
              </Grid>
            </Grid>
          </PaddedPaper>
        </Grid>

        <Grid item xs={12}>
          <PaddedPaper fullWidth withoutPadding>
            <FairsTable fairs={props.fairs} />
          </PaddedPaper>
        </Grid>
      </Grid>
    </AdminLayout>
  );
};

type Props = PromiseResult<ReturnType<typeof getInitialProps>>;

const getInitialProps = async (ctx: NextPageContext) => {
  const user = await getUserAndEnsureAdmin(ctx);
  const fairs = await FairsService.getAll(ctx);

  return {
    user,
    fairs,
  };
};

FairsPage.getInitialProps = getInitialProps;

export default withError(FairsPage);

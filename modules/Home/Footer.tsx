import { Container, Grid, Link } from "@material-ui/core";

import homeStyles from "../../styles/home.module.css";

const Footer: React.FC<Props> = () => {
  return (
    <Container>
      <Grid
        container
        className={homeStyles.footer}
        alignItems="flex-start"
        justify="center"
      >
        <Grid item xs={2} className={homeStyles.footerGrid1}>
          <div
            style={{
              display: "flex",
              alignItems: "left",
              flexDirection: "column",
              textAlign: "left",
            }}
          >
            <div className={homeStyles.footerHeader}>EDUDRIFT</div>
            <div className={homeStyles.footerGridContent}>
              <Link href="https://www.facebook.com/edudrift.official">
                <img src="/fb-icon.svg" />
              </Link>
              <Link href="https://www.instagram.com/edudrift.official">
                <img src="/google-icon.svg" />
              </Link>
              <Link href="https://www.youtube.com/channel/UCmzu9ZF0037mYlbsObeqzJg">
                <img src="/twitter-icon.svg" />
              </Link>
            </div>
          </div>
        </Grid>
        <Grid item xs={2} className={homeStyles.footerGrid1}>
          <div
            style={{
              display: "flex",
              alignItems: "left",
              flexDirection: "column",
              textAlign: "left",
              marginBottom: "1em",
            }}
          >
            <div className={homeStyles.footerHeader2}>Products</div>
            <div className={homeStyles.footerGridContent2}>
              <div>Edudrift for business</div>
            </div>
          </div>
        </Grid>
        <Grid item xs={2} className={homeStyles.footerGrid2}>
          <div
            style={{
              display: "flex",
              alignItems: "left",
              flexDirection: "column",
              textAlign: "left",
              marginBottom: "1em",
            }}
          >
            <div className={homeStyles.footerHeader2}>Pricing</div>
            <div className={homeStyles.footerGridContent2}>
              <div>Pricing</div>
            </div>
          </div>
        </Grid>
        <Grid item xs={2} className={homeStyles.footerGrid2}>
          <div
            style={{
              display: "flex",
              alignItems: "left",
              flexDirection: "column",
              textAlign: "left",
              marginBottom: "1em",
            }}
          >
            <div className={homeStyles.footerHeader2}>Company</div>
            <div className={homeStyles.footerGridContent2}>
              <div>About</div>
              <div>Partners</div>
              <div>Careers</div>
            </div>
          </div>
        </Grid>
        <Grid item xs={2} className={homeStyles.footerGrid2}>
          <div
            style={{
              display: "flex",
              alignItems: "left",
              flexDirection: "column",
              textAlign: "left",
              marginBottom: "1em",
            }}
          >
            <div className={homeStyles.footerHeader2}>Support</div>
            <div className={homeStyles.footerGridContent2}>
              <div>Contact Us</div>
              <div>FAQs</div>
              <div>Privacy Policy</div>
              <div>Cookie Policy</div>
              <div>Term of Use</div>
            </div>
          </div>
        </Grid>
      </Grid>
      <Grid
        style={{
          textAlign: "center",
          color: "#000000",
          opacity: "0.4",
          marginBottom: "1em",
        }}
      >
        2020 copyright all right reserved
      </Grid>
    </Container>
  );
};

type Props = {};

export default Footer;

import { Grid } from "@material-ui/core";

import PersonSharpIcon from "@material-ui/icons/PersonSharp";
import SentimentSatisfiedSharpIcon from "@material-ui/icons/SentimentSatisfiedSharp";
import TrendingUpIcon from "@material-ui/icons/TrendingUp";
import AccessTimeSharpIcon from "@material-ui/icons/AccessTimeSharp";
import SchoolSharpIcon from "@material-ui/icons/SchoolSharp";

const iconStyle = {
  width: "100%",
  maxHeight: "100%",
  color: "#1890FF",
  fontSize: "50px",
};

const VerticalDivider = () => (
  <div
    style={{
      width: "1px",
      height: "40px",
      background: "#1890FF",
    }}
  />
);

export default () => (
  <Grid
    container
    style={{ textAlign: "center" }}
    spacing={2}
    direction="column"
  >
    <Grid item container justify="center" direction="row">
      <h1>STUDENTS FIRST</h1>
    </Grid>
    <Grid item container justify="center" direction="row">
      Here at EduDrift, we focus on providing you the complete toolkit for your
      education business.
    </Grid>
    <Grid item />
    <Grid item container spacing={2} justify="center" direction="row">
      <Grid item container justify="center" xs={4} sm={2} md={2}>
        <PersonSharpIcon style={iconStyle} />
        <br />
        Reach more students
      </Grid>
      <Grid item>
        <VerticalDivider />
      </Grid>
      <Grid item container justify="center" xs={4} sm={2} md={2}>
        <SentimentSatisfiedSharpIcon style={iconStyle} />
        <br />
        Offer a better <br />
        customer experience
      </Grid>
      <Grid item>
        <VerticalDivider />
      </Grid>
      <Grid item container justify="center" xs={4} sm={2} md={2}>
        <TrendingUpIcon style={iconStyle} />
        <br />
        Automate <br />
        your business
      </Grid>
      <Grid item>
        <VerticalDivider />
      </Grid>
      <Grid item container justify="center" xs={4} sm={2} md={2}>
        <AccessTimeSharpIcon style={iconStyle} />
        <br />
        Save time
      </Grid>
      <Grid item>
        <VerticalDivider />
      </Grid>
      <Grid item container justify="center" xs={4} sm={2} md={2}>
        <SchoolSharpIcon style={iconStyle} />
        <br />
        Focus on the <br />
        quality of education
      </Grid>
    </Grid>
  </Grid>
);

import { Container, Grid } from "@material-ui/core";

import ButtonLink from "../../components/ButtonLink";

import homeStyles from "../../styles/home.module.css";
import Footer from "./Footer";

const LandingPage: React.FC<Props> = () => {
  return (
    <div style={{ backgroundColor: "white" }}>
      <Grid className={homeStyles.acceptBanner}>
        <div style={{ padding: "8px" }}>
          By continuing to use this website you agree to the use of cookies
          according to our Privacy Policy and terms.
        </div>
        <ButtonLink
          href="mailTo:partners@edudrift.com"
          style={{
            background: "#FF6060",
            color: "#FFFFFF",
            textTransform: "none",
            margin: "0 2em",
            fontSize: "16px",
            padding: "8px 20px",
          }}
        >
          Start Free Trial
        </ButtonLink>
      </Grid>
      <Container style={{ marginTop: "5em" }}>
        <Grid
          container
          alignItems="center"
          justify="center"
          style={{ textAlign: "center" }}
        >
          <Grid item xs={12} sm={5} md={5}>
            <div
              style={{
                textAlign: "left",
              }}
            >
              <div
                style={{
                  fontSize: "24px",
                  color: "#000000",
                  margin: "0.5em 0",
                  maxWidth: "80%",
                }}
              >
                Built to increase access and empower choices in young adults
              </div>
              <div
                style={{
                  fontSize: "14px",
                  color: "#F04343",
                  marginBottom: "1em"
                }}
              >
                With independent and unbiased assessment of programs available,
                students are better able to tap into a comprehensive range of
                educational opportunities. Our verified and reliable partners
                system helps to develop confidence and trust when participating
                in programs.
              </div>
            </div>
          </Grid>
          <Grid item xs={10} sm={5} style={{ textAlign: "right" }}>
            <img style={{ width: "100%" }} src="/program-board.svg" />
          </Grid>
        </Grid>
      </Container>
      <Container>
        <Grid
          className={homeStyles.descriptionGrid}
          container
          alignItems="center"
          justify="center"
        >
          <Grid item xs={5}>
            <div
              style={{
                textAlign: "left",
                margin: "1em",
                display: "flex",
                minHeight: "150px",
              }}
            >
              <div style={{ width: "80%" }}>
                <div
                  style={{
                    color: "#000000",
                    fontSize: "24px",
                    minHeight: "50px",
                  }}
                >
                  PLATFORM AUTOMATION
                </div>
                <div>
                  Manage bookings, send invoices and receipts, process payments
                  and receive timely analytics with EduDrift's management tools.
                </div>
              </div>
              <div style={{ display: "inline-flex", marginLeft: "10px" }}>
                <img src="/computer-icon.svg" />
              </div>
            </div>
          </Grid>
          <Grid item xs={5}>
            <div
              style={{
                textAlign: "left",
                margin: "1em",
                display: "flex",
                minHeight: "150px",
              }}
            >
              <div style={{ width: "80%" }}>
                <div
                  style={{
                    color: "#000000",
                    fontSize: "24px",
                    minHeight: "50px",
                  }}
                >
                  MARKETPLACE AND COMMUNITY
                </div>
                <div>
                  Centralised destination for summer, winter study-abroad plans,
                  local enrichment and training programs, and a community of
                  student interests.
                </div>
              </div>
              <div style={{ display: "inline-flex", marginLeft: "10px" }}>
                <img src="/marketplace-icon.svg" />
              </div>
            </div>
          </Grid>
        </Grid>

        <Grid style={{ textAlign: "center", margin: "4em 0" }}>
          <div
            style={{
              display: "flex",
              alignItems: "center",
              flexDirection: "column",
            }}
          >
            <div className={homeStyles.header1}>
              EDUDRIFT: EMPOWERING THE BUSINESS OF EDUCATION
            </div>
            <div
              style={{
                color: "#000000",
                fontSize: "14px",
                lineHeight: "26px",
                width: "80%",
              }}
            >
              <div style={{ marginTop: "1em" }}>
                EduDrift booking software has been designed to optimise
                operations for you as an education program provider. As you seek
                to develop new opportunities, it may get increasingly difficult
                to balance the customer requirements while ensuring that your
                programs are conducted to your ideal level of perfection.
                Integrating your business with our system can help you get more
                program reservations, save valuable time by automating your
                operations, and be at the top of your game with the quality of
                education and experience.
              </div>
              <div style={{ marginTop: "1em" }}>
                With EduDrift, you can quickly enroll in a payment gateway with
                Stripe or EduDrift-Pay (if your country is not supported by
                Stripe). Either way, all of your payment and booking information
                will be available in a convenient and intuitive dashboard. Track
                reports to see the information that is important to you and your
                team to constantly be on top of your game.
              </div>
              <div style={{ marginTop: "1em" }}>
                Rest assured, we're constantly improving EduDrift to deliver a
                better experience for all.
              </div>
            </div>
          </div>
        </Grid>
      </Container>
      <Grid
        style={{
          backgroundImage: `url(${"/bg.svg"})`,
          backgroundSize: "cover",
          textAlign: "center",
        }}
      >
        <div style={{ padding: "20px" }}>
          <div
            style={{ color: "#FFFFFF", fontSize: "40px", lineHeight: "48px" }}
          >
            LEARN MORE ABOUT EDUDRIFT TODAY
          </div>
          <div
            style={{
              color: "#FFFFFF",
              fontSize: "14px",
              lineHeight: "26px",
              padding: "20px 0",
            }}
          >
            Join us as a foundation member, price starting from S$29.90
          </div>
          <ButtonLink
            href="mailTo:partners@edudrift.com"
            style={{
              background: "#FF6060",
              color: "#FFFFFF",
              textTransform: "none",
              marginLeft: "2em",
              fontSize: "16px",
              padding: "8px 20px",
            }}
          >
            Start Free Trial
          </ButtonLink>
        </div>
      </Grid>
      <Footer />
    </div>
  );
};

type Props = {};

export default LandingPage;

import React from "react";
import {
  Grid,
  Toolbar,
  Button,
  withStyles,
  MenuList,
  MenuItem,
  Popper,
  Paper,
  Box,
} from "@material-ui/core";
import Link from "next/link";

import User from "../../types/user.types";

const StyledMenuItem = withStyles({
  root: {
    fontSize: "14px",
  },
})(MenuItem);

const StyledButton = withStyles({
  root: {
    color: "white",
    boxShadow: "none",
    paddingLeft: "28px",
    paddingRight: "28px",
    top: "8px",
    "&:hover": {
      background: "transparent",
      color: "rgba(0, 0, 0, 0.5)",
    },
  },
  label: {
    textTransform: "none",
  },
})(Button);

const OrangeButton = withStyles({
  root: {
    color: "#FF6060",
    border: "1px solid #FFFFFF",
    background: "white",
    boxShadow: "0px 3px 5px rgba(30, 105, 190, 0.163407)",
    borderRadius: "1.66667px",
    top: "8px",
    "&:hover": {
      color: "white",
      background: "transparent",
    },
  },
  label: {
    textTransform: "capitalize",
  },
})(Button);

const HeaderBar: React.FC<Props> = (props) => {
  const [openCompanyMenu, handleOpenCompanyMenu] = React.useState(false);
  const [openSupportMenu, handleOpenSupportMenu] = React.useState(false);
  const [anchorCompanyEl, setAnchorCompanyEl] = React.useState(null);
  const [anchorSupportEl, setAnchorSupportEl] = React.useState(null);

  const navButton = props.user ? (
    <Grid item xs={12} md={2}>
      <Link href="/biz">
        <OrangeButton fullWidth disableElevation={true}>
          Dashboard
        </OrangeButton>
      </Link>
    </Grid>
  ) : (
    <Grid item xs={12} md={2}>
      <Link href="mailTo:partners@edudrift.com" passHref>
        <OrangeButton fullWidth disableElevation={true}>
          Start Free Trial
        </OrangeButton>
      </Link>
    </Grid>
  );

  return (
    <Toolbar disableGutters={true}>
      <Box m={0} py={3} width="100%">
        <Grid item container direction="row" justify="center">
          <Grid
            item
            xs={12}
            md={2}
            container
            direction="column"
            justify="flex-start"
          >
            <Link href="/">
              <Button
                style={{
                  padding: "0",
                  justifyContent: "center",
                  backgroundColor: "transparent",
                }}
                fullWidth
                disableElevation={true}
                disableRipple={true}
              >
                <img
                  src="/logo-horizontal-white.png"
                  style={{
                    justifySelf: "center",
                    width: "100%",
                  }}
                />
              </Button>
            </Link>
          </Grid>
          <Grid item xs={12} md={1} />
          {props.user && <Grid item xs={12} md={1} />}
          <Grid item xs={12} md={1}>
            <Link href="/">
              <StyledButton fullWidth disableElevation={true}>
                Products
              </StyledButton>
            </Link>
          </Grid>
          <Grid item xs={12} md={1}>
            <Link href="/">
              <StyledButton fullWidth disableElevation={true}>
                Pricing
              </StyledButton>
            </Link>
          </Grid>
          <Grid item xs={12} md={1}>
            <StyledButton
              fullWidth
              onMouseEnter={(e: any) => {
                setAnchorCompanyEl(e.currentTarget);
                handleOpenCompanyMenu(true);
              }}
              onMouseLeave={() => handleOpenCompanyMenu(false)}
            >
              Company
              <Popper
                placement="bottom"
                open={openCompanyMenu}
                anchorEl={anchorCompanyEl}
                disablePortal
                onMouseLeave={() => handleOpenCompanyMenu(false)}
              >
                <Paper>
                  <MenuList>
                    <StyledMenuItem>About</StyledMenuItem>
                    <StyledMenuItem>Partners</StyledMenuItem>
                    <StyledMenuItem>Career</StyledMenuItem>
                  </MenuList>
                </Paper>
              </Popper>
            </StyledButton>
          </Grid>
          <Grid item xs={12} md={1}>
            <StyledButton
              fullWidth
              onMouseEnter={(e: any) => {
                setAnchorSupportEl(e.currentTarget);
                handleOpenSupportMenu(true);
              }}
              onMouseLeave={() => handleOpenSupportMenu(false)}
              disableElevation={true}
            >
              Support
              <Popper
                placement="bottom"
                open={openSupportMenu}
                anchorEl={anchorSupportEl}
                disablePortal
              >
                <Paper>
                  <MenuList>
                    <StyledMenuItem>Contact Us</StyledMenuItem>
                    <StyledMenuItem>FAQs</StyledMenuItem>
                    <StyledMenuItem>Privacy Policy</StyledMenuItem>
                    <StyledMenuItem>Terms of Use</StyledMenuItem>
                  </MenuList>
                </Paper>
              </Popper>
            </StyledButton>
          </Grid>
          {!props.user && (
            <Grid item xs={12} md={1}>
              <Link href="">
                <StyledButton
                  style={{
                    paddingLeft: "0px",
                    paddingRight: "0px",
                  }}
                  fullWidth
                  disableElevation={true}
                >
                  Log In
                </StyledButton>
              </Link>
            </Grid>
          )}
          {navButton}
        </Grid>
      </Box>
    </Toolbar>
  );
};

type Props = {
  user?: User;
};

export default HeaderBar;

import { Grid, withStyles, Button } from "@material-ui/core";

const SeeMoreButton = withStyles({
  root: {
    color: "#FF6060",
    minWidth: "140px",
    border: "1px solid #FF6060",
    boxShadow: "0px 8px 12px #FFE3E3",
    borderRadius: "1.81513px",
  },
  label: {
    textTransform: "capitalize",
  },
})(Button);

const imageStyle = {};

export default () => (
  <Grid item container spacing={10} direction="column">
    <Grid item container>
      <Grid item container spacing={2} direction="column">
        <Grid item container direction="row" justify="center">
          <h1>OUR PARTNERS</h1>
        </Grid>
        <Grid item container spacing={2} direction="row" justify="center">
          <Grid item container xs={6} sm={4} md={2} justify="center">
            <img src="/nus.png" style={imageStyle} />
          </Grid>
          <Grid item container xs={6} sm={4} md={2} justify="center">
            <img src="/hku.png" style={imageStyle} />
          </Grid>
          <Grid item container xs={6} sm={4} md={2} justify="center">
            <img src="/ewu.png" style={imageStyle} />
          </Grid>
          <Grid item container xs={6} sm={4} md={2} justify="center">
            <img src="/todai.png" style={imageStyle} />
          </Grid>
          <Grid item container xs={6} sm={4} md={2} justify="center">
            <img src="/tsinghua.png" style={imageStyle} />
          </Grid>
        </Grid>
      </Grid>
    </Grid>
    <Grid item container direction="row" justify="center">
      <SeeMoreButton>See More</SeeMoreButton>
    </Grid>
  </Grid>
);

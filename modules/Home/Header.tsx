import React from "react";
import { AppBar, Container, makeStyles } from "@material-ui/core";

import User from "../../types/user.types";
import HeaderBar from "./HeaderBar";

const useStyles = makeStyles({
  edudriftBackground: {
    backgroundImage: "url('/background.svg')",
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
    boxShadow: "none",
  },
});

const Header: React.FC<Props> = (props) => {
  const classes = useStyles();

  return (
    <AppBar position="static" className={classes.edudriftBackground}>
      <Container maxWidth="lg">
        <HeaderBar user={props.user} />
        {props.children}
      </Container>
    </AppBar>
  );
};

type Props = {
  user?: User;
};

export default Header;

import * as React from "react";

import { NextPage, NextPageContext } from "next";

import withError from "../../components/HOCs/withError";

import { ProgramsService } from "../Programs/services/ProgramsService";
import { Program } from "../Programs/types/Programs.types";
import { getUserOrReturnUndefined } from "../../services/Auth.functions";

import User from "../../types/user.types";
import LandingPage from "./LandingPage";
import TopHeader from "./TopHeader";

const HomePage: NextPage<Props> = (props) => {
  return (
    <div>
      <TopHeader user={props.user} />
      <LandingPage />
    </div>
  );
};

type Props = {
  user: User;
  // fairs: Fair[];
  programs: Program[];
};

HomePage.getInitialProps = async (ctx: NextPageContext) => {
  try {
    const user = await getUserOrReturnUndefined(ctx);
    // const fairs = await FairsService.getAllPublished();
    const programs = await ProgramsService.getAllApproved();

    return { user, programs } as Props;
  } catch (e) {
    throw e;
  }
};

export default withError(HomePage);

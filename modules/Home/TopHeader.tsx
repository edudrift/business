import React from "react";
import { Container, Box } from "@material-ui/core";

import User from "../../types/user.types";
import Header from "./Header";
import HeaderWelcomeMessage from "./HeaderWelcomeMessage";
import OurPartners from "./OurPartners";
import StudentsFirst from "./StudentsFirst";

const HorizontalDivider = () => (
  <div
    style={{
      height: "1px",
      width: "100%",
      background: "#E8ECF0",
    }}
  />
);

const TopHeader: React.FC<Props> = (props) => {
  return (
    <>
      <Header user={props.user}>
        <HeaderWelcomeMessage />
      </Header>
      <Box bgcolor="white">
        <Container maxWidth="lg">
          <Box py={5}>
            <StudentsFirst />
          </Box>
          <Box py={5}>
            <HorizontalDivider />
          </Box>
          <Box py={5} overflow="hidden">
            <OurPartners />
          </Box>
        </Container>
      </Box>
    </>
  );
};

type Props = {
  user?: User;
};

export default TopHeader;

import React from "react";
import Header from "./Header";
import Footer from "./Footer";
import { Container, Box } from "@material-ui/core";
import User from "../../types/user.types";

export default (props: Props) => (
  <Box bgcolor="white" height="100%">
    <Header user={props.user} />
    {props.banner}
    <Container>
      <Box my={2}>{props.children}</Box>
    </Container>
    <Footer />
  </Box>
);

type Props = {
  user?: User;
  children: React.ReactNode;
  banner?: React.ReactNode;
};

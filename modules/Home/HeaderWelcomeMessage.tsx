import { Box, Button, Grid, withStyles } from "@material-ui/core";
import Link from "next/link";

const StartButton = withStyles({
  root: {
    fontSize: "120%",
    color: "white",
    background: "#FF6060",
    boxShadow: "0px 8px 12px #2379F1",
    borderRadius: "2px",
    paddingLeft: "20px",
    paddingRight: "20px",
    fontFamily: "source-han-sans-simplified-c, sans-serif",
    "&:hover": {
      background: "white",
      color: "#1890FF",
    },
  },
  label: {
    textTransform: "capitalize",
  },
})(Button);

const ContactButton = withStyles({
  root: {
    fontSize: "120%",
    color: "white",
    border: "1px solid #FFFFFF",
    borderRadius: "2px",
    paddingLeft: "20px",
    paddingRight: "20px",
    fontFamily: "source-han-sans-simplified-c, sans-serif",
    "&:hover": {
      background: "white",
      color: "#1890FF",
    },
  },
  label: {
    textTransform: "capitalize",
  },
})(Button);

export default () => (
  <Box py={3}>
    <Grid item container spacing={2} direction="row" justify="center">
      <Grid item container xs={12} md={5}>
        <Box pb={3}>
          <h1>
            Designed to make life easier for students and education providers
          </h1>
          Every year, hundreds of thousands of students travel around the world
          in search of knowledge, experience and opportunities. EduDrift
          connects to the global network of students, helps businesses cater to
          more students and enables more bookings with ease.
        </Box>
      </Grid>
      <Grid item container xs={10} sm={10} md={5}>
        <Grid item container direction="row">
          <img
            src="/computer.svg"
            style={{
              justifyContent: "end",
              width: "100%",
            }}
          />
        </Grid>
      </Grid>
      <Grid item container xs={10} sm={10} md={5}>
        <Grid item container direction="row">
          <Box py={3} width="100%">
            <Grid
              item
              container
              alignItems="flex-start"
              spacing={2}
              direction="row"
            >
              <Grid item xs={12} sm={6} md={5}>
                <Link href="mailTo:partners@edudrift.com" passHref>
                  <StartButton fullWidth>Start Free Trial</StartButton>
                </Link>
              </Grid>
              <Grid item xs={12} sm={6} md={5}>
                <Link href="mailTo:partners@edudrift.com" passHref>
                  <ContactButton fullWidth>Contact Sales</ContactButton>
                </Link>
              </Grid>
            </Grid>
          </Box>
        </Grid>
      </Grid>
    </Grid>
  </Box>
);

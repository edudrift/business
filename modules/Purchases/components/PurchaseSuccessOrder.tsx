import React from "react";

import { NextPage } from "next";

import GenericLayout from "../../../components/GenericLayout/GenericLayout";
import { getUserProfileOrRedirect } from "../../../services/Auth.functions";
import { FairPackageService } from "../../../services/FairPackage.service";
import { FairPackagePurchase } from "../../../types/FairPackagePurchase.types";
import User from "../../../types/user.types";

export const PurchaseSuccessPage: NextPage<Props> = (props) => {
  return (
    <GenericLayout user={props.user}>
      <h1>{props.purchase.status}</h1>
    </GenericLayout>
  );
};

type Props = {
  user: User;
  purchase: FairPackagePurchase;
};

PurchaseSuccessPage.getInitialProps = async (ctx) => {
  const user = await getUserProfileOrRedirect(ctx);
  const purchase = await FairPackageService.getPurchase(
    ctx,
    ctx.query.session_id as string
  );

  return {
    user,
    purchase,
  };
};

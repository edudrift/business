import React from "react";

import { FormControlLabel, Grid, Switch } from "@material-ui/core";
import { NextPage } from "next";
import withError from "../../../components/HOCs/withError";
import DashboardLayout, {
  EDashboardLayoutSidebarNames,
} from "../../../components/Layouts/DashboardLayout";
import { PaddedPaper } from "../../Shared/components/PaddedPaper";
import { PurchasesTable } from "../../../components/Tables/PurchasesTable/PurchasesTable";
import { getUserProfileOrRedirect } from "../../../services/Auth.functions";
import { PurchasesService } from "../../../services/Purchases.service";
import {
  EFairPackagePurchaseStatus,
  FairPackagePurchase,
} from "../../../types/FairPackagePurchase.types";
import User from "../../../types/user.types";

export const PurchasesPage: NextPage<Props> = (props) => {
  const [showPaidOnly, setShowPaidOnly] = React.useState(true);
  const [purchases, setPurchases] = React.useState(props.purchases);

  React.useEffect(() => {
    if (showPaidOnly) {
      setPurchases(
        props.purchases.filter(
          (purchase) => purchase.status === EFairPackagePurchaseStatus.PAID
        )
      );
    } else {
      setPurchases(props.purchases);
    }
  }, [showPaidOnly]);

  return (
    <DashboardLayout
      user={props.user}
      activeSidebarName={EDashboardLayoutSidebarNames.PURCHASES}
      headerTitle="Purchases"
    >
      <PaddedPaper fullWidth>
        <Grid container spacing={2}>
          <Grid item container justify="space-between">
            <Grid item>
              <h1>Your Purchases</h1>
            </Grid>
            <Grid item>
              <FormControlLabel
                control={
                  <Switch
                    color="primary"
                    checked={showPaidOnly}
                    onChange={() => setShowPaidOnly(!showPaidOnly)}
                  />
                }
                label="Show paid only"
              />
            </Grid>
          </Grid>
        </Grid>

        <Grid item xs={12}>
          <PurchasesTable
            purchases={purchases}
            total={props.purchases.length}
          />
        </Grid>
      </PaddedPaper>
    </DashboardLayout>
  );
};

type Props = {
  user: User;
  purchases: FairPackagePurchase[];
};

PurchasesPage.getInitialProps = async (ctx) => {
  const user = await getUserProfileOrRedirect(ctx);
  const purchases = await PurchasesService.getAllPurchases(ctx);

  return {
    user,
    purchases,
  } as Props;
};

export const PurchasesPageWithError = withError(PurchasesPage);

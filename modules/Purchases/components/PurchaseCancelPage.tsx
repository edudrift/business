import React from "react";

import { Grid } from "@material-ui/core";
import { NextPage } from "next";
import FairPackagePurchaseOverview from "../../../components/FairPackagePurchaseOverview";
import GenericLayout from "../../../components/GenericLayout/GenericLayout";
import { PaddedPaper } from "../../Shared/components/PaddedPaper";
import { getUserProfileOrRedirect } from "../../../services/Auth.functions";
import { FairPackageService } from "../../../services/FairPackage.service";
import { FairPackagePurchase } from "../../../types/FairPackagePurchase.types";
import User from "../../../types/user.types";
import ButtonLink from "../../../components/ButtonLink";

export const PurchaseCancelPage: NextPage<Props> = (props) => {
  return (
    <GenericLayout user={props.user}>
      <Grid item container xs={6} spacing={2}>
        <Grid item xs={12}>
          <FairPackagePurchaseOverview
            package={props.purchase.fairPackage}
            fair={props.purchase.fairPackage.fair}
            alertOptions={{
              severity: "warning",
              title: "Cancellation notice",
              body:
                "Your purchase request has been cancelled. You have to make another order.",
            }}
          />
        </Grid>

        <Grid item xs={12}>
          <PaddedPaper fullWidth>
            <Grid container item justify="center">
              <Grid item>
                <ButtonLink
                  variant="contained"
                  color="primary"
                  href="/fair/[slug]/packages/[fairPackageId]"
                  as={`/fair/${props.purchase.fairPackage.fair.slug}/packages/${props.purchase.fairPackage.id}`}
                >
                  Proceed to reorder
                </ButtonLink>
              </Grid>
            </Grid>
          </PaddedPaper>
        </Grid>
      </Grid>
    </GenericLayout>
  );
};

type Props = {
  user: User;
  purchase: FairPackagePurchase;
};

PurchaseCancelPage.getInitialProps = async (ctx) => {
  const user = await getUserProfileOrRedirect(ctx);
  const purchase = await FairPackageService.cancelOrder(
    ctx,
    ctx.query.session_id as string
  );

  return {
    user,
    purchase,
  };
};

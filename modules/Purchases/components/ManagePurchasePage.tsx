import React from "react";

import { NextPage } from "next";

import { Typography, Grid, Button } from "@material-ui/core";
import withError from "../../../components/HOCs/withError";
import DashboardLayout, {
  EDashboardLayoutSidebarNames,
} from "../../../components/Layouts/DashboardLayout";
import { PaddedPaper } from "../../Shared/components/PaddedPaper";
import { getUserProfileOrRedirect } from "../../../services/Auth.functions";
import { PurchasesService } from "../../../services/Purchases.service";
import {
  FairPackagePurchase,
  EFairPackagePublishStatus,
} from "../../../types/FairPackagePurchase.types";
import User from "../../../types/user.types";

const ManagePurchasePage: NextPage<Props> = (props) => {
  const [requesting, setRequesting] = React.useState(false);
  const [purchase, setPurchase] = React.useState<FairPackagePurchase>(
    props.purchase
  );

  const requestToPublish = async () => {
    setRequesting(true);

    try {
      const updatedPurchase = await PurchasesService.requestToPublish(purchase);
      console.log({ updatedPurchase });
      setPurchase(updatedPurchase);
      setRequesting(false);
    } catch (e) {
      setRequesting(false);
    }
  };

  return (
    <DashboardLayout
      user={props.user}
      activeSidebarName={EDashboardLayoutSidebarNames.PURCHASES}
      headerTitle="Manage your purchase"
    >
      <PaddedPaper fullWidth>
        <Grid container spacing={2}>
          <Grid item container justify="space-between" alignItems="center">
            <Grid item>
              <Typography variant="h4" component="h1">
                Manage this purchase
              </Typography>
            </Grid>
            <Grid item>
              {purchase.publishStatus ===
                EFairPackagePublishStatus.REQUESTED && (
                <Button disabled variant="outlined" color="primary">
                  Review is pending
                </Button>
              )}
              {purchase.publishStatus === EFairPackagePublishStatus.PENDING && (
                <Button
                  disabled={requesting}
                  variant="outlined"
                  color="primary"
                  onClick={requestToPublish}
                >
                  {requesting ? "Requesting..." : "Request to publish"}
                </Button>
              )}
            </Grid>
          </Grid>
        </Grid>
      </PaddedPaper>
    </DashboardLayout>
  );
};

type Props = {
  user: User;
  purchase: FairPackagePurchase;
};

ManagePurchasePage.getInitialProps = async (ctx) => {
  const user = await getUserProfileOrRedirect(ctx);
  const purchase = await PurchasesService.getById(
    ctx,
    ctx.query.purchaseId as string
  );

  return {
    user,
    purchase,
  } as Props;
};

export const ManagePurchasePageWithError = withError(ManagePurchasePage);

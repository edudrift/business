export type Country = {
  label: string;
  code: string;
  phone: string;
};

import * as Yup from "yup";

export const CountrySchema = Yup.object().shape({
  code: Yup.string().required(),
  phone: Yup.string().required(),
  label: Yup.string().required(),
});

export const CountryEmptyState = {
  code: "",
  phone: "",
  label: "",
};

import { find } from "lodash";

import { Country } from "./types/CountryType";

import { countries } from "./countries";
import {
  CountryCodeNotFound,
  CountryLabelNotFound,
  CountryPhoneNotFound,
} from "./errors";

class CountriesServiceClass {
  getCountries(): Country[] {
    return countries;
  }

  getCountryFromCode(code: string) {
    const country = find(countries, { code });

    if (!country) throw new CountryCodeNotFound();

    return country;
  }

  getCountryFromPhone(phone: string) {
    const country = find(countries, { phone });

    if (!country) throw new CountryPhoneNotFound();

    return country;
  }

  getCountryFromLabel(label: string) {
    const country = find(countries, { label });

    if (!country) throw new CountryLabelNotFound();

    return country;
  }

  getLabelFromCountry(country: Country) {
    return country.label;
  }

  getCodeFromCountry(country: Country) {
    return country.code;
  }
}

export const CountriesService = new CountriesServiceClass();

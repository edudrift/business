export class CountryCodeNotFound extends Error {}

export class CountryPhoneNotFound extends Error {}

export class CountryLabelNotFound extends Error {}

import React from "react";

import { makeStyles, TextField as MUITextField } from "@material-ui/core";
import { Field } from "formik";
import { Autocomplete } from "material-ui-formik-components";

import { CountriesService } from "../CountriesService";
import { Country } from "../types/CountryType";

const useStyles = makeStyles({
  option: {
    fontSize: 15,
    "& > span": {
      marginRight: 10,
      fontSize: 18,
    },
  },
});

export const FormikCountrySelect: React.FC<Props> = ({
  name,
  label,
  required,
}) => {
  const classes = useStyles();

  // ISO 3166-1 alpha-2
  // ⚠️ No support for IE 11
  function countryToFlag(isoCode: string) {
    return typeof String.fromCodePoint !== "undefined"
      ? isoCode
          .toUpperCase()
          .replace(/./g, (char) =>
            String.fromCodePoint(char.charCodeAt(0) + 127397)
          )
      : isoCode;
  }

  return (
    <Field
      required
      component={Autocomplete}
      name={name}
      options={CountriesService.getCountries()}
      classes={{
        option: classes.option,
      }}
      autoHighlight
      getOptionLabel={(option: Country) => option.label}
      renderOption={(option: Country) => (
        <React.Fragment>
          <span>{countryToFlag(option.code)}</span>
          {option.label} ({option.code}) +{option.phone}
        </React.Fragment>
      )}
      renderInput={(params: any) => {
        return (
          <MUITextField
            {...params}
            label={label}
            variant="outlined"
            inputProps={{
              ...params.inputProps,
              autoComplete: "new-password", // disable autocomplete and autofill
            }}
            fullWidth
            required={required}
          />
        );
      }}
    />
  );
};

type Props = {
  name: string;
  label: string;
  required?: boolean;
};

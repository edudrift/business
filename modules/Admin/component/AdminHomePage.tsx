import { NextPage, NextPageContext } from "next";
import React from "react";

import withError from "../../../components/HOCs/withError";
import AdminLayout from "../../../components/Layouts/AdminLayout";
import { getUserAndEnsureAdmin } from "../../../services/Auth.functions";

const AdminHomePage: NextPage<Props> = () => {
  return (
    <AdminLayout>
      <h1>Admin Home Page</h1>
    </AdminLayout>
  );
};

type Props = PromiseResult<ReturnType<typeof getInitialProps>>;

const getInitialProps = async (ctx: NextPageContext) => {
  const user = await getUserAndEnsureAdmin(ctx);

  return {
    user,
  };
};

AdminHomePage.getInitialProps = getInitialProps;

export default withError(AdminHomePage);

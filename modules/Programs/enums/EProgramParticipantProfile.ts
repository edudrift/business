export enum EProgramParticipantProfile {
  PRE_NURSERY = "PRE_NURSERY",
  NURSERY1 = "NURSERY1",
  NURSERY2 = "NURSERY2",
  KINDERGARTEN1 = "KINDERGARTEN1",
  KINDERGARTEN2 = "KINDERGARTEN2",
  PRIMARY_SCHOOL = "PRIMARY_SCHOOL",
  SECONDARY_SCHOOL = "SECONDARY_SCHOOL",
  HIGH_SCHOOL_OR_PRE_UNIVERSITY = "HIGH_SCHOOL_OR_PRE_UNIVERSITY",
  UNDERGRADUATE = "UNDERGRADUATE",
  ADULT_OR_POST_GRADUATE = "ADULT_OR_POST_GRADUATE",
}

export enum EProgramParticipantProfileLabels {
  PRE_NURSERY = "Pre Nursery (18 months – 2+ yrs)",
  NURSERY1 = "Nursery 1 (3 yrs)",
  NURSERY2 = "Nursery 2 (4 yrs)",
  KINDERGARTEN1 = "Kindergarten 1 (5 yrs)",
  KINDERGARTEN2 = "Kindergarten 2 (6 yrs)",
  PRIMARY_SCHOOL = "Primary School (7 yrs - 11 yrs)",
  SECONDARY_SCHOOL = "Secondary School (12 yrs - 16 yrs)",
  HIGH_SCHOOL_OR_PRE_UNIVERSITY = "High School/Pre-university (17 yrs - 18 yrs)",
  UNDERGRADUATE = "Undergraduate (19 yrs - 22 yrs)",
  ADULT_OR_POST_GRADUATE = "Adult/Post-graduate (> 22yrs)",
}

export enum EProgramEducationalBackground {
  ADULT_OR_POST_GRADUATE = "ADULT_OR_POST_GRADUATE",
  UNDERGRADUATE = "UNDERGRADUATE",
  HIGH_SCHOOL_OR_PRE_UNIVERSITY = "HIGH_SCHOOL_OR_PRE_UNIVERSITY",
  SECONDARY_SCHOOL_OR_BELOW = "SECONDARY_SCHOOL_OR_BELOW",
}

export enum EProgramEducationalBackgroundLabels {
  ADULT_OR_POST_GRADUATE = "Adult/Post-graduate",
  UNDERGRADUATE = "Undergraduate",
  HIGH_SCHOOL_OR_PRE_UNIVERSITY = "High school/Pre-university",
  SECONDARY_SCHOOL_OR_BELOW = "Secondary School or Below",
}

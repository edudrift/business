export enum EProgramSkill {
  CREATIVITY = "CREATIVITY",
  COGNITIVE_THINKING = "COGNITIVE_THINKING",
  COOPERATION = "COOPERATION",
  SELF_AWARENESS = "SELF_AWARENESS",
  AESTHETIC_ABILITY = "AESTHETIC_ABILITY",
  COMMUNICATION_SKILLS = "COMMUNICATION_SKILLS",
  CRITICAL_THINKING = "CRITICAL_THINKING",
  PROBLEM_SOLVING = "PROBLEM_SOLVING",
  GROUP_DYNAMIC_SKILLS = "GROUP_DYNAMIC_SKILLS",
  RESEARCH_SKILLS = "RESEARCH_SKILLS",
  LITERACY_SKILLS = "LITERACY_SKILLS",
  NUMERACY_SKILLS = "NUMERACY_SKILLS",
}

export enum EProgramSkillLabels {
  CREATIVITY = "Creativity",
  COGNITIVE_THINKING = "Cognitive thinking",
  COOPERATION = "Cooperation",
  SELF_AWARENESS = "Self-awareness",
  AESTHETIC_ABILITY = "Aesthetic ability",
  COMMUNICATION_SKILLS = "Communication skills",
  CRITICAL_THINKING = "Critical thinking",
  PROBLEM_SOLVING = "Problem solving",
  GROUP_DYNAMIC_SKILLS = "Group-dynamic skills",
  RESEARCH_SKILLS = "Research skills",
  LITERACY_SKILLS = "Literacy skills",
  NUMERACY_SKILLS = "Numeracy skills",
}

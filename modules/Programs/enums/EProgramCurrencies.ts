export enum EProgramCurrencies {
  AUD = "AUD",
  CAD = "CAD",
  EUR = "EUR",
  HKD = "HKD",
  INR = "INR",
  JPY = "JPY",
  MYR = "MYR",
  NZD = "NZD",
  SGD = "SGD",
  GBP = "GBP",
  USD = "USD",
}

export enum EProgramCurrencyLabels {
  AUD = "Australian Dollar(AUD)",
  CAD = "Canadian Dollar(CAD)",
  EUR = "Euro(EUR)",
  HKD = "Hong Kong Dollar(HKD)",
  INR = "Indian Rupee(INR)",
  JPY = "Japanese Yen(JPY)",
  MYR = "Malaysian Ringgit(MYR)",
  NZD = "New Zealand Dollar(NZD)",
  SGD = "Singapore Dollar(SGD)",
  GBP = "Great Britain Pound(GBP)",
  USD = "United States Dollar(USD)",
}

export const ArrayOfCurrencies = [
  EProgramCurrencies.AUD,
  EProgramCurrencies.CAD,
  EProgramCurrencies.EUR,
  EProgramCurrencies.HKD,
  EProgramCurrencies.INR,
  EProgramCurrencies.JPY,
  EProgramCurrencies.MYR,
  EProgramCurrencies.NZD,
  EProgramCurrencies.SGD,
  EProgramCurrencies.GBP,
  EProgramCurrencies.USD,
];

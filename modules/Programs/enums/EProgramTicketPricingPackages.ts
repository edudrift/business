export enum EProgramTicketPricingPackages {
  COMMUNITY = "COMMUNITY",
  ENTERPRISE = "ENTERPRISE",
  EXTERNAL = "EXTERNAL",
}

enum EProgramTicketPricingPackageLabels {
  COMMUNITY = "Community",
  ENTERPRISE = "Enterprise",
  EXTERNAL = "External",
}

enum EProgramTicketPricingPackageDescription {
  COMMUNITY = "Coming soon to a store near you.",
  ENTERPRISE = "The one and only pricing package you will ever need.",
  EXTERNAL = "Use a ticketing system that you are already familiar with.",
}

export const ProgramTicketingPricingPackageValues = [
  {
    value: EProgramTicketPricingPackages.ENTERPRISE,
    disabled: false,
    label: EProgramTicketPricingPackageLabels.ENTERPRISE,
    description: EProgramTicketPricingPackageDescription.ENTERPRISE,
  },
  {
    value: EProgramTicketPricingPackages.COMMUNITY,
    disabled: true,
    label: EProgramTicketPricingPackageLabels.COMMUNITY,
    description: EProgramTicketPricingPackageDescription.COMMUNITY,
  },
  {
    value: EProgramTicketPricingPackages.EXTERNAL,
    disabled: true,
    label: EProgramTicketPricingPackageLabels.EXTERNAL,
    description: EProgramTicketPricingPackageDescription.EXTERNAL,
  },
];

export enum EProgramMethodology {
  DRAMA = "DRAMA",
  POETRY = "POETRY",
  THEATRE = "THEATRE",
  MIME = "MIME",
  PUPPETRY = "PUPPETRY",
  ROLE_PLAY = "ROLE_PLAY",
  STORY_TELLING = "STORY_TELLING",
  PROBLEM_SOLVING = "PROBLEM_SOLVING",
  EXPERIMENT = "EXPERIMENT",
  READING_ALOUD = "READING_ALOUD",
  CREATIVE_WRITING = "CREATIVE_WRITING",
  FIELD_TRIP = "FIELD_TRIP",
}

export enum EProgramMethodologyLabels {
  DRAMA = "Drama",
  POETRY = "Poetry",
  THEATRE = "Theatre",
  MIME = "Mime",
  PUPPETRY = "Puppetry",
  ROLE_PLAY = "Role-play",
  STORY_TELLING = "Story-telling",
  PROBLEM_SOLVING = "Problem-solving",
  EXPERIMENT = "Experiment",
  READING_ALOUD = "Reading aloud",
  CREATIVE_WRITING = "Creative writing",
  FIELD_TRIP = "Field trip",
}

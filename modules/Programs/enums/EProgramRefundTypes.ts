export enum EProgramRefundTypes {
  FULL_REFUND = "FULL_REFUND",
  PARTIAL_REFUND = "PARTIAL_REFUND",
  NO_REFUND = "NO_REFUND",
}

export enum EProgramRefundTypeLabels {
  FULL_REFUND = "Be Fully Refunded",
  PARTIAL_REFUND = "Be Partially Refunded",
  NO_REFUND = "Not be Refunded",
}

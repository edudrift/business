export enum EProgramAccommodationRoomTypes {
  SINGLE = "SINGLE",
  DOUBLE_ROOM = "DOUBLE_ROOM",
  TRIPLE_ROOM = "TRIPLE_ROOM",
  MIX_OF_DOUBLE_TRIPLE = "MIX_OF_DOUBLE_TRIPLE",
  THREE_OR_MORE = "THREE_OR_MORE",
}

export enum EProgramAccommodationRoomTypeLabels {
  SINGLE = "Single Room (Not shared)",
  DOUBLE_ROOM = "Double/Twin Rooms",
  TRIPLE_ROOM = "Triple Rooms",
  MIX_OF_DOUBLE_TRIPLE = "Mix of Double/Twin/Triple Rooms",
  THREE_OR_MORE = "Three or more roommates in one room",
}

export const ArrayOfProgramAccommodationRoomTypes = [
  EProgramAccommodationRoomTypes.SINGLE,
  EProgramAccommodationRoomTypes.DOUBLE_ROOM,
  EProgramAccommodationRoomTypes.TRIPLE_ROOM,
  EProgramAccommodationRoomTypes.MIX_OF_DOUBLE_TRIPLE,
  EProgramAccommodationRoomTypes.THREE_OR_MORE,
];

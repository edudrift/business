export enum EProgramHighlightTypes {
  FINAL_SHOW = "FINAL_SHOW",
  FIELD_TRIP = "FIELD_TRIP",
  COMPETITION = "COMPETITION",
  GUEST_SPEAKER = "GUEST_SPEAKER",
}

export enum EProgramHighlightTypesLabels {
  FINAL_SHOW = "Final show",
  FIELD_TRIP = "Field trip",
  COMPETITION = "Competition",
  GUEST_SPEAKER = "Guest speaker",
}

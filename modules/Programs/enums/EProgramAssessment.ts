export enum EProgramAssessment {
  EXAM = "EXAM",
  GROUP_PROJECT = "GROUP_PROJECT",
  COMPETITION = "COMPETITION",
  FINAL_PRESENTATION = "FINAL_PRESENTATION",
  ATTENDANCE = "ATTENDANCE",
  FIELD_TRIP = "FIELD_TRIP",
}

export enum EProgramAssessmentLabels {
  EXAM = "Exam",
  GROUP_PROJECT = "Group project",
  COMPETITION = "Competition",
  FINAL_PRESENTATION = "Final presentation",
  ATTENDANCE = "Attendance",
  FIELD_TRIP = "Field trip",
}

export enum EProgramAccommodationTypes {
  HOTEL_5_STAR = "HOTEL_5_STAR",
  HOTEL_4_STAR = "HOTEL_4_STAR",
  HOTEL_3_STAR = "HOTEL_3_STAR",
  STUDENT_DORMITORY_OR_HOSTEL = "STUDENT_DORMITORY_OR_HOSTEL",
  SERVICED_APARTMENT_OR_CONDOMINIUM = "SERVICED_APARTMENT_OR_CONDOMINIUM",
}

export enum EProgramAccommodationTypeLabels {
  HOTEL_5_STAR = "Hotel (5 Star Hotel)",
  HOTEL_4_STAR = "Hotel (4 Star Hotel)",
  HOTEL_3_STAR = "Hotel (3 Star Hotel)",
  STUDENT_DORMITORY_OR_HOSTEL = "Student Dormitory/Hostel",
  SERVICED_APARTMENT_OR_CONDOMINIUM = "Serviced Apartment/Condominium",
}

export const ArrayOfProgramAccommodationTypes = [
  EProgramAccommodationTypes.HOTEL_5_STAR,
  EProgramAccommodationTypes.HOTEL_4_STAR,
  EProgramAccommodationTypes.HOTEL_3_STAR,
  EProgramAccommodationTypes.STUDENT_DORMITORY_OR_HOSTEL,
  EProgramAccommodationTypes.SERVICED_APARTMENT_OR_CONDOMINIUM,
];

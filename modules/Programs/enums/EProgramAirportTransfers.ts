export enum EProgramAirportTransfers {
  BOTH_WAYS = "BOTH_WAYS",
  ARRIVAL_ONLY = "ARRIVAL_ONLY",
  DEPARTURE_ONLY = "DEPARTURE_ONLY",
  NONE = "NONE",
}

export enum EProgramAirportTransferLabels {
  BOTH_WAYS = "Yes, Both ways. (Scheduled)",
  ARRIVAL_ONLY = "Only for arrival. (Scheduled)",
  DEPARTURE_ONLY = "Only for departure. (Scheduled)",
  NONE = "No, airport transfers will not be provided. Participants will have to find their own way to the accommodation.",
}

export const ArrayOfProgramAirportTransfers = [
  EProgramAirportTransfers.BOTH_WAYS,
  EProgramAirportTransfers.ARRIVAL_ONLY,
  EProgramAirportTransfers.DEPARTURE_ONLY,
  EProgramAirportTransfers.NONE,
];

import React from "react";

import { NextPage, NextPageContext } from "next";

import { useRouter } from "next/router";
import { getUserProfileOrRedirect } from "../../../services/Auth.functions";
import { ProgramScheduleForm } from "./forms/ProgramScheduleForm";
import TitleWithSubtitleFragment from "../../Shared/components/TitleWithSubheaderAndSubtitle";
import { ProgramDashboard } from "./ProgramDashboard";
import { ProgramsServiceForBusiness } from "../services/ProgramsServiceForBusiness";

const ProgramSchedulePage: NextPage<Props> = ({ user, program }) => {
  const router = useRouter();

  return (
    <ProgramDashboard
      user={user}
      program={program}
      crumbs={[
        { title: "Dashboard", link: "/biz" },
        { title: "Programs", link: "/biz/programs" },
        {
          title: "Edit Program",
          link: `/biz/program/${program.id}/edit`,
        },
        { title: "Schedule" },
      ]}
      cancelRoute={[
        "/biz/program/[id]/edit",
        `/biz/program/${program.id}/edit`,
      ]}
    >
      <TitleWithSubtitleFragment
        title="Schedule"
        subtitle="What are your essentials?"
      />
      <ProgramScheduleForm
        program={program}
        afterSubmit={() => {
          router.push(
            `/biz/program/[id]/edit`,
            `/biz/program/${program.id}/edit`
          );
        }}
      />
    </ProgramDashboard>
  );
};

const getInitialProps = async (ctx: NextPageContext) => {
  const user = await getUserProfileOrRedirect(ctx);
  const program = await ProgramsServiceForBusiness.getProgramFromId(
    ctx.query.id as string,
    ctx
  );

  return { user, program };
};

type Props = PromiseResult<ReturnType<typeof getInitialProps>>;

ProgramSchedulePage.getInitialProps = getInitialProps;

export default ProgramSchedulePage;

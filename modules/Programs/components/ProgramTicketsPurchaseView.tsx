import React from "react";

import { AppBar, Grid, Tab, Tabs, Button } from "@material-ui/core";
import { filter, find } from "lodash";

import { PaddedPaper } from "../../Shared/components/PaddedPaper";
import theme from "../../../theme";
import { TypographyWithPxFontAndColor } from "../../Shared/components/TypographyWithPxFontAndColor";
import { Program } from "../types/Programs.types";
import { ProgramTicketType } from "../types/ProgramTicketType.types";
import { ProgramTicketItem } from "./ProgramTicketItem";
import { ProgramTicket } from "../types/ProgramTicket.types";

type CartItemsWithQuantity = {
  [ticketId: string]: number;
};

export const ProgramTicketsPurchaseView: React.FC<Props> = ({ program }) => {
  const [activeTabIndex, setActiveTabIndex] = React.useState(0);
  const [cart, setCart] = React.useState<CartItemsWithQuantity>({});
  const [total, setTotal] = React.useState(0);

  React.useEffect(() => {
    setTotal(calculateTotal());
  }, [cart]);

  const handleChange = (_: any, newValue: number) => {
    setActiveTabIndex(newValue);
  };

  const handleCartChange = (ticket: ProgramTicket, quantity: number) => {
    setCart({
      ...cart,
      [ticket.id]: quantity,
    });
  };

  const renderTicketsForTicketType = (type: ProgramTicketType) => {
    const tickets = filter(program.tickets, (ticket) => {
      return ticket.typeId === type.id;
    });

    return tickets.map((ticket) => (
      <ProgramTicketItem
        currencyCode={program.currency}
        ticket={ticket}
        key={ticket.id}
        onChange={(quantity) => {
          handleCartChange(ticket, quantity);
        }}
        value={cart[ticket.id]}
      />
    ));
  };

  const calculateTotal = () => {
    return Object.entries(cart).reduce(
      (rollingTotal: number, [ticketId, quantity]) => {
        const ticket = find(program.tickets, { id: ticketId });
        if (ticket) {
          return (rollingTotal += parseInt(ticket.price, 10) * quantity);
        }
        return rollingTotal;
      },
      0
    );
  };

  return (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        <AppBar
          position="static"
          color="default"
          style={{ boxShadow: "none", backgroundColor: "transparent" }}
        >
          <Tabs
            value={activeTabIndex}
            onChange={handleChange}
            indicatorColor="primary"
            textColor="primary"
            style={{ marginBottom: theme.spacing(2) }}
          >
            {program.ticketTypes.map((ticketType) => {
              return <Tab label={ticketType.name} key={ticketType.id} />;
            })}
          </Tabs>
        </AppBar>

        <Grid container spacing={4}>
          {renderTicketsForTicketType(program.ticketTypes[activeTabIndex])}
        </Grid>
      </Grid>

      <Grid item xs={12}>
        <Grid item container justify="space-between">
          <Grid item xs>
            <PaddedPaper fullWidth style={{ backgroundColor: "#F5F5F5" }}>
              <Grid container alignItems="center" spacing={1}>
                <Grid item>
                  <TypographyWithPxFontAndColor sizeInPx={14} color="#666666">
                    Total
                  </TypographyWithPxFontAndColor>
                </Grid>

                <Grid item>
                  <TypographyWithPxFontAndColor sizeInPx={28} color="#FF6F00">
                    {program.currency} {total}
                  </TypographyWithPxFontAndColor>
                </Grid>
              </Grid>
            </PaddedPaper>
          </Grid>

          <Grid item xs={4} md={2}>
            <Button
              style={{
                backgroundColor: "rgba(255,111,0,1)",
                height: "100%",
                width: "100%",
                borderRadius: "4px",
                color: "white",
                fontSize: "14px",
              }}
            >
              APPLY NOW
            </Button>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};

type Props = {
  program: Program;
};

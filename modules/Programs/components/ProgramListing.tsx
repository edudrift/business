import React from "react";

import { Grid, Button, Typography, Card } from "@material-ui/core";
import { Program } from "../types/Programs.types";
import TitleWithSubtitle from "../../Shared/components/TitleWithSubheaderAndSubtitle";
import Router from "next/router";
import { ProgramsStepsCompleteService } from "../services/ProgramStepsCompleteService";
import { findIndex } from "lodash";
import DialogPopupAlert from "../../Shared/components/DialogPopupAlert";
import VerticalStepper from "~/modules/Shared/components/Stepper/VerticalStepper";
// import AccountCircleIcon from "@material-ui/icons/AccountCircle";
// import LocationOnIcon from "@material-ui/icons/LocationOn";
// import EmailIcon from "@material-ui/icons/Email";

const getProgramListingDetails = (program: Program) => [
  {
    title: "Program Details",
    subtitle: "General information, contact details and search filters.",
    image: "/mandarin-cover.png",
    onClick: () =>
      Router.push(`${Router.pathname}/details`, `${Router.asPath}/details`),
    isStepComplete: ProgramsStepsCompleteService.isDetailsListingComplete(
      program
    ),
  },
  // {
  //   title: "Schedule",
  //   content: (
  //     <>
  //       Provide a detailed schedule or itinerary of the program.
  //       <br />
  //       Competitions/Conferences/Short Study Tours should present the schedule
  //       by day.
  //       <br />
  //       Other types of programs should present the schedule by week.
  //     </>
  //   ),
  //   onClick: () =>
  //     Router.push(`${Router.pathname}/schedule`, `${Router.asPath}/schedule`),
  //   completed: ProgramsStepsCompleteService.isProgramScheduleStepComplete(
  //     program
  //   ),
  // },
  {
    title: "Essentials & Highlights",
    subtitle: "Academic, logistical and other unique aspects of the program.",
    image: "/resume.png",
    onClick: () =>
      Router.push(
        `${Router.pathname}/essentials`,
        `${Router.asPath}/essentials`
      ),
    isStepComplete: ProgramsStepsCompleteService.isEssentialsListingComplete(
      program
    ),
  },
  {
    title: "Ticketing",
    subtitle:
      "Participant types, requirements, cancellation terms and refund policies.",
    image: "/tutor.png",
    onClick: () =>
      Router.push(`${Router.pathname}/ticketing`, `${Router.asPath}/ticketing`),
    isStepComplete: ProgramsStepsCompleteService.isTicketingListingComplete(
      program
    ),
  },
];

const ProgramListing: React.FC<Props> = ({ program }) => {
  const steps = getProgramListingDetails(program);

  const [open, setOpen] = React.useState(false);

  const [activeStep, setActiveStep] = React.useState(
    findIndex(steps, (l) => l.isStepComplete === false)
  );
  const completed = activeStep === -1;

  const handleStepClick = (stepIndex: number) => setActiveStep(stepIndex);

  return (
    <Grid
      item
      container
      direction="column"
      spacing={5}
      style={{ background: "#f5f5f5" }}
    >
      <Grid item container direction="column" spacing={2}>
        <Grid item>
          <Card>
            <Grid
              item
              container
              direction="column"
              style={{ margin: "0 0.6em" }}
            >
              <TitleWithSubtitle
                title="List new program"
                subtitle="Complete the sections below to list your purpose"
              ></TitleWithSubtitle>
              <Typography
                style={{
                  fontFamily: "Roboto",
                  color: "rgba(0,0,0,0.6)",
                  fontSize: "14px",
                }}
                gutterBottom
              >
                <>
                  You can save and go back at any point during the listing
                  process. Submission will only be available after the
                  completion of all four sections.
                </>
              </Typography>
            </Grid>
            <Grid item container xs={12} md={10}></Grid>
          </Card>
        </Grid>
      </Grid>

      <Grid item container spacing={2}>
        <Grid item xs={8} md={8}>
          <Card>
            <VerticalStepper
              activeStep={activeStep}
              steps={steps}
              onStepClick={handleStepClick}
              argumentForIsStepComplete={program}
            ></VerticalStepper>
          </Card>
        </Grid>
        <Grid item xs={4} md={4} spacing={2}></Grid>
      </Grid>
      <Grid item container spacing={7}>
        <Grid item>
          <Button
            variant="contained"
            color="primary"
            onClick={() => setOpen(true)}
          >
            Publish
          </Button>
        </Grid>
        <Grid item>
          <Button
            variant="contained"
            color="primary"
            disabled={!completed}
            onClick={() =>
              Router.push(
                "/biz/program/[id]/edit/preview",
                `/biz/program/${program.id}/edit/preview`
              )
            }
          >
            Preview
          </Button>
        </Grid>
      </Grid>
      <DialogPopupAlert
        open={open}
        onClose={() => {
          setOpen(false);
          Router.push("/biz/programs");
        }}
        text={
          "Your listing has been submitted to EduDrift. \
         EduDrift admin will review the listing in 72 hours. \
         You could still make changes to your listing in the 72 hours, \
         but the review timeline will restart if you submit a new version of the listing"
        }
      />
    </Grid>
  );
};

type Props = {
  program: Program;
};

export default ProgramListing;

import React from "react";

import { ProgramContentText } from "./ProgramContentText";
import { ProgramWeekSchedule } from "../types/ProgramSchedule.types";
import { PaddedPaper } from "../../Shared/components/PaddedPaper";

const GreyPaddedPaper = ({ children }: { children: any }) => (
  <PaddedPaper style={{ backgroundColor: "#f5f5f5" }}>{children}</PaddedPaper>
);

export const ProgramWeekScheduleContent: React.FC<Props> = ({ week }) => {
  return (
    <React.Fragment>
      <GreyPaddedPaper>
        {Object.values(week.details).map((detail, index) => {
          return <ProgramContentText key={index}>{detail}</ProgramContentText>;
        })}
      </GreyPaddedPaper>
    </React.Fragment>
  );
};

type Props = {
  week: ProgramWeekSchedule;
};

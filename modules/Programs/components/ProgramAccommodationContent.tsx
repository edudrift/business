import React from "react";

import { Grid } from "@material-ui/core";

import { Program } from "../types/Programs.types";
import { ProgramContentText } from "./ProgramContentText";
import { EProgramAccommodationTypeLabels } from "../enums/EProgramAccommodationTypes";
import { EProgramAccommodationRoomTypeLabels } from "../enums/EProgramAccommodationRoomTypes";
import { EProgramAirportTransferLabels } from "../enums/EProgramAirportTransfers";
import { ProgramImageTiler } from "./ProgramImageTiler";

export const ProgramAccommodationContent: React.FC<Props> = ({ program }) => {
  const { accommodation } = program;

  return (
    <Grid item container spacing={4}>
      <Grid item container spacing={1} direction="column">
        <Grid item>
          <ProgramContentText>
            Accommodation Name: {accommodation.details?.name}
          </ProgramContentText>
        </Grid>

        <Grid item>
          <ProgramContentText>
            Accommodation Type/Star:{" "}
            {EProgramAccommodationTypeLabels[accommodation.details!.type]}
          </ProgramContentText>
        </Grid>

        <Grid item>
          <ProgramContentText>
            Accommodation Address: {accommodation.details?.address.lineOne}{" "}
            {accommodation.details?.address.lineTwo}{" "}
            {accommodation.details?.address.postalCode}
          </ProgramContentText>
        </Grid>

        <Grid item>
          <ProgramContentText>
            Accommodation Local Address:{" "}
            {accommodation.details?.localAddress.lineOne}{" "}
            {accommodation.details?.localAddress.lineTwo}
          </ProgramContentText>
        </Grid>

        <Grid item>
          <ProgramContentText>
            Accommodation Room Types:{" "}
            {program.accommodation.details?.roomTypes
              .map((rt) => EProgramAccommodationRoomTypeLabels[rt])
              .join(", ")}
          </ProgramContentText>
        </Grid>

        <Grid item>
          <ProgramContentText>
            Airport Transfer:{" "}
            {
              EProgramAirportTransferLabels[
                program.accommodation.details!.transfer
              ]
            }
          </ProgramContentText>
        </Grid>
      </Grid>

      <ProgramImageTiler images={program.accommodation.details?.images} />
    </Grid>
  );
};

type Props = {
  program: Program;
};

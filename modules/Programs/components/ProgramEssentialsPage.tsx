import React from "react";

import { NextPage, NextPageContext } from "next";

import { useRouter } from "next/router";
import { getUserProfileOrRedirect } from "../../../services/Auth.functions";
import { ProgramDashboard } from "./ProgramDashboard";
import { ProgramsServiceForBusiness } from "../services/ProgramsServiceForBusiness";
import StepperWithTitle from "../../Shared/components/Stepper/StepperWithTitle";
import { Program } from "../types/Programs.types";
import { getProgramSteps } from "../services/ProgramStepsService";
import { EProgramSteps } from "../enums/EProgramSteps";

enum EProgramEssentialsSteps {
  CURRICULUM = 0,
  STUDENT_TESTIMONIAL = 1,
  ADDITIONAL_HIGHLIGHTS = 2,
  GALLERY = 3,
}

const steps = getProgramSteps([
  EProgramSteps.CURRICULUM,
  EProgramSteps.STUDENT_TESTIMONIAL,
  EProgramSteps.ADDITIONAL_HIGHLIGHTS,
  EProgramSteps.GALLERY,
]);

const ProgramEssentialsPage: NextPage<Props> = (props) => {
  const router = useRouter();

  const [activeStep, setActiveStep] = React.useState<number>(0);
  const [program, setProgram] = React.useState(props.program);

  const FormComponent = steps[activeStep].component;

  const handleStepClick = (step: number) => setActiveStep(step);
  const afterSubmit = (newProgram: Program) => {
    setProgram(newProgram);
    if (activeStep === EProgramEssentialsSteps.GALLERY) {
      router.push("/biz/program/[id]/edit", `/biz/program/${program.id}/edit`);
    } else {
      setActiveStep(activeStep + 1);
    }
  };

  return (
    <ProgramDashboard
      user={props.user}
      program={program}
      crumbs={[
        { title: "Dashboard", link: "/biz" },
        { title: "Programs", link: "/biz/programs" },
        {
          title: "Edit Program",
          link: `/biz/program/${program.id}/edit`,
        },
        { title: "Essentials" },
      ]}
      cancelRoute={[
        "/biz/program/[id]/edit",
        `/biz/program/${program.id}/edit`,
      ]}
    >
      <StepperWithTitle
        activeStep={activeStep}
        steps={steps}
        onStepClick={handleStepClick}
        argumentForIsStepComplete={program}
      ></StepperWithTitle>
      <FormComponent program={program} afterSubmit={afterSubmit} />
    </ProgramDashboard>
  );
};

const getInitialProps = async (ctx: NextPageContext) => {
  const user = await getUserProfileOrRedirect(ctx);
  const program = await ProgramsServiceForBusiness.getProgramFromId(
    ctx.query.id as string,
    ctx
  );

  return { user, program };
};

type Props = PromiseResult<ReturnType<typeof getInitialProps>>;

ProgramEssentialsPage.getInitialProps = getInitialProps;

export default ProgramEssentialsPage;

import React from "react";

import { Grid } from "@material-ui/core";
import { map } from "lodash";

import { TypographyWithPxFontAndColor } from "../../Shared/components/TypographyWithPxFontAndColor";
import { Program } from "../types/Programs.types";

export const ProgramPriceRangeWithRefundPolicy: React.FC<Props> = ({
  program,
}) => {
  const currency = program.currency;
  const minPrice = Math.min(
    ...map(program.tickets, (t) => {
      const price = Number(t.price);
      return isNaN(price) ? Number.POSITIVE_INFINITY : price;
    })
  );

  return (
    <Grid
      item
      container
      spacing={2}
      justify="space-between"
      alignItems="center"
    >
      <Grid item xs={2}>
        <TypographyWithPxFontAndColor sizeInPx={14} color="#999999">
          Price From
        </TypographyWithPxFontAndColor>
      </Grid>

      <Grid item xs={6}>
        <TypographyWithPxFontAndColor sizeInPx={32} color="#FF6F00">
          {`${currency} ${minPrice}`}
        </TypographyWithPxFontAndColor>
      </Grid>

      <Grid item xs={4}>
        <TypographyWithPxFontAndColor sizeInPx={12} color="#999999" rightAlign>
          Application & Refund Policy
        </TypographyWithPxFontAndColor>
      </Grid>
    </Grid>
  );
};

type Props = {
  program: Program;
};

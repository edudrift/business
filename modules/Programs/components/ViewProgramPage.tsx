import * as React from "react";

import { Divider, Grid, Typography } from "@material-ui/core";
import { isEmpty, map } from "lodash";
import { NextPage } from "next";

import ButtonLink from "../../../components/ButtonLink";
import GenericLayout from "../../../components/GenericLayout/GenericLayout";
import { PaddedPaper } from "../../Shared/components/PaddedPaper";
import {
  EProgramTypeLabels,
  Program,
} from "../../../modules/Programs/types/Programs.types";
import User from "../../../types/user.types";
import { getCountryFromCode } from "../../Countries/countries";
import { formatDateWithFullDate } from "../../../utils/dateUtils";
import { TypographyWithPxFontAndColor } from "../../Shared/components/TypographyWithPxFontAndColor";
import { EProgramFieldsOfInterestLabels } from "../enums/EProgramFieldsOfInterest";
import { OrangeChip } from "./OrangeChip";
import { ProgramBreadcrumbs } from "./ProgramBreadcrumbs";
import { ProgramContentTabs } from "./ProgramContentTabs";
import { ProgramDescriptionLine } from "./ProgramDescriptionLine";
import { ProgramDiscounts } from "./ProgramDiscounts";
import { ProgramGallery } from "./ProgramGallery";
import { ProgramPriceRangeWithRefundPolicy } from "./ProgramPriceRangeWithRefundPolicy";
import { ProgramTicketsPurchaseView } from "./ProgramTicketsPurchaseView";
import { EProgramEducationalBackgroundLabels } from "../enums/EProgramEducationalBackground";

const ViewProgramPage: NextPage<Props> = ({ program, ...props }) => {
  React.useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <GenericLayout user={props.user} whiteBackground>
      <Grid container spacing={2} justify="space-between" alignItems="center">
        <Grid item>
          <ProgramBreadcrumbs program={program} />
        </Grid>

        {props.canEdit && (
          <Grid item>
            <ButtonLink
              variant="outlined"
              color="primary"
              href="/biz/program/[id]/edit"
              as={`/biz/program/${program.id}/edit`}
            >
              Edit
            </ButtonLink>
          </Grid>
        )}
      </Grid>

      <Grid container spacing={4}>
        <Grid item container spacing={3}>
          <Grid item md={5}>
            <ProgramGallery program={program} />
          </Grid>

          <Grid item md={7} container spacing={2}>
            <Grid item>
              <Typography variant="h5" component="h1">
                {program.name}
              </Typography>
            </Grid>

            <Grid item container spacing={2}>
              <Grid item>
                <Typography style={{ color: "#666666" }}>
                  {formatDateWithFullDate(program.startDate)} -{" "}
                  {formatDateWithFullDate(program.endDate)}
                </Typography>
              </Grid>
              <Grid item>
                <Divider orientation="vertical" />
              </Grid>
              <Grid item>
                <Typography style={{ color: "#666666" }}>
                  Application deadline{" "}
                  {formatDateWithFullDate(program.applicationDeadline)}
                </Typography>
              </Grid>
            </Grid>

            <Grid item>
              <TypographyWithPxFontAndColor sizeInPx={14} color="#333">
                {program.venue.name}
              </TypographyWithPxFontAndColor>
            </Grid>

            <Grid item container spacing={2}>
              <Grid item>
                <OrangeChip
                  label={getCountryFromCode(program.venue.countryCode)}
                />
              </Grid>

              <Grid item>
                <OrangeChip label={EProgramTypeLabels[program.type]} />
              </Grid>

              {map(program.education, (education, index) => {
                return (
                  <Grid item key={index}>
                    <OrangeChip
                      label={EProgramEducationalBackgroundLabels[education]}
                    />
                  </Grid>
                );
              })}

              {program.fields.map((field, index) => {
                return (
                  <Grid item key={index}>
                    <OrangeChip label={EProgramFieldsOfInterestLabels[field]} />
                  </Grid>
                );
              })}
            </Grid>

            <Grid item xs={12}>
              <PaddedPaper fullWidth style={{ backgroundColor: "#F5F5F5" }}>
                <Grid container spacing={2} direction="column">
                  <ProgramDescriptionLine>
                    {program.briefDescription}
                  </ProgramDescriptionLine>

                  {program.highlights &&
                    program.highlights
                      .filter((highlight) => !isEmpty(highlight.title))
                      .map((highlight, index) => {
                        return (
                          <ProgramDescriptionLine key={index}>
                            {highlight.title}
                          </ProgramDescriptionLine>
                        );
                      })}
                </Grid>
              </PaddedPaper>
            </Grid>

            <Grid item xs={12}>
              <PaddedPaper fullWidth style={{ backgroundColor: "#F5F5F5" }}>
                <Grid container spacing={2} direction="column">
                  <ProgramPriceRangeWithRefundPolicy program={program} />

                  <ProgramDiscounts program={program} />
                </Grid>
              </PaddedPaper>
            </Grid>

            <Grid item xs={12}>
              <ProgramTicketsPurchaseView program={program} />
            </Grid>
          </Grid>
        </Grid>

        <ProgramContentTabs program={program} />
      </Grid>
    </GenericLayout>
  );
};

type Props = {
  user?: User;
  program: Program;
  canEdit: boolean;
};

export default ViewProgramPage;

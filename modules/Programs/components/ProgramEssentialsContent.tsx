import React from "react";

import { Grid } from "@material-ui/core";

import { Program } from "../types/Programs.types";
import {
  ProgramContentBoldText,
  ProgramContentText,
} from "./ProgramContentText";
import { ProgramImageTiler } from "./ProgramImageTiler";

export const ProgramEssentialsContent: React.FC<Props> = ({ program }) => {
  return (
    <Grid item container spacing={4}>
      <Grid item container spacing={1} direction="column">
        <Grid item>
          <ProgramContentBoldText>Academic</ProgramContentBoldText>
        </Grid>

        <Grid item container direction="column">
          <Grid item>
            <ProgramContentText>
              Lecture Description:{" "}
              {program.essentials.academic.lectureDescription}
            </ProgramContentText>
          </Grid>

          <Grid item>
            <ProgramContentText>
              Teaching Staff Description:{" "}
              {program.essentials.academic.teachingStaffDescription}
            </ProgramContentText>
          </Grid>

          <Grid item>
            <ProgramContentText>
              Certification Description:{" "}
              {program.essentials.academic.certificationDescription}
            </ProgramContentText>
          </Grid>
        </Grid>

        <ProgramImageTiler images={program.essentials?.academic.images} />
      </Grid>

      {program.essentials.travel.includesTourPackage && (
        <Grid item container spacing={1} direction="column">
          <Grid item>
            <ProgramContentBoldText>Travel</ProgramContentBoldText>
          </Grid>

          <Grid item container direction="column">
            <Grid item>
              <ProgramContentText>
                Tour Provider:{" "}
                {program.essentials.travel.fromOrganisingTeam
                  ? program.provider.name
                  : program.essentials.travel.externalTourVendor?.name}
              </ProgramContentText>
            </Grid>

            <Grid item>
              <ProgramContentText>
                Description of tour: {program.essentials.travel.tourDescription}
              </ProgramContentText>
            </Grid>
          </Grid>

          <ProgramImageTiler images={program.essentials?.travel.images} />
        </Grid>
      )}

      <Grid item container spacing={1} direction="column">
        <Grid item>
          <ProgramContentBoldText>Dining</ProgramContentBoldText>
        </Grid>

        <Grid item container direction="column">
          <Grid item>
            <ProgramContentText>
              Dining Coverage: {program.essentials.dining.providedMeals}
            </ProgramContentText>
          </Grid>

          <Grid item>
            <ProgramContentText>
              Dining Description: {program.essentials.dining.mealsDetails}
            </ProgramContentText>
          </Grid>
        </Grid>

        <ProgramImageTiler images={program.essentials?.dining.images} />
      </Grid>
    </Grid>
  );
};

type Props = {
  program: Program;
};

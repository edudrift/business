import React from "react";

import { Grid } from "@material-ui/core";
import StarIcon from "@material-ui/icons/Star";

import { TypographyWithPxFontAndColor } from "../../Shared/components/TypographyWithPxFontAndColor";

export const ProgramDescriptionLine: React.FC = ({ children }) => {
  return (
    <Grid item container alignItems="center" spacing={1} direction="row">
      <Grid
        item
        xs
        container
        alignItems="center"
        direction="row-reverse"
        style={{ flex: 0 }}
      >
        <StarIcon
          style={{
            width: "16px",
            height: "16px",
            color: "#FFA940",
          }}
        />
      </Grid>

      <Grid item xs={11}>
        <TypographyWithPxFontAndColor sizeInPx={14} color="#333" multiline>
          {children}
        </TypographyWithPxFontAndColor>
      </Grid>
    </Grid>
  );
};

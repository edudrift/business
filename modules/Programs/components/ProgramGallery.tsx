import React from "react";

import { Grid } from "@material-ui/core";
import ShareIcon from "@material-ui/icons/Share";
import StarBorderIcon from "@material-ui/icons/StarBorder";
import { isNull, isUndefined } from "lodash";

import Carousel from "../../Shared/components/Carousel";
import { TypographyWithPxFontAndColor } from "../../Shared/components/TypographyWithPxFontAndColor";
import { Program } from "../types/Programs.types";

const createGalleryImages = (program: Program) => {
  // const { venue, images } = program;
  const { images } = program;

  const programHighlights = isNull(program.highlights)
    ? []
    : program.highlights;

  const highlightFirstImages: string[] = [];
  for (const highlight of programHighlights) {
    const { images: highlightImages = [] } = highlight;
    const [firstImage] = highlightImages;

    if (!isNull(firstImage) && !isUndefined(firstImage)) {
      highlightFirstImages.push(firstImage);
    }
  }

  // return [venue.images[0], ...images, ...highlightFirstImages];
  return [...images, ...highlightFirstImages];
};

export const ProgramGallery: React.FC<Props> = ({ program }) => {
  const images = createGalleryImages(program);

  return (
    <Grid container spacing={2}>
      <Grid item xs>
        <Carousel>
          {images.map((image, index) => {
            return <img src={image} key={index} width="100%" height="100%" />;
          })}
        </Carousel>
      </Grid>

      <Grid item container spacing={2}>
        {images.map((image, index) => (
          <Grid item key={index} md={3} xs={12}>
            <img src={image} width="100%" height="100%" />
          </Grid>
        ))}
      </Grid>

      <Grid item container>
        <Grid
          item
          xs
          md={3}
          container
          spacing={1}
          alignItems="center"
          style={{ marginRight: "8px" }}
        >
          <StarBorderIcon
            style={{ color: "#FFA510", height: "1.25rem", width: "1.25rem" }}
          />

          <Grid item>
            <TypographyWithPxFontAndColor sizeInPx={14} color="#000" bold>
              COLLECTION
            </TypographyWithPxFontAndColor>
          </Grid>
        </Grid>

        <Grid item xs md={3} container spacing={1} alignItems="center">
          <ShareIcon
            style={{ color: "#FFA510", height: "1.25rem", width: "1.25rem" }}
          />

          <Grid item>
            <TypographyWithPxFontAndColor sizeInPx={14} color="#000" bold>
              SHARE
            </TypographyWithPxFontAndColor>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};

type Props = {
  program: Program;
};

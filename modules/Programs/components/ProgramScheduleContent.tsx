import React from "react";

import { Grid } from "@material-ui/core";

import { parseISOString } from "../../../utils/dateUtils";
import { Program } from "../types/Programs.types";
import { buildFunction } from "./forms/ProgramScheduleForm";
import { ProgramScheduleTabPanel } from "./ProgramScheduleTabPanel";
import { ProgramScheduleTab } from "./ProgramScheduleTab";

export const ProgramScheduleContent: React.FC<Props> = ({ program }) => {
  const startDate = parseISOString((program.startDate as any) as string);
  const endDate = parseISOString((program.endDate as any) as string);

  const schedule = buildFunction(program.type)(
    program.schedule,
    startDate,
    endDate
  );

  const [value, setValue] = React.useState(0);
  return (
    <Grid item container spacing={4}>
      <Grid item container direction="row">
        <Grid item xs={3} md={2}>
          <ProgramScheduleTab
            value={value}
            setValue={setValue}
            schedule={schedule}
          />
        </Grid>
        <Grid item xs={9} md={6}>
          <ProgramScheduleTabPanel value={value} schedule={schedule} />
        </Grid>
      </Grid>
    </Grid>
  );
};

type Props = {
  program: Program;
};

import React, { ReactChild } from "react";

import { Grid } from "@material-ui/core";

import { ProgramContentText } from "./ProgramContentText";
import { ProgramDaySchedule, DayDetails } from "../types/ProgramSchedule.types";
import { PaddedPaper } from "../../Shared/components/PaddedPaper";

const GreyPaddedPaper = ({ children }: { children: ReactChild }) => (
  <PaddedPaper style={{ backgroundColor: "#f5f5f5" }}>{children}</PaddedPaper>
);

export const ProgramDayScheduleContent: React.FC<Props> = ({ day }) => (
  <Grid container spacing={2} direction="column">
    {[DayDetails.MORNING, DayDetails.AFTERNOON, DayDetails.EVENING].map(
      (detailType) => {
        return (
          <Grid item key={detailType}>
            <Grid item container direction="column" spacing={2}>
              <Grid item>
                <ProgramContentText>
                  {(() => {
                    switch (detailType) {
                      case DayDetails.MORNING:
                        return "Morning (0900H - 1200H)";
                      case DayDetails.AFTERNOON:
                        return "Afternoon (1400H - 1700H)";
                      case DayDetails.EVENING:
                        return "Evening (1800H onwards)";
                    }
                  })()}
                </ProgramContentText>
              </Grid>
              <Grid item>
                <GreyPaddedPaper>
                  <React.Fragment>
                    {Object.values(
                      day.details[detailType].map((detail, index) => (
                        <ProgramContentText key={index}>
                          {detail}
                        </ProgramContentText>
                      ))
                    )}
                  </React.Fragment>
                </GreyPaddedPaper>
              </Grid>
            </Grid>
          </Grid>
        );
      }
    )}
  </Grid>
);

type Props = {
  day: ProgramDaySchedule;
};

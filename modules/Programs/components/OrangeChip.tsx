import React from "react";

import { Chip, makeStyles } from "@material-ui/core";
import clsx from "clsx";

const useStyles = makeStyles(() => ({
  root: {
    borderRadius: "2px",
    backgroundColor: "rgba(255,169,64,.1)",
  },
  bordered: {
    border: "1px solid rgba(255,169,64,1)",
  },
  fullWidth: {
    width: "100%",
  },
  label: {
    color: "#FFA940",
  },
}));

export const OrangeChip: React.FC<Props> = ({ label, bordered, fullWidth }) => {
  const classes = useStyles();

  return (
    <Chip
      label={label}
      classes={{
        root: clsx(classes.root, {
          [classes.bordered]: bordered,
          [classes.fullWidth]: fullWidth,
        }),
        label: classes.label,
      }}
    />
  );
};

type Props = {
  label: string;
  bordered?: boolean;
  fullWidth?: boolean;
};

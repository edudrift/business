import React from "react";

import {
  Button,
  Card,
  Checkbox,
  Divider,
  FormControl,
  FormControlLabel,
  Grid,
  Input,
  InputLabel,
  makeStyles,
  MenuItem,
  Select,
  Typography,
  useTheme,
} from "@material-ui/core";
import { Field, Form, Formik } from "formik";
import { isNull } from "lodash";
import {
  DatePicker,
  Select as FormikSelect,
} from "material-ui-formik-components";

import { FormikTextField } from "../../../../components/Forms/components/FormikTexField";
import { FormSubheaderWithSubtitle } from "../../../Shared/components/FormSubheaderWithSubtitle";
// import { MultiImageUploader } from "../../../Shared/components/MultiImageUploader";
import { ProgramsServiceForBusiness } from "../../services/ProgramsServiceForBusiness";
import {
  ProgramDetailsSchema,
  ProgramDetailsSchemaAsType,
} from "../../types/ProgramDetails.types";
import {
  EProgramType,
  EProgramTypeLabels,
  Program,
} from "../../types/Programs.types";
import {
  EProgramParticipantProfile,
  EProgramParticipantProfileLabels,
} from "../../enums/EProgramParticipantProfile";
import {
  FormikContactNumber,
  FormikContactNumberDefaultValues,
} from "~/components/Forms/components/FormikContactNumber";
import { ContactNumberService } from "~/modules/Shared/services/ContactNumberService";

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
    maxWidth: 700,
    width: "100%",
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  subtitle: {
    fontSize: theme.typography.pxToRem(12),
    color: "#666666",
  },
}));

export const ProgramGeneralInformationForm: React.FC<Props> = (props) => {
  const classes = useStyles();
  const theme = useTheme();

  const { program } = props;
  // const [venueImages, setVenueImages] = React.useState(
  //   props.program.venue?.images || []
  // );

  // const [images, setImages] = React.useState(props.program.images || []);

  const [participantProfile, setEParticipantProfile] = React.useState(
    program.participantProfile || []
  );
  const [state, setState] = React.useState({
    isOnlineProgram: false,
    isUsingBusinessaddress: false,
    isForeignLanguageSupported: true,
  });

  const handleChange = (event: { target: { value: any } }) => {
    setEParticipantProfile(event.target.value);
  };

  const handleCheckbox = (event: { target: { name: any; checked: any } }) => {
    setState({ ...state, [event.target.name]: event.target.checked });
  };

  const buildInitialValues = () => {
    const { program } = props;
    const initialValues: ProgramDetailsSchemaAsType = {
      name: program.name,
      type: EProgramType.LOCAL_ENRICHMENT_CLASSES,
      startDate: program.startDate,
      endDate: program.endDate,
      contact: {
        programManagerName: "",
        number: FormikContactNumberDefaultValues,
        email: "",
      },
      // applicationDeadline: null,
      // description: "",
      // briefDescription: "",
      // website: null,
      // venue: ProgramVenueDefaults,
    };

    if (program.type) initialValues.type = program.type;

    if (program.contact) {
      const { ...contact } = program.contact;

      const number = ContactNumberService.getAsObject(contact.number);

      initialValues.contact = {
        ...contact,
        number: number,
      };
    }

    // if (program.applicationDeadline) initialValues.applicationDeadline = program.applicationDeadline;
    // if (program.description) initialValues.description = program.description;
    // if (program.briefDescription) initialValues.briefDescription = program.briefDescription;
    // if (program.website) initialValues.website = program.website;

    // if (program.venue) {
    //   // convert persisted country to object
    //   const { countryCode, ...venue } = program.venue;

    //   initialValues.venue = {
    //     ...venue,
    //     country: getCountryObjectFromCountryCode(countryCode),
    //   };
    // }

    // if (program.onlineProgram) initialValues.onlineProgram = program.onlineProgram;

    return initialValues;
  };

  const handleSubmit = async (programDetails: ProgramDetailsSchemaAsType) => {
    // const { country, ...venueWithoutCountry } = programDetails.venue;
    // if (venueImages.length !== 3) {
    //   alert("Please upload all three venue images");
    //   return;
    // }
    // if (images.length !== 3) {
    //   alert("Please upload all three program intro images ");
    //   return;
    // }
    // const programVenue: ProgramVenue = {
    //   ...venueWithoutCountry,
    //   countryCode: programDetails.venue.country.code,
    //   // images: venueImages,
    // };

    const programDetailsPayload = {
      ...programDetails,
      contact: {
        programManagerName: programDetails.contact.programManagerName,
        number: ContactNumberService.getAsStringFromContactType(
          programDetails.contact.number
        ),
        email: programDetails.contact.email,
      },
    };
    console.log(programDetails);
    console.log(participantProfile);

    const newProgram = await ProgramsServiceForBusiness.saveDetails(
      props.program.id,
      {
        ...programDetailsPayload,
        participantProfile,
      }
    );
    props.afterSubmit(newProgram);
  };

  const ITEM_HEIGHT = 48;
  const ITEM_PADDING_TOP = 8;
  const MenuProps = {
    PaperProps: {
      style: {
        maxHeight: ITEM_HEIGHT * 8 + ITEM_PADDING_TOP,
        width: 250,
      },
    },
  };

  function getStyles(
    label: string,
    education: string[],
    theme: { typography: { fontWeightRegular: any; fontWeightMedium: any } }
  ) {
    return {
      fontWeight:
        education.indexOf(label) === -1
          ? theme.typography.fontWeightRegular
          : theme.typography.fontWeightMedium,
    };
  }

  return (
    <Formik
      initialValues={buildInitialValues()}
      validationSchema={ProgramDetailsSchema}
      onSubmit={handleSubmit}
      validateOnChange
    >
      {({ dirty, values }) => {
        return (
          <Form>
            <Grid item container direction="column">
              <Card style={{ display: "flex" }}>
                <Grid
                  item
                  xs={8}
                  md={8}
                  container
                  spacing={2}
                  direction="column"
                  style={{ margin: "8px" }}
                >
                  <Grid item>
                    <FormSubheaderWithSubtitle title="Program Information" />
                  </Grid>
                  <Grid item>
                    <FormikTextField
                      name="name"
                      label="Program name"
                      placeholder="E.g. Beats & Babies"
                      required
                    />
                  </Grid>
                  <Grid item container spacing={2}>
                    <Grid item xs={6} md={6} style={{ paddingTop: 0 }}>
                      <Field
                        name="type"
                        component={FormikSelect}
                        options={Object.values(EProgramType).map((type) => ({
                          value: type,
                          label: EProgramTypeLabels[type],
                        }))}
                        label="Program type"
                        variant="outlined"
                        fullWidth
                      />
                    </Grid>
                    <Grid item xs={6} md={6} style={{ paddingTop: 0 }}>
                      <FormControl
                        className={classes.formControl}
                        style={{ margin: "11px 0 ", padding: 2 }}
                      >
                        <InputLabel id="mutiple-name-label">
                          Participant profile
                        </InputLabel>
                        <Select
                          labelId="mutiple-name-label"
                          id="mutiple-name"
                          multiple
                          value={participantProfile}
                          onChange={handleChange}
                          input={<Input />}
                          MenuProps={MenuProps}
                        >
                          {Object.values(EProgramParticipantProfile).map(
                            (val) => (
                              <MenuItem
                                key={val}
                                value={val}
                                style={getStyles(
                                  val,
                                  participantProfile,
                                  theme
                                )}
                              >
                                {EProgramParticipantProfileLabels[val]}
                              </MenuItem>
                            )
                          )}
                        </Select>
                      </FormControl>
                    </Grid>
                  </Grid>
                  <Grid item container spacing={2}>
                    <Grid item xs>
                      <Field
                        required
                        name="startDate"
                        component={DatePicker}
                        label="Start date"
                        format="d MMMM yyyy"
                        inputVariant="outlined"
                        style={{ margin: 0 }}
                        disablePast
                      />
                    </Grid>

                    <Grid item xs>
                      <Field
                        required
                        name="endDate"
                        component={DatePicker}
                        label="End date"
                        format="d MMMM yyyy"
                        inputVariant="outlined"
                        style={{ margin: 0 }}
                        disablePast
                        minDate={values.startDate}
                        disabled={isNull(values.startDate)}
                      />
                    </Grid>
                  </Grid>
                </Grid>
                <Divider
                  orientation="vertical"
                  flexItem
                  style={{ margin: "4em 0 1em" }}
                />
                <Grid
                  item
                  xs={4}
                  md={4}
                  container
                  spacing={2}
                  direction="column"
                  style={{ margin: "8px" }}
                >
                  <Grid item>
                    <FormSubheaderWithSubtitle title="Contact Information" />
                  </Grid>
                  <Grid item>
                    <FormikTextField
                      name="contact.programManagerName"
                      label="Program manager name"
                      placeholder="Name of Program Manager"
                    />
                  </Grid>
                  <Grid item>
                    <FormikContactNumber
                      namespace={`contact.number`}
                      label="Contact Number"
                    />
                  </Grid>
                  <Grid item>
                    <FormikTextField
                      name="contact.email"
                      label="Email"
                      placeholder="Email Address"
                    />
                  </Grid>
                </Grid>
              </Card>

              <Card style={{ display: "flex", marginTop: "16px" }}>
                <Grid
                  item
                  xs={12}
                  md={12}
                  container
                  spacing={2}
                  direction="column"
                  style={{ margin: "8px" }}
                >
                  <Grid item>
                    <FormSubheaderWithSubtitle title="Location" />
                  </Grid>
                  <Grid item container spacing={2}>
                    <Grid item style={{ paddingTop: 0 }}>
                      <FormControlLabel
                        control={
                          <Checkbox
                            checked={state.isOnlineProgram}
                            onChange={handleCheckbox}
                            name="isOnlineProgram"
                            color="primary"
                          />
                        }
                        label="This is an online program"
                      />
                    </Grid>
                  </Grid>
                </Grid>
              </Card>
              <Divider></Divider>
              <Card style={{ display: "flex" }}>
                <Grid
                  item
                  xs={12}
                  md={12}
                  container
                  spacing={2}
                  direction="column"
                  style={{ margin: "8px" }}
                >
                  <Grid item container spacing={2}>
                    <Grid item style={{ paddingTop: 0 }}>
                      <FormControlLabel
                        control={
                          <Checkbox
                            checked={state.isUsingBusinessaddress}
                            onChange={handleCheckbox}
                            name="isUsingBusinessaddress"
                            color="primary"
                          />
                        }
                        label="Same address as the registered business address"
                      />
                    </Grid>
                  </Grid>
                </Grid>
              </Card>

              <Card style={{ display: "flex", marginTop: "16px" }}>
                <Grid
                  item
                  xs={12}
                  md={12}
                  container
                  spacing={2}
                  direction="column"
                  style={{ margin: "8px" }}
                >
                  <Grid item>
                    <FormSubheaderWithSubtitle title="Language" />
                  </Grid>
                  <Grid item container spacing={2}>
                    <Grid item style={{ paddingTop: 0 }}>
                      <FormControlLabel
                        control={
                          <Checkbox
                            checked={!state.isForeignLanguageSupported}
                            onChange={handleCheckbox}
                            name="isForeignLanguageSupported"
                            color="primary"
                          />
                        }
                        label="We do not have a second/foreign language staff present to assist with understanding and/translation of instructions"
                      />
                    </Grid>
                  </Grid>
                </Grid>
              </Card>
              <Divider></Divider>
              <Card style={{ display: "flex" }}>
                <Grid
                  item
                  xs={12}
                  md={12}
                  container
                  spacing={2}
                  direction="column"
                  style={{ margin: "8px" }}
                >
                  <Grid item container spacing={2}>
                    <Grid item style={{ paddingTop: 0 }}>
                      <Typography
                        variant="body2"
                        component="p"
                        className={classes.subtitle}
                      >
                        Administrative assistance and generral enquiries
                      </Typography>
                      <Typography
                        variant="body2"
                        component="p"
                        className={classes.subtitle}
                      >
                        On-site teaching assistant or chapeerone
                      </Typography>
                    </Grid>
                  </Grid>
                </Grid>
              </Card>
              {/* <Grid
                item
                container
                spacing={2}
                direction="column"
                xs={12}
                md={6}
              >
                <Grid item>
                  <FormSubheaderWithSubtitle title="Venue" />
                </Grid>

                <Grid item container spacing={2} direction="column">
                  <Grid item>
                    <FormikTextField
                      name="venue.name"
                      label="Venue name"
                      required
                    />
                  </Grid>

                  <Grid item>
                    <FormikMultilineTextField
                      rows={2}
                      name="venue.address"
                      label="Venue address"
                      required
                    />
                  </Grid>

                  <Grid item container spacing={2}>
                    <Grid item xs>
                      <CountryField
                        values={values}
                        setValues={setValues}
                        namespace="venue"
                      />
                    </Grid>
                    <Grid item xs>
                      <FormikTextField
                        name="venue.city"
                        label="City"
                        required
                      />
                    </Grid>
                  </Grid>

                  <Grid item>
                    <FormikMultilineTextField
                      rows={4}
                      name="venue.description"
                      label="Venue description"
                      required
                    />
                  </Grid>
                  <MultiImageUploader
                    count={3}
                    initialImages={venueImages}
                    onChange={(newImages) => setVenueImages(newImages)}
                  /> 
                </Grid>
              </Grid> */}
              <Grid
                item
                container
                spacing={2}
                direction="column"
                xs={12}
                md={6}
                style={{ marginTop: "8px" }}
              >
                <Grid item>
                  {dirty || true ? (
                    <Button type="submit" variant="contained" color="primary">
                      Next step
                    </Button>
                  ) : (
                    <Button
                      variant="contained"
                      color="primary"
                      onClick={() => props.afterSubmit(props.program)}
                    >
                      Next step
                    </Button>
                  )}
                </Grid>
              </Grid>
            </Grid>
          </Form>
        );
      }}
    </Formik>
  );
};

type Props = {
  program: Program;
  afterSubmit: (program: Program) => void;
};

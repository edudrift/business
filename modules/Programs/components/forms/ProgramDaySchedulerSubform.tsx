import React from "react";
import { range } from "lodash";
import {
  ProgramDaySchedule,
  DayDetails,
} from "../../types/ProgramSchedule.types";
import {
  Grid,
  makeStyles,
  Stepper,
  Step,
  StepLabel,
  StepContent,
  Typography,
} from "@material-ui/core";
import { TextField } from "material-ui-formik-components";
import { Field } from "formik";
import { formatDateWithFullDate } from "../../../../utils/dateUtils";

const useStyles = makeStyles(() => ({
  stepIcon: {
    marginLeft: "8px",
    width: "8px",
    height: "8px",
    background: "rgba(38, 107, 240, 1)",
    borderRadius: "50%",
    alignItems: "center",
  },
}));

const renderLabel = (detailType: DayDetails, index: number) => {
  const detail = `${detailType.charAt(0).toUpperCase()}${detailType.slice(1)}`;
  return `${detail} Schedule Item ${index + 1}`;
};

export const ProgramDaySchedulerSubform: React.FC<Props> = ({
  selected,
  day,
}) => {
  const classes = useStyles();
  return (
    <Grid item container direction="row">
      <Grid item container spacing={2} direction="column">
        <Grid item>
          <Typography style={{ fontSize: "16px", fontWeight: 500 }}>
            {`${formatDateWithFullDate(day.date)} - Day ${day.dayNumber}`}
          </Typography>
        </Grid>
        <Stepper orientation="vertical">
          {[DayDetails.MORNING, DayDetails.AFTERNOON, DayDetails.EVENING].map(
            (detailType) => {
              return (
                <Step active key={detailType}>
                  <StepLabel
                    key={detailType}
                    StepIconComponent={() => (
                      <span className={classes.stepIcon} />
                    )}
                  >
                    <Typography style={{ fontSize: "14px", fontWeight: 400 }}>
                      {(() => {
                        switch (detailType) {
                          case DayDetails.MORNING:
                            return "Morning (0900H -1200H)";
                          case DayDetails.AFTERNOON:
                            return "Afternoon (1400H-1700H)";
                          case DayDetails.EVENING:
                            return "Evening (1800H onwards)";
                        }
                      })()}
                    </Typography>
                  </StepLabel>
                  <StepContent>
                    <Grid item container direction="column">
                      {range(3).map((index) => {
                        return (
                          <Grid
                            item
                            key={detailType + index}
                            container
                            direction="row"
                          >
                            <Field
                              required={index === 0}
                              label={renderLabel(detailType, index)}
                              key={detailType + index}
                              style={{ width: "392px" }}
                              size="small"
                              type="text"
                              variant="outlined"
                              component={TextField}
                              InputLabelProps={{
                                shrink: true,
                              }}
                              name={`${selected}.details.${detailType}.${index}`}
                            />
                          </Grid>
                        );
                      })}
                    </Grid>
                  </StepContent>
                </Step>
              );
            }
          )}
        </Stepper>
      </Grid>
    </Grid>
  );
};

type Props = {
  selected: number;
  day: ProgramDaySchedule;
};

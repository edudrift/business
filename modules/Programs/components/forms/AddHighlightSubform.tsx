import {
  Card,
  CardContent,
  FormControl,
  Grid,
  InputLabel,
  makeStyles,
  Select,
} from "@material-ui/core";
import React from "react";
import { FormikTextField } from "../../../../components/Forms/components/FormikTexField";
import { EProgramHighlightTypesLabels } from "../../enums/EProgramHighlightTypes";
import { ProgramHighlight } from "../../types/ProgramHighlight.types";

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

export const AddHighlightSubform: React.FC<Props> = ({
  namespace,
  highlight,
  setHighlight,
}) => {
  const classes = useStyles();

  // const canUploadImage = highlight.title && highlight.description;

  const handleChange = (event: any) => {
    setHighlight({
      ...highlight,
      type: event.target.value,
    });
  };

  return (
    <Card variant="outlined">
      <CardContent>
        <Grid container spacing={2} direction="column">
          <FormControl className={classes.formControl}>
            <InputLabel shrink htmlFor="type-native-label-placeholder">
              Type of Highlight
            </InputLabel>

            <Select
              native
              value={highlight.type}
              inputProps={{
                name: "type",
                id: "type-native-label-placeholder",
              }}
              onChange={handleChange}
            >
              <option value="">Select</option>
              <option value={EProgramHighlightTypesLabels.FINAL_SHOW}>
                Final show
              </option>
              <option value={EProgramHighlightTypesLabels.FIELD_TRIP}>
                Field trip
              </option>
              <option value={EProgramHighlightTypesLabels.COMPETITION}>
                Competition
              </option>
              <option value={EProgramHighlightTypesLabels.GUEST_SPEAKER}>
                Guest speaker
              </option>
            </Select>
          </FormControl>

          <Grid item>
            <FormikTextField
              name={`${namespace}.description`}
              label="Description"
              placeholder="A one-liner description of the highlight"
            />
          </Grid>

          {/* {canUploadImage ? (
            <Grid item container spacing={2}>
              <MultiImageUploader
                count={3}
                initialImages={highlight.images}
                onChange={(newImages: string[]) => {
                  setHighlight({
                    ...highlight,
                    images: newImages,
                  });
                }}
              />
            </Grid>
          ) : (
            <Grid item>
              <TypographyWithPxFontAndColor sizeInPx={14} color="#333">
                Images can only be uploaded once there is a title and
                description
              </TypographyWithPxFontAndColor>
            </Grid>
          )} */}
        </Grid>
      </CardContent>
    </Card>
  );
};

type Props = {
  programId: string;
  highlight: ProgramHighlight;
  namespace: string;
  setHighlight: (highlight: any) => void;
};

import React from "react";

import { Grid } from "@material-ui/core";
import { Field } from "formik";
import { DatePicker } from "material-ui-formik-components";

import { FormikTextField } from "../../../../components/Forms/components/FormikTexField";
import { FormSubheaderWithChildren } from "../../../Shared/components/FormSubheaderWithChildren";
import { FormDescription } from "../../../Shared/components/FormDescription";

export const RefundPolicyProgramSubform: React.FC<Props> = ({ startDate }) => (
  <FormSubheaderWithChildren title="Refund Policy For Program Fees (Excluding Application Fee)">
    <FormDescription description="No Refund, if cancellation is made after">
      <Grid item md={3} alignItems="flex-start">
        <Field
          required
          name="noRefundAfterDate"
          component={DatePicker}
          label="Cancellation made after"
          format="d MMMM yyyy"
          inputVariant="outlined"
          maxDate={startDate}
        />
      </Grid>
    </FormDescription>

    <FormDescription description="Percentage refund, if cancellation is made before">
      <Grid item container direction="column" spacing={2}>
        <Grid item md={3}>
          <FormikTextField
            name="variableRefundPercentage"
            label="Percentage to refund"
            type="number"
          />
        </Grid>

        <Grid item md={3}>
          <Field
            name="variableRefundBeforeDate"
            component={DatePicker}
            label="Cancellation made before"
            format="d MMMM yyyy"
            inputVariant="outlined"
            style={{ margin: 0 }}
            maxDate={startDate}
          />
        </Grid>
      </Grid>
    </FormDescription>

    <FormDescription description="Full Refund, if cancellation is made before">
      <Grid item md={3}>
        <Field
          name="fullRefundBeforeDate"
          component={DatePicker}
          label="Cancellation made before"
          format="d MMMM yyyy"
          inputVariant="outlined"
          style={{ margin: 0 }}
          maxDate={startDate}
        />
      </Grid>
    </FormDescription>
  </FormSubheaderWithChildren>
);

type Props = {
  startDate: Date;
};

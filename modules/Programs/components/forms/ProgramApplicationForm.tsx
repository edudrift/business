import React from "react";

import { Button, Grid } from "@material-ui/core";
import { Form, Formik } from "formik";

import { Program } from "../../types/Programs.types";
import {
  ProgramApplicationSchema,
  ProgramApplication,
} from "../../types/ProgramApplication.types";
import { ProgramsServiceForBusiness } from "../../services/ProgramsServiceForBusiness";
import { ApplicationFeeSubform } from "./ApplicationFeeSubform";
import { AdditionalDocumentsSubform } from "./AdditionalDocumentsSubform";

export const ProgramApplicationForm: React.FC<Props> = (props) => {
  const applicationFee = props.program.applicationFee;
  const additionalDocuments = props.program.additionalDocuments;

  const handleSubmit = async (application: ProgramApplication) => {
    const updatedProgram = await ProgramsServiceForBusiness.saveApplication(
      props.program.id,
      application
    );
    props.afterSubmit(updatedProgram);
  };

  const buildInitialValues = (): ProgramApplication => ({
    hasApplicationFee: true,
    applicationFee,
    additionalDocuments,
  });

  return (
    <Formik
      validationSchema={ProgramApplicationSchema}
      initialValues={buildInitialValues()}
      onSubmit={handleSubmit}
      validateOnMount
    >
      <Form>
        <Grid container spacing={4} direction="column">
          <ApplicationFeeSubform />

          <AdditionalDocumentsSubform />

          <Grid item />

          <Grid item>
            <Button variant="contained" color="primary" type="submit">
              Next step
            </Button>
          </Grid>
        </Grid>
      </Form>
    </Formik>
  );
};

type Props = {
  program: Program;
  afterSubmit: (program: Program) => void;
};

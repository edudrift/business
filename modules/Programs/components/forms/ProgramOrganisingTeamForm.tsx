import {
  Button,
  CircularProgress,
  Divider,
  Grid,
  makeStyles,
  Typography,
} from "@material-ui/core";
import { Field, Form, Formik } from "formik";
import { isEmpty } from "lodash";
import React from "react";

import { TextField } from "material-ui-formik-components";
import { ProgramsServiceForBusiness } from "../../services/ProgramsServiceForBusiness";
import {
  ProgramOrganisingTeamSchema,
  ProgramOrganisingTeamSchemaAsType,
} from "../../types/ProgramOrganisingTeam.types";
import { ProgramPartnerSchemaAsType } from "../../types/ProgramPartner.types";
import { Program } from "../../types/Programs.types";

const useStyles = makeStyles((theme) => ({
  subheader: {
    fontWeight: theme.typography.fontWeightBold,
  },
}));

export const ProgramOrganisingTeamForm: React.FC<Props> = (props) => {
  const classes = useStyles();

  const [initialValues, setInitialValues] = React.useState<
    ProgramOrganisingTeamSchemaAsType
  >({
    partner: {
      name: "",
      descriptionOfRole: "",
      contactNumber: "",
      contactPerson: "",
    },
    organiser: {
      name: "",
      manager: "",
      email: "",
      number: "",

      emergencyPerson: "",
      emergencyNumber: "",
    },
  });
  const [fetched, setFetched] = React.useState(false);

  const buildInitialValues = async (): Promise<ProgramOrganisingTeamSchemaAsType> => {
    // fetch program provider details for preloading.

    const organiser = isEmpty(props.program.organiser)
      ? await ProgramsServiceForBusiness.getProgramProviderForPreloading()
      : props.program.organiser;

    const partner = isEmpty(props.program.partner)
      ? {
          name: "",
          descriptionOfRole: "",
          contactNumber: "",
          contactPerson: "",
        }
      : (props.program.partner as ProgramPartnerSchemaAsType);

    return {
      partner,
      organiser,
    };
  };

  React.useEffect(() => {
    const build = async () => {
      setInitialValues(await buildInitialValues());
      setFetched(true);
    };

    build();
  }, []);

  const handleSubmit = async (
    organisingTeam: ProgramOrganisingTeamSchemaAsType
  ) => {
    const newProgram = await ProgramsServiceForBusiness.saveOrganisingTeam(
      props.program.id,
      organisingTeam.organiser,
      organisingTeam.partner
    );

    props.afterSubmit(newProgram);
  };

  if (!fetched) {
    return <CircularProgress />;
  }

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={handleSubmit}
      validationSchema={ProgramOrganisingTeamSchema}
      validateOnChange
    >
      {({ dirty }) => {
        return (
          <Form>
            <Grid item xs={4} container spacing={2} direction="column">
              <Grid item>
                <Typography className={classes.subheader}>
                  Organiser Information
                </Typography>
              </Grid>

              <Grid item>
                <Field
                  component={TextField}
                  name="organiser.name"
                  label="Name of Organiser"
                  fullWidth
                  type="text"
                  variant="outlined"
                  required
                  style={{ margin: 0 }}
                />
              </Grid>

              <Grid item>
                <Field
                  component={TextField}
                  name="organiser.manager"
                  label="Name of Program Manager"
                  fullWidth
                  type="text"
                  variant="outlined"
                  required
                  style={{ margin: 0 }}
                />
              </Grid>

              <Grid item>
                <Field
                  component={TextField}
                  name="organiser.email"
                  label="Email of Program Manager"
                  fullWidth
                  type="text"
                  variant="outlined"
                  required
                  style={{ margin: 0 }}
                />
              </Grid>

              <Grid item>
                <Field
                  component={TextField}
                  name="organiser.number"
                  label="Contact Number of Program Manager"
                  fullWidth
                  type="text"
                  variant="outlined"
                  required
                  style={{ margin: 0 }}
                />
              </Grid>

              <Grid item>
                <Divider />
              </Grid>

              <Grid item>
                <Field
                  component={TextField}
                  name="organiser.emergencyPerson"
                  label="Name of Emergency Contact Person"
                  fullWidth
                  type="text"
                  variant="outlined"
                  required
                  style={{ margin: 0 }}
                />
              </Grid>

              <Grid item>
                <Field
                  component={TextField}
                  name="organiser.emergencyNumber"
                  label="Contact Number of Emergency Contact Person"
                  fullWidth
                  type="text"
                  variant="outlined"
                  required
                  style={{ margin: 0 }}
                />
              </Grid>

              <Grid item>
                <Typography className={classes.subheader}>
                  Hosting Partner
                </Typography>
              </Grid>
              <Grid item>
                <Field
                  component={TextField}
                  name="partner.name"
                  label="Name of Hosting Partner"
                  fullWidth
                  type="text"
                  variant="outlined"
                  style={{ margin: 0 }}
                  InputLabelProps={{
                    shrink: true,
                  }}
                  placeholder="Fill in if you have a hosting partner"
                />
              </Grid>

              <Grid item>
                <Field
                  component={TextField}
                  name="partner.descriptionOfRole"
                  label="Role of Hosting Partner"
                  fullWidth
                  type="text"
                  variant="outlined"
                  style={{ margin: 0 }}
                  InputLabelProps={{
                    shrink: true,
                  }}
                  placeholder="Tell use more about the role that this partner plays"
                  rows={2}
                  multiline
                />
              </Grid>

              <Grid item>
                <Field
                  component={TextField}
                  name="partner.contactPerson"
                  label="Partner's Contact Person"
                  fullWidth
                  type="text"
                  variant="outlined"
                  style={{ margin: 0 }}
                  InputLabelProps={{
                    shrink: true,
                  }}
                  placeholder="This is how it looks like if the placeholder is long"
                />
              </Grid>

              <Grid item>
                <Field
                  component={TextField}
                  name="partner.contactNumber"
                  label="Partner's Contact Number"
                  fullWidth
                  type="text"
                  variant="outlined"
                  style={{ margin: 0 }}
                  InputLabelProps={{
                    shrink: true,
                  }}
                  placeholder="Shorter description of this field"
                />
              </Grid>

              <Grid item>
                {dirty ? (
                  <Button type="submit" variant="contained" color="primary">
                    Next step
                  </Button>
                ) : (
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={() => props.afterSubmit(props.program)}
                  >
                    Next step
                  </Button>
                )}
              </Grid>
            </Grid>
          </Form>
        );
      }}
    </Formik>
  );
};

type Props = {
  program: Program;
  afterSubmit: (program: Program) => void;
};

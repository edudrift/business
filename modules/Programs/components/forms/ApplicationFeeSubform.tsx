import React from "react";

import { Grid } from "@material-ui/core";
import { useFormikContext } from "formik";

import { FormikTextField } from "../../../../components/Forms/components/FormikTexField";
import { FormSubheaderWithChildren } from "../../../Shared/components/FormSubheaderWithChildren";
import { RadioGroupBoolean } from "../../../Shared/components/RadioGroupBoolean";
import { ProgramApplication } from "../../types/ProgramApplication.types";

export const ApplicationFeeSubform: React.FC = () => {
  const {
    values,
    isSubmitting,
    errors,
    touched,
    setFieldValue,
  } = useFormikContext<ProgramApplication>();

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setFieldValue("hasApplicationFee", e.target.value === "true");
    setFieldValue("applicationFee", null);
  };

  return (
    <Grid item container spacing={4} direction="column">
      <FormSubheaderWithChildren title="Do you wish to collect an application fee?">
        <Grid item>
          <RadioGroupBoolean
            noLabel="No. Participants can register and pay only when successful in their application."
            yesLabel="Yes. The Application is separate from the Program Fees. It will count towards the Program Fees if the Applicant is accepted."
            value={values.hasApplicationFee}
            handleChange={handleChange}
            disabled
            isSubmitting={isSubmitting}
          />
        </Grid>
      </FormSubheaderWithChildren>

      {values.hasApplicationFee && (
        <FormSubheaderWithChildren title="How much is the Application Fee?">
          <Grid item container spacing={2}>
            <Grid item xs={12} md={4}>
              <FormikTextField
                required
                name="applicationFee"
                label="Application fee"
                disabled={isSubmitting}
                type="number"
                errors={errors}
                touched={touched}
              />
            </Grid>
          </Grid>
        </FormSubheaderWithChildren>
      )}
    </Grid>
  );
};

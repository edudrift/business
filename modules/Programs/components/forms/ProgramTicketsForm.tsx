import React from "react";

import { Button, Divider, Grid } from "@material-ui/core";
import { isNull, sortBy } from "lodash";
import { Form, Formik } from "formik";
import * as Y from "yup";

import {
  ProgramEarlyBird,
  ProgramEarlyBirdSchema,
} from "../../types/ProgramEarlyBird.types";
import { Program } from "../../types/Programs.types";
import { ProgramTicket } from "../../types/ProgramTicket.types";
import { ProgramTicketsTable } from "./ProgramTicketsTable";
import { ProgramCurrencySelect } from "./ProgramCurrencySelect";
import { ProgramEarlyBirdSubform } from "./ProgramEarlyBirdSubform";
import { EProgramCurrencies } from "../../enums/EProgramCurrencies";
import { ProgramsServiceForBusiness } from "../../services/ProgramsServiceForBusiness";
import { oneOfEnum } from "../../../../utils/yupUtils";
import { TicketsArraySchema } from "../../../../modules/Ticket/types/Ticket.schema";

type formikValues = {
  earlyBird: ProgramEarlyBird | null;
  currency: EProgramCurrencies;
  tickets: ProgramTicket[];
};

const validationSchema = Y.object().shape({
  currency: oneOfEnum(EProgramCurrencies),
  earlyBird: ProgramEarlyBirdSchema,
  tickets: TicketsArraySchema,
});

export const ProgramTicketsForm: React.FC<Props> = (props) => {
  const currency = props.program.currency;
  const earlyBirdProps = props.program.earlyBird;

  const tickets = sortBy(props.program.tickets, "createdAt");

  const earlyBird: ProgramEarlyBird | null = isNull(earlyBirdProps)
    ? earlyBirdProps
    : {
        startDate: new Date(earlyBirdProps.startDate),
        endDate: new Date(earlyBirdProps.endDate),
        discountType: earlyBirdProps.discountType,
        discountedAmount: earlyBirdProps.discountedAmount,
      };

  const buildInitialValues = (): formikValues => ({
    currency,
    earlyBird,
    tickets,
  });

  const handleSubmit = async ({ currency, earlyBird }: formikValues) => {
    const updatedProgram = await ProgramsServiceForBusiness.saveEarlyBirdPricing(
      props.program.id,
      currency,
      earlyBird
    );
    props.afterSubmit(updatedProgram, true);
  };

  return (
    <Formik
      validationSchema={validationSchema}
      initialValues={buildInitialValues()}
      onSubmit={handleSubmit}
      validateOnChange
    >
      {({ values, setFieldValue }) => {
        return (
          <Form>
            <Grid container spacing={4} direction="column">
              <ProgramCurrencySelect />

              <ProgramEarlyBirdSubform
                earlyBird={values.earlyBird}
                applicationDeadline={
                  new Date(props.program.applicationDeadline)
                }
                setEarlyBird={(earlyBird: ProgramEarlyBird | null) =>
                  setFieldValue("earlyBird", earlyBird)
                }
              />

              <ProgramTicketsTable
                programId={props.program.id}
                ticketTypes={props.program.ticketTypes}
                tickets={tickets}
                onUpdate={(tickets: ProgramTicket[]) => {
                  props.afterSubmit({ ...props.program, tickets }, false);
                }}
              />

              <Grid item>
                <Divider />
              </Grid>

              <Grid item>
                <Button variant="contained" color="primary" type="submit">
                  Next step
                </Button>
              </Grid>
            </Grid>
          </Form>
        );
      }}
    </Formik>
  );
};

type Props = {
  program: Program;
  afterSubmit: (program: Program, proceed?: boolean) => void;
};

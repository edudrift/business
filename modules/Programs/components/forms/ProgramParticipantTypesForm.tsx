import React from "react";

import { Button, Grid } from "@material-ui/core";
import { Form, Formik } from "formik";
import {
  includes,
  initial,
  dropWhile,
  isEmpty,
  partition,
  sortBy,
} from "lodash";

import { FormikTextField } from "../../../../components/Forms/components/FormikTexField";
import { ProgramsServiceForBusiness } from "../../services/ProgramsServiceForBusiness";
import { Program } from "../../types/Programs.types";
import {
  ProgramTicketType,
  ProgramTicketTypeArraySchema,
} from "../../types/ProgramTicketType.types";
import { FormikNullTextField } from "../../../../components/Forms/components/FormikNullTextField";
import { EProgramTicketPricingPackages } from "../../enums/EProgramTicketPricingPackages";
import { FormSubheaderWithChildren } from "~/modules/Shared/components/FormSubheaderWithChildren";

const isInside = (
  ticketTypes: ProgramTicketType[],
  ticketType: ProgramTicketType
) =>
  includes(
    ticketTypes.map((i) => i.id),
    ticketType.id
  );

export const ProgramParticipantTypesForm: React.FC<Props> = (props) => {
  const ticketTypesProp = sortBy(props.program.ticketTypes, "createdAt");
  const ticketTypeLimit = (() => {
    switch (props.program.ticketingPackage) {
      case EProgramTicketPricingPackages.ENTERPRISE:
        return 6;
      default:
        return 6;
    }
  })();

  const canCreateTicketTypes = (values: ProgramTicketType[]) =>
    values.length < ticketTypeLimit;

  const handleSubmit = async (values: ProgramTicketType[]) => {
    //TODO: Remove once problem is solved
    try {
      const [persistedTicketTypes, newTicketTypes] = partition(
        values,
        (ticketType) => isInside(ticketTypesProp, ticketType)
      );
      const updatedTicketTypes = sortBy(
        [
          ...(await ProgramsServiceForBusiness.updateTicketTypes(
            persistedTicketTypes
          )),
          ...(await ProgramsServiceForBusiness.saveTicketTypes(newTicketTypes)),
        ],
        "createdAt"
      );

      const deletedTicketTypes = dropWhile(ticketTypesProp, (i) =>
        isInside(values, i)
      );
      await ProgramsServiceForBusiness.deleteTicketTypes(deletedTicketTypes);
      props.afterSubmit({ ...props.program, ticketTypes: updatedTicketTypes });
    } catch (err) {
      alert(err);
    }
  };

  const buildNewTicketType = (number: number): ProgramTicketType => {
    const placeholder = `New Participant Type ${number}`;
    return {
      id: placeholder,
      name: placeholder,
      min: 0,
      max: 0,
      minAge: null,
      maxAge: null,
      programId: props.program.id,
    };
  };

  const buildInitialValues = () =>
    isEmpty(ticketTypesProp) ? [buildNewTicketType(1)] : ticketTypesProp;

  return (
    <Formik
      validationSchema={ProgramTicketTypeArraySchema}
      initialValues={buildInitialValues()}
      onSubmit={handleSubmit}
      validateOnChange
    >
      {({ values, setValues, errors, touched }) => {
        const handleCreate = () =>
          canCreateTicketTypes(values) &&
          setValues([...values, buildNewTicketType(values.length + 1)]);

        const handleDelete = () => setValues(initial(values));

        return (
          <Form>
            <Grid container spacing={2}>
              <FormSubheaderWithChildren title="MINIMUM/MAXIMUM QUOTA">
                <Grid item container spacing={5}>
                  {values.map((_, index) => {
                    return (
                      <Grid
                        key={index}
                        item
                        xs={12}
                        md={4}
                        container
                        direction="column"
                        spacing={2}
                      >
                        <Grid item>
                          <FormikTextField
                            name={`${index}.name`}
                            label="Name "
                            errors={errors}
                            touched={touched}
                          />
                        </Grid>

                        <Grid item container spacing={2}>
                          {/* COMING SOON IN VERSION 3.0
                          <Grid item xs>
                            <FormikTextField
                              type="text"
                              name={`${index}.min`}
                              errors={errors}
                              touched={touched}
                              label="Minimum Ticket Count "
                            />
                          </Grid> */}

                          <Grid item xs>
                            <FormikTextField
                              type="number"
                              required
                              name={`${index}.max`}
                              label="Maximum Ticket Count "
                              errors={errors}
                              touched={touched}
                            />
                          </Grid>
                        </Grid>

                        <Grid item container spacing={2}>
                          <Grid item xs>
                            <FormikNullTextField
                              type="number"
                              nullableLabel="No minimum Age"
                              required
                              variant="outlined"
                              name={`${index}.minAge`}
                              label="Minimum Age"
                            />
                          </Grid>

                          <Grid item xs>
                            <FormikNullTextField
                              type="number"
                              nullableLabel="No maximum age"
                              required
                              variant="outlined"
                              name={`${index}.maxAge`}
                              label="Maximum Age"
                            />
                          </Grid>
                        </Grid>
                      </Grid>
                    );
                  })}

                  <Grid
                    item
                    md={3}
                    container
                    justify="flex-start"
                    alignItems="center"
                  >
                    <Grid item container direction="row" spacing={2}>
                      <Grid item md={6}>
                        <Button
                          fullWidth
                          variant="outlined"
                          color="primary"
                          onClick={handleCreate}
                          disabled={!canCreateTicketTypes(values)}
                        >
                          Add new type
                        </Button>
                      </Grid>

                      <Grid item md={6}>
                        <Button
                          fullWidth
                          variant="outlined"
                          color="primary"
                          onClick={handleDelete}
                        >
                          Delete type
                        </Button>
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
              </FormSubheaderWithChildren>

              <Grid item>
                <Button color="primary" variant="contained" type="submit">
                  Next step
                </Button>
              </Grid>
            </Grid>
          </Form>
        );
      }}
    </Formik>
  );
};

type Props = {
  program: Program;
  afterSubmit: (program: Program) => void;
};

import React from "react";

import { Grid } from "@material-ui/core";
import { Field, useFormikContext } from "formik";
import { Select } from "material-ui-formik-components";

import { FormikTextField } from "../../../../components/Forms/components/FormikTexField";
import {
  EProgramRefundTypeLabels,
  EProgramRefundTypes,
} from "../../enums/EProgramRefundTypes";
import { get } from "lodash";
import { ProgramRefundPolicy } from "../../types/ProgramRefundPolicy";

export const RefundTypeSelect: React.FC<Props> = ({ participantName }) => {
  const options = Object.values(EProgramRefundTypes).map((i) => ({
    value: i,
    label: EProgramRefundTypeLabels[i],
  }));

  const { values, errors, touched, setFieldValue } = useFormikContext<
    ProgramRefundPolicy
  >();
  const refundType = get(
    values,
    `applicationFee.${participantName}.refundType`
  );

  const isPartialRefund = (refundType: EProgramRefundTypes | string) =>
    refundType === EProgramRefundTypes.PARTIAL_REFUND;

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const fieldValue = e.target.value;
    setFieldValue(`applicationFee.${participantName}.refundType`, fieldValue);
    if (!isPartialRefund(fieldValue)) {
      setFieldValue(`applicationFee.${participantName}.amount`, undefined);
    }
  };

  return (
    <Grid item container direction="row" spacing={2}>
      <Grid item xs={12} sm={12} md={3}>
        <Field
          required
          name={`applicationFee.${participantName}.refundType`}
          label="Refund Type"
          options={options}
          component={Select}
          variant="outlined"
          fullWidth
          style={{ margin: 0 }}
          onChange={handleChange}
          errors={errors}
          touched={touched}
        />
      </Grid>
      {isPartialRefund(refundType) && (
        <Grid item xs={12} sm={12} md={3}>
          <FormikTextField
            required
            name={`applicationFee.${participantName}.amount`}
            label="Partial amount"
            type="number"
            errors={errors}
            touched={touched}
          />
        </Grid>
      )}
    </Grid>
  );
};

type Props = {
  participantName: "acceptedParticipants" | "rejectedParticipants";
};

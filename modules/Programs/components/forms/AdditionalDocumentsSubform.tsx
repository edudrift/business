import React from "react";

import { Button, Grid } from "@material-ui/core";
import { Field, useFormikContext } from "formik";
import { filter, isEmpty, range } from "lodash";

import { Select } from "material-ui-formik-components";
import { FormikTextField } from "../../../../components/Forms/components/FormikTexField";
import {
  EProgramAdditionalDocumentTypes,
  ProgramAdditionalDocuments,
} from "../../types/ProgramAdditionalDocuments.types";
import { FormH6Header } from "../../../Shared/components/FormH6Header";
import { FormSubheaderWithChildren } from "../../../Shared/components/FormSubheaderWithChildren";
import { TypographyWithPxFontAndColor } from "../../../Shared/components/TypographyWithPxFontAndColor";
import { RadioGroupBoolean } from "../../../Shared/components/RadioGroupBoolean";

interface formikValues {
  additionalDocuments: ProgramAdditionalDocuments[];
}

export const AdditionalDocumentsSubform: React.FC = () => {
  const {
    errors,
    touched,
    setFieldValue,
    values,
    isSubmitting,
  } = useFormikContext<formikValues>();

  const addDocument = isEmpty(
    errors?.additionalDocuments?.[values.additionalDocuments.length - 1]
  );

  const [hasAdditionalDocuments, setHasAdditionalDocuments] = React.useState(
    !isEmpty(values.additionalDocuments)
  );

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setHasAdditionalDocuments(e.target.value === "true");
    setFieldValue("additionalDocuments", []);
  };

  const handleAdd = () =>
    setFieldValue("additionalDocuments", [
      ...values.additionalDocuments,
      {
        name: "",
        type: null,
      },
    ]);

  return (
    <Grid item container direction="column" spacing={4}>
      <FormSubheaderWithChildren
        title="Basic Information Provided"
        subtitle="Every profile on Edudrift will contain the following information
              of the participant"
      >
        <Grid item>
          <TypographyWithPxFontAndColor sizeInPx={16} color="#666666">
            - Full name as per passport
            <br />
            - Current stage of education
            <br />
            - Affiliated instution name
            <br />
            - Gender
            <br />
            - Date of birth
            <br />
            - Nationality and passport issuing country
            <br />
            - Mobile number and email address
            <br />
            - Emergency contact person and number
            <br />
          </TypographyWithPxFontAndColor>
        </Grid>
      </FormSubheaderWithChildren>

      <FormSubheaderWithChildren title="Does this program require additional information?">
        <Grid item>
          <RadioGroupBoolean
            noLabel="No. The above information is sufficient."
            yesLabel="Yes. The program requires the following additional information."
            value={hasAdditionalDocuments}
            handleChange={handleChange}
            isSubmitting={isSubmitting}
          />
        </Grid>
      </FormSubheaderWithChildren>

      {hasAdditionalDocuments && (
        <FormH6Header title="Additional Information">
          <>
            {range(values.additionalDocuments.length).map((_, index) => {
              const handleDelete = () =>
                setFieldValue(
                  "additionalDocuments",
                  filter(values.additionalDocuments, (_, i) => i !== index)
                );

              return (
                <Grid item key={index} container spacing={2}>
                  <Grid item xs={12} md={4}>
                    <FormikTextField
                      name={`additionalDocuments.${index}.name`}
                      label="Name of document"
                      errors={errors}
                      touched={touched}
                      required
                    />
                  </Grid>

                  <Grid item xs={6} md={4}>
                    <Field
                      style={{ margin: 0 }}
                      required
                      name={`additionalDocuments.${index}.type`}
                      label="Type of document"
                      options={[
                        {
                          value: EProgramAdditionalDocumentTypes.TEXT,
                          label: "Text",
                        },

                        {
                          value: EProgramAdditionalDocumentTypes.FILE,
                          label: "File",
                        },
                      ]}
                      component={Select}
                      variant="outlined"
                    />
                  </Grid>

                  <Grid item xs={6} md={3} container spacing={2}>
                    <Grid item>
                      <Button color="secondary" onClick={handleDelete}>
                        Remove
                      </Button>
                    </Grid>
                  </Grid>
                </Grid>
              );
            })}
            {addDocument && (
              <Grid item>
                <Button variant="outlined" color="primary" onClick={handleAdd}>
                  Add document
                </Button>
              </Grid>
            )}
          </>
        </FormH6Header>
      )}
    </Grid>
  );
};

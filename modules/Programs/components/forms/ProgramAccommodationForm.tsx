import React from "react";

import {
  Button,
  Card,
  CardContent,
  Checkbox,
  FormControlLabel,
  Grid,
  makeStyles,
  Radio,
  RadioGroup,
} from "@material-ui/core";
import { Form, Formik } from "formik";
import { filter } from "lodash";

import { FormikTextField } from "../../../../components/Forms/components/FormikTexField";
import { FormSubheaderWithSubtitle } from "../../../Shared/components/FormSubheaderWithSubtitle";
import { TypographyWithPxFontAndColor } from "../../../Shared/components/TypographyWithPxFontAndColor";
import {
  ArrayOfProgramAccommodationRoomTypes,
  EProgramAccommodationRoomTypeLabels,
  EProgramAccommodationRoomTypes,
} from "../../enums/EProgramAccommodationRoomTypes";
import {
  ArrayOfProgramAccommodationTypes,
  EProgramAccommodationTypeLabels,
  EProgramAccommodationTypes,
} from "../../enums/EProgramAccommodationTypes";
import {
  ArrayOfProgramAirportTransfers,
  EProgramAirportTransferLabels,
  EProgramAirportTransfers,
} from "../../enums/EProgramAirportTransfers";
import { ProgramsServiceForBusiness } from "../../services/ProgramsServiceForBusiness";
import { ProgramAccommodationDetails } from "../../types/ProgramAccommodation.types";
import { Program } from "../../types/Programs.types";
import { MultiImageUploader } from "../../../Shared/components/MultiImageUploader";

const useStyles = makeStyles((theme) => {
  return {
    label: {
      fontWeight: theme.typography.fontWeightBold,
      fontSize: theme.typography.pxToRem(14),
    },
  };
});

export const ProgramAccommodationForm: React.FC<Props> = (props) => {
  const [images, setImages] = React.useState(
    props.program.accommodation?.details?.images || []
  );
  const [hasAccommodation, setHasAccommodation] = React.useState(false);
  const [accommodationType, setAccommodationType] = React.useState<
    EProgramAccommodationTypes
  >();
  const [roomTypes, setRoomTypes] = React.useState<
    EProgramAccommodationRoomTypes[]
  >([]);
  const [airportTransfer, setAirportTransfer] = React.useState(
    props.program.accommodation?.details?.transfer ||
      EProgramAirportTransfers.NONE
  );

  React.useEffect(() => {
    if (props.program.accommodation?.provided) {
      setHasAccommodation(true);
      if (props.program.accommodation.details) {
        setAccommodationType(props.program.accommodation.details.type);
        setRoomTypes(props.program.accommodation.details.roomTypes);
      }
    }
  }, []);

  const classes = useStyles();

  const buildInitialValues = () => {
    if (!props.program.accommodation?.provided) {
      return {
        name: "",
        localName: "",
        address: {
          lineOne: "",
          lineTwo: "",
          postalCode: "",
        },
        localAddress: {
          lineOne: "",
          lineTwo: "",
        },
      };
    } else {
      const { details } = props.program.accommodation;
      if (details) {
        return {
          name: details.name,
          localName: details.localName,
          address: {
            lineOne: details.address.lineOne,
            lineTwo: details.address.lineTwo,
            postalCode: details.address.postalCode,
          },
          localAddress: {
            lineOne: details.localAddress.lineOne,
            lineTwo: details.localAddress.lineTwo,
          },
        };
      } else {
        throw new Error("Cannot build default values.");
      }
    }
  };

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setHasAccommodation((event.target as HTMLInputElement).value === "true");
  };

  const handleSubmit = async (v: any) => {
    if (!hasAccommodation) {
      const newProgram = await ProgramsServiceForBusiness.saveAccommodation(
        props.program.id
      );
      props.afterSubmit(newProgram);
    } else {
      const accommodationDetails: ProgramAccommodationDetails = {
        ...v,
        type: accommodationType,
        transfer: airportTransfer,
        roomTypes,
        images,
      };

      const newProgram = await ProgramsServiceForBusiness.saveAccommodation(
        props.program.id,
        accommodationDetails
      );
      props.afterSubmit(newProgram);
    }
  };

  const handleAccommodationTypeChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setAccommodationType(event.target.name as EProgramAccommodationTypes);
  };

  const handleRoomTypeChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const currentType = event.target.name as EProgramAccommodationRoomTypes;

    if (roomTypes.includes(currentType)) {
      setRoomTypes(filter(roomTypes, (rt) => rt !== currentType));
    } else {
      setRoomTypes([...roomTypes, currentType]);
    }
  };
  const handleAirportTransferChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setAirportTransfer(event.target.name as EProgramAirportTransfers);
  };

  return (
    <Formik initialValues={buildInitialValues()} onSubmit={handleSubmit}>
      {({ isSubmitting }) => {
        return (
          <Form>
            <Grid container spacing={5} direction="column">
              <Grid item container spacing={2} direction="row">
                <Grid item>
                  <FormSubheaderWithSubtitle
                    title="Is Accommodation Provided?"
                    subtitle="Select up to 3 most relevant profile that best fits the
                    program. Note that if you select multiple groups, there is a
                    very high likelyhood that these participant from different
                    academic profiles will register and be in the same program."
                  />
                </Grid>
                <Grid item>
                  <RadioGroup value={hasAccommodation} onChange={handleChange}>
                    <Grid item container spacing={1} direction="column">
                      <Grid item>
                        <Card variant="outlined">
                          <CardContent>
                            <Grid container direction="column">
                              <Grid item>
                                <FormControlLabel
                                  value={true}
                                  control={
                                    <Radio
                                      color="primary"
                                      disabled={isSubmitting}
                                    />
                                  }
                                  label="Yes, accommodation is included in the tickets"
                                  disabled={isSubmitting}
                                  classes={{
                                    label: classes.label,
                                  }}
                                />
                              </Grid>
                            </Grid>
                          </CardContent>
                        </Card>
                      </Grid>

                      <Grid item>
                        <Card variant="outlined">
                          <CardContent>
                            <Grid container direction="column">
                              <Grid item>
                                <FormControlLabel
                                  value={false}
                                  control={
                                    <Radio
                                      color="primary"
                                      disabled={isSubmitting}
                                    />
                                  }
                                  label="No. We do not provide accommodation"
                                  disabled={isSubmitting}
                                  classes={{
                                    label: classes.label,
                                  }}
                                />
                              </Grid>
                            </Grid>
                          </CardContent>
                        </Card>
                      </Grid>
                    </Grid>
                  </RadioGroup>
                </Grid>
              </Grid>

              {hasAccommodation && (
                <React.Fragment>
                  <Grid item container spacing={2} direction="column" md={4}>
                    <Grid item>
                      <FormSubheaderWithSubtitle title="Accommodation Details" />
                    </Grid>

                    <Grid item container spacing={2}>
                      <Grid item>
                        <TypographyWithPxFontAndColor
                          sizeInPx={14}
                          color="#999999"
                        >
                          In English language
                        </TypographyWithPxFontAndColor>
                      </Grid>
                      <Grid item>
                        <FormikTextField name="name" label="Name in English" />
                      </Grid>
                    </Grid>

                    <Grid item container spacing={2}>
                      <Grid item>
                        <TypographyWithPxFontAndColor
                          sizeInPx={14}
                          color="#999999"
                        >
                          In Local language
                        </TypographyWithPxFontAndColor>
                      </Grid>
                      <Grid item>
                        <FormikTextField
                          name="localName"
                          label="Name in local language"
                        />
                      </Grid>
                    </Grid>
                  </Grid>

                  <Grid item container spacing={3} direction="column" md={4}>
                    <Grid item>
                      <FormSubheaderWithSubtitle title="Address of Accommodation" />
                    </Grid>

                    <Grid item container spacing={2}>
                      <Grid item>
                        <TypographyWithPxFontAndColor
                          sizeInPx={14}
                          color="#999999"
                        >
                          In English Language
                        </TypographyWithPxFontAndColor>
                      </Grid>
                      <Grid item>
                        <FormikTextField
                          name="address.lineOne"
                          label="Address Line One"
                        />
                      </Grid>
                      <Grid item>
                        <FormikTextField
                          name="address.lineTwo"
                          label="Address Line Two"
                        />
                      </Grid>
                      <Grid item>
                        <FormikTextField
                          name="address.postalCode"
                          label="Postal Code"
                        />
                      </Grid>
                    </Grid>

                    <Grid item container spacing={2}>
                      <Grid item>
                        <TypographyWithPxFontAndColor
                          sizeInPx={14}
                          color="#999999"
                        >
                          In Local Language
                        </TypographyWithPxFontAndColor>
                      </Grid>
                      <Grid item>
                        <FormikTextField
                          name="localAddress.lineOne"
                          label="Address Line One"
                        />
                      </Grid>
                      <Grid item>
                        <FormikTextField
                          name="localAddress.lineTwo"
                          label="Address Line Two"
                        />
                      </Grid>
                    </Grid>
                  </Grid>

                  <Grid item container spacing={2} direction="column">
                    <Grid item>
                      <FormSubheaderWithSubtitle title="Accommodation Type/Star" />
                    </Grid>

                    <Grid item container spacing={1} direction="column">
                      {ArrayOfProgramAccommodationTypes.map((at) => {
                        return (
                          <Grid item key={at}>
                            <FormControlLabel
                              control={
                                <Checkbox
                                  checked={accommodationType === at}
                                  onChange={handleAccommodationTypeChange}
                                  name={at}
                                  color="primary"
                                />
                              }
                              label={EProgramAccommodationTypeLabels[at]}
                            />
                          </Grid>
                        );
                      })}
                      <Grid item>
                        <TypographyWithPxFontAndColor
                          sizeInPx={14}
                          color="#999999"
                        >
                          *In case the above hotel is unavailable, an
                          alternative of equivalent standard will be arranged
                        </TypographyWithPxFontAndColor>
                      </Grid>
                    </Grid>
                  </Grid>

                  <Grid item container spacing={2} direction="column">
                    <Grid item>
                      <FormSubheaderWithSubtitle title="Airport Transfer" />
                    </Grid>

                    <Grid item container spacing={1} direction="column">
                      {ArrayOfProgramAirportTransfers.map((at) => {
                        return (
                          <Grid item key={at}>
                            <FormControlLabel
                              control={
                                <Checkbox
                                  checked={airportTransfer === at}
                                  onChange={handleAirportTransferChange}
                                  name={at}
                                  color="primary"
                                />
                              }
                              label={EProgramAirportTransferLabels[at]}
                            />
                          </Grid>
                        );
                      })}
                    </Grid>
                  </Grid>

                  <Grid item container spacing={2} direction="column">
                    <Grid item>
                      <FormSubheaderWithSubtitle title="Room Types" />
                    </Grid>

                    <Grid item container spacing={1} direction="column">
                      {ArrayOfProgramAccommodationRoomTypes.map((rt) => {
                        return (
                          <Grid item key={rt}>
                            <FormControlLabel
                              control={
                                <Checkbox
                                  checked={roomTypes.includes(rt)}
                                  onChange={handleRoomTypeChange}
                                  name={rt}
                                  color="primary"
                                />
                              }
                              label={EProgramAccommodationRoomTypeLabels[rt]}
                            />
                          </Grid>
                        );
                      })}
                    </Grid>
                  </Grid>

                  <Grid item container spacing={2} direction="column">
                    <Grid item>
                      <FormSubheaderWithSubtitle title="Gallery" />
                    </Grid>

                    <MultiImageUploader
                      count={6}
                      initialImages={images}
                      onChange={(newImages: string[]) => setImages(newImages)}
                    />
                  </Grid>
                </React.Fragment>
              )}

              <Grid item>
                <Button type="submit" variant="contained" color="primary">
                  Next step
                </Button>
              </Grid>
            </Grid>
          </Form>
        );
      }}
    </Formik>
  );
};

type Props = {
  program: Program;
  afterSubmit: (newProgram: Program) => void;
};

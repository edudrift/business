import React from "react";
import { isEmpty, head } from "lodash";
import {
  ProgramSchedule,
  ProgramDaySchedule,
  ProgramWeekSchedule,
} from "../../types/ProgramSchedule.types";
import { Badge, Button, Grid, makeStyles } from "@material-ui/core";

import {
  formatDateWithDayAndMonth,
  formatDateWithDateAndDayOfWeek,
} from "../../../../utils/dateUtils";

const useStyles = makeStyles(() => ({
  badge: {
    height: "12px",
    minWidth: "12px",
    width: "12px",
  },
  badgeIcon: {
    fontSize: "6px",
  },
  datesOnWeek: {
    whiteSpace: "nowrap",
    paddingTop: 0,
    fontSize: "12px",
  },
}));

const dayCompleted = (day: ProgramDaySchedule) =>
  Object.values(day.details).every((detail) => !isEmpty(head(detail)));

const weekCompleted = (week: ProgramWeekSchedule) =>
  Object.values(week.details).some((i) => !isEmpty(i));

export const ProgramSchedulerSelectionButton: React.FC<Props> = ({
  selected,
  setSelected,
  schedule,
}) => {
  const classes = useStyles();
  return (
    <React.Fragment>
      {schedule.map((scheduleItem, index: number) => {
        const isSelected = selected === index;
        const color = isSelected ? "primary" : "default";
        const style = isSelected ? {} : { opacity: ".65" };
        return (
          <Grid
            item
            style={{ width: "14.2%" }}
            key={index}
            container
            alignItems="center"
            direction="column"
          >
            <Grid item container justify="center">
              <Badge
                classes={{ badge: classes.badge }}
                color="primary"
                badgeContent={
                  <span className={classes.badgeIcon}>&#10004;</span>
                }
                invisible={(() => {
                  switch (scheduleItem.type) {
                    case "day":
                      return !dayCompleted(scheduleItem);
                    case "week":
                      return !weekCompleted(scheduleItem);
                  }
                })()}
              >
                <Button
                  fullWidth
                  variant="outlined"
                  color={color}
                  onClick={() => setSelected(index)}
                  style={{ ...style, textTransform: "none", minWidth: "77px" }}
                >
                  {(() => {
                    switch (scheduleItem.type) {
                      case "day":
                        return `Day ${index + 1}`;
                      case "week":
                        return `Week ${index + 1}`;
                    }
                  })()}
                </Button>
              </Badge>
            </Grid>
            <Grid item>
              <Button disabled className={classes.datesOnWeek}>
                {(() => {
                  switch (scheduleItem.type) {
                    case "day":
                      return formatDateWithDateAndDayOfWeek(scheduleItem.date);
                    case "week":
                      return `\
                        ${formatDateWithDayAndMonth(scheduleItem.startDate)}
                        - ${formatDateWithDayAndMonth(scheduleItem.endDate)}`;
                  }
                })()}
              </Button>
            </Grid>
          </Grid>
        );
      })}
    </React.Fragment>
  );
};

type Props = {
  selected: number;
  setSelected: React.Dispatch<React.SetStateAction<number>>;
  schedule: ProgramSchedule;
};

import React from "react";

import { Grid, Typography } from "@material-ui/core";
import { range } from "lodash";
import { TextField } from "material-ui-formik-components";
import { Field } from "formik";
import { formatDateWithDayAndMonth } from "../../../../utils/dateUtils";
import { ProgramWeekSchedule } from "../../types/ProgramSchedule.types";

const renderLabel = (index: number) => {
  return `Schedule Item ${index + 1}`;
};

export const ProgramWeekSchedulerSubform: React.FC<Props> = ({
  selected,
  week,
}) => {
  return (
    <Grid item container spacing={2} direction="column">
      <Grid item>
        <Typography style={{ fontSize: "16px", fontWeight: 500 }}>
          {`${formatDateWithDayAndMonth(week.startDate)} - \
            ${formatDateWithDayAndMonth(week.endDate)} \
            Week ${week.weekNumber}`}
        </Typography>
      </Grid>

      <Grid item container spacing={2}>
        <Grid item>
          <Typography style={{ fontSize: "14px", fontWeight: 400 }}>
            Details
          </Typography>
        </Grid>

        <Grid item container spacing={2} direction="column">
          <Grid item container direction="column">
            {range(7).map((index) => {
              return (
                <Grid item key={index}>
                  <Field
                    label={renderLabel(index)}
                    key={index}
                    size="small"
                    style={{ width: "392px" }}
                    type="text"
                    variant="outlined"
                    component={TextField}
                    InputLabelProps={{
                      shrink: true,
                    }}
                    name={`${selected}.details.${index}`}
                  />
                </Grid>
              );
            })}
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};

type Props = {
  selected: number;
  week: ProgramWeekSchedule;
};

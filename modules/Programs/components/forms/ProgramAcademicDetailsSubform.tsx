import React from "react";

import { Grid } from "@material-ui/core";
import { Field } from "formik";
import { TextField } from "material-ui-formik-components";
import { MultiImageUploader } from "../../../Shared/components/MultiImageUploader";

export const ProgramAcademicDetailsSubform: React.FC<Props> = ({
  namespace,
  images,
  onChange,
}) => {
  const buildFieldName = (name: string) => `${namespace}.${name}`;

  return (
    <React.Fragment>
      <Grid item container direction="column" spacing={2}>
        <Grid item zeroMinWidth />
        <Grid item>
          <Field
            component={TextField}
            name={buildFieldName("lectureDescription")}
            label="Description of Lecture/Academic Content"
            fullWidth
            type="text"
            variant="outlined"
            required
            style={{ margin: 0 }}
            InputLabelProps={{
              shrink: true,
            }}
            placeholder="What are the lectures in this program all about?"
            multiline
            rows={4}
          />
        </Grid>

        <Grid item>
          <Field
            component={TextField}
            name={buildFieldName("teachingStaffDescription")}
            label="Description of The Relevant Teaching Staffs"
            fullWidth
            type="text"
            variant="outlined"
            required
            style={{ margin: 0 }}
            InputLabelProps={{
              shrink: true,
            }}
            placeholder="Who will be conducting the lectures? Tell us more"
            multiline
            rows={4}
          />
        </Grid>

        <Grid item>
          <Field
            component={TextField}
            name={buildFieldName("certificationDescription")}
            label="Description of the attained certificate"
            fullWidth
            type="text"
            variant="outlined"
            required
            style={{ margin: 0 }}
            InputLabelProps={{
              shrink: true,
            }}
            placeholder="What is this certificate all about?"
            multiline
            rows={4}
          />
        </Grid>

        <MultiImageUploader
          count={6}
          initialImages={images}
          onChange={onChange}
        />
      </Grid>
    </React.Fragment>
  );
};

type Props = {
  namespace: string;
  images: string[];
  onChange: (newImages: string[]) => void;
};

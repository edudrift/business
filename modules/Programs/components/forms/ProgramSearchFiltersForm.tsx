import React from "react";

import { Button, Grid } from "@material-ui/core";
import { Formik, Form } from "formik";
import * as Y from "yup";
import { ProgramsServiceForBusiness } from "../../services/ProgramsServiceForBusiness";
import { Program } from "../../types/Programs.types";
import { ProgramEducationalBackgroundSubform } from "./ProgramEducationalBackgroundSubform";
import { ProgramFieldsOfInterestSubForm } from "./ProgramFieldsOfInterestSubForm";
import { EProgramFieldsOfInterest } from "../../enums/EProgramFieldsOfInterest";
import { EProgramEducationalBackground } from "../../enums/EProgramEducationalBackground";
import { oneOfEnum } from "~/utils/yupUtils";

export const ProgramSearchFiltersForm: React.FC<Props> = ({
  program,
  afterSubmit,
}) => {
  const buildInitialValues = () => ({
    fields: program.fields,
    education: program.education || [],
  });

  const handleSubmit = async ({ fields, education }: formikValues) => {
    const updatedProgram = await ProgramsServiceForBusiness.saveSearchFilters(
      program.id,
      fields.sort(),
      education.sort()
    );
    afterSubmit(updatedProgram);
  };

  return (
    <Formik
      initialValues={buildInitialValues()}
      validationSchema={Y.object({
        fields: Y.array().of(oneOfEnum(EProgramFieldsOfInterest)),
        education: Y.array().of(oneOfEnum(EProgramEducationalBackground)),
      })}
      onSubmit={handleSubmit}
    >
      {({ values }) => {
        console.log(values);
        return (
          <Form>
            <Grid container spacing={4} direction="column">
              <ProgramFieldsOfInterestSubForm />

              <ProgramEducationalBackgroundSubform />

              <Grid item>
                <Button color="primary" variant="contained" type="submit">
                  Save and Go Back
                </Button>
              </Grid>
            </Grid>
          </Form>
        );
      }}
    </Formik>
  );
};

type Props = {
  program: Program;
  afterSubmit: (program: Program) => void;
};

type formikValues = {
  fields: EProgramFieldsOfInterest[];
  education: EProgramEducationalBackground[];
};

import React from "react";

import { useFormikContext } from "formik";
import { Grid } from "@material-ui/core";

import {
  EProgramEducationalBackground,
  EProgramEducationalBackgroundLabels,
} from "../../enums/EProgramEducationalBackground";
import { FormSubheaderWithChildren } from "../../../Shared/components/FormSubheaderWithChildren";
import { CheckboxGroupMd9 } from "../../../Shared/components/CheckboxGroupMd9";

export const ProgramEducationalBackgroundSubform: React.FC = () => {
  const { values, setFieldValue } = useFormikContext<{
    education: EProgramEducationalBackground[];
  }>();

  return (
    <FormSubheaderWithChildren
      title="Education Background"
      subtitle={
        "Select up to 3 most relevant profile that best fits the program.\
               Note that if you select multiple groups, there is a very high   \
               likelihood that these participant from different academic       \
               profiles will register and be in the same program."
      }
    >
      <Grid item>
        <CheckboxGroupMd9
          checked={values.education}
          values={Object.values(EProgramEducationalBackground)}
          labels={Object.values(EProgramEducationalBackgroundLabels)}
          setValues={(v) => setFieldValue("education", v)}
        />
      </Grid>
    </FormSubheaderWithChildren>
  );
};

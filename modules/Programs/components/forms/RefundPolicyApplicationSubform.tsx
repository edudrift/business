import React from "react";
import { FormSubheaderWithChildren } from "../../../Shared/components/FormSubheaderWithChildren";
import { FormDescription } from "../../../Shared/components/FormDescription";
import { RefundTypeSelect } from "./RefundTypeSelect";

enum EParticipantsName {
  ACCEPTED = "acceptedParticipants",
  REJECTED = "rejectedParticipants",
}

export const RefundPolicyApplicationSubform: React.FC = () => (
  <FormSubheaderWithChildren title="Refund Policy for the Applicaton">
    {Object.values(EParticipantsName).map((name) => (
      <FormDescription
        description={`If the Applicant is \
        ${name === EParticipantsName.ACCEPTED ? "accepted" : "rejected"}, and \
        the Applicant chooses to cancel the Booking, the Application Fee will`}
      >
        <RefundTypeSelect key={name} participantName={name} />
      </FormDescription>
    ))}
  </FormSubheaderWithChildren>
);

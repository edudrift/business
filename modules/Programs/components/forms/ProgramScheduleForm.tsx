import React from "react";

import { Button, Grid, Typography } from "@material-ui/core";
import {
  isNull,
  isUndefined,
  range,
  isEmpty,
  head,
  findKey,
  omitBy,
  mapValues,
  zipObject,
} from "lodash";

import { Formik, Form } from "formik";
import { requiredText } from "../../../../utils/formikUtils";
import { ProgramsServiceForBusiness } from "../../services/ProgramsServiceForBusiness";
import { Program, EProgramType } from "../../types/Programs.types";
import { ProgramSchedulerSelectionButton } from "./ProgramSchedulerSelectionButton";
import { ProgramWeekSchedulerSubform } from "./ProgramWeekSchedulerSubform";
import { ProgramDaySchedulerSubform } from "./ProgramDaySchedulerSubform";
import {
  DayDetails,
  ProgramDaySchedule,
  ProgramWeekSchedule,
  ProgramSchedule,
  isProgramDay,
  isProgramWeek,
} from "../../types/ProgramSchedule.types";

import {
  differenceInDays,
  addDaysToDate,
  parseISOString,
  isSameDate,
} from "../../../../utils/dateUtils";

const buildInitialProgramDayWithSchedule = (
  schedule: ProgramSchedule,
  startDate: Date,
  endDate: Date
): ProgramDaySchedule[] => {
  const numberOfDays =
    differenceInDays(new Date(startDate), new Date(endDate)) + 1;

  const isSameStartDate = (schedule: ProgramDaySchedule[]) =>
    isSameDate(
      parseISOString((head(schedule)!.date as any) as string),
      startDate
    );

  const buildProgramDay = (
    date: Date,
    dayNumber: number
  ): ProgramDaySchedule => {
    return {
      type: "day",
      dayNumber: dayNumber,
      date: date,
      details: {
        [DayDetails.MORNING]: ["", "", ""],
        [DayDetails.AFTERNOON]: ["", "", ""],
        [DayDetails.EVENING]: ["", "", ""],
      },
    };
  };

  return range(numberOfDays).map((index) => {
    const date: Date = addDaysToDate(startDate, index);

    return isNull(schedule) ||
      isUndefined(schedule[index]) ||
      isProgramWeek(schedule[index]) ||
      !isSameStartDate(schedule as ProgramDaySchedule[])
      ? buildProgramDay(date, index + 1)
      : //replace string dates from db to a real Date
        Object.assign({}, schedule[index] as ProgramDaySchedule, { date });
  });
};

const buildInitialProgramWeekWithSchedule = (
  schedule: ProgramSchedule,
  startDate: Date,
  endDate: Date
): ProgramWeekSchedule[] => {
  const numberOfWeeks = Math.ceil(differenceInDays(startDate, endDate) / 7);

  const isSameStartDate = (schedule: ProgramWeekSchedule[]) =>
    isSameDate(
      parseISOString((head(schedule)!.startDate as any) as string),
      startDate
    );

  const buildProgramWeek = (
    startDate: Date,
    endDate: Date,
    weekNumber: number
  ): ProgramWeekSchedule => {
    return {
      type: "week",
      weekNumber: weekNumber,
      startDate: startDate,
      endDate: endDate,
      details: range(7).reduce(
        (details, _, index) => ({ ...details, [index]: "" }),
        {}
      ),
    };
  };

  return range(numberOfWeeks).map((index) => {
    const startOfWeek = addDaysToDate(startDate, index * 7);
    const endOfWeek =
      numberOfWeeks !== index + 1 ? addDaysToDate(startOfWeek, 6) : endDate;
    return isNull(schedule) ||
      isUndefined(schedule[index]) ||
      isProgramDay(schedule[index]) ||
      !isSameStartDate(schedule as ProgramWeekSchedule[])
      ? buildProgramWeek(startOfWeek, endDate, index + 1)
      : Object.assign({}, schedule[index] as ProgramWeekSchedule, {
          startDate: startOfWeek,
          endDate: endOfWeek,
        });
  });
};

export const buildFunction = (programType: EProgramType) => {
  switch (programType) {
    case EProgramType.SHORT_STUDY_TOURS:
      return buildInitialProgramDayWithSchedule;
    case EProgramType.COMPETITIONS_OR_CONFERENCES:
      return buildInitialProgramDayWithSchedule;
    default:
      return buildInitialProgramWeekWithSchedule;
  }
};

export const ProgramScheduleForm: React.FC<Props> = (props) => {
  if (isNull(props.program.startDate) || isNull(props.program.endDate)) {
    return (
      <Grid item container spacing={2} direction="column">
        <Grid item>
          <Typography>Program's start and end date not set.</Typography>
        </Grid>
      </Grid>
    );
  }

  const startDate = parseISOString((props.program.startDate as any) as string);
  const endDate = parseISOString((props.program.endDate as any) as string);

  const [selected, setSelected] = React.useState(0);

  const validate = (values: ProgramSchedule) => {
    const hasFirstDetail = (detail: string[]) => !isEmpty(head(detail));
    const errors = values.map((item) => {
      switch (item.type) {
        case "week":
          return {};
        case "day":
          return {
            details: mapValues(omitBy(item.details, hasFirstDetail), () => [
              requiredText,
            ]),
          };
      }
    });
    return omitBy(zipObject(range(values.length), errors), (i) =>
      isEmpty(i?.details)
    );
  };

  const handleSubmit = async (schedule: ProgramSchedule) => {
    const newProgram = await ProgramsServiceForBusiness.saveSchedule(
      props.program.id,
      schedule
    );
    props.afterSubmit(newProgram);
  };

  return (
    <Formik
      initialValues={buildFunction(props.program.type)(
        props.program.schedule,
        startDate,
        endDate
      )}
      onSubmit={handleSubmit}
      validate={validate}
    >
      {(formikProps) => {
        return (
          <Form>
            {differenceInDays(startDate, endDate) < 0 ? (
              <Grid item container spacing={2} direction="column">
                <Grid item>
                  <Typography>
                    Program's start date is greater than end date.
                  </Typography>
                </Grid>
              </Grid>
            ) : (
              <Grid item xs={7} container spacing={3} direction="column">
                <Grid item container spacing={1} direction="column">
                  <Grid item>
                    <Typography style={{ fontSize: "18px", fontWeight: 500 }}>
                      {(() => {
                        const scheduleItem = formikProps.values[selected];
                        switch (scheduleItem.type) {
                          case "week":
                            return "Weekly Schedule";
                          case "day":
                            return "Daily Schedule";
                        }
                      })()}
                    </Typography>
                  </Grid>
                  <Grid item container direction="row">
                    <ProgramSchedulerSelectionButton
                      schedule={formikProps.values}
                      selected={selected}
                      setSelected={setSelected}
                    />
                  </Grid>
                  <Grid item>
                    {(() => {
                      const scheduleItem = formikProps.values[selected];
                      switch (scheduleItem.type) {
                        case "week":
                          return (
                            <ProgramWeekSchedulerSubform
                              week={scheduleItem}
                              selected={selected}
                            />
                          );
                        case "day":
                          return (
                            <ProgramDaySchedulerSubform
                              day={scheduleItem}
                              selected={selected}
                            />
                          );
                      }
                    })()}
                  </Grid>
                </Grid>
                <Grid item>
                  <Button
                    type="submit"
                    variant="contained"
                    color="primary"
                    onClick={() => {
                      //set selected to first schedule item with error
                      if (
                        !isEmpty(formikProps.errors) &&
                        isEmpty(formikProps.errors[selected])
                      ) {
                        setSelected(
                          parseInt(
                            findKey(formikProps.errors, (i) => !isEmpty(i))!
                          )
                        );
                      }
                    }}
                  >
                    Save and Go Back
                  </Button>
                </Grid>
              </Grid>
            )}
          </Form>
        );
      }}
    </Formik>
  );
};

type Props = {
  program: Program;
  afterSubmit: (program: Program) => void;
};

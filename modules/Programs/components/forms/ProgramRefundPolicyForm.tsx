import React from "react";

import { Button, Grid } from "@material-ui/core";
import { Form, Formik } from "formik";
import { isNull } from "lodash";

import { EProgramRefundTypes } from "../../enums/EProgramRefundTypes";
import {
  ProgramRefundPolicy,
  ProgramRefundPolicySchema,
} from "../../types/ProgramRefundPolicy";
import { Program } from "../../types/Programs.types";
import { RefundPolicyApplicationSubform } from "./RefundPolicyApplicationSubform";
import { RefundPolicyProgramSubform } from "./RefundPolicyProgramSubform";
import { ProgramsServiceForBusiness } from "../../services/ProgramsServiceForBusiness";

export const ProgramRefundPolicyForm: React.FC<Props> = (props) => {
  const refundPolicy = props.program.refundPolicy;

  const defaultRefundForParticipants = {
    refundType: EProgramRefundTypes.FULL_REFUND,
  };

  const defaultRefundPolicy: ProgramRefundPolicy = {
    fullRefundBeforeDate: null,
    noRefundAfterDate: null,
    variableRefundPercentage: 50,
    variableRefundBeforeDate: null,
    applicationFee: {
      acceptedParticipants: defaultRefundForParticipants,
      rejectedParticipants: defaultRefundForParticipants,
    },
  };

  const buildInitialValues = () => {
    return isNull(refundPolicy) ? defaultRefundPolicy : refundPolicy;
  };

  const handleSubmit = async (refundPolicy: ProgramRefundPolicy) => {
    const newProgram = await ProgramsServiceForBusiness.saveRefundPolicy(
      props.program.id,
      refundPolicy
    );

    props.afterSubmit(newProgram);
  };

  return (
    <Formik
      validationSchema={ProgramRefundPolicySchema}
      initialValues={buildInitialValues()}
      onSubmit={handleSubmit}
    >
      <Form>
        <Grid container spacing={4} direction="column">
          <RefundPolicyApplicationSubform />

          <RefundPolicyProgramSubform
            startDate={new Date(props.program.startDate)}
          />

          <Grid item>
            <Button variant="contained" color="primary" type="submit">
              Save and Go Back
            </Button>
          </Grid>
        </Grid>
      </Form>
    </Formik>
  );
};

type Props = {
  program: Program;
  afterSubmit: (program: Program) => void;
};

import React from "react";

import { Grid } from "@material-ui/core";
import { Field } from "formik";
import { CheckboxWithLabel } from "formik-material-ui";
import { TextField } from "material-ui-formik-components";

import { MultiImageUploader } from "../../../Shared/components/MultiImageUploader";
import { ProgramTravelDetails } from "../../types/ProgramTravelDetails";
import TitleWithSubtitleFragment from "../../../Shared/components/TitleWithSubheaderAndSubtitle";

export const ProgramTravelDetailsSubform: React.FC<Props> = ({
  values,
  namespace,
  images,
  onChange,
}) => {
  const buildFieldName = (fieldName: string) => {
    if (namespace) return `${namespace}.${fieldName}`;

    return fieldName;
  };

  const requiresExternalTourDetails =
    values.includesTourPackage && !values.fromOrganisingTeam;

  return (
    <React.Fragment>
      <Grid item container direction="column" spacing={2}>
        <Grid item>
          <TitleWithSubtitleFragment
            title="Travel"
            subtitle="So how?"
            withoutBottomMargin
          />
        </Grid>
        <Grid item xs={12} md={4}>
          <Field
            defaultChecked={values.includesTourPackage}
            component={CheckboxWithLabel}
            name={buildFieldName("includesTourPackage")}
            Label={{ label: "Includes tour package" }}
            color="primary"
          />
        </Grid>

        <Grid item xs={12} md={4}>
          <Field
            defaultChecked={values.fromOrganisingTeam}
            component={CheckboxWithLabel}
            name={buildFieldName("fromOrganisingTeam")}
            Label={{ label: "Tour package from organising team" }}
            color="primary"
            disabled={!values.includesTourPackage}
          />
        </Grid>

        <Grid item>
          <Field
            component={TextField}
            name={buildFieldName("tourDescription")}
            label="Description of The Tour"
            fullWidth
            type="text"
            variant="outlined"
            required
            style={{ margin: 0 }}
            InputLabelProps={{
              shrink: true,
            }}
            placeholder="Tell us more about the tour that this vendor is providing"
            multiline
            rows={4}
            disabled={!values.includesTourPackage}
          />
        </Grid>

        <Grid item>
          <Field
            component={TextField}
            name={buildFieldName("externalTourVendor.name")}
            label="Name of Travel Vendor"
            fullWidth
            type="text"
            variant="outlined"
            required={requiresExternalTourDetails}
            style={{ margin: 0 }}
            InputLabelProps={{
              shrink: true,
            }}
            placeholder="We need to know the name of your vendor"
            disabled={!requiresExternalTourDetails}
          />
        </Grid>

        <Grid item>
          <Field
            component={TextField}
            name={buildFieldName("externalTourVendor.vendorDescription")}
            label="Description of Travel Vendor"
            fullWidth
            type="text"
            variant="outlined"
            required={requiresExternalTourDetails}
            style={{ margin: 0 }}
            InputLabelProps={{
              shrink: true,
            }}
            placeholder="Tell us more about this vendor"
            multiline
            rows={4}
            disabled={!requiresExternalTourDetails}
          />
        </Grid>

        <MultiImageUploader
          count={6}
          initialImages={images}
          onChange={onChange}
        />
      </Grid>
    </React.Fragment>
  );
};

type Props = {
  namespace: string;
  values: ProgramTravelDetails;
  images: string[];
  onChange: (newImages: string[]) => void;
};

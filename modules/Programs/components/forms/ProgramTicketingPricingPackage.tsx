import React from "react";

import { Button, Grid } from "@material-ui/core";
import { Formik, Form } from "formik";
import { isNull } from "lodash";

import { FormSubheaderWithChildren } from "../../../Shared/components/FormSubheaderWithChildren";
import {
  EProgramTicketPricingPackages,
  ProgramTicketingPricingPackageValues,
} from "../../enums/EProgramTicketPricingPackages";
import { ProgramsServiceForBusiness } from "../../services/ProgramsServiceForBusiness";
import { Program } from "../../types/Programs.types";
import { RadioGroupCard } from "../../../Shared/components/RadioGroupCard";

export const ProgramTicketingPricingPackage: React.FC<Props> = (props) => {
  const [selectedPackage, setSelectedPackage] = React.useState(
    isNull(props.program.ticketingPackage)
      ? EProgramTicketPricingPackages.ENTERPRISE
      : props.program.ticketingPackage
  );

  const handleSubmit = async () => {
    props.afterSubmit(
      await ProgramsServiceForBusiness.saveTicketingPackage(
        props.program.id,
        selectedPackage
      )
    );
  };

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setSelectedPackage(e.target.value as EProgramTicketPricingPackages);
  };

  return (
    <Formik initialValues={{}} onSubmit={handleSubmit}>
      {({ isSubmitting }) => (
        <Form>
          <Grid container spacing={4} direction="column">
            <FormSubheaderWithChildren
              title="Which package fits your program?"
              subtitle="For now, only the enterprise package is available"
            >
              <Grid item>
                <RadioGroupCard
                  direction="row"
                  value={selectedPackage}
                  values={ProgramTicketingPricingPackageValues}
                  isSubmitting={isSubmitting}
                  handleChange={handleChange}
                />
              </Grid>
            </FormSubheaderWithChildren>

            <Grid item>
              <Button variant="contained" color="primary" type="submit">
                Next step
              </Button>
            </Grid>
          </Grid>
        </Form>
      )}
    </Formik>
  );
};

type Props = {
  program: Program;
  afterSubmit: (program: Program) => void;
};

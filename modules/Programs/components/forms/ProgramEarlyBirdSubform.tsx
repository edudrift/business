import React from "react";

import { Grid } from "@material-ui/core";
import { Field, useFormikContext } from "formik";
import { DatePicker, Select } from "material-ui-formik-components";

import {
  formatDateWithFullDate,
  minusDaysFromDate,
} from "../../../../utils/dateUtils";
import { TypographyWithPxFontAndColor } from "../../../Shared/components/TypographyWithPxFontAndColor";
import { ProgramEarlyBird } from "../../types/ProgramEarlyBird.types";
import { FormSubheaderWithSubtitle } from "~/modules/Shared/components/FormSubheaderWithSubtitle";
import { isNull } from "lodash";
import { FormikTextField } from "../../../../components/Forms/components/FormikTexField";
import { RadioGroupBoolean } from "../../../Shared/components/RadioGroupBoolean";

export const ProgramEarlyBirdSubform: React.FC<Props> = ({
  earlyBird,
  applicationDeadline,
  setEarlyBird,
}) => {
  const defaultEarlyBird = {
    startDate: minusDaysFromDate(applicationDeadline, 14),
    endDate: minusDaysFromDate(applicationDeadline, 7),
    discountedAmount: 10,
    discountType: "PERCENTAGE",
  };
  const [
    previousEarlyBird,
    saveEarlyBird,
  ] = React.useState<ProgramEarlyBird | null>(defaultEarlyBird);

  const [hasEarlyBird, setHasEarlyBird] = React.useState(!isNull(earlyBird));

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const earlyBirdPricingSelected = event.target.value === "true";
    setHasEarlyBird(earlyBirdPricingSelected);
    if (earlyBirdPricingSelected) {
      setEarlyBird(previousEarlyBird);
    } else {
      saveEarlyBird(earlyBird);
      setEarlyBird(null);
    }
  };

  const { errors, touched, isSubmitting } = useFormikContext();

  return (
    <Grid item container spacing={2} alignItems="center">
      <Grid item xs={12}>
        <FormSubheaderWithSubtitle title="EARLY BIRD DISCOUNT PACKAGE" />
      </Grid>
      <Grid item>
        <RadioGroupBoolean
          value={hasEarlyBird}
          handleChange={handleChange}
          noLabel="No. We do not provide early bird pricing"
          yesLabel="Yes, we have early bird pricing"
          isSubmitting={isSubmitting}
        >
          <Grid item container spacing={2}>
            <Grid item xs={12} md={3}>
              <Field
                name="earlyBird.startDate"
                component={DatePicker}
                label="Start date"
                format="d MMMM yyyy"
                inputVariant="outlined"
                style={{ margin: 0 }}
                disabled={!hasEarlyBird}
                maxDate={applicationDeadline}
                required
              />
            </Grid>

            <Grid item xs={12} md={3}>
              <Field
                name="earlyBird.endDate"
                component={DatePicker}
                label="End date"
                format="d MMMM yyyy"
                inputVariant="outlined"
                style={{ margin: 0 }}
                disabled={!hasEarlyBird}
                minDate={earlyBird?.startDate}
                required
              />
            </Grid>

            <Grid item xs={12} md={3}>
              <Field
                name="earlyBird.discountType"
                label="Discount type"
                options={[
                  {
                    value: "PERCENTAGE",
                    label: "Percentage",
                  },
                  {
                    value: "FIXED",
                    label: "Fixed amount",
                  },
                ]}
                component={Select}
                variant="outlined"
                style={{ margin: 0 }}
                InputLabelProps={{
                  shrink: true,
                }}
                disabled={!hasEarlyBird}
                required
              />
            </Grid>

            <Grid item xs={12} md={3}>
              <FormikTextField
                name="earlyBird.discountedAmount"
                label="Discounted amount"
                type="number"
                disabled={!hasEarlyBird}
                required
                errors={errors}
                touched={touched}
              />
            </Grid>

            <Grid item xs={12}>
              <TypographyWithPxFontAndColor sizeInPx={14} color="#666">
                Application deadline:{" "}
                {formatDateWithFullDate(applicationDeadline)}
              </TypographyWithPxFontAndColor>
            </Grid>
          </Grid>
        </RadioGroupBoolean>
      </Grid>
    </Grid>
  );
};

type Props = {
  earlyBird: ProgramEarlyBird | null;
  applicationDeadline: Date;
  setEarlyBird: (earlyBird: ProgramEarlyBird | null) => void;
};

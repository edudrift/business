import React from "react";

import { Grid } from "@material-ui/core";
import { Field } from "formik";
import { Select } from "material-ui-formik-components";

import {
  ArrayOfCurrencies,
  EProgramCurrencyLabels,
} from "../../enums/EProgramCurrencies";
import { FormSubheaderWithChildren } from "../../../Shared/components/FormSubheaderWithChildren";

export const ProgramCurrencySelect: React.FC = () => {
  return (
    <FormSubheaderWithChildren
      title="PRICING"
      subtitle="The amount that each participant to pay to secure their spot
                  in the program. This fee is non-refundable and should be
                  calculated in as the processing fee for the participation. In
                  case of a dispute due to miscommunications or an approved
                  exception, our team will be able to facilitate the refund"
    >
      <Grid item xs={12} md={4}>
        <Field
          name="currency"
          component={Select}
          options={ArrayOfCurrencies.map((curr) => ({
            value: curr,
            label: EProgramCurrencyLabels[curr],
          }))}
          label="Currency"
          required
          variant="outlined"
        />
      </Grid>
    </FormSubheaderWithChildren>
  );
};

import React from "react";

import { Grid } from "@material-ui/core";
import { Field } from "formik";
import { TextField } from "material-ui-formik-components";
import { MultiImageUploader } from "../../../Shared/components/MultiImageUploader";
import TitleWithSubtitleFragment from "../../../Shared/components/TitleWithSubheaderAndSubtitle";

export const ProgramDiningDetailsSubform: React.FC<Props> = ({
  namespace,
  images,
  onChange,
}) => {
  const buildFieldName = (name: string) => `${namespace}.${name}`;

  return (
    <React.Fragment>
      <Grid item container direction="column" spacing={2}>
        <Grid item>
          <TitleWithSubtitleFragment
            title="Dining"
            subtitle="Tell us more about the food that is provided"
            withoutBottomMargin
          />
        </Grid>
        <Grid item>
          <Field
            component={TextField}
            name={buildFieldName("providedMeals")}
            label="Provided meals"
            fullWidth
            type="text"
            variant="outlined"
            required
            style={{ margin: 0 }}
            InputLabelProps={{
              shrink: true,
            }}
            placeholder="Which meals are provided?"
          />
        </Grid>

        <Grid item>
          <Field
            component={TextField}
            name={buildFieldName("mealsDetails")}
            label="Details of Provided Meals"
            fullWidth
            type="text"
            variant="outlined"
            required
            style={{ margin: 0 }}
            InputLabelProps={{
              shrink: true,
            }}
            placeholder="More details about the meals that are provided"
            multiline
            rows={4}
          />
        </Grid>

        <MultiImageUploader
          count={6}
          initialImages={images}
          onChange={onChange}
        />
      </Grid>
    </React.Fragment>
  );
};

type Props = {
  namespace: string;
  images: string[];
  onChange: (newImages: string[]) => void;
};

import React from "react";

import {
  Button,
  Card,
  CardContent,
  FormControlLabel,
  Grid,
  Radio,
  RadioGroup,
} from "@material-ui/core";

import { TypographyWithPxFontAndColor } from "../../../Shared/components/TypographyWithPxFontAndColor";
import DialogPopupAlert from "../../../Shared/components/DialogPopupAlert";
import TokenService from "../../../../services/Token.service";
import { EProgramAPIRoutes } from "../../../../utils/EAPIRoutes";
import EHttpMethods from "../../../../utils/EHttpMethods";
import isofetch from "../../../../utils/isofetch";
import { ProgramsServiceForBusiness } from "../../services/ProgramsServiceForBusiness";
import {
  Program,
  EProgramType,
  EProgramTypeDescriptions,
  EProgramTypeLabels,
} from "../../types/Programs.types";

const ProgramTypeForm: React.FC<Props> = (props) => {
  const [value, setValue] = React.useState(props.program.type);
  const [submitting, setSubmitting] = React.useState(false);
  const [open, setOpen] = React.useState(false);
  const [approvedProgramTypes, setApprovedProgramTypes] = React.useState<
    EProgramType[]
  >([]);

  const hasData = props.program.schedule != null;
  const hasChanged = props.program.type !== value;

  React.useEffect(() => {
    const fetchData = async () => {
      const response = await isofetch<{ programType: EProgramType }[]>(
        EProgramAPIRoutes.GET_APPROVED_PROGRAM_TYPES,
        EHttpMethods.GET,
        TokenService.getToken()
      );
      if (response.status === 200 && response.data) {
        setApprovedProgramTypes(response.data.map((d) => d.programType));
      }
    };

    fetchData();
  }, []);

  const handleRadioChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setValue((event.target as HTMLInputElement).value as EProgramType);
  };

  const handleSubmit = async () => {
    setSubmitting(true);
    const newProgram = await ProgramsServiceForBusiness.saveProgramType(
      props.program.id,
      value
    );
    props.afterSubmit(newProgram);
    setSubmitting(false);
  };

  const handleClose = () => setOpen(false);
  const handleNext = hasData && hasChanged ? () => setOpen(true) : handleSubmit;

  return (
    <React.Fragment>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <RadioGroup
            aria-label="quiz"
            name="quiz"
            value={value}
            onChange={handleRadioChange}
          >
            <Grid container direction="column" spacing={2}>
              {[
                EProgramType.SUMMER_WINTER_SCHOOL,
                EProgramType.COMPETITIONS_OR_CONFERENCES,
                EProgramType.LOCAL_ENRICHMENT_CLASSES,
                EProgramType.OVERSEAS_INTERNSHIPS,
                EProgramType.SHORT_STUDY_TOURS,
              ].map((pt) => {
                return (
                  <Grid item key={pt}>
                    <Card variant="outlined">
                      <CardContent>
                        <Grid container spacing={2} direction="column">
                          <Grid item>
                            <FormControlLabel
                              value={pt}
                              control={
                                <Radio
                                  color="primary"
                                  disabled={!approvedProgramTypes.includes(pt)}
                                />
                              }
                              label={EProgramTypeLabels[pt]}
                            />
                          </Grid>

                          <Grid item>
                            <TypographyWithPxFontAndColor
                              sizeInPx={16}
                              color={
                                approvedProgramTypes.includes(pt)
                                  ? ""
                                  : "rgba(0, 0, 0, 0.38)"
                              }
                            >
                              {EProgramTypeDescriptions[pt]}
                            </TypographyWithPxFontAndColor>
                          </Grid>
                        </Grid>
                      </CardContent>
                    </Card>
                  </Grid>
                );
              })}
            </Grid>
          </RadioGroup>
        </Grid>
        <Grid item container spacing={2}>
          <Grid item>
            <Button
              variant="contained"
              color="primary"
              onClick={handleNext}
              disabled={submitting}
            >
              Next Step
            </Button>
            <DialogPopupAlert
              open={hasData && open}
              text={
                "Program Providers shall try not to change the program type    \
                  after entering program details, as some of the data entered  \
                  might be specific to certain program types and would be lost \
                  if the program type is changed."
              }
              onClose={handleClose}
              onConfirm={handleSubmit}
            />
          </Grid>
        </Grid>
      </Grid>
    </React.Fragment>
  );
};

type Props = {
  program: Program;
  afterSubmit: (program: Program) => void;
};

export default ProgramTypeForm;

import React from "react";

import { useFormikContext } from "formik";
import { Grid } from "@material-ui/core";

import {
  EProgramMethodology,
  EProgramMethodologyLabels,
} from "../../enums/EProgramMethodology";
import { FormSubheaderWithChildren } from "../../../Shared/components/FormSubheaderWithChildren";
import { CheckboxGroupMd9 } from "../../../Shared/components/CheckboxGroupMd9";

export const ProgramMethodologySubform: React.FC = () => {
  const { values, setFieldValue } = useFormikContext<{
    methodology: EProgramMethodology[];
  }>();

  return (
    <FormSubheaderWithChildren title="Methodology/Pedagogy">
      <Grid item>
        <CheckboxGroupMd9
          checked={values.methodology}
          values={Object.values(EProgramMethodology)}
          labels={Object.values(EProgramMethodologyLabels)}
          setValues={(v) => setFieldValue("methodology", v)}
        />
      </Grid>
    </FormSubheaderWithChildren>
  );
};

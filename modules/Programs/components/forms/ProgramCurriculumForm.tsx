import React from "react";

import { Button, Grid } from "@material-ui/core";
import { Formik, Form } from "formik";
import * as Y from "yup";
import { ProgramsServiceForBusiness } from "../../services/ProgramsServiceForBusiness";
import { Program } from "../../types/Programs.types";
import { oneOfEnum } from "~/utils/yupUtils";
import { ProgramSkillsSubForm } from "./ProgramSkillsSubForm";
import { ProgramMethodologySubform } from "./ProgramMethodologySubform";
import { ProgramAssessmentSubform } from "./ProgramAssessmentSubform";
import { EProgramSkill } from "../../enums/EProgramSkill";
import { EProgramMethodology } from "../../enums/EProgramMethodology";
import { EProgramAssessment } from "../../enums/EProgramAssessment";
import { FormikTextField } from "~/components/Forms/components/FormikTexField";
import { FormSubheaderWithSubtitle } from "~/modules/Shared/components/FormSubheaderWithSubtitle";

export const ProgramCurriculumForm: React.FC<Props> = ({
  program,
  afterSubmit,
}) => {
  const buildInitialValues = () => ({
    minAge: program.minAge,
    maxAge: program.maxAge,
    skills: program.skills || [],
    methodology: program.methodology || [],
    assessment: program.assessment || [],
  });

  const handleSubmit = async ({
    minAge,
    maxAge,
    skills,
    methodology,
    assessment,
  }: formikValues) => {
    const updatedProgram = await ProgramsServiceForBusiness.saveCurriculum(
      program.id,
      minAge,
      maxAge,
      skills.sort(),
      methodology.sort(),
      assessment.sort()
    );
    afterSubmit(updatedProgram);
  };

  return (
    <Formik
      initialValues={buildInitialValues()}
      validationSchema={Y.object({
        skills: Y.array().of(oneOfEnum(EProgramSkill)),
        methodology: Y.array().of(oneOfEnum(EProgramMethodology)),
        assessment: Y.array().of(oneOfEnum(EProgramAssessment)),
      })}
      onSubmit={handleSubmit}
    >
      {() => {
        return (
          <Form style={{ margin: "2em 0" }}>
            <Grid container spacing={2} direction="column">
              <Grid item>
                <FormSubheaderWithSubtitle title="Target Age Group" />
              </Grid>
              <Grid item container spacing={2} xs={12} md={6}>
                <Grid item xs>
                  <FormikTextField
                    type="number"
                    name="minAge"
                    label="Minimum Age"
                    required
                  />
                </Grid>

                <Grid item xs>
                  <FormikTextField
                    type="number"
                    name="maxAge"
                    label="Maximum Age"
                    required
                  />
                </Grid>
              </Grid>
              <ProgramSkillsSubForm />
              <ProgramMethodologySubform />
              <ProgramAssessmentSubform />
              <Grid item>
                <Button color="primary" variant="contained" type="submit">
                  Next Step
                </Button>
              </Grid>
            </Grid>
          </Form>
        );
      }}
    </Formik>
  );
};

type Props = {
  program: Program;
  afterSubmit: (program: Program) => void;
};

type formikValues = {
  minAge: number | null;
  maxAge: number | null;
  skills: EProgramSkill[];
  methodology: EProgramMethodology[];
  assessment: EProgramAssessment[];
};

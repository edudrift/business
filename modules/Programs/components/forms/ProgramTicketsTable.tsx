import React from "react";

import { AppBar, Button, Grid, Tab, Tabs } from "@material-ui/core";
import { isEqual, range, find } from "lodash";
import { useFormikContext } from "formik";

import theme from "../../../../theme";
import { FormSubheaderWithChildren } from "../../../Shared/components/FormSubheaderWithChildren";
import { EditTicketForm } from "../../../Ticket/components/EditTicketForm";
import { Program } from "../../types/Programs.types";
import { ProgramTicket } from "../../types/ProgramTicket.types";
import { ProgramTicketType } from "../../types/ProgramTicketType.types";

export const ProgramTicketsTable: React.FC<Props> = (props) => {
  const ticketTypes = props.ticketTypes;
  const ticketsProps = props.tickets;

  const { values, setFieldValue, setFieldTouched } = useFormikContext<
    formikValues
  >();

  const hasNoUnsavedChanges = values.tickets.every((t, i) =>
    isEqual(t, ticketsProps[i])
  );

  const [activeTabIndex, setActiveTabIndex] = React.useState(0);

  const buildNewTicket = () => ({
    name: "",
    description: "",
    price: "",
    typeId: ticketTypes[activeTabIndex].id,
  });

  const handleAdd = () => {
    setFieldValue("tickets", [...values.tickets, buildNewTicket()]);
    setFieldTouched(`tickets[${values.tickets.length}]`, false);
  };

  const handleChange = (_: any, newValue: number) => {
    setActiveTabIndex(newValue);
  };

  return (
    <FormSubheaderWithChildren
      title="PACKAGE"
      subtitle="One package consists of member classification and accommodation criteria"
    >
      <Grid item>
        <AppBar
          position="static"
          color="default"
          style={{ boxShadow: "none", backgroundColor: "transparent" }}
        >
          <Tabs
            value={activeTabIndex}
            onChange={handleChange}
            indicatorColor="primary"
            textColor="primary"
            style={{ marginBottom: theme.spacing(2) }}
          >
            {props.ticketTypes.map((ticketType) => {
              return <Tab label={ticketType.name} key={ticketType.id} />;
            })}
          </Tabs>
        </AppBar>
        <Grid item container spacing={2}>
          {range(values.tickets.length)
            .filter(
              (i) => values.tickets[i].typeId === ticketTypes[activeTabIndex].id
            )
            .map((index) => (
              <EditTicketForm
                key={index}
                index={index}
                programId={props.programId}
                ticket={find(ticketsProps, { id: values.tickets[index].id })}
                afterSubmit={(ticket?: ProgramTicket) => {
                  const updatedTickets = ((): ProgramTicket[] => {
                    if (ticket) {
                      return values.tickets.map((t, i) =>
                        i === index ? ticket : t
                      );
                    } else {
                      return values.tickets.filter((_, i) => i !== index);
                    }
                  })();
                  props.onUpdate(updatedTickets);
                  setFieldValue("tickets", updatedTickets);
                }}
              />
            ))}
          {hasNoUnsavedChanges && (
            <Grid item>
              <Button variant="outlined" color="primary" onClick={handleAdd}>
                + Add ticket
              </Button>
            </Grid>
          )}
        </Grid>
      </Grid>
    </FormSubheaderWithChildren>
  );
};

type Props = {
  ticketTypes: ProgramTicketType[];
  tickets: ProgramTicket[];
  programId: Program["id"];
  onUpdate: (tickets: ProgramTicket[]) => void;
};

interface formikValues {
  tickets: ProgramTicket[];
}

import React from "react";

import { Grid } from "@material-ui/core";
import { useFormikContext } from "formik";

import { EProgramSkill, EProgramSkillLabels } from "../../enums/EProgramSkill";
import { CheckboxGroupMd9 } from "../../../Shared/components/CheckboxGroupMd9";
import { FormSubheaderWithChildren } from "../../../Shared/components/FormSubheaderWithChildren";

export const ProgramSkillsSubForm: React.FC = () => {
  const { values, setFieldValue } = useFormikContext<{
    skills: EProgramSkill[];
  }>();
  return (
    <FormSubheaderWithChildren title="Skills to Cultivate">
      <Grid item>
        <CheckboxGroupMd9
          checked={values.skills}
          values={Object.values(EProgramSkill)}
          labels={Object.values(EProgramSkillLabels)}
          setValues={(v) => setFieldValue("skills", v)}
        />
      </Grid>
    </FormSubheaderWithChildren>
  );
};

import React from "react";

import { Button, Grid } from "@material-ui/core";
import { Form, Formik } from "formik";

import { ProgramsServiceForBusiness } from "../../services/ProgramsServiceForBusiness";
import { ProgramAcademicDetails } from "../../types/ProgramAcademicDetails.types";
import { ProgramDiningDetails } from "../../types/ProgramDiningDetails.types";
import { ProgramEssentials } from "../../types/ProgramEssentials.types";
import { Program } from "../../types/Programs.types";
import { ProgramTravelDetails } from "../../types/ProgramTravelDetails";
import { ProgramAcademicDetailsSubform } from "./ProgramAcademicDetailsSubform";
import { ProgramDiningDetailsSubform } from "./ProgramDiningDetailsSubform";
import { ProgramTravelDetailsSubform } from "./ProgramTravelDetailsSubform";

export const ProgramEssentialsForm: React.FC<Props> = (props) => {
  const [academicImages, setAcademicImages] = React.useState(
    props.program.essentials?.academic?.images || []
  );
  const [travelImages, setTravelImages] = React.useState(
    props.program.essentials?.travel?.images || []
  );

  const [diningImages, setDiningImages] = React.useState(
    props.program.essentials?.dining?.images || []
  );

  const buildInitialValues = () => {
    const defaultTravel: ProgramTravelDetails = {
      includesTourPackage: false,
      fromOrganisingTeam: false,
      tourDescription: "",
      externalTourVendor: {
        name: "",
        vendorDescription: "",
      },
      images: [],
    };
    const hasTravel = !!props.program.essentials?.travel;
    const travel = {
      includesTourPackage: hasTravel
        ? props.program.essentials.travel.includesTourPackage
        : defaultTravel.includesTourPackage,
      fromOrganisingTeam:
        hasTravel && props.program.essentials.travel.fromOrganisingTeam
          ? props.program.essentials.travel.fromOrganisingTeam
          : defaultTravel.fromOrganisingTeam,
      tourDescription:
        hasTravel && props.program.essentials.travel.tourDescription
          ? props.program.essentials.travel.tourDescription
          : defaultTravel.tourDescription,
      externalTourVendor:
        hasTravel && props.program.essentials.travel.externalTourVendor
          ? props.program.essentials.travel.externalTourVendor
          : defaultTravel.externalTourVendor,
      images:
        hasTravel && props.program.essentials.travel.images
          ? props.program.essentials.travel.images
          : defaultTravel.images,
    };

    const academic: ProgramAcademicDetails = {
      lectureDescription: "",
      teachingStaffDescription: "",
      certificationDescription: "",
      images: [],
    };

    const dining: ProgramDiningDetails = {
      providedMeals: "",
      mealsDetails: "",
      images: [],
    };

    return {
      travel,
      academic: props.program.essentials?.academic
        ? props.program.essentials.academic
        : academic,
      dining: props.program.essentials?.dining
        ? props.program.essentials.dining
        : dining,
    };
  };

  const handleSubmit = async (essentials: ProgramEssentials) => {
    essentials.academic.images = academicImages;
    essentials.travel.images = travelImages;
    essentials.dining.images = diningImages;

    const newProgram = await ProgramsServiceForBusiness.saveEssentials(
      props.program.id,
      essentials
    );
    props.afterSubmit(newProgram);
  };

  return (
    <Formik initialValues={buildInitialValues()} onSubmit={handleSubmit}>
      {({ values }) => {
        return (
          <Form>
            <Grid container spacing={5} direction="column" item>
              <ProgramAcademicDetailsSubform
                namespace="academic"
                images={academicImages}
                onChange={(newImages: string[]) => {
                  setAcademicImages(newImages);
                }}
              />

              <ProgramTravelDetailsSubform
                namespace="travel"
                values={values.travel}
                images={travelImages}
                onChange={(newImages: string[]) => {
                  setTravelImages(newImages);
                }}
              />

              <ProgramDiningDetailsSubform
                namespace="dining"
                images={diningImages}
                onChange={(newImages: string[]) => {
                  setDiningImages(newImages);
                }}
              />

              <Grid item>
                <Button type="submit" variant="contained" color="primary">
                  Next step
                </Button>
              </Grid>
            </Grid>
          </Form>
        );
      }}
    </Formik>
  );
};

type Props = {
  program: Program;
  afterSubmit: (program: Program) => void;
};

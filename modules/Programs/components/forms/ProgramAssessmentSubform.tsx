import React from "react";

import { useFormikContext } from "formik";
import { Grid } from "@material-ui/core";

import {
  EProgramAssessment,
  EProgramAssessmentLabels,
} from "../../enums/EProgramAssessment";
import { FormSubheaderWithChildren } from "../../../Shared/components/FormSubheaderWithChildren";
import { CheckboxGroupMd9 } from "../../../Shared/components/CheckboxGroupMd9";

export const ProgramAssessmentSubform: React.FC = () => {
  const { values, setFieldValue } = useFormikContext<{
    assessment: EProgramAssessment[];
  }>();

  return (
    <FormSubheaderWithChildren title="Assessment">
      <Grid item>
        <CheckboxGroupMd9
          checked={values.assessment}
          values={Object.values(EProgramAssessment)}
          labels={Object.values(EProgramAssessmentLabels)}
          setValues={(v) => setFieldValue("assessment", v)}
        />
      </Grid>
    </FormSubheaderWithChildren>
  );
};

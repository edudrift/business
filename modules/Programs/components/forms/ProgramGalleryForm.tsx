import { Button, Grid } from "@material-ui/core";
import { Form, Formik } from "formik";
import React from "react";
import { FormSubheaderWithSubtitle } from "~/modules/Shared/components/FormSubheaderWithSubtitle";
import { MultiImageUploader } from "~/modules/Shared/components/MultiImageUploader";
import { ProgramsServiceForBusiness } from "../../services/ProgramsServiceForBusiness";
import { ProgramApplicationSchema } from "../../types/ProgramApplication.types";
import { Program } from "../../types/Programs.types";

export const ProgramGalleryForm: React.FC<Props> = (props) => {
  const [mainImages, setMainImages] = React.useState([props.program.mainImage]);
  const [featuredImages, setFeaturedImages] = React.useState(
    props.program.featuredImages || []
  );
  const [images, setImages] = React.useState(props.program.images || []);
  const [images2, setImages2] = React.useState(
    props.program.images?.slice(3, 6) || []
  );
  const [images3, setImages3] = React.useState(
    props.program.images?.slice(6, 9) || []
  );

  const handleSubmit = async () => {
    if (mainImages.length !== 1) {
      alert("Please upload main program image");
      return;
    }

    if (featuredImages.length !== 3) {
      alert("Please upload all three program featured images ");
      return;
    }
    const updatedProgram = await ProgramsServiceForBusiness.saveGallery(
      props.program.id,
      mainImages[0],
      featuredImages,
      images.concat(images2).concat(images3)
    );
    props.afterSubmit(updatedProgram);
  };

  const buildInitialValues = () => ({});

  return (
    <Formik
      validationSchema={ProgramApplicationSchema}
      initialValues={buildInitialValues()}
      onSubmit={handleSubmit}
      validateOnMount
    >
      <Form>
        <Grid container spacing={4} direction="column">
          <Grid item xs={6} md={6}>
            <FormSubheaderWithSubtitle title="Main Program Images*" />
            <MultiImageUploader
              count={1}
              initialImages={mainImages}
              onChange={(newImages) => setMainImages(newImages)}
            />
          </Grid>
          <Grid item container spacing={2} direction="column">
            <Grid item>
              <FormSubheaderWithSubtitle title="Three More Featured Images*" />
            </Grid>
            <Grid item>
              <MultiImageUploader
                count={3}
                initialImages={featuredImages}
                onChange={(newImages) => setFeaturedImages(newImages)}
              />
            </Grid>
          </Grid>
          <Grid
            item
            container
            spacing={2}
            direction="column"
            style={{ marginTop: "3em" }}
          >
            <Grid item>
              <FormSubheaderWithSubtitle title="Upload More images to the Gallery" />
            </Grid>
            <Grid item>
              <MultiImageUploader
                count={3}
                initialImages={images}
                onChange={(newImages) => setImages(newImages)}
              />
            </Grid>
            <Grid item>
              <MultiImageUploader
                count={3}
                initialImages={images2}
                onChange={(newImages) => setImages2(newImages)}
              />
            </Grid>
            <Grid item>
              <MultiImageUploader
                count={3}
                initialImages={images3}
                onChange={(newImages) => setImages3(newImages)}
              />
            </Grid>
          </Grid>
          <Grid item>
            <Button variant="contained" color="primary" type="submit">
              Save and Go Back
            </Button>
          </Grid>
        </Grid>
      </Form>
    </Formik>
  );
};

type Props = {
  program: Program;
  afterSubmit: (program: Program) => void;
};

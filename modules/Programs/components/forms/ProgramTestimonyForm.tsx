import {
  Button,
  FormControlLabel,
  Grid,
  makeStyles,
  Radio,
  RadioGroup,
} from "@material-ui/core";
import { Field, Form, Formik } from "formik";
import { DatePicker, TextField } from "material-ui-formik-components";
import React from "react";
import { FormikMultilineTextField } from "~/components/Forms/components/FormikMultilineTextField";
import { FormikTextField } from "~/components/Forms/components/FormikTexField";
import { EProgramTestimonyTypes } from "../../enums/EProgramTestimonyTypes";
import { ProgramsServiceForBusiness } from "../../services/ProgramsServiceForBusiness";
import { Program } from "../../types/Programs.types";
import {
  ProgramTestimonySchema,
  ProgramTestimonySchemaAsType,
} from "../../types/ProgramTestimony";

const useStyles = makeStyles((theme) => {
  return {
    label: {
      fontWeight: theme.typography.fontWeightBold,
      fontSize: theme.typography.pxToRem(14),
    },
  };
});

export const ProgramTestimonyForm: React.FC<Props> = ({
  program,
  afterSubmit,
}) => {
  const classes = useStyles();

  const [testimonyType, setTestimonyType] = React.useState(
    program.testimony?.testimonyType || EProgramTestimonyTypes.STUDENT
  );

  const buildInitialValues = () => {
    const { testimony } = program;
    const firstDayOfMonth = new Date();
    firstDayOfMonth.setDate(1);
    const initValues = {
      displayName: testimony?.displayName,
      datesOfAttendance: testimony?.datesOfAttendance || firstDayOfMonth,
      programType: testimony?.programType,
      programName: testimony?.programName,
      review: testimony?.review,
    };

    return initValues;
  };

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setTestimonyType(e.target.value as EProgramTestimonyTypes);
  };

  const handleSubmit = async (
    programTestimony: ProgramTestimonySchemaAsType
  ) => {
    console.log(programTestimony);
    const updatedProgram = await ProgramsServiceForBusiness.saveTestimony(
      program.id,
      programTestimony
    );
    afterSubmit(updatedProgram);
  };

  return (
    <Formik
      initialValues={buildInitialValues()}
      validationSchema={ProgramTestimonySchema}
      onSubmit={handleSubmit}
    >
      {({ isSubmitting }) => {
        return (
          <Form style={{ margin: "2em 0" }}>
            <Grid container spacing={2} direction="column">
              <Grid item xs={12} md={6}>
                <RadioGroup value={testimonyType} onChange={handleChange}>
                  <Grid item container spacing={1}>
                    <Grid item>
                      <Grid container direction="column">
                        <Grid item>
                          <FormControlLabel
                            value={EProgramTestimonyTypes.PARENT}
                            control={
                              <Radio color="primary" disabled={isSubmitting} />
                            }
                            label="Parent"
                            disabled={isSubmitting}
                            classes={{
                              label: classes.label,
                            }}
                          />
                        </Grid>
                      </Grid>
                    </Grid>

                    <Grid item>
                      <Grid container direction="column">
                        <Grid item>
                          <FormControlLabel
                            value={EProgramTestimonyTypes.STUDENT}
                            control={
                              <Radio color="primary" disabled={isSubmitting} />
                            }
                            label="Student"
                            disabled={isSubmitting}
                            classes={{
                              label: classes.label,
                            }}
                          />
                        </Grid>
                      </Grid>
                    </Grid>
                  </Grid>
                </RadioGroup>
              </Grid>
              <Grid item xs={12} md={6}>
                <FormikTextField
                  name="displayName"
                  label="Display Name"
                  required
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <Field
                  name="datesOfAttendance"
                  component={DatePicker}
                  label="Review for progrram attended in"
                  format="MMMM yyyy"
                  inputVariant="outlined"
                  style={{ margin: 0 }}
                  disableFuture
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <Field
                  component={TextField}
                  name="programType"
                  label="Program Type"
                  fullWidth
                  type="text"
                  variant="outlined"
                  style={{ margin: 0 }}
                  InputLabelProps={{
                    shrink: true,
                  }}
                  placeholder="Program Type (e.g. Trial)"
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <Field
                  component={TextField}
                  name="programName"
                  label="Program Name"
                  fullWidth
                  type="text"
                  variant="outlined"
                  style={{ margin: 0 }}
                  InputLabelProps={{
                    shrink: true,
                  }}
                  placeholder="Program Name"
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <FormikMultilineTextField
                  rows={2}
                  name="review"
                  label="Review"
                />
              </Grid>
              <Grid item>
                <Button color="primary" variant="contained" type="submit">
                  Next Step
                </Button>
              </Grid>
            </Grid>
          </Form>
        );
      }}
    </Formik>
  );
};

type Props = {
  program: Program;
  afterSubmit: (program: Program) => void;
};

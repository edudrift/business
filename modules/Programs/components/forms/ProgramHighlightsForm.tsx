import { Button, Grid } from "@material-ui/core";
// import { AddHighlightForm } from "./AddHighlightForm";
import { Form, Formik } from "formik";
import { isNull, isUndefined, range } from "lodash";
import React from "react";
import { ProgramsServiceForBusiness } from "../../services/ProgramsServiceForBusiness";
import { ProgramHighlight } from "../../types/ProgramHighlight.types";
import { Program } from "../../types/Programs.types";
import { AddHighlightSubform } from "./AddHighlightSubform";

export const ProgramHighlightsForm: React.FC<Props> = (props) => {
  const buildEmptyHighlight = () => ({
    title: "",
    description: "",
    images: new Array(3),
    type: "",
  });
  const numberOfHighlights = 4;

  const buildInitialValues = () => {
    let { highlights } = props.program;
    if (isNull(highlights)) highlights = [];

    return range(numberOfHighlights).reduce(
      (
        accum: {
          [key: number]: ProgramHighlight;
        },
        curr: number
      ) => {
        const highlight = highlights[curr];
        if (isUndefined(highlight)) {
          accum[curr] = buildEmptyHighlight();
        } else {
          accum[curr] = highlight;
        }

        return accum;
      },
      {}
    );
  };

  const handleSubmit = async (v: { [key: string]: ProgramHighlight }) => {
    const highlights: ProgramHighlight[] = Object.entries(v).map(
      ([, highlight]) => highlight
    );

    const newProgram = await ProgramsServiceForBusiness.saveHighlights(
      props.program.id,
      highlights
    );
    props.afterSubmit(newProgram);
  };

  return (
    <Formik initialValues={buildInitialValues()} onSubmit={handleSubmit}>
      {({ values, setValues }) => {
        return (
          <Form>
            <Grid container spacing={2} direction="column">
              <Grid item container spacing={6}>
                {Object.entries(values).map(([namespace, highlight]) => {
                  return (
                    <Grid item key={namespace} xs={12} md={6}>
                      <AddHighlightSubform
                        namespace={namespace}
                        highlight={highlight}
                        programId={props.program.id}
                        setHighlight={(newHighlight: any) => {
                          setValues({
                            ...values,
                            [namespace]: newHighlight,
                          });
                        }}
                      />
                    </Grid>
                  );
                })}
              </Grid>

              <Grid item>
                <Button type="submit" color="primary" variant="contained">
                  Next Step
                </Button>
              </Grid>
            </Grid>
          </Form>
        );
      }}
    </Formik>
  );
};

type Props = {
  program: Program;
  afterSubmit: (program: Program) => void;
};

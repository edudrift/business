import React from "react";

import { Grid } from "@material-ui/core";
import { useFormikContext } from "formik";

import {
  EProgramFieldsOfInterest,
  EProgramFieldsOfInterestLabels,
} from "../../enums/EProgramFieldsOfInterest";
import { CheckboxGroupMd9 } from "../../../Shared/components/CheckboxGroupMd9";
import { FormSubheaderWithChildren } from "../../../Shared/components/FormSubheaderWithChildren";

export const ProgramFieldsOfInterestSubForm: React.FC = () => {
  const { values, setFieldValue } = useFormikContext<{
    fields: EProgramFieldsOfInterest[];
  }>();

  return (
    <FormSubheaderWithChildren
      title="Fields of Interest"
      subtitle="Which fields of interest would you like to be the search filters for this program?"
    >
      <Grid item>
        <CheckboxGroupMd9
          checked={values.fields}
          values={Object.values(EProgramFieldsOfInterest)}
          labels={Object.values(EProgramFieldsOfInterestLabels)}
          setValues={(v) => setFieldValue("fields", v)}
        />
      </Grid>
    </FormSubheaderWithChildren>
  );
};

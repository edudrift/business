import React from "react";

import {
  Button,
  Checkbox,
  FormControlLabel,
  FormGroup,
  Grid,
} from "@material-ui/core";
import {
  EProgramFieldsOfInterest,
  EProgramFieldsOfInterestLabels,
} from "../../enums/EProgramFieldsOfInterest";
import { ProgramsServiceForBusiness } from "../../services/ProgramsServiceForBusiness";
import { Program } from "../../types/Programs.types";

export const ProgramFieldsOfInterestForm: React.FC<Props> = (props) => {
  const buildState = () => {
    const initialisedState: any = {};
    for (const key of Object.keys(EProgramFieldsOfInterest)) {
      if (!props.program.fields) {
        initialisedState[key] = false;
      } else {
        initialisedState[key] = props.program.fields.includes(
          key as EProgramFieldsOfInterest
        );
      }
    }

    return initialisedState;
  };

  const [state, setState] = React.useState(buildState());

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setState({ ...state, [event.target.name]: event.target.checked });
  };

  const handleSubmit = async () => {
    const fieldsOfInterest = Object.keys(state).filter((pi) => state[pi]);
    const newProgram = await ProgramsServiceForBusiness.saveSearchFilters(
      props.program.id,
      fieldsOfInterest as EProgramFieldsOfInterest[],
      []
    );

    props.afterSubmit(newProgram);
  };

  return (
    <Grid container spacing={2} direction="column">
      <Grid item>
        <FormGroup>
          <Grid container spacing={2} item xs={12} md={9}>
            {Object.keys(EProgramFieldsOfInterest).map((pi) => {
              return (
                <Grid item key={pi} md={3} xs={12}>
                  <FormControlLabel
                    control={
                      <Checkbox
                        color="primary"
                        checked={state[pi]}
                        onChange={handleChange}
                        name={pi}
                      />
                    }
                    label={
                      EProgramFieldsOfInterestLabels[
                        pi as EProgramFieldsOfInterest
                      ]
                    }
                  />
                </Grid>
              );
            })}
          </Grid>
        </FormGroup>
      </Grid>

      <Grid item>
        <Button color="primary" variant="contained" onClick={handleSubmit}>
          Save and Go Back
        </Button>
      </Grid>
    </Grid>
  );
};

type Props = {
  program: Program;
  afterSubmit: (program: Program) => void;
};

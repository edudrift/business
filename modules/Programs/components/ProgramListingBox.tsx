import React from "react";

import clsx from "clsx";
import { Button, Grid, makeStyles } from "@material-ui/core";

import { TypographyWithPxFontAndColor } from "~/modules/Shared/components/TypographyWithPxFontAndColor";

const DISABLED_COLOR = "#E0E0E0";
const BLUE_COLOR = "#1890FF";

const useStyles = makeStyles(() => ({
  box: {
    marginLeft: "26px",
    borderRadius: "4px",
    minHeight: "110px",
  },
  boxActive: {
    border: `1px solid ${BLUE_COLOR}`,
    padding: "1px",
    "&:hover": {
      border: `2px solid ${BLUE_COLOR}`,
      padding: "0px",
      cursor: "pointer",
    },
  },
  boxDisabled: {
    border: `2px solid ${DISABLED_COLOR}`,
  },
  checkmark: {
    marginBottom: "2px",
    transform: "rotate(45deg)",
    height: "16px",
    width: "8px",
    borderBottom: "2px solid white",
    borderRight: "2px solid white",
  },
  circle: {
    display: "flex",
    fontSize: "18px",
    fontFamily: "Roboto",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: "50%",
    minWidth: "32px",
    width: "32px",
    minHeight: "32px",
    height: "32px",
    margin: "auto auto",
    color: "white",
  },
  disabled: {
    backgroundColor: DISABLED_COLOR,
  },
  active: {
    backgroundColor: BLUE_COLOR,
  },
}));

export const ProgramListingBox: React.FC<Props> = (props) => {
  const classes = useStyles();

  const CheckMark = () => <div className={classes.checkmark} />;
  const Circle = ({ children }: { children: React.ReactNode }) => (
    <div
      className={clsx(classes.circle, {
        [classes.disabled]: props.disabled,
        [classes.active]: !props.disabled,
      })}
    >
      {children}
    </div>
  );

  const CircleWithNumber = (props: { number: number }) => (
    <Circle>
      <p>{props.number}</p>
    </Circle>
  );
  const CircleWithCheckMark = () => <Circle children={<CheckMark />} />;

  return (
    <Grid
      item
      container
      md={8}
      onClick={props.disabled ? undefined : props.onClick}
    >
      <Grid
        item
        container
        spacing={2}
        className={clsx(classes.box, {
          [classes.boxDisabled]: props.disabled,
          [classes.boxActive]: !props.disabled,
        })}
      >
        <Grid item xs={2} container>
          {props.completed ? (
            <CircleWithCheckMark />
          ) : (
            <CircleWithNumber number={props.index + 1} />
          )}
        </Grid>
        <Grid item xs={10}>
          <Grid item>
            <TypographyWithPxFontAndColor
              bold
              sizeInPx={18}
              color={props.disabled ? DISABLED_COLOR : BLUE_COLOR}
            >
              {props.title}
            </TypographyWithPxFontAndColor>
          </Grid>
          <Grid item>
            <TypographyWithPxFontAndColor
              sizeInPx={12}
              color={props.disabled ? DISABLED_COLOR : "rgba(0, 0, 0, 0.6)"}
            >
              {props.content}
            </TypographyWithPxFontAndColor>
          </Grid>
          {!props.disabled && !props.completed && (
            <Grid item xs={12} md={12}>
              <img
                src={props.image}
                style={{
                  width: "100%",
                  justifySelf: "start",
                  paddingLeft: 0,
                }}
              />
              <Button
                variant="contained"
                color="primary"
                style={{ marginTop: "-100px", marginLeft: "10px" }}
              >
                Continue
              </Button>
            </Grid>
          )}
        </Grid>
      </Grid>
    </Grid>
  );
};

type Props = {
  title: string;
  content: string | React.ReactNode;
  image: string;
  onClick?: (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => void;
  index: number;
  disabled?: boolean;
  completed?: boolean;
};

import React from "react";

import { Grid } from "@material-ui/core";

export const ProgramImageTiler: React.FC<Props> = ({ images }) => {
  if (!images || !images.length) return null;
  return (
    <Grid item container spacing={2}>
      {images.map(
        (image, index) =>
          image && (
            <Grid item xs={12} md={4} key={index}>
              <img src={image} width="100%" height="100%" />
            </Grid>
          )
      )}
    </Grid>
  );
};

type Props = {
  images?: string[];
};

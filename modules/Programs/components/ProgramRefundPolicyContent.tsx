import React from "react";

import { Grid } from "@material-ui/core";

import { formatDateWithFullDate } from "../../../utils/dateUtils";
import { Program } from "../types/Programs.types";
import {
  ProgramContentBoldText,
  ProgramContentText,
} from "./ProgramContentText";

export const ProgramRefundPolicyContent: React.FC<Props> = ({ program }) => {
  const { refundPolicy } = program;

  return (
    <Grid item container spacing={4}>
      <Grid item container spacing={2} direction="column">
        <Grid item container spacing={1} direction="column">
          <Grid item>
            <ProgramContentBoldText>Application Policy</ProgramContentBoldText>
          </Grid>

          <Grid item>
            <ProgramContentText>
              Determined by EduDrift base on Program Type and Ticketing Service
              Package
            </ProgramContentText>
          </Grid>
        </Grid>

        <Grid item container spacing={1} direction="column">
          <Grid item>
            <ProgramContentBoldText>Refund Policy</ProgramContentBoldText>
          </Grid>

          {refundPolicy.fullRefundBeforeDate && (
            <Grid item>
              <ProgramContentText>
                Cancel before{" "}
                {formatDateWithFullDate(refundPolicy.fullRefundBeforeDate!)}:
                100%
              </ProgramContentText>
            </Grid>
          )}

          {refundPolicy.variableRefundBeforeDate && (
            <Grid item>
              <ProgramContentText>
                Cancel before{" "}
                {formatDateWithFullDate(refundPolicy.variableRefundBeforeDate!)}
                : {refundPolicy.variableRefundPercentage}%
              </ProgramContentText>
            </Grid>
          )}

          <Grid item>
            <ProgramContentText>
              <strong>No refund</strong> after{" "}
              {formatDateWithFullDate(refundPolicy.noRefundAfterDate!)}
            </ProgramContentText>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};

type Props = {
  program: Program;
};

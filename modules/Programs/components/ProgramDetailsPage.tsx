import React from "react";

import { NextPage, NextPageContext } from "next";

import { useRouter } from "next/router";
import { getUserProfileOrRedirect } from "../../../services/Auth.functions";
import { ProgramDashboard } from "./ProgramDashboard";
import { ProgramsServiceForBusiness } from "../services/ProgramsServiceForBusiness";
import { Program } from "../types/Programs.types";
import { getProgramSteps } from "../services/ProgramStepsService";
import { EProgramSteps } from "../enums/EProgramSteps";
import { Card } from "@material-ui/core";
import LinearStepper from "../../Shared/components/Stepper/LinearStepper";

enum EProgramDetailsSteps {
  GENERAL_INFORMATION = 0,
  ORGANISING_TEAM = 1,
  SUBJECT_FIELDS = 2,
}

const steps = getProgramSteps([
  EProgramSteps.GENERAL_INFORMATION,
  EProgramSteps.ORGANISING_TEAM,
  EProgramSteps.SUBJECT_FIELDS,
]);

const ProgramDetailsPage: NextPage<Props> = (props) => {
  const router = useRouter();

  const [activeStep, setActiveStep] = React.useState<number>(0);
  const [program, setProgram] = React.useState(props.program);

  const FormComponent = steps[activeStep].component;

  const handleStepClick = (step: number) => setActiveStep(step);
  const afterSubmit = (newProgram: Program) => {
    setProgram(newProgram);
    if (activeStep === EProgramDetailsSteps.SUBJECT_FIELDS) {
      router.push("/biz/program/[id]/edit", `/biz/program/${program.id}/edit`);
    } else {
      setActiveStep(activeStep + 1);
    }
  };

  return (
    <ProgramDashboard
      user={props.user}
      program={program}
      crumbs={[
        { title: "Dashboard", link: "/biz" },
        { title: "Programs", link: "/biz/programs" },
        {
          title: "Edit Program",
          link: `/biz/program/${program.id}/edit`,
        },
        { title: "Details" },
      ]}
      cancelRoute={[
        "/biz/program/[id]/edit",
        `/biz/program/${program.id}/edit`,
      ]}
    >
      <Card style={{ marginBottom: "1em" }}>
        <LinearStepper
          activeStep={activeStep}
          steps={steps}
          onStepClick={handleStepClick}
          argumentForIsStepComplete={program}
        ></LinearStepper>
      </Card>

      <FormComponent program={program} afterSubmit={afterSubmit} />
    </ProgramDashboard>
  );
};

const getInitialProps = async (ctx: NextPageContext) => {
  const user = await getUserProfileOrRedirect(ctx);
  const program = await ProgramsServiceForBusiness.getProgramFromId(
    ctx.query.id as string,
    ctx
  );

  return { user, program };
};

type Props = PromiseResult<ReturnType<typeof getInitialProps>>;

ProgramDetailsPage.getInitialProps = getInitialProps;

export default ProgramDetailsPage;

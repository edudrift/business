import React from "react";

import { NextPage, NextPageContext } from "next";

import { getUserProfileOrRedirect } from "../../../services/Auth.functions";
import { ProgramsServiceForBusiness } from "../services/ProgramsServiceForBusiness";
import ProgramListing from "./ProgramListing";
import ProgramDashboard from "./ProgramDashboard";

const CreateSpecificProgram: NextPage<Props> = (props) => {
  return (
    <ProgramDashboard
      program={props.program}
      user={props.user}
      crumbs={[
        { title: "Dashboard", link: "/biz" },
        { title: "Programs", link: "/biz/programs" },
        { title: "Edit Program" },
      ]}
      cancelRoute={["/biz/programs"]}
    >
      <ProgramListing program={props.program} />
    </ProgramDashboard>
  );
};

const getInitialProps = async (ctx: NextPageContext) => {
  const user = await getUserProfileOrRedirect(ctx);
  const program = await ProgramsServiceForBusiness.getProgramFromId(
    ctx.query.id as string,
    ctx
  );

  return { user, program };
};

type Props = PromiseResult<ReturnType<typeof getInitialProps>>;

CreateSpecificProgram.getInitialProps = getInitialProps;

export default CreateSpecificProgram;

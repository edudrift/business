import React from "react";

import { Grid, TextField } from "@material-ui/core";

import { TypographyWithPxFontAndColor } from "../../Shared/components/TypographyWithPxFontAndColor";
import { ProgramTicket } from "../types/ProgramTicket.types";
export const ProgramTicketItem: React.FC<Props> = ({
  ticket,
  onChange,
  value,
  currencyCode,
}) => {
  return (
    <Grid item container spacing={1} direction="column">
      <Grid item container alignItems="center">
        <Grid item xs={12} md={6}>
          <TypographyWithPxFontAndColor sizeInPx={16} color="#000">
            {ticket.name}
          </TypographyWithPxFontAndColor>
        </Grid>

        <Grid item xs={6} md={3}>
          <TypographyWithPxFontAndColor sizeInPx={16} color="#333">
            {currencyCode} {ticket.price}
          </TypographyWithPxFontAndColor>
        </Grid>

        <Grid item xs={6} md={3}>
          <TextField
            type="number"
            label="Quantity"
            variant="outlined"
            style={{ margin: 0 }}
            size="small"
            fullWidth
            onChange={(e) => {
              onChange(parseInt(e.target.value, 10));
            }}
            value={value}
          />
        </Grid>
      </Grid>

      <Grid item>
        <TypographyWithPxFontAndColor sizeInPx={14} color="black">
          {ticket.description}
        </TypographyWithPxFontAndColor>
      </Grid>

      {ticket.eligibility && (
        <Grid item>
          <TypographyWithPxFontAndColor sizeInPx={14} color="red">
            {ticket.eligibility}
          </TypographyWithPxFontAndColor>
        </Grid>
      )}
    </Grid>
  );
};

type Props = {
  currencyCode: string;
  ticket: ProgramTicket;
  onChange: (value: number) => void;
  value: number;
};

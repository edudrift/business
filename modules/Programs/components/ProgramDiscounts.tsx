import React from "react";

import { Grid } from "@material-ui/core";
import { isNull } from "lodash";

import { TypographyWithPxFontAndColor } from "../../Shared/components/TypographyWithPxFontAndColor";
import { ProgramEarlyBird } from "../types/ProgramEarlyBird.types";
import { Program } from "../types/Programs.types";
import { OrangeChip } from "./OrangeChip";
import { differenceInDays } from "../../../utils/dateUtils";

export const ProgramDiscounts: React.FC<Props> = ({ program }) => {
  const getDiscountAmount = ({
    discountType,
    discountedAmount,
  }: ProgramEarlyBird) => {
    if (discountType === "PERCENTAGE") {
      return `${discountedAmount}%`;
    } else {
      return `$${discountedAmount}`;
    }
  };

  const getDaysLeft = () => {
    const { endDate } = program.earlyBird!;

    return differenceInDays(new Date(), new Date(endDate));
  };

  const hasEarlyBird = !isNull(program.earlyBird);
  if (!hasEarlyBird) return null;

  const daysLeft = getDaysLeft();
  if (daysLeft < 1) return null;

  return (
    <Grid item container spacing={2} alignItems="center">
      <Grid item xs={2}>
        <TypographyWithPxFontAndColor sizeInPx={14} color="#999999">
          Discount
        </TypographyWithPxFontAndColor>
      </Grid>
      <Grid item xs container spacing={2} alignItems="center">
        <Grid item xs={3}>
          <OrangeChip
            label={`Early bird - ${getDiscountAmount(program.earlyBird!)}`}
            bordered
          />
        </Grid>

        <Grid item xs>
          <TypographyWithPxFontAndColor sizeInPx={14} color="#333">
            Early bird privilege, only {daysLeft} days left
          </TypographyWithPxFontAndColor>
        </Grid>
      </Grid>
    </Grid>
  );
};

type Props = {
  program: Program;
};

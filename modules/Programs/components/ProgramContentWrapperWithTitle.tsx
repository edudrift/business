import React from "react";

import { Divider, Grid, Typography } from "@material-ui/core";
import useIsInViewport from "use-is-in-viewport";

import theme from "../../../theme";

export const ProgramContentWrapperWithTitle: React.FC<Props> = ({
  title,
  children,
  setVisible,
  setHidden,
  sectionName,
}) => {
  const [isInViewport, targetRef] = useIsInViewport({ threshold: 50 });
  React.useEffect(() => {
    if (isInViewport) {
      setVisible(sectionName);
    } else {
      setHidden(sectionName);
    }
  }, [isInViewport]);

  return (
    <Grid item container spacing={2} id={title} ref={targetRef}>
      <Grid item container spacing={2}>
        <Grid item>
          <Divider
            orientation="vertical"
            style={{ width: "4px", backgroundColor: "#FFA940" }}
          />
        </Grid>

        <Grid item>
          <Typography
            component="p"
            style={{
              fontSize: theme.typography.pxToRem(18),
              textTransform: "uppercase",
            }}
          >
            {title}
          </Typography>
        </Grid>
      </Grid>

      <Grid item container style={{ paddingLeft: theme.spacing(3) + 4 }}>
        {children}
      </Grid>
    </Grid>
  );
};

type Props = {
  title: string;
  setVisible: (sectionName: any) => void;
  setHidden: (sectionName: any) => void;
  sectionName: string;
};

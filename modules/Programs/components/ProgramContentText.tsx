import React from "react";
import { TypographyWithPxFontAndColor } from "../../Shared/components/TypographyWithPxFontAndColor";

export const ProgramContentText: React.FC = ({ children }) => {
  return (
    <TypographyWithPxFontAndColor sizeInPx={14} color="#666666">
      {children}
    </TypographyWithPxFontAndColor>
  );
};

export const ProgramContentMultilineText: React.FC = ({ children }) => {
  return (
    <TypographyWithPxFontAndColor sizeInPx={14} color="#666666" multiline>
      {children}
    </TypographyWithPxFontAndColor>
  );
};

export const ProgramContentBoldText: React.FC = ({ children }) => {
  return (
    <TypographyWithPxFontAndColor sizeInPx={14} color="#333" bold>
      {children}
    </TypographyWithPxFontAndColor>
  );
};

import React from "react";

import { AppBar, Grid, makeStyles, Tab, Tabs } from "@material-ui/core";
import { findIndex, pull, uniq, indexOf } from "lodash";

import theme from "../../../theme";
import { Program } from "../types/Programs.types";
import { ProgramAccommodationContent } from "./ProgramAccommodationContent";
import { ProgramContentWrapperWithTitle } from "./ProgramContentWrapperWithTitle";
import { ProgramEssentialsContent } from "./ProgramEssentialsContent";
import { ProgramIntroductionContent } from "./ProgramIntroductionContent";
import { ProgramRefundPolicyContent } from "./ProgramRefundPolicyContent";
import { ProgramScheduleContent } from "./ProgramScheduleContent";

const useStyles = makeStyles(() => ({
  tabRoot: {
    marginBottom: theme.spacing(2),
  },
  tabContainer: {
    borderBottom: "2px solid rgba(0, 0, 0,.2)",
    position: "sticky",
    top: 0,
    backgroundColor: "#fff",
  },
  tabHeader: {
    fontSize: "18px",
  },
}));

enum SECTIONS {
  INTRO = "INTRODUCTION",
  ESSENTIALS = "ESSENTIALS",
  ACCOMMODATION = "ACCOMMODATION",
  ITINERARY_SCHEDULE = "ITINERARY/SCHEDULE",
  APPLICATION_REFUND_POLICY = "APPLICATION & REFUND POLICY",
}

type ProgramContentSection = {
  name: SECTIONS;
  component: React.FC<{ program: Program }>;
  ref: React.RefObject<HTMLDivElement>;
};

export const ProgramContentTabs: React.FC<Props> = ({ program }) => {
  const classes = useStyles();
  const [activeTabIndex, setActiveTabIndex] = React.useState(0);
  const [visibleSections, setVisibleSections] = React.useState<SECTIONS[]>([]);

  let sections: ProgramContentSection[] = [
    {
      name: SECTIONS.INTRO,
      component: ProgramIntroductionContent,
      ref: React.createRef(),
    },
    {
      name: SECTIONS.ESSENTIALS,
      component: ProgramEssentialsContent,
      ref: React.createRef(),
    },
    {
      name: SECTIONS.ACCOMMODATION,
      component: ProgramAccommodationContent,
      ref: React.createRef(),
    },
    {
      name: SECTIONS.ITINERARY_SCHEDULE,
      component: ProgramScheduleContent,
      ref: React.createRef(),
    },
    {
      name: SECTIONS.APPLICATION_REFUND_POLICY,
      component: ProgramRefundPolicyContent,
      ref: React.createRef(),
    },
  ];

  React.useEffect(() => {
    decideOnActiveTab();
  }, [visibleSections]);

  const handleChange = (_: any, newValue: number) => {
    setActiveTabIndex(newValue);
  };

  if (!program.accommodation.provided) {
    sections = sections.filter(
      (section) => section.name !== SECTIONS.ACCOMMODATION
    );
  }

  const findIndexOfSection = (section: SECTIONS) => {
    return findIndex(sections, { name: section });
  };

  const setActiveTabFromSectionName = (sectionName: SECTIONS) => {
    setActiveTabIndex(findIndexOfSection(sectionName));
  };

  const decideOnActiveTab = () => {
    if (visibleSections.length) {
      const sorted = [
        SECTIONS.INTRO,
        SECTIONS.ESSENTIALS,
        SECTIONS.ITINERARY_SCHEDULE,
        SECTIONS.APPLICATION_REFUND_POLICY,
      ];
      setActiveTabFromSectionName(
        sorted[Math.min(...visibleSections.map((i) => indexOf(sorted, i)))]
      );
    }
  };

  const markVisible = (sectionName: SECTIONS) => {
    setVisibleSections(uniq([...visibleSections, sectionName]));
  };

  const markHidden = (sectionName: SECTIONS) => {
    const newVisibleSections = [...visibleSections];
    pull(newVisibleSections, sectionName);

    setVisibleSections(uniq(newVisibleSections));
  };

  const appBarRef = React.createRef<HTMLDivElement>();

  const scrollToRelevantSection = (section: ProgramContentSection) => {
    const thisSection = section.ref.current;
    const appBar = appBarRef.current;
    if (thisSection && appBar) {
      window.scroll(0, thisSection.offsetTop - appBar.clientHeight);
      setVisibleSections([section.name]);
    }
  };

  return (
    <Grid item container spacing={2}>
      <Grid
        item
        xs={12}
        style={{
          position: "sticky",
          top: 0,
          paddingTop: 0,
          backgroundColor: "#fff",
          zIndex: 1,
        }}
      >
        <AppBar
          ref={appBarRef}
          position="sticky"
          color="default"
          style={{ boxShadow: "none", backgroundColor: "transparent" }}
        >
          <Tabs
            value={activeTabIndex}
            onChange={handleChange}
            indicatorColor="primary"
            textColor="primary"
            classes={{
              root: classes.tabRoot,
              flexContainer: classes.tabContainer,
            }}
            variant="scrollable"
          >
            {sections.map((section, index) => {
              return (
                <Tab
                  label={section.name}
                  key={index}
                  onClick={() => scrollToRelevantSection(section)}
                  className={classes.tabHeader}
                  wrapped
                />
              );
            })}
          </Tabs>
        </AppBar>
      </Grid>
      <Grid item container spacing={4}>
        {sections.map((section, index) => {
          const { name, ref, component: Component } = section;

          return (
            <Grid item container ref={ref}>
              <ProgramContentWrapperWithTitle
                key={index}
                title={name}
                sectionName={name}
                setVisible={markVisible}
                setHidden={markHidden}
              >
                <Component program={program} />
              </ProgramContentWrapperWithTitle>
            </Grid>
          );
        })}
      </Grid>
    </Grid>
  );
};

type Props = {
  program: Program;
};

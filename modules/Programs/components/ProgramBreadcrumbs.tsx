import React from "react";

import {
  Breadcrumbs,
  Link,
  makeStyles,
  Paper,
  Typography,
} from "@material-ui/core";
import NextLink from "next/link";

import {
  EProgramTypeLabels,
  Program,
} from "../../../modules/Programs/types/Programs.types";

const useStyles = makeStyles((theme) => ({
  breadcrumbs: {
    padding: theme.spacing(1),
    paddingLeft: 0,
  },
  crumbLink: {
    color: theme.palette.primary.main,
  },
  crumbText: {
    color: "rgba(0, 0, 0, 0.45)",
  },
}));

export const ProgramBreadcrumbs: React.FC<Props> = ({ program }) => {
  const classes = useStyles();

  return (
    <Paper elevation={0} className={classes.breadcrumbs}>
      <Breadcrumbs aria-label="breadcrumb">
        <NextLink href="/">
          <Link href="/">
            <Typography variant="subtitle2" className={classes.crumbLink}>
              Home
            </Typography>
          </Link>
        </NextLink>

        <NextLink href="/">
          <Link href="/">
            <Typography variant="subtitle2" className={classes.crumbLink}>
              {EProgramTypeLabels[program.type]}
            </Typography>
          </Link>
        </NextLink>

        <Typography
          color="textPrimary"
          variant="subtitle2"
          className={classes.crumbText}
        >
          {program.name}
        </Typography>
      </Breadcrumbs>
    </Paper>
  );
};

type Props = {
  program: Program;
};

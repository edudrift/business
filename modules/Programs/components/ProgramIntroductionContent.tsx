import React from "react";

import { Grid } from "@material-ui/core";
import { isEmpty } from "lodash";

import { getCountryFromCode } from "../../Countries/countries";
import { formatDateWithFullDate } from "../../../utils/dateUtils";
import { Program } from "../types/Programs.types";
import {
  ProgramContentBoldText,
  ProgramContentMultilineText,
  ProgramContentText,
} from "./ProgramContentText";
import { ProgramImageTiler } from "./ProgramImageTiler";

export const ProgramIntroductionContent: React.FC<Props> = ({ program }) => {
  return (
    <Grid item container spacing={4}>
      <Grid item container spacing={1} direction="column">
        <Grid item>
          <ProgramContentText>Program Name: {program.name}</ProgramContentText>
        </Grid>

        <Grid item>
          <ProgramContentText>
            Program Provider: {program.provider.name}
          </ProgramContentText>
        </Grid>

        <Grid item>
          <ProgramContentText>
            {" "}
            Co-Hosting Partner: {program.partner.name}
          </ProgramContentText>
        </Grid>

        <Grid item>
          <ProgramContentText>
            Time: {formatDateWithFullDate(program.startDate)} –{" "}
            {formatDateWithFullDate(program.endDate)}
          </ProgramContentText>
        </Grid>

        <Grid item>
          <ProgramContentText>
            Application Deadline:{" "}
            {formatDateWithFullDate(program.applicationDeadline)}
          </ProgramContentText>
        </Grid>

        <Grid item>
          <ProgramContentText>
            Location: {getCountryFromCode(program.venue.countryCode)}
          </ProgramContentText>
        </Grid>

        <Grid item>
          <ProgramContentText>
            Participant Types:{" "}
            {program.ticketTypes.map((tt) => tt.name).join(", ")}
          </ProgramContentText>
        </Grid>

        <Grid item>
          <ProgramContentText>
            Quota:{" "}
            {program.ticketTypes.map((tt) => `${tt.max} ${tt.name}`).join(", ")}
          </ProgramContentText>
        </Grid>
      </Grid>

      {/* <Grid item container spacing={1} direction="column">
        <Grid item>
          <ProgramContentBoldText>VENUE</ProgramContentBoldText>
        </Grid>

        <Grid item>
          <ProgramContentText>{program.venue.description}</ProgramContentText>
        </Grid>

        <ProgramImageTiler images={program.venue.images} />
      </Grid> */}

      <Grid item container spacing={1} direction="column">
        <Grid item>
          <ProgramContentBoldText>PROGRAM DESCRIPTION</ProgramContentBoldText>
        </Grid>

        <Grid item>
          <ProgramContentMultilineText>
            {program.description}
          </ProgramContentMultilineText>
        </Grid>

        <ProgramImageTiler images={program.images} />
      </Grid>

      {program.highlights &&
        program.highlights
          .filter((highlight) => !isEmpty(highlight.title))
          .map((highlight, index) => {
            return (
              <Grid item container spacing={1} direction="column" key={index}>
                <Grid item>
                  <ProgramContentBoldText>
                    {highlight.title}
                  </ProgramContentBoldText>
                </Grid>

                <Grid item>
                  <ProgramContentMultilineText>
                    {highlight.description}
                  </ProgramContentMultilineText>
                </Grid>

                <ProgramImageTiler images={highlight.images} />
              </Grid>
            );
          })}
    </Grid>
  );
};

type Props = {
  program: Program;
};

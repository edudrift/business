import React from "react";

import { Box, Grid, Typography } from "@material-ui/core";

import { ProgramContentBoldText } from "./ProgramContentText";
import { formatDateWithFullDate } from "../../../utils/dateUtils";
import { ProgramSchedule } from "../types/ProgramSchedule.types";
import { ProgramDayScheduleContent } from "./ProgramDayScheduleContent";
import { ProgramWeekScheduleContent } from "./ProgramWeekScheduleContent";

interface TabPanelProps {
  children?: React.ReactNode;
  index: any;
  value: any;
}

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

export const ProgramScheduleTabPanel: React.FC<Props> = ({
  schedule,
  value,
}) => {
  return (
    <React.Fragment>
      {schedule.map((item, index) => {
        return (
          <TabPanel value={value} index={index} key={index}>
            <Grid container spacing={2} direction="column">
              <Grid item>
                <ProgramContentBoldText>
                  {(() => {
                    switch (item.type) {
                      case "week":
                        return `Week ${item.weekNumber}  \
                               ${formatDateWithFullDate(item.startDate)} - 
                               ${formatDateWithFullDate(item.endDate)}`;
                      case "day":
                        return `Day ${item.dayNumber}  
                               ${formatDateWithFullDate(item.date)}`;
                    }
                  })()}
                </ProgramContentBoldText>
              </Grid>
              <Grid item>
                {(() => {
                  switch (item.type) {
                    case "week":
                      return <ProgramWeekScheduleContent week={item} />;
                    case "day":
                      return <ProgramDayScheduleContent day={item} />;
                  }
                })()}
              </Grid>
            </Grid>
          </TabPanel>
        );
      })}
    </React.Fragment>
  );
};

type Props = {
  schedule: ProgramSchedule;
  value: number;
};

import {
  makeStyles,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  CircularProgress,
} from "@material-ui/core";
import { format } from "date-fns";
import React from "react";

import ButtonLink from "../../../components/ButtonLink";
import { PaddedPaper } from "../../Shared/components/PaddedPaper";
import { StyledTableCell } from "../../../components/Tables/StyledTableCell";
import { getCountryObjectFromCountryCode } from "../../Countries/countries";
import { formatDateWithFullDate } from "../../../utils/dateUtils";
import { Program } from "../types/Programs.types";

const useStyles = makeStyles(() => ({
  normal: {
    fontWeight: 500,
    color: "#333",
  },
  header: {
    fontWeight: 700,
  },
}));

const getEventDate = (program: Program) => {
  if (program.startDate && program.endDate) {
    // format: 01 August 2019 - 01 August 2019
    return `${formatDateWithFullDate(
      new Date((program.startDate as any) as string)
    )} - ${formatDateWithFullDate(
      new Date((program.endDate as any) as string)
    )}`;
  } else {
    return "Event dates not set yet";
  }
};

const getPlace = (program: Program) => {
  if (program.venue && program.venue.countryCode) {
    return `${
      getCountryObjectFromCountryCode(program.venue.countryCode).label
    }`;
  } else {
    return "Place not ready";
  }
};

const ProgramsTable: React.FC<Props> = (props) => {
  const classes = useStyles();

  return (
    <TableContainer>
      <Table stickyHeader>
        <TableHead>
          <TableRow>
            <StyledTableCell className={classes.header}>Title</StyledTableCell>
            <StyledTableCell className={classes.header}>State</StyledTableCell>
            <StyledTableCell className={classes.header}>Place</StyledTableCell>
            <StyledTableCell className={classes.header}>
              Program time
            </StyledTableCell>
            <StyledTableCell className={classes.header}>
              Creation time
            </StyledTableCell>
            <StyledTableCell className={classes.header}>Action</StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {props.loading && (
            <PaddedPaper>
              <CircularProgress />
            </PaddedPaper>
          )}
          {!props.loading && props.programs.length === 0 && (
            <PaddedPaper>No programs to show.</PaddedPaper>
          )}
          {!props.loading &&
            props.programs.map((program) => {
              const formattedCreatedAt = format(
                new Date(program.createdAt),
                "yyyy-MM-dd kk:mm:ss"
              );
              return (
                <TableRow key={program.id} hover>
                  <TableCell
                    className={classes.normal}
                    component="th"
                    scope="row"
                  >
                    <strong>{program.name}</strong>
                  </TableCell>
                  <TableCell className={classes.normal}>
                    {program.status}
                  </TableCell>
                  <TableCell className={classes.normal}>
                    {getPlace(program)}
                  </TableCell>
                  <TableCell className={classes.normal}>
                    {getEventDate(program)}
                  </TableCell>
                  <TableCell>{formattedCreatedAt}</TableCell>
                  <TableCell>
                    <ButtonLink
                      color="primary"
                      href="/biz/program/[id]/edit"
                      as={`/biz/program/${program.id}/edit`}
                    >
                      Edit
                    </ButtonLink>
                  </TableCell>
                </TableRow>
              );
            })}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

type Props = {
  programs: Program[];
  loading: boolean;
};

export default ProgramsTable;

import React from "react";
import { Grid } from "@material-ui/core";
import { useRouter } from "next/router";

import DashboardBreadcrumbs, {
  DashboardBreadcrumbCrumb,
} from "../../../components/DashboardBreadcrumbs/DashboardBreadcrumbs";
import DashboardLayout, {
  EDashboardLayoutSidebarNames,
} from "../../../components/Layouts/DashboardLayout";
import { PaddedPaper } from "../../../modules/Shared/components/PaddedPaper";
import { OutlinedRoundButtonWithConfirmation } from "../../Shared/components/OutlinedRoundButtonWithConfirmation";
import { ProgramsServiceForBusiness } from "../services/ProgramsServiceForBusiness";
import { Program } from "../types/Programs.types";
import User from "../../../types/user.types";

export const ProgramDashboard: React.FC<Props> = (props) => {
  const router = useRouter();

  const handleDelete = async () => {
    try {
      await ProgramsServiceForBusiness.delete(props.program.id);

      router.push("/biz/programs");
    } catch (e) {
      alert(e);
    }
  };

  const programType = ((type) => {
    const capitalizeEachWord = (words: string[]) =>
      words
        .map(
          (str) => `${str.charAt(0).toUpperCase() + str.slice(1).toLowerCase()}`
        )
        .join(" ");
    return capitalizeEachWord(type.split("_"));
  })(props.program.type);

  return (
    <DashboardLayout
      user={props.user}
      activeSidebarName={EDashboardLayoutSidebarNames.PROGRAMS}
      headerTitle={`${props.program.name} - ${programType}`}
      renderCancelButton={() => (
        <Grid
          item
          style={{ minWidth: "194px" }}
          spacing={2}
          container
          justify="space-evenly"
        >
          <Grid item>
            <OutlinedRoundButtonWithConfirmation
              buttonText="Delete"
              confirmText="Yes, delete this."
              onConfirm={handleDelete}
              buttonColor="secondary"
            />
          </Grid>

          <Grid item>
            <OutlinedRoundButtonWithConfirmation
              buttonText="Cancel"
              confirmText="Yes"
              onConfirm={() => {
                const [url, as] = props.cancelRoute;
                router.push(url, as);
              }}
            />
          </Grid>
        </Grid>
      )}
      renderBreadcrumb={() => <DashboardBreadcrumbs crumbs={props.crumbs} />}
    >
      <Grid item container>
        <PaddedPaper fullWidth style={{ background: "#f0f2f5" }}>
          {props.children}
        </PaddedPaper>
      </Grid>
    </DashboardLayout>
  );
};

type Props = {
  program: Program;
  user: User;
  children: React.ReactNode;
  crumbs: DashboardBreadcrumbCrumb[];
  cancelRoute: [string, string?];
};

export default ProgramDashboard;

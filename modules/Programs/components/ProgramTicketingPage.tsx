import React from "react";

import { NextPage, NextPageContext } from "next";

import { useRouter } from "next/router";
import { getUserProfileOrRedirect } from "../../../services/Auth.functions";
import { ProgramDashboard } from "./ProgramDashboard";
import { ProgramsServiceForBusiness } from "../services/ProgramsServiceForBusiness";
import StepperWithTitle from "../../Shared/components/Stepper/StepperWithTitle";
import { Program } from "../types/Programs.types";
import { getProgramSteps } from "../services/ProgramStepsService";
import { EProgramSteps } from "../enums/EProgramSteps";

enum EProgramTicketingSteps {
  TICKETING_PRICING_PACKAGE = 0,
  PARTICIPANT_TYPES_QUOTA = 1,
  TICKETS = 2,
  APPLICATION = 3,
  REFUND_POLICY = 4,
}

const steps = getProgramSteps([
  EProgramSteps.TICKETING_PRICING_PACKAGE,
  EProgramSteps.PARTICIPANT_TYPES_QUOTA,
  EProgramSteps.TICKETS,
  EProgramSteps.APPLICATION,
  EProgramSteps.REFUND_POLICY,
]);

const ProgramTicketingPage: NextPage<Props> = (props) => {
  const router = useRouter();

  const [activeStep, setActiveStep] = React.useState<number>(0);
  const [program, setProgram] = React.useState(props.program);

  const FormComponent = steps[activeStep].component;

  const handleStepClick = (step: number) => setActiveStep(step);
  const afterSubmit = (newProgram: Program, progress: boolean = true) => {
    setProgram(newProgram);
    if (progress) {
      activeStep === EProgramTicketingSteps.REFUND_POLICY
        ? router.push(
            "/biz/program/[id]/edit",
            `/biz/program/${program.id}/edit`
          )
        : setActiveStep(activeStep + 1);
    }
  };

  return (
    <ProgramDashboard
      user={props.user}
      program={program}
      crumbs={[
        { title: "Dashboard", link: "/biz" },
        { title: "Programs", link: "/biz/programs" },
        {
          title: "Edit Program",
          link: `/biz/program/${program.id}/edit`,
        },
        { title: "Ticketing" },
      ]}
      cancelRoute={[
        "/biz/program/[id]/edit",
        `/biz/program/${program.id}/edit`,
      ]}
    >
      <StepperWithTitle
        activeStep={activeStep}
        steps={steps}
        onStepClick={handleStepClick}
        argumentForIsStepComplete={program}
      ></StepperWithTitle>
      <FormComponent program={program} afterSubmit={afterSubmit} />
    </ProgramDashboard>
  );
};

const getInitialProps = async (ctx: NextPageContext) => {
  const user = await getUserProfileOrRedirect(ctx);
  const program = await ProgramsServiceForBusiness.getProgramFromId(
    ctx.query.id as string,
    ctx
  );

  return { user, program };
};

type Props = PromiseResult<ReturnType<typeof getInitialProps>>;

ProgramTicketingPage.getInitialProps = getInitialProps;

export default ProgramTicketingPage;

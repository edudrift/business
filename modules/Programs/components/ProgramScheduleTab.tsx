import React from "react";

import { makeStyles, Tab, Tabs } from "@material-ui/core";

import { ProgramSchedule } from "../types/ProgramSchedule.types";

const useStyles = makeStyles({
  tab: {
    textTransform: "none",
  },
});

export const ProgramScheduleTab: React.FC<Props> = ({
  schedule,
  value,
  setValue,
}) => {
  const classes = useStyles();

  const handleChange = (_: React.ChangeEvent<{}>, newValue: number) => {
    setValue(newValue);
  };

  function a11yProps(index: any) {
    return {
      id: `vertical-tab-${index}`,
      "aria-controls": `vertical-tabpanel-${index}`,
    };
  }

  return (
    <Tabs
      orientation="vertical"
      value={value}
      onChange={handleChange}
      aria-label="Vertical tabs example"
      indicatorColor="primary"
      textColor="primary"
    >
      {schedule.map((item, index) => {
        switch (item.type) {
          case "week":
            return (
              <Tab
                className={classes.tab}
                label={`Week ${item.weekNumber}`}
                {...a11yProps(0)}
                key={`tab-schedule-${index}`}
              />
            );
          case "day":
            return (
              <Tab
                className={classes.tab}
                label={`Day ${item.dayNumber}`}
                {...a11yProps(0)}
                key={`tab-schedule-${index}`}
              />
            );
        }
      })}
    </Tabs>
  );
};

type Props = {
  schedule: ProgramSchedule;
  value: number;
  setValue: (newValue: number) => any;
};

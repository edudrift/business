import React from "react";

import { Grid, Button } from "@material-ui/core";
import { NextPage, NextPageContext } from "next";
import { isEmpty } from "lodash";

import DashboardLayout, {
  EDashboardLayoutSidebarNames,
} from "../../../components/Layouts/DashboardLayout";
import { getUserProfileOrRedirect } from "../../../services/Auth.functions";
import { PaddedPaper } from "../../Shared/components/PaddedPaper";

import DashboardBreadcrumbs from "../../../components/DashboardBreadcrumbs/DashboardBreadcrumbs";
import StatusTabs, {
  TabTypes,
} from "../../../components/ProgramsPage/StatusTabs";
import TokenService from "../../../services/Token.service";
import { EProgramAPIRoutes } from "../../../utils/EAPIRoutes";
import EHttpMethods from "../../../utils/EHttpMethods";
import isofetch from "../../../utils/isofetch";
import { IProgram } from "../../../utils/IProgram";
import { Program, EProgramType } from "../types/Programs.types";
import ProgramsTable from "./ProgramsTable";
import { useRouter } from "next/router";

export const getAllPrograms = async (
  ctx: NextPageContext,
  tabType: TabTypes
) => {
  const response = await isofetch<Program[]>(
    `${EProgramAPIRoutes.GET_ALL_PROGRAMS}?status=${tabType}`,
    EHttpMethods.GET,
    TokenService.getToken(ctx)
  );

  if (response.status === 200 && response.data) {
    return response.data;
  } else {
    console.error(response);
    throw new Error("Something went wrong while fetching all programs");
  }
};

const getAllProgramTypeEligibilities = async (ctx: NextPageContext) => {
  const response = await isofetch<{ programType: EProgramType }[]>(
    EProgramAPIRoutes.GET_APPROVED_PROGRAM_TYPES,
    EHttpMethods.GET,
    TokenService.getToken(ctx)
  );

  if (response.status === 200 && response.data) {
    return response.data;
  } else {
    console.error(response);
    throw new Error(
      "Something went wrong while fetching program type eligibilities"
    );
  }
};

const ProgramsPage: NextPage<Props> = (props) => {
  const router = useRouter();
  const [loading, setLoading] = React.useState(false);
  const [submitting, setSubmitting] = React.useState(false);

  const handleProgramCreation = async () => {
    setSubmitting(true);

    const response = await isofetch<IProgram>(
      EProgramAPIRoutes.CREATE_PROGRAM_FROM_PROGRAM_TYPE,
      EHttpMethods.POST,
      TokenService.getToken()
    );

    if (response.status === 201 && response.data) {
      const programId = response.data.id;
      router.push(`/biz/program/${programId}/edit`);
    } else {
      console.error(response.data);
    }
    setSubmitting(false);
  };

  React.useEffect(() => {
    setLoading(false);
  }, [props.programs]);

  return (
    <DashboardLayout
      user={props.user}
      activeSidebarName={EDashboardLayoutSidebarNames.PROGRAMS}
      headerTitle="All your programs"
      renderBreadcrumb={() => (
        <DashboardBreadcrumbs
          crumbs={[{ title: "Dashboard", link: "/biz" }, { title: "Programs" }]}
        />
      )}
    >
      <Grid item container spacing={2}>
        <Grid item xs={12}>
          <PaddedPaper fullWidth>
            <Grid item container spacing={2} justify="space-between">
              <StatusTabs tab={props.tab} onChange={() => setLoading(true)} />

              <Grid item>
                <Button
                  onClick={handleProgramCreation}
                  style={{ borderRadius: "30px" }}
                  disableElevation
                  disabled={props.disabled || submitting}
                  variant="contained"
                  color="primary"
                >
                  New Program
                </Button>
              </Grid>
            </Grid>
          </PaddedPaper>
        </Grid>

        <Grid item xs={12}>
          <PaddedPaper fullWidth withoutPadding>
            <ProgramsTable programs={props.programs} loading={loading} />
          </PaddedPaper>
        </Grid>
      </Grid>
    </DashboardLayout>
  );
};

type Props = PromiseResult<ReturnType<typeof getInitialProps>>;

const getInitialProps = async (ctx: NextPageContext) => {
  const { tab = "PENDING" } = ctx.query;

  const user = await getUserProfileOrRedirect(ctx);
  const programs = await getAllPrograms(ctx, tab as TabTypes);

  const disabled = isEmpty(await getAllProgramTypeEligibilities(ctx));
  return {
    user,
    disabled,
    programs,
    tab: tab as TabTypes,
  };
};

ProgramsPage.getInitialProps = getInitialProps;

export default ProgramsPage;

import EHttpMethods from "../../../utils/EHttpMethods";
import isofetch from "../../../utils/isofetch";
import { ensureStatus200WithData } from "./../../../utils/isofetch";
import { Program } from "./../types/Programs.types";

class ProgramsServiceClass {
  async getOne(programId: Program["id"]) {
    const response = await isofetch<Program>(
      `/api/programs/${programId}`,
      EHttpMethods.GET
    );

    ensureStatus200WithData(response);
    return response.data!;
  }

  async getAllApproved() {
    const response = await isofetch<Program[]>(
      "/api/programs",
      EHttpMethods.GET
    );

    ensureStatus200WithData(response);
    return response.data!;
  }
}

export const ProgramsService = new ProgramsServiceClass();

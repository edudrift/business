import React from "react";
import { Step } from "../../Shared/components/Stepper/StepperWithTitle";
import { ProgramAccommodationForm } from "../components/forms/ProgramAccommodationForm";
import { ProgramApplicationForm } from "../components/forms/ProgramApplicationForm";
import { ProgramCurriculumForm } from "../components/forms/ProgramCurriculumForm";
import { ProgramGeneralInformationForm } from "../components/forms/ProgramGeneralInformationForm";
import { ProgramEssentialsForm } from "../components/forms/ProgramEssentialsForm";
import { ProgramGalleryForm } from "../components/forms/ProgramGalleryForm";
import { ProgramHighlightsForm } from "../components/forms/ProgramHighlightsForm";
import { ProgramOrganisingTeamForm } from "../components/forms/ProgramOrganisingTeamForm";
import { ProgramParticipantTypesForm } from "../components/forms/ProgramParticipantTypesForm";
import { ProgramRefundPolicyForm } from "../components/forms/ProgramRefundPolicyForm";
import { ProgramScheduleForm } from "../components/forms/ProgramScheduleForm";
import { ProgramSearchFiltersForm } from "../components/forms/ProgramSearchFiltersForm";
import { ProgramTestimonyForm } from "../components/forms/ProgramTestimonyForm";
import { ProgramTicketingPricingPackage } from "../components/forms/ProgramTicketingPricingPackage";
import { ProgramTicketsForm } from "../components/forms/ProgramTicketsForm";
import { EProgramSteps } from "../enums/EProgramSteps";
import { Program } from "../types/Programs.types";
import { ProgramsStepsCompleteService } from "./ProgramStepsCompleteService";

type Props = {
  program: Program;
  afterSubmit: (program: Program) => void;
};

interface ProgramStep extends Step {
  key: EProgramSteps;
  component: React.FC<Props>;
}

const dictionary: { [k in EProgramSteps]: ProgramStep } = {
  [EProgramSteps.GENERAL_INFORMATION]: {
    key: EProgramSteps.GENERAL_INFORMATION,
    title: "",
    subtitle: "",
    stepTitle: "General Information",
    isStepComplete: ProgramsStepsCompleteService.isProgramDetailsStepComplete,
    component: ProgramGeneralInformationForm,
  },
  [EProgramSteps.ORGANISING_TEAM]: {
    key: EProgramSteps.ORGANISING_TEAM,
    title: "Organising Team",
    subtitle: "Where are you running your program?",
    stepTitle: "Organising Team",
    isStepComplete: ProgramsStepsCompleteService.isOrganisingStepComplete,
    component: ProgramOrganisingTeamForm,
  },
  [EProgramSteps.SUBJECT_FIELDS]: {
    key: EProgramSteps.SUBJECT_FIELDS,
    title: "",
    subtitle: "",
    stepTitle: "Subject Fields",
    isStepComplete: ProgramsStepsCompleteService.isSearchFiltersStepComplete,
    component: ProgramSearchFiltersForm,
  },
  [EProgramSteps.ESSENTIALS]: {
    key: EProgramSteps.ESSENTIALS,
    title: "Essentials",
    subtitle: "What are your essentials?",
    stepTitle: "Essentials",
    isStepComplete:
      ProgramsStepsCompleteService.isProgramEssentialsStepComplete,
    component: ProgramEssentialsForm,
  },
  [EProgramSteps.CURRICULUM]: {
    key: EProgramSteps.CURRICULUM,
    title: "Review",
    subtitle: "Share the best student testimonial of your program",
    stepTitle: "Curriculum",
    isStepComplete:
      ProgramsStepsCompleteService.isProgramCurriculumStepComplete,
    component: ProgramCurriculumForm,
  },
  [EProgramSteps.STUDENT_TESTIMONIAL]: {
    key: EProgramSteps.STUDENT_TESTIMONIAL,
    title: "Student Testimonial",
    subtitle: "What is your student testimonial?",
    stepTitle: "Student Testimonial",
    isStepComplete: ProgramsStepsCompleteService.isProgramTestimonyStepComplete,
    component: ProgramTestimonyForm,
  },
  [EProgramSteps.ADDITIONAL_HIGHLIGHTS]: {
    key: EProgramSteps.ADDITIONAL_HIGHLIGHTS,
    title: "Additional Highlights",
    subtitle: "Describe the additional highlights of your program",
    stepTitle: "Additional Highlights",
    isStepComplete: ProgramsStepsCompleteService.isHighlightsStepComplete,
    component: ProgramHighlightsForm,
  },
  [EProgramSteps.GALLERY]: {
    key: EProgramSteps.GALLERY,
    title: "GALLERY",
    subtitle: "Upload images to the program gallery",
    stepTitle: "Gallerys",
    isStepComplete: ProgramsStepsCompleteService.isProgramGalleryStepComplete,
    component: ProgramGalleryForm,
  },
  [EProgramSteps.SCHEDULE]: {
    key: EProgramSteps.SCHEDULE,
    title: "SCHEDULE",
    subtitle: "What are your essentials?",
    stepTitle: "Schedule",
    isStepComplete: ProgramsStepsCompleteService.isProgramScheduleStepComplete,
    component: ProgramScheduleForm,
  },
  [EProgramSteps.ACCOMMODATION]: {
    key: EProgramSteps.ACCOMMODATION,
    title: "ACCOMMODATION",
    subtitle: "What are your accommodations?",
    stepTitle: "Accommodation",
    isStepComplete: ProgramsStepsCompleteService.isAccomodationStepComplete,
    component: ProgramAccommodationForm,
  },
  [EProgramSteps.TICKETING_PRICING_PACKAGE]: {
    key: EProgramSteps.TICKETING_PRICING_PACKAGE,
    title: "TICKETING PRICING PACKAGE",
    subtitle: "Which package is best for you?",
    stepTitle: "Ticketing Pricing Packages",
    isStepComplete:
      ProgramsStepsCompleteService.isTicketingPricingPackageStepComplete,
    component: ProgramTicketingPricingPackage,
  },
  [EProgramSteps.PARTICIPANT_TYPES_QUOTA]: {
    key: EProgramSteps.PARTICIPANT_TYPES_QUOTA,
    title: "PARTICIPANT TYPES & QUOTA",
    subtitle: "Who and how many?",
    stepTitle: "Participant Types and Quota",
    isStepComplete: ProgramsStepsCompleteService.isParticipantTypesStepComplete,
    component: ProgramParticipantTypesForm,
  },
  [EProgramSteps.TICKETS]: {
    key: EProgramSteps.TICKETS,
    title: "TICKETS",
    subtitle: "Money, money & money.",
    stepTitle: "Tickets",
    isStepComplete: ProgramsStepsCompleteService.isTicketsStepComplete,
    component: ProgramTicketsForm,
  },
  [EProgramSteps.HIGHLIGHTS]: {
    key: EProgramSteps.HIGHLIGHTS,
    title: "HIGHLIGHTS",
    subtitle: "What are your highlights?",
    stepTitle: "Highlights",
    isStepComplete: ProgramsStepsCompleteService.isHighlightsStepComplete,
    component: ProgramHighlightsForm,
  },
  [EProgramSteps.APPLICATION]: {
    key: EProgramSteps.APPLICATION,
    title: "APPLICATION",
    subtitle: "What is your application?",
    stepTitle: "Application",
    isStepComplete: ProgramsStepsCompleteService.isApplicationStepComplete,
    component: ProgramApplicationForm,
  },
  [EProgramSteps.REFUND_POLICY]: {
    key: EProgramSteps.REFUND_POLICY,
    title: "REFUND POLICY",
    subtitle: "What is your refund policy?",
    stepTitle: "Refund Policy",
    isStepComplete: ProgramsStepsCompleteService.isRefundPolicyStepComplete,
    component: ProgramRefundPolicyForm,
  },
};

export const getProgramSteps = (steps: EProgramSteps[]) =>
  steps.map((step) => dictionary[step]);

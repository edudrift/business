import {
  every,
  flatMap,
  get,
  includes,
  isEmpty,
  isNull,
  isNumber,
  map,
  some,
  zip,
} from "lodash";
import { EProgramType, Program } from "../types/Programs.types";
import { ProgramTicketType } from "../types/ProgramTicketType.types";

function withProgram(program: Program) {
  if (!program) {
    return () => false;
  }
  return (test: (p: Program) => boolean) => test(program);
}

function isRequiredField(program: Program, path: string) {
  const val = get(program, path);
  return !isEmpty(val) || isNumber(val);
}

function isNullableField(
  program: Program,
  path: string,
  test: (program: Program, path: string) => boolean = isRequiredField
) {
  return isNull(get(program, path)) || test(program, path);
}

function applyPathOnFunction(
  f: (program: Program, path: string) => boolean,
  path: string | string[]
) {
  if (typeof path === "string" || typeof path === "undefined") {
    return (program: Program) => f(program, path);
  } else {
    return (program: Program) => path.every((p) => f(program, p));
  }
}

function isRequired(program: Program, path: string | string[]) {
  return withProgram(program)(applyPathOnFunction(isRequiredField, path));
}

function isValidMultiple(
  f: Array<(program: Program, path: string) => boolean>,
  path: Array<string | string[]>
) {
  return (program: Program) =>
    zip(f, path).reduce((prev, curr) => {
      const [f, path] = curr;
      if (f && typeof path !== "undefined") {
        return prev && applyPathOnFunction(f, path)(program);
      }
      return false;
    }, true);
}

class ProgramsStepsCompleteServiceClass {
  isProgramTypeStepComplete(program: Program) {
    return isRequired(program, "type");
  }

  isOrganisingStepComplete(program: Program) {
    return isRequired(program, ["partner", "organiser"]);
  }

  isProgramDetailsStepComplete(program: Program) {
    return (
      isRequired(program, [
        "name",
        "description",
        "briefDescription",
        "startDate",
        "endDate",
        // "applicationDeadline",
      ]) && isNullableField(program, "website")
    );
  }

  isSearchFiltersStepComplete(program: Program) {
    return isRequired(program, ["fields", "education"]);
  }

  isProgramEssentialsStepComplete(program: Program) {
    return isRequired(program, [
      "essentials.travel",
      "essentials.academic",
      "essentials.dining",
    ]);
  }

  isProgramCurriculumStepComplete(program: Program) {
    return isRequired(program, ["minAge", "maxAge"]);
  }

  isProgramTestimonyStepComplete(program: Program) {
    return isRequired(program, ["testimony"]);
  }

  isProgramGalleryStepComplete(program: Program) {
    return isRequired(program, ["mainImage", "featuredImages"]);
  }

  isProgramScheduleStepComplete(program: Program) {
    function isDayScheduleCompleted(program: Program) {
      const schedule = get(program, "schedule");
      const path = flatMap(schedule, (s, i) =>
        Object.keys(s.details).map((k) => `schedule[${i}].details.${k}[0]`)
      );
      return isRequired(program, path);
    }

    function isWeekScheduleCompleted(program: Program) {
      const schedule = get(program, "schedule");
      const path = map(schedule, (_, i) => `schedule[${i}].details`);
      const isWeekDetailsFilled = (program: Program, path: string) => {
        const details = get(program, path);
        if (!details) return false;
        return some(details, (i) => !isEmpty(i));
      };
      return withProgram(program)(
        applyPathOnFunction(isWeekDetailsFilled, path)
      );
    }

    const isScheduleCompleted = (program: Program) => {
      switch (program.type) {
        case EProgramType.COMPETITIONS_OR_CONFERENCES ||
          EProgramType.SHORT_STUDY_TOURS:
          return isDayScheduleCompleted(program);
        default:
          return isWeekScheduleCompleted(program);
      }
    };

    return withProgram(program)(
      isValidMultiple(
        [isRequiredField, isScheduleCompleted],
        [["startDate", "endDate", "schedule"], ""]
      )
    );
  }

  isProgramEligibilityStepComplete(program: Program) {
    return isRequired(program, "eligibility.education");
  }

  isTicketingPricingPackageStepComplete(program: Program) {
    return isRequired(program, "ticketingPackage");
  }

  isParticipantTypesStepComplete(program: Program) {
    return isRequired(program, "ticketTypes");
  }

  isHighlightsStepComplete(program: Program) {
    return isRequired(program, "highlights");
  }

  isApplicationStepComplete(program: Program) {
    return isRequired(program, "applicationFee");
  }

  isRefundPolicyStepComplete(program: Program) {
    return isRequired(program, "refundPolicy");
  }

  isAccomodationStepComplete(program: Program) {
    return isRequired(program, "accommodation");
  }

  isTicketsStepComplete(program: Program) {
    const hasOneTicket = (program: Program) => {
      const ticketIds = map(get(program, "tickets"), (t) => t.typeId);
      return every(
        map(get(program, "ticketTypes"), (t: ProgramTicketType) =>
          includes(ticketIds, t.id)
        )
      );
    };
    return withProgram(program)(
      isValidMultiple(
        [isRequiredField, hasOneTicket],
        [["ticketTypes", "tickets"], ""]
      )
    );
  }

  isDetailsListingComplete(program: Program) {
    return (
      this.isProgramTypeStepComplete(program) &&
      this.isProgramDetailsStepComplete(program) &&
      this.isOrganisingStepComplete(program) &&
      this.isSearchFiltersStepComplete(program)
    );
  }

  isScheduleListingComplete(program: Program) {
    return this.isProgramScheduleStepComplete(program);
  }

  isEssentialsListingComplete(program: Program) {
    return (
      this.isProgramCurriculumStepComplete(program) &&
      this.isProgramTestimonyStepComplete(program) &&
      this.isHighlightsStepComplete(program) &&
      this.isProgramGalleryStepComplete(program)
    );
  }

  isTicketingListingComplete(program: Program) {
    return (
      this.isTicketingPricingPackageStepComplete(program) &&
      this.isParticipantTypesStepComplete(program) &&
      this.isTicketsStepComplete(program) &&
      this.isApplicationStepComplete(program) &&
      this.isRefundPolicyStepComplete(program)
    );
  }
}

export const ProgramsStepsCompleteService = new ProgramsStepsCompleteServiceClass();

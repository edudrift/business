import { NextPageContext } from "next";
import { NotLoggedIn } from "../../../services/Auth.errors";
import TokenService from "../../../services/Token.service";
import { EProgramAPIRoutes } from "../../../utils/EAPIRoutes";
import EHttpMethods from "../../../utils/EHttpMethods";
import isofetch from "../../../utils/isofetch";
import { EProgramAssessment } from "../enums/EProgramAssessment";
import { EProgramCurrencies } from "../enums/EProgramCurrencies";
import { EProgramEducationalBackground } from "../enums/EProgramEducationalBackground";
import { EProgramFieldsOfInterest } from "../enums/EProgramFieldsOfInterest";
import { EProgramMethodology } from "../enums/EProgramMethodology";
import { EProgramSkill } from "../enums/EProgramSkill";
import { EProgramTicketPricingPackages } from "../enums/EProgramTicketPricingPackages";
import { ProgramAccommodationDetails } from "../types/ProgramAccommodation.types";
import { ProgramApplication } from "../types/ProgramApplication.types";
import { ProgramDetails } from "../types/ProgramDetails.types";
import { ProgramEarlyBird } from "../types/ProgramEarlyBird.types";
import { ProgramEssentials } from "../types/ProgramEssentials.types";
import { ProgramHighlight } from "../types/ProgramHighlight.types";
import { ProgramOrganiser } from "../types/ProgramOrganiser.types";
import { ProgramPartner } from "../types/ProgramPartner.types";
import { ProgramRefundPolicy } from "../types/ProgramRefundPolicy";
import { ProgramSchedule } from "../types/ProgramSchedule.types";
import { ProgramTestimonySchemaAsType } from "../types/ProgramTestimony";
import { ProgramTicket } from "../types/ProgramTicket.types";
import { ProgramTicketType } from "../types/ProgramTicketType.types";
import { EProgramType, Program } from "./../types/Programs.types";

class ProgramsServiceForBusinessClass {
  async getProgramFromId(
    programId: string,
    ctx?: NextPageContext
  ): Promise<Program> {
    const response = await isofetch<Program>(
      EProgramAPIRoutes.GET_SINGLE_PROGRAM.replace(":programId", programId),
      EHttpMethods.GET,
      TokenService.getToken(ctx)
    );

    if (response.status === 200 && response.data) {
      return response.data;
    } else {
      console.log(response);
      throw new Error("Something went wrong while fetching program.");
    }
  }

  async delete(programId: string) {
    const token = await TokenService.getToken();

    if (!token) throw new NotLoggedIn();

    const response = await isofetch<Program | { message: string }>(
      `/api/programs/b/${programId}`,
      EHttpMethods.DELETE,
      token
    );

    if (response.status !== 200) {
      // todo: better error
      const errorResponse = (response as any) as { data: { message: string } };
      throw new Error(errorResponse.data.message);
    }
  }

  async saveRefundPolicy(
    programId: Program["id"],
    refundPolicy: ProgramRefundPolicy
  ) {
    const token = await TokenService.getToken();

    if (!token) throw new NotLoggedIn();

    const response = await isofetch<Program>(
      `/api/programs/b/${programId}/refundPolicy`,
      EHttpMethods.PUT,
      token,
      {
        refundPolicy,
      }
    );

    if (response.status === 200 && response.data) {
      return response.data;
    } else {
      // todo: better error
      console.error(response);
      throw new Error();
    }
  }

  async saveApplication(
    programId: Program["id"],
    { applicationFee, additionalDocuments }: ProgramApplication
  ) {
    const token = await TokenService.getToken();

    if (!token) throw new NotLoggedIn();

    const response = await isofetch<Program>(
      `/api/programs/b/${programId}/application`,
      EHttpMethods.PUT,
      token,
      {
        applicationFee,
        additionalDocuments,
      }
    );

    if (response.status === 200 && response.data) {
      return response.data;
    } else {
      // todo: better error
      console.error(response);
      throw new Error();
    }
  }

  async saveEarlyBirdPricing(
    programId: Program["id"],
    currency: EProgramCurrencies,
    earlyBird: ProgramEarlyBird | null
  ) {
    const token = await TokenService.getToken();

    if (!token) throw new NotLoggedIn();

    const response = await isofetch<Program>(
      `/api/programs/b/${programId}/earlyBirdPricing`,
      EHttpMethods.PUT,
      token,
      {
        currency,
        earlyBird,
      }
    );

    if (response.status === 200 && response.data) {
      return response.data;
    } else {
      // todo: better error
      console.error(response);
      throw new Error();
    }
  }

  async saveTicketTypes(ticketTypes: Omit<ProgramTicketType, "id">[]) {
    const token = await TokenService.getToken();

    if (!token) throw new NotLoggedIn();

    const response = await isofetch<ProgramTicketType[]>(
      `/api/ticket-types`,
      EHttpMethods.POST,
      token,
      {
        ticketTypes,
      }
    );

    if (response.status === 201 && response.data) {
      return response.data;
    } else {
      const errorData = (response.data as any) as { message: string };
      throw new Error(errorData.message);
    }
  }

  async updateTicketTypes(ticketTypes: ProgramTicketType[]) {
    const token = await TokenService.getToken();

    if (!token) throw new NotLoggedIn();

    const response = await isofetch<ProgramTicketType[]>(
      `/api/ticket-types`,
      EHttpMethods.PUT,
      token,
      {
        ticketTypes,
      }
    );

    if (response.status === 200 && response.data) {
      return response.data;
    } else {
      const errorData = (response.data as any) as { message: string };
      throw new Error(errorData.message);
    }
  }

  async deleteTicketTypes(ticketTypes: ProgramTicketType[]) {
    const token = await TokenService.getToken();

    if (!token) throw new NotLoggedIn();

    const response = await isofetch<{
      ticketType: ProgramTicketType[];
      tickets: ProgramTicket[][];
    }>(`/api/ticket-types`, EHttpMethods.DELETE, token, {
      ticketTypes,
    });

    if (response.status === 200 && response.data) {
      const { ticketType } = response.data;
      return ticketType;
    } else {
      const errorData = (response.data as any) as { message: string };
      throw new Error(errorData.message);
    }
  }

  async saveTicketingPackage(
    programId: string,
    ticketingPackage: EProgramTicketPricingPackages
  ) {
    const token = await TokenService.getToken();

    if (!token) throw new NotLoggedIn();

    const response = await isofetch<Program>(
      `/api/programs/b/${programId}/ticketingPackage`,
      EHttpMethods.PUT,
      token,
      {
        ticketingPackage,
      }
    );

    if (response.status === 200 && response.data) {
      return response.data;
    } else {
      // todo: better error
      console.error(response);
      throw new Error();
    }
  }

  async saveHighlights(programId: string, highlights: ProgramHighlight[]) {
    const token = await TokenService.getToken();

    if (!token) throw new NotLoggedIn();

    const response = await isofetch<Program>(
      `/api/programs/b/${programId}/highlights`,
      EHttpMethods.PUT,
      token,
      {
        highlights,
      }
    );

    if (response.status === 200 && response.data) {
      return response.data;
    } else {
      // todo: better error
      console.error(response);
      throw new Error();
    }
  }

  async saveGallery(
    programId: string,
    mainImage: string,
    featuredImages: string[],
    images: string[]
  ) {
    const token = await TokenService.getToken();

    if (!token) throw new NotLoggedIn();

    const response = await isofetch<Program>(
      `/api/programs/b/${programId}/gallery`,
      EHttpMethods.PUT,
      token,
      {
        gallery: {
          mainImage,
          featuredImages,
          images,
        },
      }
    );

    if (response.status === 200 && response.data) {
      return response.data;
    } else {
      // todo: better error
      console.error(response);
      throw new Error();
    }
  }

  async saveAccommodation(
    programId: string,
    details?: ProgramAccommodationDetails
  ) {
    const token = await TokenService.getToken();

    if (!token) throw new NotLoggedIn();

    const response = await isofetch<Program>(
      `/api/programs/b/${programId}/accommodation`,
      EHttpMethods.PUT,
      token,
      { details }
    );

    if (response.status === 200 && response.data) {
      return response.data;
    } else {
      // todo: better error
      console.error(response);
      throw new Error();
    }
  }

  async saveSchedule(programId: string, schedule: ProgramSchedule) {
    const token = await TokenService.getToken();

    if (!token) throw new NotLoggedIn();

    const response = await isofetch<Program>(
      `/api/programs/b/${programId}/schedule`,
      EHttpMethods.PUT,
      token,
      {
        schedule,
      }
    );

    if (response.status === 200 && response.data) {
      return response.data;
    } else {
      // todo: better error
      console.error(response);
      throw new Error();
    }
  }

  async saveEssentials(
    programId: string,
    essentials: ProgramEssentials
  ): Promise<Program> {
    const token = await TokenService.getToken();

    if (!token) throw new NotLoggedIn();
    const sanitizedEssentials = Object.assign({}, essentials);

    if (!essentials.travel.includesTourPackage) {
      delete sanitizedEssentials.travel.fromOrganisingTeam;
      delete sanitizedEssentials.travel.externalTourVendor;
    }

    if (
      essentials.travel.includesTourPackage &&
      essentials.travel.fromOrganisingTeam
    ) {
      delete sanitizedEssentials.travel.externalTourVendor;
    }

    const response = await isofetch<Program>(
      `/api/programs/b/${programId}/essentials`,
      EHttpMethods.PUT,
      token,
      {
        essentials,
      }
    );

    if (response.status === 200 && response.data) {
      return response.data;
    } else {
      // todo: better error
      console.error(response);
      throw new Error();
    }
  }

  async saveSearchFilters(
    programId: string,
    fields: EProgramFieldsOfInterest[],
    education: EProgramEducationalBackground[]
  ) {
    const token = await TokenService.getToken();

    if (!token) throw new NotLoggedIn();

    const response = await isofetch<Program>(
      `/api/programs/b/${programId}/search-filters`,
      EHttpMethods.PUT,
      token,
      {
        fields,
        education,
      }
    );

    if (response.status === 200 && response.data) {
      return response.data;
    } else {
      // todo: better error
      console.error(response);
      throw new Error();
    }
  }

  async saveCurriculum(
    programId: string,
    minAge: number | null,
    maxAge: number | null,
    skills: EProgramSkill[],
    methodology: EProgramMethodology[],
    assessment: EProgramAssessment[]
  ) {
    const token = await TokenService.getToken();

    if (!token) throw new NotLoggedIn();

    const response = await isofetch<Program>(
      `/api/programs/b/${programId}/curriculum`,
      EHttpMethods.PUT,
      token,
      {
        curriculum: {
          minAge,
          maxAge,
          skills,
          methodology,
          assessment,
        },
      }
    );

    if (response.status === 200 && response.data) {
      return response.data;
    } else {
      // todo: better error
      console.error(response);
      throw new Error();
    }
  }

  async saveTestimony(
    programId: string,
    testimony: ProgramTestimonySchemaAsType
  ) {
    const token = await TokenService.getToken();

    if (!token) throw new NotLoggedIn();

    const response = await isofetch<Program>(
      `/api/programs/b/${programId}/testimony`,
      EHttpMethods.PUT,
      token,
      { testimony }
    );

    if (response.status === 200 && response.data) {
      return response.data;
    } else {
      // todo: better error
      console.error(response);
      throw new Error();
    }
  }

  async saveDetails(
    programId: string,
    details: ProgramDetails
  ): Promise<Program> {
    const token = await TokenService.getToken();

    if (!token) throw new NotLoggedIn();

    const response = await isofetch<Program>(
      `/api/programs/b/${programId}/details`,
      EHttpMethods.PUT,
      token,
      {
        details,
      }
    );

    if (response.status === 200 && response.data) {
      return response.data;
    } else {
      // todo: better error
      console.error(response);
      throw new Error();
    }
  }

  async saveOrganisingTeam(
    programId: string,
    organiser: ProgramOrganiser,
    partner: ProgramPartner
  ): Promise<Program> {
    const token = await TokenService.getToken();

    if (!token) throw new NotLoggedIn();

    const response = await isofetch<Program>(
      `/api/programs/b/${programId}/organising-team`,
      EHttpMethods.PUT,
      token,
      {
        organiser,
        partner,
      }
    );

    if (response.status === 200 && response.data) {
      console.log(response);
      return response.data;
    } else {
      // todo: better error
      console.log(response);
      throw new Error();
    }
  }

  async saveProgramType(
    programId: string,
    programType: EProgramType
  ): Promise<Program> {
    const token = await TokenService.getToken();

    if (!token) throw new NotLoggedIn();

    const response = await isofetch<Program>(
      `/api/programs/b/${programId}/program-types`,
      EHttpMethods.PUT,
      token,
      {
        programType,
      }
    );

    if (response.status === 200 && response.data) {
      console.log(response);
      return response.data;
    } else {
      // todo: better error
      console.log(response);
      throw new Error();
    }
  }

  async getProgramProviderForPreloading(): Promise<ProgramOrganiser> {
    const token = TokenService.getToken();

    if (!token) throw new NotLoggedIn();

    const response = await isofetch<ProgramOrganiser>(
      "/api/program-providers/preload",
      EHttpMethods.GET,
      token
    );

    if (response.status === 200 && response.data) {
      return response.data;
    } else {
      // todo: better error
      console.error(response);
      throw new Error();
    }
  }

  async getAllPrograms() {
    const response = await isofetch<Program[]>(
      "/api/programs/b",
      EHttpMethods.GET
    );

    if (response.status === 200 && response.data) {
      return response.data;
    } else {
      // todo: better error
      console.error(response);
      throw new Error();
    }
  }
}

export const ProgramsServiceForBusiness = new ProgramsServiceForBusinessClass();

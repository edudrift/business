import * as Y from "yup";
import {
  findIndex,
  findLastIndex,
  isEqual,
  map,
  range,
  size,
  zipObject,
} from "lodash";

import { Program } from "./Programs.types";
import { requiredText } from "../../../utils/formikUtils";
import { positiveInteger } from "../../../utils/yupUtils";

export type ProgramTicketType = {
  id: string;
  name: string;
  min: number;
  max: number;
  minAge: number | null;
  maxAge: number | null;
  programId: Program["id"];
};

const ProgramTicketTypeSchema = Y.object().shape({
  id: Y.string(),
  name: Y.string().required(requiredText),
  min: Y.number(),
  max: positiveInteger.default(0).required(requiredText),
  minAge: positiveInteger.nullable(),
  maxAge: positiveInteger.nullable(),
  programId: Y.string(),
});

const hasDuplicate = (names: string[], name: string) => {
  return !isEqual(
    findIndex(names, (n) => n === name),
    findLastIndex(names, (n) => n === name)
  );
};

export const ProgramTicketTypeArraySchema = Y.lazy(
  (value: { [k: number]: ProgramTicketType }) => {
    const number = Object.keys(value).length;
    return Y.object()
      .shape(
        zipObject(
          range(number),
          range(number).map(() => ProgramTicketTypeSchema)
        )
      )
      .test({
        test: function(ticketTypes: { [k: number]: ProgramTicketType }) {
          const ticketTypeNames = map(ticketTypes, "name");
          return range(size(ticketTypes))
            .filter((i) => hasDuplicate(ticketTypeNames, ticketTypeNames[i]))
            .reduce((err, i) => {
              err.inner.push(
                this.createError({
                  path: `${i}.name`,
                  message: "Name must be unique",
                })
              );
              return err;
            }, this.createError());
        },
      });
  }
);

import { Program } from "./Programs.types";
import { ProgramTicketType } from "./ProgramTicketType.types";

export type ProgramTicket = {
  id: string;
  name: string;
  price: string;
  eligibility?: string;
  description: string;
  type: ProgramTicketType;
  typeId: ProgramTicketType["id"];
  program: Program;
  createdAt: string;
};

import * as Y from "yup";

export type ProgramPartner = {
  name: string;
  descriptionOfRole: string;
  contactPerson?: string;
  contactNumber?: string;
};

export const ProgramPartnerSchema = Y.object().shape({
  name: Y.string(),
  descriptionOfRole: Y.string(),
  contactPerson: Y.string(),
  contactNumber: Y.string(),
});

export type ProgramPartnerSchemaAsType = Y.InferType<
  typeof ProgramPartnerSchema
>;

import * as Y from "yup";
import { requiredText } from "../../../utils/formikUtils";

export type ProgramOrganiser = {
  name: string;
  manager: string;
  email: string;
  number: string;
  emergencyPerson: string;
  emergencyNumber: string;
};

export const ProgramOrganiserSchema = Y.object().shape({
  name: Y.string().required(requiredText),
  manager: Y.string().required(requiredText),
  email: Y.string().required(requiredText),
  number: Y.string().required(requiredText),

  emergencyPerson: Y.string().required(requiredText),
  emergencyNumber: Y.string().required(requiredText),
});

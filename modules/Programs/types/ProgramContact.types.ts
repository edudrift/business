import * as Y from "yup";
import { requiredText } from "../../../utils/formikUtils";

import { FormikContactNumberSchema } from "../../../components/Forms/components/FormikContactNumber";

export type ProgramContact = {
  programManagerName: string;
  number: string;
  email: string;
};

export const ProgramContactSchema = Y.object().shape({
  programManagerName: Y.string().required(requiredText),
  number: FormikContactNumberSchema,
  email: Y.string().required(requiredText),
});

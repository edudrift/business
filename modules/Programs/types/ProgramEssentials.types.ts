import * as Y from "yup";

import {
  ProgramAcademicDetails,
  ProgramAcademicDetailsSchema,
} from "./ProgramAcademicDetails.types";
import {
  ProgramDiningDetails,
  ProgramDiningDetailsSchema,
} from "./ProgramDiningDetails.types";
import {
  ProgramTravelDetails,
  ProgramTravelDetailsSchema,
} from "./ProgramTravelDetails";

export type ProgramEssentials = {
  travel: ProgramTravelDetails;
  academic: ProgramAcademicDetails;
  dining: ProgramDiningDetails;
};

export const ProgramEssentialsSchema = Y.object().shape({
  travel: ProgramTravelDetailsSchema,
  academic: ProgramAcademicDetailsSchema,
  dining: ProgramDiningDetailsSchema,
});

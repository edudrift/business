export const isProgramDay = (
  programDay: ProgramDaySchedule | ProgramWeekSchedule
) => programDay.type === "day";

export const isProgramWeek = (
  programDay: ProgramDaySchedule | ProgramWeekSchedule
) => programDay.type === "week";

export type ProgramSchedule = (ProgramDaySchedule | ProgramWeekSchedule)[];

export type ProgramWeekSchedule = {
  type: "week";
  weekNumber: number;
  startDate: Date;
  endDate: Date;
  details: {
    [key: number]: string;
  };
};

export enum DayDetails {
  MORNING = "morning",
  AFTERNOON = "afternoon",
  EVENING = "evening",
}

export type ProgramDaySchedule = {
  type: "day";
  dayNumber: number;
  date: Date;
  details: {
    [DayDetails.MORNING]: string[];
    [DayDetails.AFTERNOON]: string[];
    [DayDetails.EVENING]: string[];
  };
};

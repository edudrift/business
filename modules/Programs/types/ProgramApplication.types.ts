import * as Y from "yup";

import { requiredText } from "../../../utils/formikUtils";
import { numberIn2Decimal } from "../../../utils/yupUtils";
import {
  ProgramAdditionalDocuments,
  ProgramAdditionalDocumentsSchema,
} from "./ProgramAdditionalDocuments.types";

export type ProgramApplication = {
  hasApplicationFee: boolean;
  applicationFee: number | null;
  additionalDocuments: ProgramAdditionalDocuments[];
};

export const ProgramApplicationSchema = Y.object().shape({
  applicationFee: Y.mixed().when("hasApplicationFee", {
    is: true,
    then: numberIn2Decimal.required(requiredText),
    otherwise: Y.mixed().nullable(),
  }),
  additionalDocuments: ProgramAdditionalDocumentsSchema,
});

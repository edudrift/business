import * as Y from "yup";
import { requiredText } from "./../../../utils/formikUtils";

export type ProgramExternalTravelVendor = {
  name: string;
  vendorDescription: string;
};

export const ProgramExternalTravelVendorSchema = Y.object().shape({
  name: Y.string().required(requiredText),
  vendorDescription: Y.string().required(requiredText),
  tourDescription: Y.string().required(requiredText),
});

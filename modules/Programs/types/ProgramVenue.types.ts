import * as Y from "yup";
import { requiredText } from "../../../utils/formikUtils";

export type ProgramVenue = {
  name: string;
  address: string;
  addressInLocal: string | null;
  countryCode: string;
  city: string;
  // description: string;
  // images: string[];
};

export const ProgramVenueSchema = Y.object().shape({
  name: Y.string().required(requiredText),
  address: Y.string().required(requiredText),
  addressInLocal: Y.string().nullable(),
  country: Y.object()
    .shape({
      label: Y.string(),
      code: Y.string(),
      phone: Y.string(),
    })
    .required(requiredText),
  city: Y.string().required(requiredText),
  // description: Y.string().required(requiredText),
});

export const ProgramVenueDefaults = {
  name: "",
  address: "",
  addressInLocal: "",
  country: {
    label: "Singapore",
    phone: "65",
    code: "SG",
  },
  city: "",
  // description: "",
};

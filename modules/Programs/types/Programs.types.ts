import ProgramProvider from "../../../types/ProgramProvider.types";
import { EProgramFieldsOfInterest } from "../enums/EProgramFieldsOfInterest";
import { ProgramAccommodation } from "./ProgramAccommodation.types";
import { ProgramEarlyBird } from "./ProgramEarlyBird.types";
import { ProgramEssentials } from "./ProgramEssentials.types";
import { ProgramHighlight } from "./ProgramHighlight.types";
import { ProgramOrganiser } from "./ProgramOrganiser.types";
import { ProgramPartner } from "./ProgramPartner.types";
import { ProgramTicket } from "./ProgramTicket.types";
import { ProgramTicketType } from "./ProgramTicketType.types";
import { ProgramVenue } from "./ProgramVenue.types";
import { ProgramSchedule } from "./ProgramSchedule.types";
import { EProgramCurrencies } from "../enums/EProgramCurrencies";
import { ProgramRefundPolicy } from "./ProgramRefundPolicy";
import { EProgramTicketPricingPackages } from "../enums/EProgramTicketPricingPackages";
import { ProgramAdditionalDocuments } from "./ProgramAdditionalDocuments.types";
import { EProgramEducationalBackground } from "../enums/EProgramEducationalBackground";
import { EProgramSkill } from "../enums/EProgramSkill";
import { EProgramMethodology } from "../enums/EProgramMethodology";
import { EProgramAssessment } from "../enums/EProgramAssessment";
import { ProgramTestimony } from "./ProgramTestimony";
import { ProgramContact } from "./ProgramContact.types";
import { EProgramParticipantProfile } from "../enums/EProgramParticipantProfile";

export type Program = {
  id: string;
  status: EProgramStatus;
  provider: ProgramProvider;
  createdById: string;
  createdAt: Date;
  updatedAt: Date;

  /**
   * Program Details
   */
  type: EProgramType;

  name: string;
  startDate: Date;
  endDate: Date;
  applicationDeadline: Date;
  website: string | null;
  briefDescription: string;
  description: string;
  mainImage: string;
  featuredImages: string[];
  images: string[];

  venue: ProgramVenue;
  onlineProgram: OnlineProgram;

  minAge: number | null;
  maxAge: number | null;

  organiser: ProgramOrganiser;
  partner: ProgramPartner;
  testimony: ProgramTestimony;
  contact: ProgramContact;

  fields: EProgramFieldsOfInterest[];
  education: EProgramEducationalBackground[];
  participantProfile: EProgramParticipantProfile[];
  skills: EProgramSkill[];
  methodology: EProgramMethodology[];
  assessment: EProgramAssessment[];

  /**
   * Schedule
   */
  schedule: ProgramSchedule;

  /**
   * Essentails & Highlights
   */
  essentials: ProgramEssentials;

  accommodation: ProgramAccommodation;

  highlights: ProgramHighlight[];

  /**
   * Ticketing
   */
  ticketingPackage: EProgramTicketPricingPackages;

  ticketTypes: ProgramTicketType[];

  currency: EProgramCurrencies;
  earlyBird: ProgramEarlyBird | null;
  tickets: ProgramTicket[];

  applicationFee: number | null;
  additionalDocuments: ProgramAdditionalDocuments[];

  refundPolicy: ProgramRefundPolicy;
};

export type OnlineProgram = {
  hostingLink: string | null;
};

export enum EProgramStatus {
  PENDING = "PENDING",
  SUBMITTED = "SUBMITTED",
  REVIEWING = "REVIEWING",
  APPROVED = "APPROVED",
  REJECTED = "REJECTED",
  PAST = "PAST",
}

export enum EProgramType {
  SUMMER_WINTER_SCHOOL = "SUMMER_WINTER_SCHOOL",
  SHORT_STUDY_TOURS = "SHORT_STUDY_TOURS",
  OVERSEAS_INTERNSHIPS = "OVERSEAS_INTERNSHIPS",
  COMPETITIONS_OR_CONFERENCES = "COMPETITIONS_OR_CONFERENCES",
  LOCAL_ENRICHMENT_CLASSES = "LOCAL_ENRICHMENT_CLASSES",
}

export enum EProgramTypeLabels {
  SUMMER_WINTER_SCHOOL = "Summer/Winter School",
  SHORT_STUDY_TOURS = "Short Study Tours",
  OVERSEAS_INTERNSHIPS = "Overseas Internships",
  COMPETITIONS_OR_CONFERENCES = "Competitions or Conferences",
  LOCAL_ENRICHMENT_CLASSES = "Local Enrichment Classes",
}

export enum EProgramTypeDescriptions {
  SUMMER_WINTER_SCHOOL = "Official accredited/non-accredited university programs conducted in the summer or winter school breaks. These programs usually last between 3-12 weeks with a variety of courses for students to choose from.",
  SHORT_STUDY_TOURS = "Tournaments, competitions and other international events for both international and local student participants. These programs include, but not limited to, debate competitions, mooting competitions, robotics, public speaking, tech exhibitions, etc.",
  OVERSEAS_INTERNSHIPS = "Arranged internships in various countries done in partnership with start-ups, SMEs, NGOs, and other partnering organisations. These program may involve participants receiving an internship allowance/stipend. Length of programs vary from 3 weeks to 6 months.",
  COMPETITIONS_OR_CONFERENCES = "Reputable programs conducted in collaboration with institutions and/or universities. These programs are usually theme-based, and also provide a cultural-immersion experience for visiting students. Length of programs vary from 3 days to 2 weeks.",
  LOCAL_ENRICHMENT_CLASSES = "Reputable programs conducted in collaboration with institutions and/or universities. These programs are usually theme-based, and also provide a cultural-immersion experience for visiting students. Length of programs vary from 3 days to 2 weeks.",
}

export const OnlineProgramDefaults = {
  name: "",
};

export type ProgramHighlight = {
  title: string;
  description: string;
  images: string[];
  type: string;
};

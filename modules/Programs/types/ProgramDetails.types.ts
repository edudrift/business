import * as Y from "yup";
import { requiredText } from "../../../utils/formikUtils";
// import { OnlineProgram } from "./Programs.types";
import { ProgramContact } from "./ProgramContact.types";
import { FormikContactNumberSchema } from "~/components/Forms/components/FormikContactNumber";

export type ProgramDetails = {
  name: string;
  type: string;
  participantProfile: string[];
  startDate: Date | null;
  endDate: Date | null;
  contact: ProgramContact;
  // onlineProgram: OnlineProgram;
  // educaton: string[];
  // website: string | null;
  // briefDescription: string;
  // description: string;
  // applicationDeadline: Date | null;
  // venue: ProgramVenue;
  // images: string[];
};

export const ProgramDetailsSchema = Y.object().shape({
  name: Y.string().required(requiredText),
  type: Y.string().required(requiredText),
  startDate: Y.date()
    .nullable()
    .required(requiredText),
  endDate: Y.date()
    .nullable()
    .required(requiredText),
  contact: Y.object().shape({
    programManagerName: Y.string().required(requiredText),
    number: FormikContactNumberSchema,
    email: Y.string().required(requiredText),
  }),
  // onlineProgram: Y.object().shape({
  //   hostingLink: Y.string().nullable(),
  // }),
  // website: Y.string()
  //   .url("We do not recognise that website")
  //   .nullable(),
  // briefDescription: Y.string().required(requiredText),
  // description: Y.string().required(requiredText),
  // applicationDeadline: Y.date()
  //   .nullable()
  //   .required(requiredText),
  // venue: Y.object().shape({
  //   name: Y.string().required(requiredText),
  //   address: Y.string().required(requiredText),
  //   addressInLocal: Y.string().nullable(),
  //   country: Y.object()
  //     .shape({
  //       label: Y.string(),
  //       code: Y.string(),
  //       phone: Y.string(),
  //     })
  //     .required(requiredText),
  //   city: Y.string().required(requiredText),
  //   description: Y.string().required(requiredText),
  // }),
});

export type ProgramDetailsSchemaAsType = Y.InferType<
  typeof ProgramDetailsSchema
>;

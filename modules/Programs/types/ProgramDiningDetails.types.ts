import * as Y from "yup";
import { requiredText } from "./../../../utils/formikUtils";

export type ProgramDiningDetails = {
  providedMeals: string;
  mealsDetails: string;
  images: string[];
};

export const ProgramDiningDetailsSchema = Y.object().shape({
  providedMeals: Y.string().required(requiredText),
  mealsDetails: Y.string().required(requiredText),
});

import * as Y from "yup";
import { requiredText } from "../../../utils/formikUtils";

export enum EProgramAdditionalDocumentTypes {
  TEXT = "TEXTFIELD",
  FILE = "FILE",
}

export type ProgramAdditionalDocuments = {
  name: string;
  type: EProgramAdditionalDocumentTypes;
};

export const ProgramAdditionalDocumentsSchema = Y.array().of(
  Y.object()
    .shape({
      name: Y.string().required(requiredText),
      type: Y.string()
        .typeError(requiredText)
        .oneOf(Object.values(EProgramAdditionalDocumentTypes), requiredText),
    })
    .nullable()
);

import * as Y from "yup";
import { requiredText } from "../../../utils/formikUtils";

export type ProgramTestimony = {
  testimonyType: string;
  displayName: string;
  datesOfAttendance: Date;
  programType: string;
  programName: string;
  review: string;
};

export const ProgramTestimonySchema = Y.object().shape({
  displayName: Y.string().required(requiredText),
  datesOfAttendance: Y.date(),
  programType: Y.string(),
  programName: Y.string(),
  review: Y.string(),
});

export type ProgramTestimonySchemaAsType = Y.InferType<
  typeof ProgramTestimonySchema
>;

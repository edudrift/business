import * as Y from "yup";

import { EProgramRefundTypes } from "./../enums/EProgramRefundTypes";
import { oneOfEnum, numberIn2Decimal } from "../../../utils/yupUtils";
import { requiredText } from "../../../utils/formikUtils";

export type ProgramRefundPolicy = {
  fullRefundBeforeDate: Date | null;
  noRefundAfterDate: Date | null;
  variableRefundBeforeDate: Date | null;
  variableRefundPercentage: number | null;
  applicationFee: {
    acceptedParticipants: {
      refundType: EProgramRefundTypes;
      amount?: number;
    };
    rejectedParticipants: {
      refundType: EProgramRefundTypes;
      amount?: number;
    };
  };
};

export const ProgramRefundPolicySchema = Y.object().shape({
  noRefundAfterDate: Y.date()
    .typeError(requiredText)
    .required(requiredText),
  variableRefundPercentage: Y.number().nullable(),
  variableRefundBeforeDate: Y.date().nullable(),
  fullRefundBeforeDate: Y.date().nullable(),
  applicationFee: Y.object().shape({
    acceptedParticipants: Y.object().shape({
      refundType: oneOfEnum(EProgramRefundTypes),
      amount: Y.mixed().when("refundType", {
        is: EProgramRefundTypes.PARTIAL_REFUND,
        then: numberIn2Decimal.required(requiredText),
        otherwise: Y.mixed().nullable(),
      }),
    }),
    rejectedParticipants: Y.object().shape({
      refundType: oneOfEnum(EProgramRefundTypes),
      amount: Y.mixed().when("refundType", {
        is: EProgramRefundTypes.PARTIAL_REFUND,
        then: numberIn2Decimal.required(requiredText),
        otherwise: Y.mixed().nullable(),
      }),
    }),
  }),
});

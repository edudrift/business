import * as Y from "yup";
import { requiredText } from "./../../../utils/formikUtils";

import {
  ProgramExternalTravelVendor,
  ProgramExternalTravelVendorSchema,
} from "./ProgramExternalTravelVendor.types";

export type ProgramTravelDetails = {
  includesTourPackage: boolean;
  fromOrganisingTeam?: boolean;
  tourDescription: string;
  externalTourVendor?: ProgramExternalTravelVendor;
  images: string[];
};

export const ProgramTravelDetailsSchema = Y.object().shape({
  includesTourPackage: Y.boolean().required(requiredText),
  fromOrganisingTeam: Y.boolean().notRequired(),
  externalTourVendor: ProgramExternalTravelVendorSchema.notRequired(),
});

import * as Y from "yup";
import { requiredText } from "./../../../utils/formikUtils";

export type ProgramAcademicDetails = {
  lectureDescription: string;
  teachingStaffDescription: string;
  certificationDescription: string;
  images: string[];
};

export const ProgramAcademicDetailsSchema = Y.object().shape({
  lectureDescription: Y.string().required(requiredText),
  teachingStaffDescription: Y.string().required(requiredText),
  certificationDescription: Y.string().required(requiredText),
});

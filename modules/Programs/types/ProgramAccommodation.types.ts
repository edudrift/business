import { EProgramAccommodationRoomTypes } from "../enums/EProgramAccommodationRoomTypes";
import { EProgramAccommodationTypes } from "../enums/EProgramAccommodationTypes";
import { EProgramAirportTransfers } from "./../enums/EProgramAirportTransfers";

export type ProgramAccommodation = {
  provided: boolean;
  details?: ProgramAccommodationDetails;
};

export type ProgramAccommodationDetails = {
  name: string;
  localName: string;
  address: {
    lineOne: string;
    lineTwo: string;
    postalCode: string;
  };
  localAddress: {
    lineOne: string;
    lineTwo: string;
  };
  type: EProgramAccommodationTypes;
  transfer: EProgramAirportTransfers;
  roomTypes: EProgramAccommodationRoomTypes[];
  images: string[];
};

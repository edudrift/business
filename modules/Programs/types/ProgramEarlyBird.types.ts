import * as Y from "yup";
import { requiredText } from "../../../utils/formikUtils";
import { numberWithDecimal } from "../../../utils/yupUtils";

export type ProgramEarlyBird = {
  startDate: Date;
  endDate: Date;
  discountedAmount: number;
  discountType: string;
};

export const ProgramEarlyBirdSchema = Y.object()
  .shape({
    startDate: Y.date().required(requiredText),
    endDate: Y.date().required(requiredText),
    discountedAmount: numberWithDecimal.required(requiredText),
    discountType: Y.string()
      .oneOf(["FIXED", "PERCENTAGE"], "Discount Type is not selected")
      .required(requiredText),
  })
  .nullable();

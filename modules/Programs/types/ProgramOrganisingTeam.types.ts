import * as Yup from "yup";

import {
  ProgramOrganiser,
  ProgramOrganiserSchema,
} from "./ProgramOrganiser.types";
import { ProgramPartner, ProgramPartnerSchema } from "./ProgramPartner.types";

export type ProgramOrganisingTeam = {
  partner: ProgramPartner;
  organiser: ProgramOrganiser;
};

export const ProgramOrganisingTeamSchema = Yup.object().shape({
  partner: ProgramPartnerSchema,
  organiser: ProgramOrganiserSchema,
});

export type ProgramOrganisingTeamSchemaAsType = Yup.InferType<
  typeof ProgramOrganisingTeamSchema
>;

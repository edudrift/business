import { NotLoggedIn } from "../../services/Auth.errors";
import TokenService from "../../services/Token.service";
import EHttpMethods from "../../utils/EHttpMethods";
import isofetch from "../../utils/isofetch";
import { Program } from "../Programs/types/Programs.types";
import { ProgramTicket } from "../Programs/types/ProgramTicket.types";
import { ProgramTicketType } from "../Programs/types/ProgramTicketType.types";

class TicketsServiceClass {
  async create(
    programId: Program["id"],
    ticketTypeId: ProgramTicketType["id"],
    ticket: Pick<
      ProgramTicket,
      "name" | "description" | "price" | "eligibility"
    >
  ) {
    const token = await TokenService.getToken();

    if (!token) throw new NotLoggedIn();

    const response = await isofetch<ProgramTicket>(
      `/api/tickets`,
      EHttpMethods.POST,
      token,
      {
        ticket: {
          ...ticket,
          program: {
            id: programId,
          },
          type: {
            id: ticketTypeId,
          },
        },
      }
    );

    if (response.status === 201 && response.data) {
      return response.data;
    } else {
      // todo: better error
      console.error(response);
      throw new Error();
    }
  }

  async delete(ticketId: ProgramTicket["id"]) {
    const token = await TokenService.getToken();

    if (!token) throw new NotLoggedIn();

    const response = await isofetch<number>(
      `/api/tickets/${ticketId}`,
      EHttpMethods.DELETE,
      token
    );

    if (response.status === 200 && response.data) {
      return response.data;
    } else {
      // todo: better error
      console.error(response);
      throw new Error();
    }
  }

  async save(
    ticketId: ProgramTicket["id"],
    ticket: Pick<
      ProgramTicket,
      "name" | "description" | "price" | "eligibility"
    >
  ): Promise<ProgramTicket> {
    const token = await TokenService.getToken();

    if (!token) throw new NotLoggedIn();

    const response = await isofetch<ProgramTicket>(
      `/api/tickets/${ticketId}`,
      EHttpMethods.PUT,
      token,
      {
        ticket,
      }
    );

    if (response.status === 200 && response.data) {
      return response.data;
    } else {
      // todo: better error
      console.error(response);
      throw new Error();
    }
  }
}

export const TicketsService = new TicketsServiceClass();

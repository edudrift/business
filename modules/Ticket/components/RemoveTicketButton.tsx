import React from "react";

import { Popover, Grid, Button } from "@material-ui/core";
import { PaddedPaper } from "../../Shared/components/PaddedPaper";

export const RemoveTicketButton: React.FC<{ onClick: any }> = ({ onClick }) => {
  const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(
    null
  );

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? "create-program-cancel-button" : undefined;

  return (
    <React.Fragment>
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "center",
        }}
      >
        <PaddedPaper>
          <Grid container spacing={2} direction="column">
            <Grid item>Are you sure? This cannot be undone.</Grid>

            <Button
              onClick={() => {
                handleClose();
                onClick();
              }}
            >
              Yes
            </Button>
          </Grid>
        </PaddedPaper>
      </Popover>
      <Button onClick={handleClick} color="secondary">
        Remove
      </Button>
    </React.Fragment>
  );
};

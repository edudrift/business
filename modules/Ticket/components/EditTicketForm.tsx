import React from "react";

import { Button, Grid } from "@material-ui/core";
import { useFormikContext } from "formik";

import { FormikTextField } from "../../../components/Forms/components/FormikTexField";
import { ProgramTicket } from "../../Programs/types/ProgramTicket.types";
import { RemoveTicketButton } from "./RemoveTicketButton";
import { isEqual, isUndefined, get } from "lodash";
import { TicketsService } from "../TicketsService";
import { TicketSchema } from "../types/Ticket.schema";

export const EditTicketForm: React.FC<Props> = (props) => {
  const { values, isValidating, errors, touched } = useFormikContext<
    formikValues
  >();

  const ticketProps = props.ticket;
  const ticket = values.tickets[props.index];

  const [submitting, setSubmitting] = React.useState(false);

  const dirty = !isEqual(ticketProps, ticket);
  const error =
    !TicketSchema.isValidSync(ticket) ||
    !isUndefined(get(errors, `tickets[${props.index}].name`));

  const withSubmit = (f: () => Promise<void>, isDelete = false) => {
    return async () => {
      setSubmitting(false);
      await f();
      if (!isDelete) {
        setSubmitting(true);
      }
    };
  };

  const handleUpdate = async () => {
    //TODO: Remove once problem is solved
    try {
      const isInProps = !isUndefined(ticketProps);
      const updatedTicket = await (async () => {
        if (isInProps) {
          return await TicketsService.save(ticket.id, ticket);
        } else {
          return await TicketsService.create(
            props.programId,
            ticket.typeId,
            ticket
          );
        }
      })();
      props.afterSubmit(updatedTicket);
    } catch (err) {
      alert(err);
    }
  };

  const handleDelete = async () => {
    const isInProps = !isUndefined(ticketProps);
    if (isInProps) {
      await TicketsService.delete(ticket.id);
    }
    props.afterSubmit();
  };

  return (
    <Grid item container spacing={3} direction="row">
      <Grid item container md={2}>
        <FormikTextField
          name={`tickets[${props.index}].name`}
          label="Name"
          required
          errors={errors}
          touched={touched}
        />
      </Grid>

      <Grid item container md={3}>
        <FormikTextField
          name={`tickets[${props.index}].description`}
          label="Description"
          required
        />
      </Grid>

      <Grid item container md={3}>
        <FormikTextField
          name={`tickets[${props.index}].eligibility`}
          label="Eligbility"
        />
      </Grid>

      <Grid item container md={2}>
        <FormikTextField
          name={`tickets[${props.index}].price`}
          label="Price"
          required
        />
      </Grid>

      <Grid item md={2} container justify="flex-start" alignItems="center">
        <Grid item container spacing={3}>
          <Grid item>
            <Button
              onClick={withSubmit(handleUpdate)}
              color="primary"
              variant="contained"
              disabled={!dirty || error || isValidating || submitting}
            >
              Save
            </Button>
          </Grid>

          <Grid item>
            <RemoveTicketButton onClick={withSubmit(handleDelete, true)} />
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};

type Props = {
  ticket: ProgramTicket | undefined;
  programId: string;
  index: number;
  afterSubmit: (t?: ProgramTicket) => void;
};

interface formikValues {
  tickets: ProgramTicket[];
}

import * as Y from "yup";
import { requiredText } from "../../../utils/formikUtils";
import { ProgramTicket } from "../../../modules/Programs/types/ProgramTicket.types";
import {
  findIndex,
  findLastIndex,
  isEmpty,
  isEqual,
  map,
  range,
  uniq,
  zipObject,
} from "lodash";

export const TicketSchema = Y.object().shape({
  name: Y.string().required(requiredText),
  description: Y.string().required(requiredText),
  price: Y.string().required(requiredText),
});

const hasDuplicate = (names: string[], name: string) => {
  return !isEqual(
    findIndex(names, (n) => n === name),
    findLastIndex(names, (n) => n === name)
  );
};

export const TicketsArraySchema = Y.array()
  .of(TicketSchema)
  .test({
    test: function(tickets: ProgramTicket[]) {
      const types = uniq(map(tickets, "typeId"));
      const dictionary: { [k: string]: string[] } = zipObject(
        types,
        uniq(
          map(types, (tT) =>
            tickets.filter((t) => isEqual(t.typeId, tT)).map((t) => t.name)
          )
        )
      );

      const error = range(tickets.length)
        .filter((i) =>
          hasDuplicate(dictionary[tickets[i].typeId], tickets[i].name)
        )
        .reduce((err, i) => {
          err.inner.push(
            this.createError({
              path: `tickets[${i}].name`,
              message: "Name must be unique",
            })
          );
          return err;
        }, this.createError());
      return isEmpty(error.inner) ? true : error;
    },
  });

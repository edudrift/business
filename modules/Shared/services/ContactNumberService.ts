import { find } from "lodash";

import { countries } from "../../Countries/countries";
import { Country } from "../../Countries/types/CountryType";
import { FormikContactNumberType } from "./../../../components/Forms/components/FormikContactNumber";

class ContactNumberServiceClass {
  // format of storing contact numbers with country code:
  // code::number
  // 65::91239123

  getAsStringFromContactType(contact: FormikContactNumberType) {
    return `${contact.code?.phone}::${contact.number}`;
  }

  getAsString(country: Country, number: string | number) {
    return `${country.phone}::${number}`;
  }

  getAsObject(
    numberWithEncodedCountryCode: string
  ): {
    code: {
      label: string;
      phone: string;
      code: string;
    };
    number: string;
  } {
    const arrayOfCodeAndNumber = numberWithEncodedCountryCode.split("::");

    if (arrayOfCodeAndNumber.length !== 2) {
      throw new Error(
        `Decoding number, ${numberWithEncodedCountryCode}, failed. Does not contain separator '::'.`
      );
    }

    const [code, contactNumber] = arrayOfCodeAndNumber;

    const country = find(countries, { phone: code });
    if (!country) {
      throw new Error(
        `Decoding number, ${numberWithEncodedCountryCode} failed. Country code(${code}) not found.`
      );
    }

    return {
      code: country,
      number: contactNumber,
    };
  }
}

export const ContactNumberService = new ContactNumberServiceClass();

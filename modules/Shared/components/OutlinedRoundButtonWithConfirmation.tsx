import React from "react";

import { Button, Grid, Popover, Typography } from "@material-ui/core";

import { PaddedPaper } from "./PaddedPaper";
import { OutlinedRoundButton } from "./OutlinedRoundButton";

export const OutlinedRoundButtonWithConfirmation: React.FC<Props> = ({
  buttonColor,
  buttonText,
  onConfirm,
  confirmText,
}) => {
  const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(
    null
  );

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleConfirmClick = () => {
    handleClose();
    onConfirm();
  };

  const open = Boolean(anchorEl);
  const id = open ? "create-program-cancel-button" : undefined;

  return (
    <React.Fragment>
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "center",
        }}
      >
        <PaddedPaper>
          <Grid container spacing={2} direction="column">
            <Grid item>
              <Typography align="center">Are you sure?</Typography>
            </Grid>
            <Button size="small" onClick={handleConfirmClick}>
              {confirmText}
            </Button>
          </Grid>
        </PaddedPaper>
      </Popover>

      <OutlinedRoundButton
        buttonText={buttonText}
        onClick={handleClick}
        color={buttonColor}
      />
    </React.Fragment>
  );
};

type Props = {
  onConfirm: () => any;
  confirmText: string;
  buttonColor?: "default" | "inherit" | "primary" | "secondary";
  buttonText: string;
};

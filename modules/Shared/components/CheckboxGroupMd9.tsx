import React from "react";

import { Checkbox, FormControlLabel, FormGroup, Grid } from "@material-ui/core";
import { zip } from "lodash";

export const CheckboxGroupMd9 = ({
  checked,
  values,
  labels,
  setValues,
}: Props) => {
  const dictionary: { [k: string]: boolean } = checked.reduce(
    (prev, curr) => ({ ...prev, ...{ [curr]: true } }),
    {}
  );

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const name = event.target.name;
    setValues(
      event.target.checked
        ? [...checked, name]
        : checked.filter((i) => i !== name)
    );
  };

  return (
    <FormGroup>
      <Grid container spacing={2} item xs={12} md={9}>
        {zip(values, labels).map(([v, l]) => {
          return (
            l &&
            v && (
              <Grid item key={v} md={3} xs={12}>
                <FormControlLabel
                  control={
                    <Checkbox
                      color="primary"
                      checked={dictionary[v]}
                      onChange={handleChange}
                      name={v}
                    />
                  }
                  label={l}
                />
              </Grid>
            )
          );
        })}
      </Grid>
    </FormGroup>
  );
};

type Props = {
  values: string[];
  checked: string[];
  labels: string[];
  setValues: (value: string[]) => void;
};

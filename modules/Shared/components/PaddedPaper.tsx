import { makeStyles, Paper, PaperProps } from "@material-ui/core";
import clsx from "clsx";
import React, { FunctionComponent } from "react";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
  fullWidth: {
    flex: 1,
  },
  withOutline: {
    border: "1px solid gray",
  },
  withoutPadding: {
    padding: 0,
  },
  curvedBorders: {
    borderRadius: "4px",
  },
  largerPadding: {
    padding: theme.spacing(4),
  },
}));

export const PaddedPaper: FunctionComponent<PaddedPaperProps> = ({
  withElevation,
  fullWidth,
  withOutline,
  withoutPadding,
  curvedBorders,
  largerPadding,
  ...props
}) => {
  const classes = useStyles();

  let elevation = 0;
  if (typeof withElevation === "boolean" && withElevation) {
    elevation = 1;
  } else if (typeof withElevation === "number") {
    elevation = withElevation;
  }

  return (
    <Paper
      square
      className={clsx(classes.root, {
        [classes.fullWidth]: fullWidth,
        [classes.withOutline]: withOutline,
        [classes.withoutPadding]: withoutPadding,
        [classes.curvedBorders]: curvedBorders,
        [classes.largerPadding]: largerPadding,
      })}
      {...props}
      elevation={elevation}
    >
      {props.children}
    </Paper>
  );
};

export interface PaddedPaperProps extends PaperProps {
  withElevation?: boolean | number;
  fullWidth?: boolean;
  withOutline?: boolean;
  withoutPadding?: boolean;
  curvedBorders?: boolean;
  largerPadding?: boolean;
}

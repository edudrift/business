import React from "react";

import { Grid, Box, Paper, Divider } from "@material-ui/core";

import TitleWithSubtitleFragment from "./TitleWithSubheaderAndSubtitle";

export const CardWithTitleDivider: React.FC<Props> = ({
  title,
  subtitle = "",
  color = "#2196f3",
  children,
}) => {
  return (
    <Box width="100%">
      <Paper>
        <Box py={2} px={4}>
          <Grid item container direction="column" spacing={2}>
            <Grid item>
              <TitleWithSubtitleFragment
                color={color}
                title={title}
                subtitle={subtitle}
              />
              <Divider
                orientation="horizontal"
                style={{ backgroundColor: color }}
              />
            </Grid>
            {children}
          </Grid>
        </Box>
      </Paper>
    </Box>
  );
};

export default CardWithTitleDivider;

type Props = {
  title: string;
  subtitle?: string;
  color?: string;
  children: React.ReactNode;
};

import React from "react";

import { Button, makeStyles } from "@material-ui/core";

const useStyles = makeStyles(() => ({
  button: {
    fontSize: "0.8125rem",
    borderRadius: "18px",
  },
}));

export const OutlinedRoundButton: React.FC<Props> = ({
  buttonText,
  onClick,
  color = "default",
}) => {
  const classes = useStyles();

  return (
    <Button
      variant="outlined"
      className={classes.button}
      onClick={onClick}
      color={color}
    >
      {buttonText}
    </Button>
  );
};

type Props = {
  onClick: any;
  buttonText: string;
  color?: "default" | "inherit" | "primary" | "secondary";
};

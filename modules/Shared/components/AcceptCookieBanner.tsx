import React from "react";

import cookie from "js-cookie";
import NextLink from "next/link";
import {
  Button,
  Container,
  Grid,
  makeStyles,
  Typography,
  Link,
} from "@material-ui/core";

import { PaddedPaper } from "./PaddedPaper";

const useStyles = makeStyles(() => ({
  footer: {
    bottom: 0,
    // height: "50px",
    position: "fixed",
    width: "100vw",
    zIndex: 9999,
    backgroundColor: "#fff",
  },
}));

export const AcceptCookieBanner = () => {
  const [show, setShow] = React.useState(false);

  React.useEffect(() => {
    if (!cookie.get("agreeToCookies")) {
      setShow(true);
    }
  }, []);

  const classes = useStyles();

  const agreeToCookies = () => {
    cookie.set("agreeToCookies", new Date());
    setShow(false);
  };

  if (!show) return null;

  return (
    <footer className={classes.footer}>
      <Container maxWidth="lg">
        <PaddedPaper fullWidth>
          <Grid container justify="space-around" alignItems="center">
            <Grid item>
              <Typography>
                By continuing to use this website you agree to the use of
                cookies according to our{" "}
                <NextLink href="privacy" passHref>
                  <Link color="inherit">
                    <strong>Privacy Policy</strong>
                  </Link>
                </NextLink>{" "}
                and terms.
              </Typography>
            </Grid>

            <Grid item>
              <Button
                variant="contained"
                color="primary"
                disableElevation
                size="large"
                onClick={agreeToCookies}
              >
                Okay
              </Button>
            </Grid>
          </Grid>
        </PaddedPaper>
      </Container>
    </footer>
  );
};

import { Grid, makeStyles, StepButton, StepLabel } from "@material-ui/core";
import React from "react";
import clsx from "clsx";
import TitleWithSubtitleFragment from "./TitleWithSubheaderAndSubtitle";

const DISABLED_COLOR = "#E0E0E0";
const DARK_GREY_COLOR = "rgb(158,158,158)";
const BLUE_COLOR = "#1890FF";

const useStyles = makeStyles(() => ({
  circle: {
    display: "flex",
    fontSize: "12px",
    fontFamily: "Roboto",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: "50%",
    minWidth: "24px",
    width: "24px",
    minHeight: "24px",
    height: "24px",
    margin: "auto auto",
    color: "white",
  },
  checkmark: {
    marginBottom: "2px",
    transform: "rotate(45deg)",
    height: "50%",
    width: "30%",
    borderBottom: "2px solid white",
    borderRight: "2px solid white",
  },
  fullHeight: {
    height: "100%",
  },
  fullWidth: {
    width: "100%",
  },
  text: {
    whiteSpace: "nowrap",
    color: "black",
  },
  stepperContainer: {
    padding: "24px 0",
  },
  stepConnector: {
    width: "100%",
    height: "2px",
    top: "12px",
    position: "relative",
  },
  transparent: {
    backgroundColor: "transparent",
  },
  grey: {
    backgroundColor: DISABLED_COLOR,
  },
  darkGrey: {
    backgroundColor: DARK_GREY_COLOR,
  },
  blue: {
    background: BLUE_COLOR,
  },
}));

const StepperWithTitle: React.FC<Props> = (props) => {
  const classes = useStyles();
  const currentStep = props.steps[props.activeStep];

  const CircleWithCheckMark = () => (
    <div className={clsx(classes.circle, classes.blue)}>
      <div className={classes.checkmark} />
    </div>
  );

  const CircleWithNumber = ({ number }: { number: number }) => (
    <div className={clsx(classes.circle, classes.darkGrey)}>
      <p>{number}</p>
    </div>
  );

  const renderSteps = (
    steps: Step[],
    onStepClick?: (stepIndex: number) => void,
    argumentForIsStepComplete?: any
  ) => {
    console.log([onStepClick, argumentForIsStepComplete]);
    return steps.map(({ stepTitle, isStepComplete }, index) => {
      console.log([stepTitle, isStepComplete, index]);
      const isLast = index === steps.length - 1;
      return (
        <Grid
          item
          container
          direction="column"
          style={{ width: `calc(100% / ${steps.length}` }}
        >
          <StepButton>
            <StepLabel className={clsx(classes.fullHeight, classes.fullWidth)}>
              <Grid
                item
                container
                direction="row"
                justify="flex-start"
                onClick={onStepClick ? () => onStepClick(index) : undefined}
              >
                <Grid item container style={{ width: "calc(10% + 80px)" }}>
                  <Grid
                    item
                    container
                    direction="column"
                    justify="center"
                    className={classes.text}
                  >
                    <Grid item container>
                      {isStepComplete(argumentForIsStepComplete) ? (
                        <CircleWithCheckMark />
                      ) : (
                        <CircleWithNumber number={index + 1} />
                      )}
                    </Grid>
                    {stepTitle}
                  </Grid>
                </Grid>
                <Grid item container style={{ width: "calc(90% - 80px)" }}>
                  <Grid
                    className={clsx(classes.stepConnector, {
                      [classes.grey]: !isLast,
                      [classes.transparent]: isLast,
                    })}
                  />
                </Grid>
              </Grid>
            </StepLabel>
          </StepButton>
        </Grid>
      );
    });
  };

  return (
    <React.Fragment>
      <Grid
        container
        direction="row"
        justify="space-between"
        className={classes.stepperContainer}
      >
        {renderSteps(
          props.steps,
          props.onStepClick,
          props.argumentForIsStepComplete
        )}
      </Grid>

      <TitleWithSubtitleFragment
        title={currentStep.title}
        subtitle={currentStep.subtitle}
        subheader={currentStep.subheader}
      />
    </React.Fragment>
  );
};

type Props = {
  activeStep: number;
  steps: Step[];
  onStepClick?: (stepIndex: number) => void;
  argumentForIsStepComplete?: any;
};

export type Step = {
  title: string;
  stepTitle: string;
  subtitle: string;
  subheader?: string;
  isStepComplete: (props?: any) => boolean;
};

export default StepperWithTitle;

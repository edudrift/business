import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
} from "@material-ui/core";

const DialogPopupAlert: React.FC<Props> = ({
  open,
  text,
  onClose: handleClose,
  onCancel: handleCancel,
  onConfirm: handleConfirm,
}) => {
  return (
    <Dialog open={open} onClose={handleClose}>
      <DialogContent>
        <DialogContentText>{text}</DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button color="default" onClick={handleCancel || handleClose}>
          Cancel
        </Button>
        {handleConfirm && (
          <Button color="primary" onClick={handleConfirm}>
            Confirm
          </Button>
        )}
      </DialogActions>
    </Dialog>
  );
};

export default DialogPopupAlert;

type Props = {
  open: boolean;
  text: string;
  onClose: (e: any) => void;
  onCancel?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
  onConfirm?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
};

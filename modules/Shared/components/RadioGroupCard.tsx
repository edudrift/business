import React from "react";

import {
  Card,
  CardContent,
  FormControlLabel,
  Grid,
  makeStyles,
  Radio,
  RadioGroup,
} from "@material-ui/core";

import { TypographyWithPxFontAndColor } from "./TypographyWithPxFontAndColor";

const useStyles = makeStyles((theme) => {
  return {
    label: {
      fontWeight: theme.typography.fontWeightBold,
      fontSize: theme.typography.pxToRem(14),
    },
  };
});

export const RadioGroupCard = <T,>(props: Props<T>) => {
  const classes = useStyles();

  const isRow = props.direction === "row";

  return (
    <RadioGroup value={props.value} onChange={props.handleChange}>
      <Grid item>
        <Grid item container direction={props.direction} spacing={2}>
          {props.values.map((value, index) => {
            return (
              <Grid
                item
                key={index}
                xs={12}
                md="auto"
                style={
                  isRow ? { width: `${100 / props.values.length}%` } : undefined
                }
              >
                <Card variant="outlined">
                  <CardContent>
                    <Grid container direction="column">
                      <Grid item>
                        <FormControlLabel
                          value={value.value}
                          control={
                            <Radio
                              color="primary"
                              disabled={value.disabled || props.isSubmitting}
                            />
                          }
                          label={value.label}
                          disabled={value.disabled || props.isSubmitting}
                          classes={{
                            label: classes.label,
                          }}
                        />
                      </Grid>

                      <Grid item>
                        <TypographyWithPxFontAndColor
                          sizeInPx={14}
                          color="#666666"
                        >
                          {value.description}
                        </TypographyWithPxFontAndColor>
                      </Grid>
                    </Grid>
                  </CardContent>
                </Card>
              </Grid>
            );
          })}
        </Grid>
      </Grid>
    </RadioGroup>
  );
};

type Values<T> = {
  value: T;
  label: string;
  description: string;
  disabled?: boolean;
};

type Props<T> = {
  handleChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
  direction: "row" | "column";
  value: T;
  values: Values<T>[];
  isSubmitting: boolean;
};

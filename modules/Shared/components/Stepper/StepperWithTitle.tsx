import { Step, StepButton, StepButtonProps, Stepper } from "@material-ui/core";
import React from "react";

const renderSteps = (
  steps: Step[],
  onStepClick?: (stepIndex: number) => void,
  argumentForIsStepComplete?: any
) => {
  return steps.map(({ stepTitle, isStepComplete }, index) => {
    const stepButtonProps: StepButtonProps = {};

    const preparedIsStepComplete = argumentForIsStepComplete
      ? () => isStepComplete(argumentForIsStepComplete)
      : isStepComplete;
    stepButtonProps.completed = preparedIsStepComplete();

    if (onStepClick) {
      stepButtonProps.onClick = () => onStepClick(index);
    }

    return (
      <Step key={stepTitle}>
        <StepButton {...stepButtonProps}>{stepTitle}</StepButton>
      </Step>
    );
  });
};

const StepperWithTitle: React.FC<Props> = (props) => {
  return (
    <React.Fragment>
      <Stepper nonLinear alternativeLabel activeStep={props.activeStep}>
        {renderSteps(
          props.steps,
          props.onStepClick,
          props.argumentForIsStepComplete
        )}
      </Stepper>
    </React.Fragment>
  );
};

type Props = {
  activeStep: number;
  steps: Step[];
  onStepClick?: (stepIndex: number) => void;
  argumentForIsStepComplete?: any;
};

export type Step = {
  title: string;
  stepTitle: string;
  subtitle: string;
  subheader?: string;
  isStepComplete: (props?: any) => boolean;
};

export default StepperWithTitle;

import {
  Button,
  Grid,
  Step,
  StepButton,
  StepButtonProps,
  StepContent,
  StepLabel,
  Stepper,
} from "@material-ui/core";
import React from "react";

const renderSteps = (
  steps: Step[],
  activeStep: number,
  onStepClick?: (stepIndex: number) => void
) => {
  return steps.map((step: Step, index) => {
    const stepButtonProps: StepButtonProps = {};

    stepButtonProps.completed = step.isStepComplete;

    if (onStepClick) {
      stepButtonProps.onClick = () => onStepClick(index);
    }

    return (
      <Step key={step.title}>
        <StepButton {...stepButtonProps}>
          <StepLabel style={{ flexDirection: "row" }}>
            <h3 style={{ margin: "0 0 0 1em", textAlign: "left" }}>
              {step.title}
            </h3>
            <h4 style={{ margin: "0 0 0 1.2em", color: "rgba(0, 0, 0, 0.54)" }}>
              {step.subtitle}
            </h4>
          </StepLabel>
        </StepButton>

        <StepContent>
          {activeStep == index && (
            <Grid item xs={12} md={7}>
              <img
                src={step.image}
                style={{
                  width: "100%",
                  justifySelf: "start",
                  paddingLeft: 0,
                }}
              />
              <Button
                variant="contained"
                color="primary"
                style={{ marginTop: "-100px", marginLeft: "10px" }}
                onClick={step.onClick}
              >
                Continue
              </Button>
            </Grid>
          )}
        </StepContent>
      </Step>
    );
  });
};

const VerticalStepper: React.FC<Props> = (props) => {
  return (
    <React.Fragment>
      <Stepper
        nonLinear
        alternativeLabel
        activeStep={props.activeStep}
        orientation="vertical"
      >
        {renderSteps(props.steps, props.activeStep, props.onStepClick)}
      </Stepper>
    </React.Fragment>
  );
};

type Props = {
  activeStep: number;
  steps: Step[];
  onStepClick?: (stepIndex: number) => void;
  argumentForIsStepComplete?: any;
};

export type Step = {
  image: string;
  title: string;
  subtitle: string;
  subheader?: string;
  isStepComplete: boolean | undefined;
  onClick: (props?: any) => void;
};

export default VerticalStepper;

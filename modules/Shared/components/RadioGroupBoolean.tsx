import React from "react";

import {
  Card,
  CardContent,
  FormControlLabel,
  Grid,
  makeStyles,
  Radio,
  RadioGroup,
} from "@material-ui/core";

const useStyles = makeStyles((theme) => {
  return {
    label: {
      fontWeight: theme.typography.fontWeightBold,
      fontSize: theme.typography.pxToRem(14),
    },
  };
});

export const RadioGroupBoolean: React.FC<Props> = ({
  value,
  handleChange,
  noLabel,
  yesLabel,
  disabled,
  children,
  isSubmitting,
}) => {
  const classes = useStyles();

  return (
    <RadioGroup value={value} onChange={handleChange}>
      <Grid item container spacing={1} direction="column">
        <Grid item>
          <Card variant="outlined">
            <CardContent>
              <Grid container direction="column">
                <Grid item>
                  <FormControlLabel
                    value={false}
                    control={<Radio color="primary" disabled={isSubmitting} />}
                    label={noLabel}
                    classes={{
                      label: classes.label,
                    }}
                    disabled={disabled}
                  />
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </Grid>

        <Grid item>
          <Card variant="outlined">
            <CardContent>
              <Grid container direction="column" spacing={2}>
                <Grid item>
                  <FormControlLabel
                    value={true}
                    control={<Radio color="primary" disabled={isSubmitting} />}
                    label={yesLabel}
                    classes={{
                      label: classes.label,
                    }}
                    disabled={isSubmitting}
                  />
                </Grid>
                {children}
              </Grid>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </RadioGroup>
  );
};

type Props = {
  noLabel: string;
  yesLabel: string;
  disabled?: boolean;
  value: any;
  handleChange: (v: any) => void;
  isSubmitting: boolean;
};

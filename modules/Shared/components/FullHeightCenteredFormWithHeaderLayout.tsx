import React from "react";

import { Container, Grid, Typography } from "@material-ui/core";
import { withStyles } from "@material-ui/styles";

import { useRouter } from "next/router";
import theme from "../../../theme";
import { PaddedPaper } from "./PaddedPaper";

export const FullHeightCenteredFormWithHeaderLayout: React.FC<Props> = ({
  header,
  renderBelowPaper,
  children,
}) => {
  const router = useRouter();

  const GlobalCss = withStyles({
    // @global is handled by jss-plugin-global.
    "@global": {
      // You should target [class*="MuiButton-root"] instead if you nest themes.
      body: {
        background: `${theme.palette.primary.main} !important`,
      },
    },
  })(() => null);

  return (
    <div style={{ backgroundImage: `url(${"/bg-blue.svg"})`, backgroundSize: "cover", minHeight: "100vh", overflow: "hidden" }}>
      <Container maxWidth="lg">
        <GlobalCss />
        <Grid
          container
          alignItems="center"
          justify="center"
          direction="column"
          spacing={6}
          style={{ minHeight: "100vh" }}
        >
          <Grid item>
            <img
              src="/logo-white-4x.png"
              style={{ height: "5.25rem", cursor: "pointer" }}
              onClick={() => router.push("/")}
            />
          </Grid>

          <Grid
            item
            container
            alignItems="center"
            justify="center"
            direction="column"
            spacing={1}
          >
            <Grid item md={5}>
              <PaddedPaper curvedBorders fullWidth largerPadding>
                <Grid container spacing={4} direction="column">
                  <Grid item xs={12}>
                    <Typography component="p" variant="h6">
                      {header}
                    </Typography>
                  </Grid>

                  <Grid item xs={12}>
                    {children}
                  </Grid>
                </Grid>
              </PaddedPaper>
            </Grid>

            {renderBelowPaper && <Grid item>{renderBelowPaper()}</Grid>}
          </Grid>
        </Grid>
      </Container>
    </div>
  );
};

type Props = {
  header: string;
  renderBelowPaper?: () => any;
};

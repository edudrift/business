import React from "react";

import { Grid, Typography } from "@material-ui/core";

import theme from "../../../../theme";

export const FormH6Header: React.FC<Props> = (props) => {
  return (
    <Grid item container spacing={2} direction="column">
      <Grid item>
        <Typography
          variant="h6"
          component="p"
          style={{
            fontSize: theme.typography.pxToRem(18),
            fontWeight: theme.typography.fontWeightMedium,
          }}
        >
          {props.title}
        </Typography>
      </Grid>
      {props.children}
    </Grid>
  );
};

type Props = {
  title: string;
};

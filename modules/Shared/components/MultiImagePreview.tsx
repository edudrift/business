import React from "react";

import { Grid } from "@material-ui/core";

const getPreviewUrl = (url: string) => {
  return url;
};

const getTitle= (titles: string[], key: string) => {
  return titles[Number(key)];
};

export const MultiImagePreview: React.FC<Props> = ({
  titles,
  images,
}) => {
 
  return (
    <Grid item container spacing={2}>
      { Object.entries(images || []).map(([index, image]) => {
          return (
            <Grid 
              item 
              xs 
              container 
              key={index} 
              spacing={1}
              justify="center"
            >
              <Grid
                item
                md={8}
                container
                spacing={1}
                direction="column"
                style={{ textAlign: "center" }}
                
              >
                <Grid item>
                  <img
                    src={getPreviewUrl(image)}
                  />
                </Grid>
                <Grid item>
                  {getTitle(titles || [], index)}
                </Grid>
              </Grid>
            </Grid>
          )
      })}
    </Grid>  
  );
};

type Props = {
  images?: string[];
  titles?: string[];
};

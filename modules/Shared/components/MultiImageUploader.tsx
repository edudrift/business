import React from "react";

import { Button, Grid } from "@material-ui/core";
import { Widget } from "@uploadcare/react-widget";
import { pullAt, range } from "lodash";

const getPreviewUrl = (url: string) => {
  const previewUrl = url;

  return previewUrl;
};

export const MultiImageUploader: React.FC<Props> = ({
  initialImages,
  count,
  onChange,
}) => {
  const [uploadedImages, setUploadedImages] = React.useState(
    initialImages || new Array(count)
  );

  React.useEffect(() => {
    onChange(uploadedImages);
  }, [uploadedImages]);

  return (
    <Grid item xs={12} container spacing={2}>
      {range(count).map((_, index) => {
        if (!uploadedImages[index])
          return (
            <Grid item xs key={index}>
              <Widget
                publicKey="6bdea531c367f102136d"
                tabs="file"
                previewStep
                imagesOnly
                crop="3:2"
                onChange={(fileChange) => {
                  if (fileChange.cdnUrl) {
                    const newUploadedImages = [...uploadedImages];
                    newUploadedImages[index] = fileChange.cdnUrl;

                    setUploadedImages(newUploadedImages);
                  }
                }}
              />
            </Grid>
          );

        return (
          <Grid item container xs key={index} spacing={1} direction="column">
            <Grid item>
              <Widget
                publicKey="6bdea531c367f102136d"
                tabs="file"
                previewStep
                imagesOnly
                crop="3:2"
                onChange={(fileChange) => {
                  if (fileChange.cdnUrl) {
                    const newUploadedImages = [...uploadedImages];
                    newUploadedImages[index] = fileChange.cdnUrl;

                    setUploadedImages(newUploadedImages);
                  }
                }}
                value={uploadedImages[index]}
              />
            </Grid>

            {uploadedImages[index] && (
              <Grid item container spacing={1} direction="column">
                <Grid item>
                  <img
                    src={getPreviewUrl(uploadedImages[index])}
                    width="100%"
                    height="100%"
                  />
                </Grid>

                <Grid item>
                  <Button
                    size="small"
                    onClick={() => {
                      const newUploadedImages = [...uploadedImages];

                      pullAt(newUploadedImages, index);
                      setUploadedImages(newUploadedImages);
                    }}
                  >
                    Remove
                  </Button>
                </Grid>
              </Grid>
            )}
          </Grid>
        );
      })}
    </Grid>
  );
};

type Props = {
  count: number;
  initialImages?: string[];
  onChange: (newImages: string[]) => void;
};

import React from "react";

import { makeStyles, Typography } from "@material-ui/core";

const useStyles = makeStyles((theme) => {
  return {
    title: {
      fontWeight: theme.typography.fontWeightBold,
      fontSize: theme.typography.pxToRem(18),
    },
    subtitle: {
      fontSize: theme.typography.pxToRem(12),
      color: "#666666",
    },
  };
});

export const FormSubheaderWithSubtitle: React.FC<Props> = ({
  title,
  subtitle,
}) => {
  const classes = useStyles();

  return (
    <React.Fragment>
      <Typography variant="h6" component="p" className={classes.title}>
        {title}
      </Typography>
      {subtitle && (
        <Typography variant="body2" component="p" className={classes.subtitle}>
          {subtitle}
        </Typography>
      )}
    </React.Fragment>
  );
};

type Props = {
  title: string;
  subtitle?: string;
};

import React from "react";

import { Grid } from "@material-ui/core";

import { FormSubheaderWithSubtitle } from "../FormSubheaderWithSubtitle";

export const FormSubheaderWithChildren: React.FC<Props> = (props) => {
  return (
    <Grid item container spacing={2} direction="column">
      <Grid item>
        <FormSubheaderWithSubtitle
          title={props.title}
          subtitle={props.subtitle}
        />
      </Grid>
      {props.children}
    </Grid>
  );
};

type Props = {
  title: string;
  subtitle?: string;
};

import React from "react";

import { Typography } from "@material-ui/core";
import theme from "../../../theme";

export const TypographyWithPxFontAndColor: React.FC<Props> = ({
  sizeInPx,
  color,
  bold,
  multiline,
  rightAlign,
  centerAlign,
  children,
  ...props
}) => {
  const sizeInRem = theme.typography.pxToRem(sizeInPx);
  const style: any = { fontSize: sizeInRem, color, ...props.style };

  if (multiline) {
    style.whiteSpace = "pre-wrap";
  }

  if (rightAlign) {
    style.textAlign = "right";
  }

  if (centerAlign) {
    style.textAlign = "center";
  }

  if (bold) {
    style.fontWeight = "bold";
  }

  return (
    <Typography style={style} {...props}>
      {children}
    </Typography>
  );
};

interface Props {
  sizeInPx: number;
  color: string;
  multiline?: boolean;
  rightAlign?: boolean;
  centerAlign?: boolean;
  bold?: boolean;
  style?: object;
}

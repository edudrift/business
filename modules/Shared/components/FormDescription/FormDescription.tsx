import React from "react";

import { Grid } from "@material-ui/core";
import { TypographyWithPxFontAndColor } from "../TypographyWithPxFontAndColor";

export const FormDescription: React.FC<Props> = (props) => {
  return (
    <Grid item container spacing={2} direction="column">
      <Grid item>
        <TypographyWithPxFontAndColor sizeInPx={16} color="#666">
          {props.description}
        </TypographyWithPxFontAndColor>
      </Grid>
      {props.children}
    </Grid>
  );
};

type Props = {
  description: string;
};

import { makeStyles, Typography } from "@material-ui/core";
import clsx from "clsx";
import React from "react";

const useStyles = makeStyles((theme) => ({
  title: {
    fontWeight: theme.typography.fontWeightBold,
    fontSize: theme.typography.pxToRem(24),
  },
  subtitle: {
    color: "#666666",
  },
  subheader: {},
  marginBottomless: {
    marginBottom: 0,
  },
}));

const TitleWithSubtitleFragment: React.FC<Props> = (props) => {
  const classes = useStyles();

  return (
    <React.Fragment>
      <Typography
        className={classes.title}
        style={{ color: props.color }}
        gutterBottom
      >
        {props.title}
      </Typography>
      {props.subheader && (
        <Typography variant="body1" gutterBottom>
          {props.subheader}
        </Typography>
      )}
      <Typography
        variant="body1"
        className={clsx(classes.subtitle, {
          [classes.marginBottomless]: props.withoutBottomMargin,
        })}
      >
        {props.subtitle}
      </Typography>
    </React.Fragment>
  );
};

type Props = {
  title: string;
  subtitle: string;
  color?: string;
  subheader?: string;
  withoutBottomMargin?: boolean;
};

export default TitleWithSubtitleFragment;

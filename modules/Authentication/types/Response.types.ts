import { EProgramProviderStatus } from "../../../types/ProgramProvider.types";
import { EUserRoles } from "../../../types/user.types";

export interface IResponse {
  message?: string;
}

interface ISessionResponse extends IResponse {
  authToken: string;
  id: string;
  email: string;
  role: EUserRoles;
  programProviderId?: string;
  businessId?: string;
}

export interface ILoginResponse extends ISessionResponse {
  isEmailVerified: boolean;
  programProviderStatus: EProgramProviderStatus;
}

export interface ResetPasswordResponse extends ILoginResponse {}

export interface SignUpResponse extends ISessionResponse {}

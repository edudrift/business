export type SignUpPayload = {
  email: string;
  password: string;
  programProviderName: string;
};

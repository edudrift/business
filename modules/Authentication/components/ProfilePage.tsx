import React from "react";

import { Button, Grid } from "@material-ui/core";
import { NextPage } from "next";
import ButtonLink from "../../../components/ButtonLink";
import GenericLayout from "../../../components/GenericLayout/GenericLayout";
import withError from "../../../components/HOCs/withError";
import {
  getUserProfileOrThrow,
  logout,
} from "../../../services/Auth.functions";
import User from "../../../types/user.types";

const ProfilePage: NextPage<Props> = (props) => {
  return (
    <GenericLayout user={props.user}>
      <Grid container direction="column-reverse" spacing={2}>
        <Grid item>
          <Button
            variant="contained"
            color="primary"
            onClick={() => {
              logout();
            }}
          >
            Log out
          </Button>
        </Grid>
        <Grid item>
          <ButtonLink color="primary" variant="contained" href="/purchases">
            View Purchases
          </ButtonLink>
        </Grid>
      </Grid>
    </GenericLayout>
  );
};

type Props = {
  user: User;
};

ProfilePage.getInitialProps = async (ctx) => {
  const user = await getUserProfileOrThrow(ctx);

  return {
    user,
  };
};

export const ProfilePageWithError = withError(ProfilePage);

import { Button, Grid } from "@material-ui/core";
import React from "react";

import { AuthService } from "../../../../modules/Authentication/AuthService";
import { FullHeightCenteredFormWithHeaderLayout } from "../../../../modules/Shared/components/FullHeightCenteredFormWithHeaderLayout";
import { TypographyWithPxFontAndColor } from "../../../../modules/Shared/components/TypographyWithPxFontAndColor";
import theme from "../../../../theme";

interface IProps {
  email: string;
}

const VerifyEmailAlertPage = (props: IProps) => {
  const [codeResent, setCodeResent] = React.useState(false);
  const [resendingCode, setResendingCode] = React.useState(false);

  const resendCode = async () => {
    setResendingCode(true);

    try {
      await AuthService.resendVerificationLink();
      setResendingCode(false);
      setCodeResent(true);
    } catch (e) {
      setResendingCode(false);
    }
  };

  return (
    <FullHeightCenteredFormWithHeaderLayout header="Verification code">
      <Grid container spacing={10}>
        <Grid item xs container spacing={2} direction="column">
          <Grid item>
            <TypographyWithPxFontAndColor
              sizeInPx={14}
              color="rgba(0, 0, 0, 0.5)"
            >
              A verification link has been sent to
            </TypographyWithPxFontAndColor>
          </Grid>
          <Grid item>
            <TypographyWithPxFontAndColor sizeInPx={14} color="#000">
              <strong>{props.email}</strong>
            </TypographyWithPxFontAndColor>
          </Grid>
          <Grid item>
            <TypographyWithPxFontAndColor
              sizeInPx={14}
              color="rgba(0, 0, 0, 0.5)"
            >
              Please click on the link provided in the email to proceed with the
              verification.
            </TypographyWithPxFontAndColor>
          </Grid>
        </Grid>
        <Grid item container>
          <Grid item xs container spacing={1} direction="column">
            <Grid item>
              <TypographyWithPxFontAndColor
                centerAlign
                sizeInPx={10}
                color={theme.palette.primary.main}
              >
                Did not receive the email?
              </TypographyWithPxFontAndColor>
            </Grid>

            <Grid item>
              <Button
                onClick={resendCode}
                variant="contained"
                fullWidth
                color="primary"
                size="large"
                disableElevation
                disabled={resendingCode || codeResent}
              >
                {codeResent ? "Code has been resent" : "Send again"}
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </FullHeightCenteredFormWithHeaderLayout>
  );
};

export default VerifyEmailAlertPage;

import {
  NextPage,
  NextPageContext
} from "next";
import React from "react";
import { getUserProfileOrRedirect } from "../../../services/Auth.functions";
import redirect from "../../../utils/redirect";
import VerifyYourEmailMessage from "./VerifyYourEmailMessage";

type Props = PromiseResult<ReturnType<typeof getInitialProps>>;

const VerifyEmailAlertPage : NextPage<Props> = (props) => {

  return (<VerifyYourEmailMessage email={props.user.email} />);
};

const getInitialProps = async (ctx: NextPageContext) => {
  const user = await getUserProfileOrRedirect(ctx, false);

  if (user?.isEmailVerified) {
    redirect("/biz", 301, ctx);
  }

  return {
    user,
  };
};

VerifyEmailAlertPage.getInitialProps = getInitialProps;

export default VerifyEmailAlertPage;

export class InvalidCredentialsError extends Error {}

export class WrongPasswordError extends Error {}

export class EmailNotFoundError extends Error {}

import TokenService from "../../../../services/Token.service";
import EHttpMethods from "../../../../utils/EHttpMethods";
import isofetch from "../../../../utils/isofetch";
import {
  InvalidCredentialsError,
  WrongPasswordError,
  EmailNotFoundError,
} from "./errors";
import { ILoginResponse } from "../../types/Response.types";

async function saveAccessTokenToCookies(accessToken: string) {
  await TokenService.saveToken(accessToken);
}

export async function loginOrThrow(email: string, password: string) {
  const response = await isofetch<ILoginResponse>(
    "/api/user/login",
    EHttpMethods.POST,
    undefined,
    { email, password }
  );

  if (response.status === 200) {
    if (response.data) {
      const { authToken, ...userInformation } = response.data;
      await saveAccessTokenToCookies(authToken);
      return userInformation;
    }
  } else {
    switch (response.data!.message) {
      case "WRONG_PASSWORD":
        throw new WrongPasswordError();
      case "EMAIL_NOT_FOUND":
        throw new EmailNotFoundError();
    }
    throw new InvalidCredentialsError(response.data?.message);
  }
}

import {
  Button,
  Container,
  LinearProgress,
  Link,
  makeStyles,
  Typography,
} from "@material-ui/core";
import { Alert } from "@material-ui/lab";
import { Field, Form, Formik } from "formik";
import { TextField } from "material-ui-formik-components";
import NextLink from "next/link";
import React from "react";
import * as Yup from "yup";

import EHttpMethods from "../../../utils/EHttpMethods";
import isofetch from "../../../utils/isofetch";

import CenteredContentWithLogoLayout from "../../../components/CenteredContentWithLogoLayout";

const useStyles = makeStyles((theme) => ({
  title: {
    fontWeight: "bold",
    fontSize: theme.typography.pxToRem(22),
    marginBottom: theme.spacing(3),
  },
  link: {
    cursor: "pointer",
  },
  field: {
    marginBottom: theme.spacing(1),
  },
  successAlert: {
    marginTop: theme.spacing(2),
  },
}));

const ForgotPasswordPage = () => {
  const classes = useStyles();
  const [isSubmitting, setSubmitting] = React.useState(false);
  const [linkSent, setLinkSent] = React.useState(false);

  async function handleSubmit(values: { email: string }) {
    setSubmitting(true);
    try {
      await isofetch(
        "/api/auth/password/send-reset-link",
        EHttpMethods.POST,
        undefined,
        { email: values.email }
      );
      console.log("Submitting,", values);

      setSubmitting(false);
      setLinkSent(true);
    } catch (e) {
      console.log(e);
    }
  }

  return (
    <CenteredContentWithLogoLayout>
      <Container maxWidth="xs">
        <Typography
          component="p"
          variant="h6"
          className={classes.title}
          gutterBottom
        >
          So you forgot password?
        </Typography>
        <Formik
          validationSchema={Yup.object().shape({
            email: Yup.string()
              .email("This is not an email")
              .required("We need an email"),
          })}
          onSubmit={handleSubmit}
          initialValues={{ email: "" }}
        >
          {({ submitForm }) => (
            <Form>
              <Field
                autoFocus
                component={TextField}
                size="small"
                name="email"
                type="email"
                label="Email"
                fullWidth
                variant="outlined"
                className={classes.field}
                disabled
              />
              <NextLink href="/login">
                <Link variant="body2" component="a" className={classes.link}>
                  Log into an account
                </Link>
              </NextLink>

              <Button
                variant="contained"
                color="primary"
                fullWidth
                onClick={submitForm}
                type="submit"
                style={{ marginTop: "16px" }}
                disabled
              >
                Work in progress
              </Button>
            </Form>
          )}
        </Formik>
        {isSubmitting && <LinearProgress />}

        {linkSent && (
          <Alert className={classes.successAlert}>
            If that email is found in our system, you will get an email.
          </Alert>
        )}
      </Container>
    </CenteredContentWithLogoLayout>
  );
};

export default ForgotPasswordPage;

import React from "react";

import { Grid, Typography } from "@material-ui/core";
import { NextPage, NextPageContext } from "next";

import ButtonLink from "../../../components/ButtonLink";
import CenteredContentWithLogoLayout from "../../../components/CenteredContentWithLogoLayout";
import EHttpMethods from "../../../utils/EHttpMethods";
import isofetch from "../../../utils/isofetch";
import redirect from "../../../utils/redirect";

const ConnectPage: NextPage<Props> = (props) => {
  console.log(props);
  const successMessage = "Account has been connected!";

  const message = props.success ? successMessage : props.message;
  const redirectMessage = props.success ? "Complete Business Profile" : "Back to Profile";

  return (
    <CenteredContentWithLogoLayout>
      <Grid
        container
        spacing={2}
        direction="column"
        justify="center"
        alignItems="center"
      >
        <Grid item>
          <Typography>{message}</Typography>
        </Grid>
   
        <Grid item>
          <ButtonLink href="/biz/profile" variant>
            {redirectMessage}
          </ButtonLink>
        </Grid>
        
      </Grid>
    </CenteredContentWithLogoLayout>
  );
};

const getInitialProps = async (ctx: NextPageContext) => {
  const { state, code } = ctx.query;

  if (state && code) {
    const response = await isofetch<{ message?: string }>(
      "/api/program-providers/connect",
      EHttpMethods.POST,
      undefined,
      {
        state,
        code,
      }
    );

    if (response.status === 200) {
      redirect("/biz/profile", 302, ctx);
    } else {
      return {
        success: false,
        message: response.data?.message,
      };
    }
  }

  return {
    success: false,
    message:
      "Stripe's redirect does not contain the required parameters. Admin has been contacted.",
  };
};

ConnectPage.getInitialProps = getInitialProps;

type Props = {
  success: boolean;
  message?: string;
};

export default ConnectPage;

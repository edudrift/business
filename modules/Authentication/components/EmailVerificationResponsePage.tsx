import React from "react";

import { Grid } from "@material-ui/core";
import { NextPage, NextPageContext } from "next";

import EHttpMethods from "../../../utils/EHttpMethods";
import isofetch from "../../../utils/isofetch";
import TokenService from "../../../services/Token.service";

import { TypographyWithPxFontAndColor } from "../../Shared/components/TypographyWithPxFontAndColor";
import ButtonLink from "../../../components/ButtonLink";
import { FullHeightCenteredFormWithHeaderLayout } from "~/modules/Shared/components/FullHeightCenteredFormWithHeaderLayout";

type Props = PromiseResult<ReturnType<typeof getInitialProps>>;

const getInitialProps = async (ctx: NextPageContext) => {
  const response = await isofetch<{ success: boolean }>(
    `/api/user/verify?token=${ctx.query.token}`,
    EHttpMethods.GET,
    TokenService.getToken(ctx)
  );

  let success = false;

  if (response.status === 202) success = true;

  return { success };
};

const EmailVerificationResponsePage: NextPage<Props> = (props) => {
  const header = props.success ? "Success!" : "Oops!";
  const message = props.success
    ? "Your verification was successful. You can now take advantage \
  of member-exclusive benefits as an EduDrift partner."
    : "Something went wrong. Please request another link.";
  const buttonText = props.success ? "Get Started" : "Home";

  return (
    <FullHeightCenteredFormWithHeaderLayout header={header}>
      <Grid container spacing={10} direction="column">
        <Grid item>
          <TypographyWithPxFontAndColor
            sizeInPx={16}
            color="rgba(0, 0, 0, 0.5)"
          >
            {message}
          </TypographyWithPxFontAndColor>
        </Grid>
        <Grid item>
          <ButtonLink
            href="/biz/profile"
            variant="contained"
            color="primary"
            type="submit"
            disableElevation
            fullWidth
            size="large"
          >
            {buttonText}
          </ButtonLink>
        </Grid>
      </Grid>
    </FullHeightCenteredFormWithHeaderLayout>
  );
};

EmailVerificationResponsePage.getInitialProps = getInitialProps;

export default EmailVerificationResponsePage;

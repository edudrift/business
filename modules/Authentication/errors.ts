export class ProgramProviderNameTakenError extends Error {}

export class EmailTakenError extends Error {}

export class AccountWithEmailNotFound extends Error {}

export class ResetPasswordTokenExpired extends Error {}

export class ResetPasswordTokenInvalid extends Error {}

export class ResetPasswordTokenUsed extends Error {}

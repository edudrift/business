import cookie from "js-cookie";
import jwtDecode from "jwt-decode";
import { NextPageContext } from "next";
import nextCookie from "next-cookies";

import { SignUpPayload } from "./types/SignUpPayload.types";
import {
  IResponse,
  SignUpResponse,
  ResetPasswordResponse,
} from "./types/Response.types";

import EHttpMethods from "../../utils/EHttpMethods";
import isofetch from "../../utils/isofetch";

import {
  AccountWithEmailNotFound,
  EmailTakenError,
  ProgramProviderNameTakenError,
  ResetPasswordTokenExpired,
  ResetPasswordTokenInvalid,
  ResetPasswordTokenUsed,
} from "./errors";

class AuthServiceClass {
  async resetPassword(password: string, token: string) {
    const response = await isofetch<ResetPasswordResponse>(
      `/api/user/reset-password/${token}`,
      EHttpMethods.POST,
      undefined,
      {
        password,
      }
    );

    if (response.status === 200 && response.data) {
      this.saveToken(response.data.authToken);
      return response.data;
    } else if (response.status !== 200) {
      switch (response.data!.message!) {
        case "TOKEN_EXPIRED":
          throw new ResetPasswordTokenExpired();
        case "INVALID_TOKEN":
          throw new ResetPasswordTokenInvalid();
        case "TOKEN_USED":
          throw new ResetPasswordTokenUsed();
      }
    }
  }

  async sendResetPasswordLink(email: string) {
    const response = await isofetch<IResponse>(
      "/api/user/send-password-reset-link",
      EHttpMethods.POST,
      undefined,
      { email }
    );

    if (response.status !== 200) {
      switch (response.data!.message!) {
        case "EMAIL_NOT_FOUND":
          throw new AccountWithEmailNotFound();
      }
    }
  }

  async signUp(payload: SignUpPayload) {
    const response = await isofetch<SignUpResponse>(
      "/api/user/register",
      EHttpMethods.POST,
      undefined,
      { ...payload }
    );

    if (response.status === 201 && response.data) {
      this.saveToken(response.data.authToken);
    } else {
      if (response.data!.message === "EMAIL_TAKEN") {
        throw new EmailTakenError();
      } else if (response.data!.message === "PROGRAM_PROVIDER_NAME_TAKEN") {
        throw new ProgramProviderNameTakenError();
      }
    }
  }

  async resendVerificationLink() {
    const token = this.getToken();
    if (token) {
      const response = await isofetch<undefined>(
        "/api/user/send-email-verification-link",
        EHttpMethods.POST,
        token
      );

      if (response.status !== 200) {
        throw new Error();
      }
    }
  }

  private getToken(ctx?: NextPageContext) {
    if (ctx) {
      const { token } = nextCookie(ctx);

      return token;
    } else {
      return cookie.get("token");
    }
  }

  async saveToken(token: string) {
    cookie.set("token", token);

    return Promise.resolve();
  }

  public decodeToken(token: string): { email: string } {
    if (!token) throw new Error("Token not found");

    try {
      const decoded = jwtDecode(token);
      return decoded as { email: string };
    } catch (e) {
      throw new Error("Error decoding JWT");
    }
  }
}

export const AuthService = new AuthServiceClass();

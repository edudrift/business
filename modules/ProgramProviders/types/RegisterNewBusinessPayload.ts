import { EBusinessEntityType } from "../enums/EBusinessEntityType";

export type RegisterNewBusinessPayload = {
  name: string;

  type: EBusinessEntityType;
  uen: string;

  website: string | null;
  facebookPage: string | null;

  officeNumber: string;

  address: {
    street: string;
    street2?: string;

    postalCode: string;
    countryCode: string;
  };
};

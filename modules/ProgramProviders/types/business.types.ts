import { AddressSchemaFromDatabaseType } from "../../../components/Forms/AddressForm/schema";

import Contact from "../../../types/contacts.types";
import ProgramProvider from "../../../types/ProgramProvider.types";

type Business = {
  id: string;

  name: string;
  website: string;
  facebookPage: string;
  officeNumber: string;

  uen: string;

  verified: boolean;

  programProvider: ProgramProvider;
  programProviderId: string;

  address?: AddressSchemaFromDatabaseType;

  contacts: Contact[];
};

export default Business;

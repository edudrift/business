import { NextPageContext } from "next";

import { NotLoggedIn } from "../../../services/Auth.errors";
import TokenService from "../../../services/Token.service";
import { ProgramTypeEligibility } from "../../../types/ProgramEligiblity.types";
import { EProgramTypeEligibilityStatus } from "../../../types/ProgramEligiblity.types";
import ProgramProvider, {
  EProgramProviderStatus,
} from "../../../types/ProgramProvider.types";
import EHttpMethods from "../../../utils/EHttpMethods";
import isofetch from "../../../utils/isofetch";
import Business from "../types/business.types";

export class ProgramProviderAdminServiceClass {
  async getOneAsAdmin(ctx: NextPageContext, programProviderId: string) {
    const token = TokenService.getToken(ctx);

    if (token) {
      const response = await isofetch<ProgramProvider>(
        `/api/admin/program-providers/${programProviderId}`,
        EHttpMethods.GET,
        token
      );

      if (response.status === 200 && response.data) {
        return response.data;
      } else {
        console.error(response);
        // todo: throw a better error
        throw new Error();
      }
    } else {
      throw new NotLoggedIn();
    }
  }

  async getAllPendingProgramProviders(
    ctx: NextPageContext
  ): Promise<[ProgramProvider[], number]> {
    const token = TokenService.getToken(ctx);

    if (token) {
      const response = await isofetch<[ProgramProvider[], number]>(
        `/api/admin/program-providers?status=${EProgramProviderStatus.PENDING}`,
        EHttpMethods.GET,
        token
      );

      if (response.status === 200 && response.data) {
        return response.data;
      } else {
        console.error(response);
        // todo: throw a better error
        throw new Error();
      }
    } else {
      throw new NotLoggedIn();
    }
  }

  async markAsVerified(programProviderId: string) {
    const token = await TokenService.getToken();
    if (!token) throw new NotLoggedIn();

    const response = await isofetch<ProgramProvider>(
      `/api/admin/program-providers/${programProviderId}/status`,
      EHttpMethods.PUT,
      token,
      {
        status: EProgramProviderStatus.VERIFIED,
      }
    );

    if (response.status === 200 && response.data) {
      return response.data;
    } else {
      // todo: throw better error here
      console.log(response);
      throw new Error();
    }
  }

  getProgramTypeEligibilities = async (
    ctx: NextPageContext,
    businessId: Business["id"]
  ) => {
    const token = await TokenService.getToken(ctx);
    if (token) {
      const response = await isofetch<ProgramTypeEligibility[]>(
        `/api/business/${businessId}/program-types`,
        EHttpMethods.GET,
        token
      );

      if (response.status === 200 && response.data) {
        return response.data;
      } else {
        // todo: throw better error
        console.error(response);
        throw new Error();
      }
    } else {
      throw new Error();
    }
  };

  async approveProgramType(
    programProviderId: ProgramProvider["id"],
    programTypeId: ProgramTypeEligibility["id"]
  ) {
    const token = await TokenService.getToken();
    if (!token) throw new NotLoggedIn();

    const response = await isofetch<ProgramTypeEligibility>(
      `/api/admin/program-providers/${programProviderId}/program-types/${programTypeId}`,
      EHttpMethods.PUT,
      token,
      {
        status: EProgramTypeEligibilityStatus.APPROVED,
      }
    );

    if (response.status === 200 && response.data) {
      return response.data;
    } else {
      // todo: throw better error
      throw new Error();
    }
  }

  async markProgramTypeAsPending(
    programProviderId: ProgramProvider["id"],
    programTypeId: ProgramTypeEligibility["id"]
  ) {
    const token = await TokenService.getToken();
    if (!token) throw new NotLoggedIn();

    const response = await isofetch<ProgramTypeEligibility>(
      `/api/admin/program-providers/${programProviderId}/program-types/${programTypeId}`,
      EHttpMethods.PUT,
      token,
      {
        status: EProgramTypeEligibilityStatus.PENDING,
      }
    );

    if (response.status === 200 && response.data) {
      return response.data;
    } else {
      // todo: throw better error
      throw new Error();
    }
  }
}

export const ProgramProviderAdminService = new ProgramProviderAdminServiceClass();

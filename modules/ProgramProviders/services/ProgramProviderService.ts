import { AddressSchemaFromDatabaseType } from "../../../components/Forms/AddressForm/schema";
import { NotLoggedIn } from "../../../services/Auth.errors";
import TokenService from "../../../services/Token.service";
import { ProgramTypeEligibility } from "../../../types/ProgramEligiblity.types";
import EHttpMethods from "../../../utils/EHttpMethods";
import isofetch, { isofetchMultipart } from "../../../utils/isofetch";
import { ContactSchemaType } from "../components/onboarding/forms/contacts/BusinessAddContactsForm";
import { RegisterNewBusinessPayload } from "../types/RegisterNewBusinessPayload";

import { AddressSchemaType } from "../../../components/Forms/AddressForm/schema";
import Contact, { EContactTypes } from "../../../types/contacts.types";
import ProgramProvider, {
  EProgramProviderStatus,
} from "../../../types/ProgramProvider.types";
import { EProgramType } from "../../Programs/types/Programs.types";

class ProgramProviderServiceClass {
  async getOne(programProviderId: string) {
    const response = await isofetch<ProgramProvider>(
      `/api/program-providers/${programProviderId}`,
      EHttpMethods.GET
    );

    if (response.status === 200 && response.data) {
      return response.data;
    } else {
      console.error(response);
      // todo: throw a better error
      throw new Error();
    }
  }

  async markForReview() {
    const token = TokenService.getToken();
    if (token) {
      const response = await isofetch<ProgramProvider>(
        `/api/program-providers/status`,
        EHttpMethods.PUT,
        token,
        {
          status: EProgramProviderStatus.PENDING,
        }
      );

      if (response.status === 200 && response.data) {
        return response.data;
      } else {
        // todo: better error
        console.error(response);
        throw new Error("Error registering new business.");
      }
    } else {
      throw new NotLoggedIn();
    }
  }

  async getStripeConnectLink() {
    const token = TokenService.getToken();
    if (token) {
      const response = await isofetch<{ link: string }>(
        `/api/program-providers/connect/link`,
        EHttpMethods.GET,
        token
      );

      if (response.status === 200 && response.data) {
        return response.data.link;
      } else {
        // todo: better error
        console.error(response);
        throw new Error("Error registering new business.");
      }
    } else {
      throw new NotLoggedIn();
    }
  }

  async registerNewBusiness(
    payload: RegisterNewBusinessPayload
  ): Promise<ProgramProvider> {
    const token = TokenService.getToken();
    if (token) {
      const response = await isofetch<ProgramProvider>(
        `/api/program-providers/business`,
        EHttpMethods.POST,
        token,
        payload
      );

      if (response.status === 201 && response.data) {
        return response.data!;
      } else {
        // todo: better error
        console.error(response);
        throw new Error("Error registering new business.");
      }
    } else {
      throw new NotLoggedIn();
    }
  }

  async updateProgramProvider(
    name: string,
    logoImages: string[],
    introductions: string[],
    imageTitles: string[],
    images: string[]
  ): Promise<ProgramProvider> {
    const token = await TokenService.getToken();

    if (!token) throw new NotLoggedIn();

    const response = await isofetch<ProgramProvider>(
      `/api/program-providers/business`,
      EHttpMethods.PUT,
      token,
      {
        "programProvider": {
          name,
          logoImages,
          introductions,
          imageTitles,
          images
        }
      }
    );

    if (response.status === 200 && response.data) {
      return response.data;
    } else {
      // todo: better error
      console.error(response);
      throw new Error();
    }
  }

  async addProgramTypes(
    programTypes: EProgramType[]
  ): Promise<ProgramTypeEligibility[]> {
    const response = await isofetch<ProgramTypeEligibility[]>(
      `/api/program-providers/program-types`,
      EHttpMethods.POST,
      TokenService.getToken(),
      {
        programTypes,
      }
    );

    if (response.status === 201 && response.data) {
      return response.data;
    } else {
      // todo better error
      console.error(response);
      throw new Error("Error adding program types");
    }
  }

  async shortCircuit() {
    const token = TokenService.getToken();
    if (token) {
      const response = await isofetch(
        "/api/business/shortcut",
        EHttpMethods.POST,
        token
      );

      if (response.status !== 201) {
        console.log(response);
      }
    }
  }

  async saveVerification(businessId: string, data: FormData) {
    const token = TokenService.getToken();
    if (token) {
      const response = await isofetchMultipart(
        `/api/business/${businessId}/verification`,
        data,
        token
      );

      console.log(response);
    }
  }

  async updateContact(
    businessId: string,
    contactId: string,
    contact: ContactSchemaType,
    type: EContactTypes
  ) {
    const response = await isofetch<Contact>(
      `/api/business/${businessId}/contact/${contactId}`,
      EHttpMethods.PUT,
      TokenService.getToken(),
      {
        ...contact,
        type,
      }
    );

    if (response.status === 200 && response.data) {
      return response.data;
    } else {
      console.log(response);
    }
  }

  async createContact(businessId: string, contact: Contact) {
    const response = await isofetch<Contact>(
      `/api/business/${businessId}/contact`,
      EHttpMethods.POST,
      TokenService.getToken(),
      {
        ...contact,
      }
    );

    if (response.status === 201 && response.data) {
      return response.data;
    } else {
      // todo: throw better error
      throw new Error("Error creating contact.");
    }
  }

  async createAddress(businessId: string, address: AddressSchemaType) {
    const response = await isofetch<AddressSchemaFromDatabaseType>(
      `/api/business/${businessId}/address`,
      EHttpMethods.POST,
      TokenService.getToken(),
      this.buildAddressPayload(address)
    );

    if (response.status === 201 && response.data) {
      return response.data;
    } else {
      console.error(response);
      throw new CreatingAddressError();
    }
  }

  async saveAddress(
    businessId: string,
    addressId: string,
    address: AddressSchemaType
  ) {
    const response = await isofetch<AddressSchemaFromDatabaseType>(
      `/api/business/${businessId}/address/${addressId}`,
      EHttpMethods.PUT,
      TokenService.getToken(),
      this.buildAddressPayload(address)
    );

    if (response.status === 200 && response.data) {
      return response.data;
    } else {
      console.error(response);
      throw new SavingAddressError();
    }
  }

  buildAddressPayload(
    address: AddressSchemaType
  ): Omit<AddressSchemaFromDatabaseType, "id"> {
    const { country, ...addressWithoutCountry } = address;
    const payload = {
      ...addressWithoutCountry,
      countryCode: country.code,
    };

    return payload;
  }
}

export const ProgramProviderService = new ProgramProviderServiceClass();

export class SavingAddressError extends Error {}
export class CreatingAddressError extends Error {}

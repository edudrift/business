export enum EBusinessEntityType {
  PARTNERSHIP = "PARTNERSHIP",
  SOLE_PROPRIETORSHIP = "SOLE_PROPRIETORSHIP",
  PRIVATE_LIMITED = "PRIVATE_LIMITED",
}

export enum EBusinessEntityTypeLabel {
  PARTNERSHIP = "Partnership",
  SOLE_PROPRIETORSHIP = "Sole proprietorship",
  PRIVATE_LIMITED = "Private limited",
}

export const ArrayOfBusinessEntityTypes = [
  EBusinessEntityType.PARTNERSHIP,
  EBusinessEntityType.PRIVATE_LIMITED,
  EBusinessEntityType.SOLE_PROPRIETORSHIP,
];

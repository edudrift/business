export enum EProgramProviderStatus {
  NEW = "NEW",
  PENDING = "PENDING",
  VERIFIED = "VERIFIED",
}

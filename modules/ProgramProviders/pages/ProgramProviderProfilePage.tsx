import React from "react";

import { Grid } from "@material-ui/core";
import { NextPage, NextPageContext } from "next";

import DashboardBreadcrumbs from "../../../components/DashboardBreadcrumbs/DashboardBreadcrumbs";
import withError from "../../../components/HOCs/withError";
import DashboardLayout, {
  EDashboardLayoutSidebarNames,
} from "../../../components/Layouts/DashboardLayout";
import { getUserProfileOrRedirect } from "../../../services/Auth.functions";
import { PaddedPaper } from "../../Shared/components/PaddedPaper";
import { ProgramProviderOnboarding } from "../components/onboarding/ProgramProviderOnboarding";

const BusinessProfilePage: NextPage<Props> = (props) => {
  const conditionallyRenderChildren = () => {
    const businessIsVerified = props.user.programProvider?.business?.verified;

    if (businessIsVerified) {
      return <h1>Profile page</h1>;
    } else {
      return <ProgramProviderOnboarding user={props.user} />;
    }
  };

  return (
    <DashboardLayout
      user={props.user}
      activeSidebarName={EDashboardLayoutSidebarNames.PROFILE}
      headerTitle="Registration"
      renderBreadcrumb={() => (
        <DashboardBreadcrumbs
          crumbs={[{ title: "Dashboard", link: "/biz" }, { title: "Profile" }]}
        />
      )}
    >
      <Grid item xs>
        <PaddedPaper largerPadding>{conditionallyRenderChildren()}</PaddedPaper>
      </Grid>
    </DashboardLayout>
  );
};

const getInitialProps = async (ctx: NextPageContext) => {
  const user = await getUserProfileOrRedirect(ctx);

  return {
    user,
  };
};

type Props = PromiseResult<ReturnType<typeof getInitialProps>>;

BusinessProfilePage.getInitialProps = getInitialProps;

export default withError(BusinessProfilePage);

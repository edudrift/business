// @ts-ignore
import React from "react";

import { Grid } from "@material-ui/core";
import { Tab, Tabs } from "@material-ui/core";
import { NextPageContext } from "next";
import { getUserOrReturnUndefined } from "~/services/Auth.functions";
import withError from "../../../components/HOCs/withError";

import HeaderLayoutWithFooter from "../../../modules/Home/HeaderLayoutWithFooter";

import { ProgramProviderService } from "../../../modules/ProgramProviders/services/ProgramProviderService";
import { MultiImagePreview } from "../../../modules/Shared/components/MultiImagePreview";
import ProgramProvider from "../../../types/ProgramProvider.types";
import User from "../../../types/user.types";
import { Avatar, Button } from "@material-ui/core";

enum IntroTabsLabel {
  ENTITY_INTRODUCTION = "Entity Introduction",
  BRAND_ADVANTAGE = "Brand Advantage",
  PROFESSIONAL_TEAM = "Professional Team",
}

const ViewBusinessCenterPage = (props: Props) => {
  const { programProvider } = props;
  const introductions = programProvider.introductions || ["", "", ""];
  
  const { name } = programProvider;
  
  const [value, setValue] = React.useState(0);
  const [intro, setIntro] =  React.useState(introductions[0]);
  const [logoImages] = React.useState(programProvider.logoImages || []);
  const [images] = React.useState(programProvider.images || []);
  const [imageTitles] = React.useState(programProvider.imageTitles || []);
  
  
  const getPreviewUrl = (url: string) => {
    return url;
  };

  const handleChange = (_: React.ChangeEvent<{}>, newValue: number) => {
    setIntro(introductions[newValue]);
    setValue(newValue);
  };

  return (
    <HeaderLayoutWithFooter user={props.user}>
      <Grid
        item
        container
        spacing={4}
        style={{ overflowX: "hidden", width: "100%", margin: "unset", background: "#ffff" }}
      >
    
        <Grid item xs md={12} container spacing={1} style={{ margin: "3em", background: "#F5F5F5" }}>
          <Grid
            item
            xs
            container
            md={12} 
            spacing={1}
          >
            <Grid item container md={11}>
              <Grid item xs md={1}>
                <Avatar src={getPreviewUrl(logoImages[0])} />
              </Grid>
              <Grid item xs md={10} style={{ fontSize: 22 }}>
                {name}
              </Grid>
            </Grid>
            <Grid item md={1}>
              <Button
                variant="contained"
                color="secondary"
                style={{ textTransform: "none" }}
              >
                Follow
              </Button>
            </Grid>
          </Grid>

          <Grid
            item
            xs
            container
            spacing={1}
            md={12}
            style={{ display: "flex", flexDirection: "column", justifyContent: "end"}}
          >
          <Tabs
              value={value}
              indicatorColor="primary"
              textColor="primary"
              onChange={handleChange}
            >
              <Tab style={{ textTransform: "none" }} label={IntroTabsLabel.ENTITY_INTRODUCTION} />
              <Tab style={{ textTransform: "none" }} label={IntroTabsLabel.BRAND_ADVANTAGE} />
              <Tab style={{ textTransform: "none" }} label={IntroTabsLabel.PROFESSIONAL_TEAM} />
            </Tabs>
          </Grid>

          <Grid
            item
            xs
            container
            spacing={3}
            style={{ minHeight: "8em", margin: "2em 0" }}
          >
            {intro}
          </Grid>
        </Grid>
    
        <Grid item xs container md={12} spacing={1} style={{ margin: "1em 3em 2em" }}>
          <MultiImagePreview images={images} titles={imageTitles}/>
        </Grid>
      </Grid>
    </HeaderLayoutWithFooter>  
  );
}

type Props = {
  user: User;
  programProvider: ProgramProvider;
};

ViewBusinessCenterPage.getInitialProps = async (ctx: NextPageContext) => {
  try {
    const user = await getUserOrReturnUndefined(ctx);
    const programProviderId = ctx.query.id as string;
    const programProvider = await ProgramProviderService.getOne(
      programProviderId
    );

    return { user, programProvider };
  } catch (e) {
    throw e;
  }
};

export default withError(ViewBusinessCenterPage);
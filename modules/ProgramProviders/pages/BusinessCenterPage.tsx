import React from "react";

import { Grid } from "@material-ui/core";
import { NextPage, NextPageContext } from "next";

import { useRouter } from "next/router";
import DashboardBreadcrumbs from "../../../components/DashboardBreadcrumbs/DashboardBreadcrumbs";
import withError from "../../../components/HOCs/withError";
import DashboardLayout, {
  EDashboardLayoutSidebarNames,
} from "../../../components/Layouts/DashboardLayout";
import { getUserProfileOrRedirect } from "../../../services/Auth.functions";
import { OutlinedRoundButton } from "../../Shared/components/OutlinedRoundButton";
import { PaddedPaper } from "../../Shared/components/PaddedPaper";
import { BusinessCenterForm } from "../components/forms/BusinessCenterForm";

const BusinessCenterPage: NextPage<Props> = (props) => {
  const router = useRouter();

  return (
    <DashboardLayout
      user={props.user}
      activeSidebarName={EDashboardLayoutSidebarNames.BUSINESS_CENTER}
      headerTitle="Business Center"
      renderCancelButton={() => (
        <Grid container spacing={2}>
          <Grid item>
            <OutlinedRoundButton
              buttonText="Preview"
              onClick={() => {
                router.push(`/biz/business/${props.user.programProvider.id}`);
              }}
              color="primary"
            />
          </Grid>
        </Grid>
      )}

      renderBreadcrumb={() => (
        <DashboardBreadcrumbs
          crumbs={[{ title: "Dashboard", link: "/biz" }, { title: "Business Center" }]}
        />
      )}
    >
      <Grid item xs> 
        <PaddedPaper largerPadding>
          <BusinessCenterForm programProvider={props.user.programProvider}/>
        </PaddedPaper>
      </Grid> 
    </DashboardLayout>
  );
};

const getInitialProps = async (ctx: NextPageContext) => {
  const user = await getUserProfileOrRedirect(ctx);

  return {user};
};

type Props = PromiseResult<ReturnType<typeof getInitialProps>>;

BusinessCenterPage.getInitialProps = getInitialProps;

export default withError(BusinessCenterPage);

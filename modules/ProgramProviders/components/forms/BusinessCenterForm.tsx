import { Button, Grid } from "@material-ui/core";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { Form, Formik } from "formik";
import React from "react";
import { FormikMultilineTextField } from "../../../../components/Forms/components/FormikMultilineTextField";
import { FormikTextField } from "../../../../components/Forms/components/FormikTexField";
import ProgramProvider, {
  ProgramProviderSchema,
  ProgramProviderSchemaAsType,
} from "../../../../types/ProgramProvider.types";
import { FormSubheaderWithSubtitle } from "../../../Shared/components/FormSubheaderWithSubtitle";
import { MultiImageUploader } from "../../../Shared/components/MultiImageUploader";
import { ProgramProviderService } from "../../services/ProgramProviderService";

export const BusinessCenterForm: React.FC<Props> = (props) => {
  const [logoImages, setLogoImages] = React.useState(
    props.programProvider.logoImages || []
  );
  const [images, setImages] = React.useState(
    props.programProvider.images || []
  );
  const [open, setOpen] = React.useState(false);

  const buildInitialValues = () => {
    const { programProvider } = props;
    const initValues = {
      name: props.programProvider.name,
      introductions: ["", "", ""],
      imageTitles: ["", "", ""],
    };

    if (programProvider.introductions)
      initValues.introductions = programProvider.introductions;
    if (programProvider.imageTitles)
      initValues.imageTitles = programProvider.imageTitles;

    return initValues;
  };

  const handleSubmit = async (programProvider: ProgramProviderSchemaAsType) => {
    if (logoImages.length !== 1) {
      alert("Please upload logo image");
      return;
    }

    if (images.length !== 3) {
      alert("Please upload all three program intro images ");
      return;
    }

    const updatedProgramProvider = await ProgramProviderService.updateProgramProvider(
      programProvider.name,
      logoImages,
      programProvider.introductions,
      programProvider.imageTitles,
      images
    );
    if (updatedProgramProvider) {
      setOpen(true);
    }

    return updatedProgramProvider;
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <Formik
      initialValues={buildInitialValues()}
      validationSchema={ProgramProviderSchema}
      onSubmit={handleSubmit}
      validateOnChange
    >
      {() => {
        return (
          <Form>
            <Grid item container spacing={5} direction="column">
              <Grid
                item
                container
                spacing={2}
                direction="column"
                xs={12}
                md={6}
              >
                <Grid item>
                  <FormSubheaderWithSubtitle title="DISPLAY NAME AND LOGO" />
                </Grid>

                <Grid item container spacing={2} direction="column">
                  <Grid item>
                    <FormikTextField
                      name="name"
                      label="Display Name in Business Center (In Customer View)"
                      required
                    />
                  </Grid>

                  <MultiImageUploader
                    count={1}
                    initialImages={logoImages}
                    onChange={(newImages) => setLogoImages(newImages)}
                  />
                </Grid>
              </Grid>

              <Grid
                item
                xs={12}
                md={12}
                container
                spacing={4}
                direction="column"
              >
                <Grid item>
                  <FormSubheaderWithSubtitle title="BRIEF INTRODUCTION" />
                </Grid>

                <Grid item>
                  <FormikMultilineTextField
                    rows={4}
                    name="introductions[0]"
                    label="Introduction to the Entity"
                    placeholder="A generic write-up about the program provider's previous experience, the years in the industry, the range of other programs offered and 
                    information that may contribute to the reliability of the program and the fulfiling of promises made in the program details offered."
                    required
                  />
                </Grid>

                <Grid item>
                  <FormikMultilineTextField
                    rows={4}
                    name="introductions[1]"
                    label="Brand advantage"
                    placeholder="A write-up highlighting the program provider’s expertise and edge in the industry and relevant information that would boost confidence in the program provider."
                    required
                  />
                </Grid>

                <Grid item>
                  <FormikMultilineTextField
                    rows={4}
                    name="introductions[2]"
                    label="Professional team"
                    placeholder="A write-up about key personnels of the program provider. EduDrift suggests highlighing both acamedic staff and organisers,"
                    required
                  />
                </Grid>

                <Grid
                  item
                  container
                  spacing={4}
                  direction="column"
                  xs={12}
                  md={6}
                >
                  <Grid item>
                    <FormSubheaderWithSubtitle title="IMAGES" />
                  </Grid>

                  <Grid item container spacing={2} direction="column">
                    <Grid item>
                      <FormikTextField
                        name="imageTitles[0]"
                        label="Image 1 Title"
                        required
                      />
                    </Grid>

                    <Grid item>
                      <FormikTextField
                        name="imageTitles[1]"
                        label="Image 2 Title"
                        required
                      />
                    </Grid>

                    <Grid item>
                      <FormikTextField
                        name="imageTitles[2]"
                        label="Image 3 Title"
                        required
                      />
                    </Grid>
                  </Grid>
                </Grid>

                <Grid
                  item
                  container
                  spacing={4}
                  direction="column"
                  xs={12}
                  md={8}
                >
                  <MultiImageUploader
                    count={3}
                    initialImages={images}
                    onChange={(newImages) => setImages(newImages)}
                  />
                </Grid>

                <Grid item style={{ marginBottom: "15em" }}>
                  <Button type="submit" variant="contained" color="primary">
                    Save
                  </Button>

                  <Dialog
                    open={open}
                    onClose={handleClose}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                  >
                    <DialogTitle id="alert-dialog-title">
                      {"Success"}
                    </DialogTitle>
                    <DialogContent>
                      <DialogContentText id="alert-dialog-description">
                        Your Business Center has been updated.
                      </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                      <Button onClick={handleClose} color="primary" autoFocus>
                        Close
                      </Button>
                    </DialogActions>
                  </Dialog>
                </Grid>
              </Grid>
            </Grid>
          </Form>
        );
      }}
    </Formik>
  );
};

type Props = {
  programProvider: ProgramProvider;
};

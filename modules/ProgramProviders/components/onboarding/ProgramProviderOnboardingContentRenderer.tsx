import React from "react";

import ProgramProvider from "../../../../types/ProgramProvider.types";
import User from "../../../../types/user.types";
import { BusinessAddContactsForm } from "./forms/contacts/BusinessAddContactsForm";
import { ProgramProviderFinancesForm } from "./forms/finances/ProgramProviderFinancesForm";
import { ProgramProviderProgramTypesForm } from "./forms/programTypes/ProgramProviderProgramTypesForm";
import { BusinessRegistrationForm } from "./forms/registration/BusinessRegistrationForm";
import { ProgramProviderActivationForm } from "./ProgramProviderActivation";

export const ProgramProviderOnboardingContentRenderer: React.FC<Props> = ({
  activeIndex,
  ...props
}) => {
  switch (activeIndex) {
    case 0:
      return <BusinessRegistrationForm {...props} />;
    case 1:
      return <BusinessAddContactsForm {...props} />;
    case 2:
      return <ProgramProviderProgramTypesForm {...props} />;
    case 3:
      return <ProgramProviderFinancesForm {...props} />;
    case 4:
      return <ProgramProviderActivationForm {...props} />;

    default:
      return <h1>Tab not ready</h1>;
  }
};

type Props = {
  activeIndex: number;
  user: User;
  programProvider: ProgramProvider;
  onSubmit: (programProvider: ProgramProvider) => void;
};

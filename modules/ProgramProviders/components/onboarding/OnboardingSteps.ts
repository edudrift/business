import { isNull, isUndefined } from "lodash";

import ProgramProvider, {
  EProgramProviderStatus,
} from "../../../../types/ProgramProvider.types";
import { Step } from "../../../Shared/components/Stepper/StepperWithTitle";

const programProviderHasNoBusiness = (pp: ProgramProvider) => {
  return isUndefined(pp.business) || isNull(pp.business);
};

export const ONBOARDING_STEPS: {
  [index: number]: (arg?: EProgramProviderStatus) => Step;
} = {
  0: () => ({
    stepTitle: "Profile",
    title: "Profile",
    subtitle:
      "As part of the verification process, please ensure that the information provided below are true to your best knowledge.",
    subheader:
      "Due to legal restrictions, student organisations without a legal identity will have limited access to the functions on EduDrift.",
    isStepComplete: (pp: ProgramProvider) => {
      return !programProviderHasNoBusiness(pp);
    },
  }),
  1: () => ({
    stepTitle: "Contact",
    title: "Account manager's contact information",
    subtitle:
      "In case of emergencies and client disputes, the contact details below will be used by EduDrift to reach out and provide assistance.",
    subheader:
      "These information will not be publicly accessible and will only be used for internal communications between EduDrift and your organisation.",
    isStepComplete: (pp: ProgramProvider) => {
      if (programProviderHasNoBusiness(pp)) {
        return false;
      }

      return pp.business.contacts.length === 2;
    },
  }),
  2: () => ({
    stepTitle: "Programs",
    title: "Program Types",
    subtitle:
      "Your product is your educational tour, activity or experience that you offer to customers. Here in EduDrift, you'll find a section called 'Programs' under the menu. You are able to create multiple activities or experiences and get these program ready to sell.",
    subheader:
      "Select and unlock the program type that best represent the products that you are offering.",
    isStepComplete: (pp: ProgramProvider) => {
      if (programProviderHasNoBusiness(pp)) {
        return false;
      }

      return pp.programTypes?.length > 0;
    },
  }),
  3: () => ({
    stepTitle: "Finance",
    title: "Finance and payment preferences",
    subheader:
      "It's time to choose the payment method for the collection of your participation fees.",
    subtitle:
      "We know that dealing with payments and deposits can be a major headache when you run programs, especially so with international transactions. By having payment options built right into our booking platform, EduDrift makes payments a breeze for both you and your customers. You'll receive email notification of every confirmed booking. A payment confirmation along with a booking confirmation will also be sent out to your customer with each successful payment.",
    isStepComplete: (pp: ProgramProvider) => {
      if (programProviderHasNoBusiness(pp)) {
        return false;
      }

      return !isNull(pp.stripeAccountConnectId);
    },
  }),
  4: (status) => ({
    stepTitle: "Activate",
    title:
      status === EProgramProviderStatus.NEW
        ? "Almost there! Activate your account now."
        : "We will review and get back to you in 72 hours.",
    subheader:
      "Let's connect you to a world of students yearning for a diversity of experience.",
    subtitle:
      "It's time to set up your Dedicated Partner's Page to be ready for the EduDrift marketplace. With your organisation's information and payment options ready to go it's time to build your brand and increase your reach. Up next, get your company featured in a network that is visible by students anywhere in the world. Pick your featured photos, craft a sweet description of your experience in the education scene and manage your programs all in one place. Let's go!",
    isStepComplete: (pp: ProgramProvider) => {
      if (programProviderHasNoBusiness(pp)) {
        return false;
      }

      return pp.status === EProgramProviderStatus.PENDING;
    },
  }),
};

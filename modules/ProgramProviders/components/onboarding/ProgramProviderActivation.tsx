import React from "react";

import { Button, Grid } from "@material-ui/core";

import ProgramProvider, {
  EProgramProviderStatus,
} from "../../../../types/ProgramProvider.types";
import { ProgramProviderService } from "../../services/ProgramProviderService";

export const ProgramProviderActivationForm: React.FC<Props> = (props) => {
  const [currentlyInReview, setCurrentlyInReview] = React.useState(
    props.programProvider.status === EProgramProviderStatus.PENDING
  );
  const [submitting, setSubmitting] = React.useState(false);

  const updateToPendingStatus = async () => {
    setSubmitting(true);
    const updatedProgramProvider = await ProgramProviderService.markForReview();

    if (updatedProgramProvider.status === EProgramProviderStatus.PENDING) {
      setCurrentlyInReview(true);
      setSubmitting(false);

      props.onSubmit(updatedProgramProvider);
    } else {
      alert("Error marking as pending");
      setSubmitting(false);
    }
  };

  return (
    <Grid item>
      <Button
        color="primary"
        variant="contained"
        disabled={currentlyInReview || submitting}
        onClick={updateToPendingStatus}
      >
        {currentlyInReview ? "Edit Dedicated Page" : "Activate"}
      </Button>
    </Grid>
  );
};

type Props = {
  programProvider: ProgramProvider;
  onSubmit: (programProvider: ProgramProvider) => void;
};

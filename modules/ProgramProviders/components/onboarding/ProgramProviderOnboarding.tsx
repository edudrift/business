import React from "react";

import { Grid } from "@material-ui/core";

import ProgramProvider, {
  EProgramProviderStatus,
} from "../../../../types/ProgramProvider.types";
import User from "../../../../types/user.types";
import StepperWithTitle from "../../../Shared/components/Stepper/StepperWithTitle";
import { ONBOARDING_STEPS } from "./OnboardingSteps";
import { ProgramProviderOnboardingContentRenderer } from "./ProgramProviderOnboardingContentRenderer";

export function getOnboardingSteps(status: EProgramProviderStatus): any[] {
  return Object.values(ONBOARDING_STEPS).map((f) => f(status));
}

export const ProgramProviderOnboarding: React.FC<Props> = (props) => {
  const [
    localisedProgramProvider,
    setLocalisedProgramProvider,
  ] = React.useState<ProgramProvider>(props.user.programProvider);

  React.useEffect(() => {
    if (!localisedProgramProvider.business) {
      setActiveStep(0);
    } else {
      if (localisedProgramProvider.business.contacts.length !== 2) {
        setActiveStep(1);
      } else if (!localisedProgramProvider.programTypes.length) {
        setActiveStep(2);
      } else if (!localisedProgramProvider.stripeAccountConnectId) {
        setActiveStep(3);
      } else {
        setActiveStep(4);
      }
    }
  }, [localisedProgramProvider]);

  const [activeStep, setActiveStep] = React.useState(0);

  return (
    <Grid container direction="column" spacing={6}>
      <Grid item>
        <StepperWithTitle
          steps={getOnboardingSteps(localisedProgramProvider.status)}
          activeStep={activeStep}
          argumentForIsStepComplete={localisedProgramProvider}
        />
      </Grid>

      <ProgramProviderOnboardingContentRenderer
        activeIndex={activeStep}
        user={props.user}
        onSubmit={(business: ProgramProvider) => {
          setLocalisedProgramProvider(business);
        }}
        programProvider={localisedProgramProvider}
      />
    </Grid>
  );
};

type Props = {
  user: User;
};

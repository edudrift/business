import React from "react";

import { Button, Divider, Grid } from "@material-ui/core";
import { Form, Formik } from "formik";
import * as Yup from "yup";

import {
  FormikContactNumberDefaultValues,
  FormikContactNumberSchema,
} from "../../../../../../components/Forms/components/FormikContactNumber";
import { EContactTypes } from "../../../../../../types/contacts.types";
import ProgramProvider from "../../../../../../types/ProgramProvider.types";
import User from "../../../../../../types/user.types";
import { FormSubheaderWithSubtitle } from "../../../../../Shared/components/FormSubheaderWithSubtitle";
import { ContactNumberService } from "../../../../../Shared/services/ContactNumberService";
import { ProgramProviderService } from "../../../../services/ProgramProviderService";
import { ContactInformationForm } from "./ContactInformationForm";

export const ContactSchema = Yup.object().shape({
  firstName: Yup.string().required(),
  lastName: Yup.string().required(),

  title: Yup.string().required(),
  department: Yup.string().required(),

  email: Yup.string()
    .email()
    .required(),

  mobile: FormikContactNumberSchema,
  office: FormikContactNumberSchema,
});

export type ContactSchemaType = Yup.InferType<typeof ContactSchema>;

export const ContactFormDefaultValues = {
  firstName: "First Name",
  lastName: "Last Name",

  title: "Title",
  department: "Department",

  email: "email@xample.com",

  mobile: FormikContactNumberDefaultValues,
  office: FormikContactNumberDefaultValues,
};

export const AddContactsSchema = Yup.object().shape({
  primary: ContactSchema,
  secondary: ContactSchema,
});

export const BusinessAddContactsForm: React.FC<Props> = (props) => {
  const buildInitialValues = () => {
    return {
      primary: {
        ...ContactFormDefaultValues,
        email: props.user.email,
      },
      secondary: ContactFormDefaultValues,
    };
  };

  const handleSubmit = async (
    contacts: Yup.InferType<typeof AddContactsSchema>
  ) => {
    const businessId = props.user.programProvider.business.id;

    const primaryContact = await ProgramProviderService.createContact(
      businessId,
      {
        ...contacts.primary,
        office: ContactNumberService.getAsStringFromContactType(
          contacts.primary.office
        ),
        mobile: ContactNumberService.getAsStringFromContactType(
          contacts.primary.mobile
        ),
        type: EContactTypes.PRIMARY,
      }
    );

    const secondaryContact = await ProgramProviderService.createContact(
      businessId,
      {
        ...contacts.secondary,
        office: ContactNumberService.getAsStringFromContactType(
          contacts.secondary.office
        ),
        mobile: ContactNumberService.getAsStringFromContactType(
          contacts.secondary.mobile
        ),
        type: EContactTypes.SECONDARY,
      }
    );

    props.onSubmit({
      ...props.programProvider,
      business: {
        ...props.programProvider.business,
        contacts: [primaryContact, secondaryContact],
      },
    });
  };

  return (
    <Grid item xs md={6}>
      <Formik
        onSubmit={handleSubmit}
        initialValues={buildInitialValues()}
        validationSchema={AddContactsSchema}
      >
        {({ touched, errors }) => {
          return (
            <Form>
              <Grid container spacing={4} direction="column">
                <Grid item>
                  <FormSubheaderWithSubtitle title="Contact details of account manager" />
                </Grid>

                <Grid item>
                  <ContactInformationForm
                    namespace="primary"
                    touched={touched}
                    errors={errors}
                  />
                </Grid>

                <Grid item>
                  <Divider />
                </Grid>

                <Grid item>
                  <FormSubheaderWithSubtitle title="Secondary contact details" />
                </Grid>

                <Grid item>
                  <ContactInformationForm
                    namespace="secondary"
                    touched={touched}
                    errors={errors}
                  />
                </Grid>

                <Grid item>
                  <Button variant="contained" color="primary" type="submit">
                    Next
                  </Button>
                </Grid>
              </Grid>
            </Form>
          );
        }}
      </Formik>
    </Grid>
  );
};

type Props = {
  user: User;
  programProvider: ProgramProvider;
  onSubmit: (programProvider: ProgramProvider) => void;
};

import React from "react";

import { Grid } from "@material-ui/core";
import { FormikContactNumber } from "../../../../../../components/Forms/components/FormikContactNumber";
import { FormikTextField } from "../../../../../../components/Forms/components/FormikTexField";

export const ContactInformationForm: React.FC<Props> = ({
  namespace,
  touched,
  errors,
}) => {
  return (
    <Grid item xs container spacing={2} direction="column">
      <Grid item container spacing={2}>
        <Grid item xs>
          <FormikTextField
            touched={touched}
            errors={errors}
            name={`${namespace}.firstName`}
            label="First name"
          />
        </Grid>

        <Grid item xs>
          <FormikTextField
            touched={touched}
            errors={errors}
            name={`${namespace}.lastName`}
            label="Last name"
          />
        </Grid>
      </Grid>

      <Grid item container spacing={2}>
        <Grid item xs>
          <FormikTextField
            touched={touched}
            errors={errors}
            name={`${namespace}.title`}
            label="Title/position"
          />
        </Grid>

        <Grid item xs>
          <FormikTextField
            touched={touched}
            errors={errors}
            name={`${namespace}.department`}
            label="Department"
          />
        </Grid>
      </Grid>

      <Grid item>
        <FormikTextField
          touched={touched}
          errors={errors}
          name={`${namespace}.email`}
          label="Email address"
        />
      </Grid>

      <Grid item>
        <FormikContactNumber
          namespace={`${namespace}.mobile`}
          label="Mobile number"
        />
      </Grid>

      <Grid item>
        <FormikContactNumber
          namespace={`${namespace}.office`}
          label="Office number"
        />
      </Grid>
    </Grid>
  );
};

type Props = {
  namespace: string;
  touched: any;
  errors: any;
};

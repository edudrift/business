import React from "react";

import {
  Card,
  CardContent,
  Grid,
  makeStyles,
  Typography,
} from "@material-ui/core";
import clsx from "clsx";

const useStyles = makeStyles({
  root: {
    minWidth: 275,
    cursor: "pointer",
  },

  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
});

export const AttachFinanceTypeCard: React.FC<Props> = ({
  onClick,
  title,
  subtitle,
  secondSubtitle,
  disabled,
}) => {
  const classes = useStyles();

  const handleClick = () => {
    if (!disabled) {
      onClick(title);
    }
  };

  return (
    <Card
      className={clsx({ [classes.root]: !disabled })}
      variant={disabled ? "outlined" : "elevation"}
      onClick={handleClick}
    >
      <Grid container>
        <Grid
          item
          xs={3}
          style={{ backgroundImage: "url('/default-card-image.png')" }}
        />
        <Grid item xs>
          <CardContent>
            <Typography gutterBottom variant="h6" component="p">
              {title}
            </Typography>
            <Typography variant="body2" component="p">
              {subtitle}
            </Typography>
            <Typography variant="body2" component="p">
              {secondSubtitle}
            </Typography>
          </CardContent>
        </Grid>
      </Grid>
    </Card>
  );
};

type Props = {
  title: string;
  subtitle: string;
  secondSubtitle: string;
  onClick: (title: string) => void;
  disabled?: boolean;
};

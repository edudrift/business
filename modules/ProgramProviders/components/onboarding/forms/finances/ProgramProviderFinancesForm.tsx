import React from "react";

import { Grid } from "@material-ui/core";

import ProgramProvider from "../../../../../../types/ProgramProvider.types";
import { ProgramProviderService } from "../../../../services/ProgramProviderService";
import { AttachFinanceTypeCard } from "./AttachFinanceTypeCard";

export const ProgramProviderFinancesForm: React.FC<Props> = () => {
  const beginStripeLink = async () => {
    const link = await ProgramProviderService.getStripeConnectLink();

    window.location.replace(link);
  };

  const handleClick = (title: string) => {
    switch (title) {
      case "STRIPE":
        beginStripeLink();
    }
  };

  return (
    <Grid item container spacing={4} direction="column">
      <Grid item xs>
        <AttachFinanceTypeCard
          title="STRIPE"
          subtitle="[Only applicable for entities registered in Singapore, Hong Kong, Japan, Australia and USA]"
          secondSubtitle="The payment gateway provided by Stripe supports payment via Visa, MasterCard, and Alipay."
          onClick={handleClick}
        />
      </Grid>

      <Grid item xs>
        <AttachFinanceTypeCard
          disabled
          title="ALIPAY INTERNATIONAL"
          subtitle="[Only applicable for entities registered in Mainland China]"
          secondSubtitle="Official university courses that take place in the summer and/or winter vacation period. Usually lasting between 4-12 weeks."
          onClick={handleClick}
        />
      </Grid>

      <Grid item xs>
        <AttachFinanceTypeCard
          disabled
          title="EDUDRIFT PAYMENT AND FINANCE - BANK TRANSFER"
          subtitle="[Applicable for entities registered in places that are not supported by Stripe or AliPay International]"
          secondSubtitle="Countries excluded from the above payment gateway options may select the option for bank transfer payments made via EduDrift"
          onClick={handleClick}
        />
      </Grid>
    </Grid>
  );
};

type Props = {
  programProvider: ProgramProvider;
  onSubmit: (programProvider: ProgramProvider) => void;
};

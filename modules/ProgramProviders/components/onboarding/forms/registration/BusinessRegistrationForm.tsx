import React from "react";

import { Button, Grid } from "@material-ui/core";
import { Field, Form, Formik } from "formik";
import * as Yup from "yup";

import { Select } from "material-ui-formik-components";
import {
  FormikContactNumber,
  FormikContactNumberSchema,
} from "../../../../../../components/Forms/components/FormikContactNumber";
import { FormikNullableTextField } from "../../../../../../components/Forms/components/FormikNullableTextField";
import { FormikTextField } from "../../../../../../components/Forms/components/FormikTexField";
import ProgramProvider from "../../../../../../types/ProgramProvider.types";
import User from "../../../../../../types/user.types";
import { FormikCountrySelect } from "../../../../../Countries/components/FormikCountrySelect";
import { CountriesService } from "../../../../../Countries/CountriesService";
import {
  CountryEmptyState,
  CountrySchema,
} from "../../../../../Countries/schema";
import { FormSubheaderWithSubtitle } from "../../../../../Shared/components/FormSubheaderWithSubtitle";
import { ContactNumberService } from "../../../../../Shared/services/ContactNumberService";
import {
  ArrayOfBusinessEntityTypes,
  EBusinessEntityTypeLabel,
  EBusinessEntityType,
} from "../../../../enums/EBusinessEntityType";
import { EBusinessType } from "../../../../enums/EBusinessType";
import { ProgramProviderService } from "../../../../services/ProgramProviderService";
import { RegisterNewBusinessPayload } from "../../../../types/RegisterNewBusinessPayload";
import { BusinessTypeSelector } from "./BusinessTypeSelector";

const Schema = Yup.object().shape({
  programProviderName: Yup.string().required(),
  businessName: Yup.string().required(
    "A valid registered business name is required."
  ),

  entityType: Yup.string().required("Please select from the list provided."),
  uen: Yup.string().required("A valid registration number is required"),

  website: Yup.string()
    .url()
    .nullable(),
  facebookPage: Yup.string()
    .url()
    .nullable(),

  officeContact: FormikContactNumberSchema,

  address: Yup.object().shape({
    street: Yup.string().required("A valid address street is required"),
    street2: Yup.string(),

    postalCode: Yup.string().required("A valid postal code is required"),
    country: CountrySchema,
  }),
});

export const BusinessRegistrationForm: React.FC<Props> = (props) => {
  const [businessType, setBusinessType] = React.useState<EBusinessType>(
    EBusinessType.BUSINESS
  );

  const buildInitialValues = () => {
    return {
      programProviderName: props.user.programProvider.name,
      businessName: "",

      entityType: "",
      uen: "",

      website: "",
      facebookPage: "",

      officeContact: {
        code: {
          label: "",
          phone: "",
          code: "",
        },
        number: "",
      },
      address: {
        street: "",
        street2: "",

        country: CountryEmptyState,
        postalCode: "",
      },
    };
  };

  const handleSubmit = async (values: Yup.InferType<typeof Schema>) => {
    const officeNumber = ContactNumberService.getAsString(
      values.officeContact.code!,
      values.officeContact.number!
    );

    const payload: RegisterNewBusinessPayload = {
      name: values.businessName,

      type: values.entityType as EBusinessEntityType,
      uen: values.uen,

      website: values.website,
      facebookPage: values.facebookPage,

      officeNumber,
      address: {
        street: values.address.street,
        street2: values.address.street2,

        postalCode: values.address.postalCode,
        countryCode: CountriesService.getCodeFromCountry(
          values.address.country
        ),
      },
    };

    try {
      const updatedProgramProvider = await ProgramProviderService.registerNewBusiness(
        payload
      );
      props.onSubmit(updatedProgramProvider);
    } catch (e) {
      alert("Error saving information");
      // todo handle errors better
      console.error(e);
    }
  };

  return (
    <Grid container item spacing={4}>
      <BusinessTypeSelector
        selectedBusinessType={businessType}
        setBusinessType={setBusinessType}
      />

      {businessType === EBusinessType.BUSINESS && (
        <Grid item xs>
          <Formik
            initialValues={buildInitialValues()}
            onSubmit={handleSubmit}
            validationSchema={Schema}
          >
            {({ touched, errors, setFieldValue }) => {
              return (
                <Form>
                  <Grid container spacing={2}>
                    <Grid item container spacing={2}>
                      <Grid item xs container direction="column" spacing={1}>
                        <Grid item>
                          <FormSubheaderWithSubtitle title="General Information" />
                          <br />
                        </Grid>

                        <Grid item>
                          <FormikTextField
                            touched={touched}
                            errors={errors}
                            name="programProviderName"
                            label="Business Alias"
                            disabled
                            required
                          />
                        </Grid>

                        <Grid item>
                          <FormikTextField
                            touched={touched}
                            errors={errors}
                            name="businessName"
                            label="Registered business name"
                            required
                          />
                        </Grid>

                        <Grid item container spacing={2}>
                          <Grid item xs>
                            <Field
                              style={{ margin: 0 }}
                              required
                              name="entityType"
                              label="Business type"
                              options={ArrayOfBusinessEntityTypes.map(
                                (type) => ({
                                  value: type,
                                  label: EBusinessEntityTypeLabel[type],
                                })
                              )}
                              component={Select}
                              variant="outlined"
                              fullWidth
                            />
                          </Grid>

                          <Grid item xs>
                            <FormikTextField
                              touched={touched}
                              errors={errors}
                              name="uen"
                              label="Company Registration Number"
                              required
                            />
                          </Grid>
                        </Grid>

                        <Grid item>
                          <FormikNullableTextField
                            touched={touched}
                            errors={errors}
                            setFieldValue={setFieldValue}
                            name="website"
                            label="Official website"
                            nullableLabel="Our organisation does not have a website"
                            required
                          />
                        </Grid>

                        <Grid item>
                          <FormikNullableTextField
                            touched={touched}
                            errors={errors}
                            setFieldValue={setFieldValue}
                            name="facebookPage"
                            label="Official Facebook page"
                            nullableLabel="Our organisation does not have a Facebook page"
                            required
                          />
                        </Grid>

                        <Grid item>
                          <FormikContactNumber
                            label="Office number"
                            namespace="officeContact"
                          />
                        </Grid>
                      </Grid>

                      <Grid item xs container direction="column" spacing={1}>
                        <Grid item>
                          <FormSubheaderWithSubtitle title="Address" />
                          <br />
                        </Grid>

                        <Grid item>
                          <FormikTextField
                            touched={touched}
                            errors={errors}
                            name="address.street"
                            label="Address line 1"
                            required
                          />
                        </Grid>

                        <Grid item>
                          <FormikTextField
                            touched={touched}
                            errors={errors}
                            name="address.street2"
                            label="Address line 2"
                          />
                        </Grid>

                        <Grid item container spacing={2}>
                          <Grid item xs>
                            <FormikCountrySelect
                              name="address.country"
                              label="Place/country"
                              required
                            />
                          </Grid>

                          <Grid item xs>
                            <FormikTextField
                              touched={touched}
                              errors={errors}
                              name="address.postalCode"
                              label="Postal code"
                              required
                            />
                          </Grid>
                        </Grid>
                      </Grid>
                    </Grid>

                    <Grid item>
                      <Button type="submit" variant="contained" color="primary">
                        Next step
                      </Button>
                    </Grid>
                  </Grid>
                </Form>
              );
            }}
          </Formik>
        </Grid>
      )}
    </Grid>
  );
};

type Props = {
  user: User;
  programProvider: ProgramProvider;
  onSubmit: (programProvider: ProgramProvider) => void;
};

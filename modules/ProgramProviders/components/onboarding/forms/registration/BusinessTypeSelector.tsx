import React from "react";

import {
  Card,
  CardContent,
  Grid,
  Typography,
  makeStyles,
  CardMedia,
} from "@material-ui/core";

import { EBusinessType } from "../../../../enums/EBusinessType";

const useStyles = makeStyles(() => ({
  card: {
    cursor: "pointer",
  },
  details: {
    display: "flex",
    flexDirection: "column",
  },
  cover: {
    width: 151,
  },
}));

export const BusinessTypeSelector: React.FC<Props> = ({
  selectedBusinessType,
  setBusinessType,
}) => {
  const classes = useStyles();

  return (
    <Grid item container spacing={4}>
      <Grid item xs={6}>
        <Card
          className={classes.card}
          raised={selectedBusinessType === EBusinessType.BUSINESS}
          onClick={() => setBusinessType(EBusinessType.BUSINESS)}
        >
          <Grid container>
            <Grid item xs={3}>
              <CardMedia
                className={classes.cover}
                image="/default-card-image.png"
                title="Business entity card cover"
              />
            </Grid>
            <Grid item xs>
              <CardContent>
                <Typography gutterBottom>Business Entity</Typography>
                <Typography variant="body2" component="p">
                  Education providers ranging from universities, schools,
                  student-travel companies, activities vendors, enrichment
                  centres and more.
                </Typography>
              </CardContent>
            </Grid>
          </Grid>
        </Card>
      </Grid>

      <Grid item xs={6}>
        <Card
          className={classes.card}
          raised={selectedBusinessType === EBusinessType.STUDENT_ORGANISATION}
          onClick={() => setBusinessType(EBusinessType.STUDENT_ORGANISATION)}
        >
          <Grid container>
            <Grid item xs={3}>
              <CardMedia
                className={classes.cover}
                image="/default-card-image.png"
                title="Business entity card cover"
              />
            </Grid>
            <Grid item xs>
              <CardContent>
                <Typography gutterBottom>Student Organisation</Typography>
                <Typography variant="body2" component="p">
                  Clubs, societies, unions and student groups with recognition
                  and/or endorsement from the school or university.
                </Typography>
              </CardContent>
            </Grid>
          </Grid>
        </Card>
      </Grid>
    </Grid>
  );
};

type Props = {
  selectedBusinessType: EBusinessType | undefined;
  setBusinessType: (type: EBusinessType) => void;
};

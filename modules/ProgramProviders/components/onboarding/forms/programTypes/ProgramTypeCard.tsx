import React from "react";

import {
  Card,
  CardContent,
  Grid,
  makeStyles,
  Typography,
} from "@material-ui/core";

import { EProgramType } from "../../../../../Programs/types/Programs.types";

const useStyles = makeStyles({
  root: {
    minWidth: 275,
    cursor: "pointer",
  },

  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
});

export const ProgramTypeCard: React.FC<Props> = ({
  programType,
  raised,
  onClick,
}) => {
  const classes = useStyles();
  return (
    <Card
      className={classes.root}
      variant="elevation"
      raised={raised}
      onClick={() => onClick(programType.key)}
    >
      <Grid container>
        <Grid
          item
          xs={3}
          style={{ backgroundImage: "url('/default-card-image.png')" }}
        />
        <Grid item xs>
          <CardContent>
            <Typography gutterBottom variant="h6" component="p">
              {programType.title}
            </Typography>
            <Typography variant="body2" component="p">
              Education providers ranging from universities, schools,
              student-travel companies, activities vendors, enrichment centres
              and more.
            </Typography>
          </CardContent>
        </Grid>
      </Grid>
    </Card>
  );
};

type Props = {
  programType: {
    title: string;
    description: string;
    key: EProgramType;
  };
  onClick: (cardKey: EProgramType) => void;
  raised: boolean;
};

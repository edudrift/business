import React from "react";

import { Button, Grid } from "@material-ui/core";
import { filter } from "lodash";

import ProgramProvider from "../../../../../../types/ProgramProvider.types";
import {
  EProgramType,
  EProgramTypeLabels,
} from "../../../../../Programs/types/Programs.types";
import { ProgramProviderService } from "../../../../services/ProgramProviderService";
import { ProgramTypeCard } from "./ProgramTypeCard";

export const ProgramProviderProgramTypesForm: React.FC<Props> = (props) => {
  const [selectedCards, setSelectedCards] = React.useState<EProgramType[]>(
    props.programProvider.programTypes?.map((pt: any) => pt.programType) || []
  );

  const isSelected = (cardKey: EProgramType) => {
    return selectedCards?.includes(cardKey);
  };

  const toggleSelection = (cardKey: EProgramType) => {
    if (isSelected(cardKey)) {
      setSelectedCards(filter(selectedCards, (ck) => ck !== cardKey));
    } else {
      setSelectedCards([...selectedCards, cardKey]);
    }
  };

  const handleSubmit = async () => {
    const programTypeEligibilities = await ProgramProviderService.addProgramTypes(
      selectedCards
    );
    props.onSubmit({
      ...props.programProvider,
      programTypes: programTypeEligibilities,
    });
  };

  const allProgramTypes = [
    // EProgramType.COMPETITIONS_OR_CONFERENCES,
    EProgramType.LOCAL_ENRICHMENT_CLASSES,
    // EProgramType.OVERSEAS_INTERNSHIPS,
    // EProgramType.SHORT_STUDY_TOURS,
    // EProgramType.SUMMER_WINTER_SCHOOL,
  ];

  return (
    <Grid item container spacing={6}>
      {allProgramTypes.map((pt) => {
        const summerWinterSchool = pt === EProgramType.SUMMER_WINTER_SCHOOL;

        return (
          <Grid item xs={12} md={summerWinterSchool ? 12 : 6} key={pt}>
            <ProgramTypeCard
              programType={{
                title: EProgramTypeLabels[pt],
                description: "Description coming soon",
                key: pt,
              }}
              raised={isSelected(pt)}
              onClick={toggleSelection}
            />
          </Grid>
        );
      })}

      <Grid item xs={12}>
        <Button onClick={handleSubmit} color="primary" variant="contained">
          Next
        </Button>
      </Grid>
    </Grid>
  );
};

type Props = {
  programProvider: ProgramProvider;
  onSubmit: (programProvider: ProgramProvider) => void;
};

import React from "react";

import { Grid, Typography } from "@material-ui/core";
import { NextPage } from "next";

import withError from "../../../components/HOCs/withError";
import AdminLayout from "../../../components/Layouts/AdminLayout";
import { getUserAndEnsureAdmin } from "../../../services/Auth.functions";
import ProgramProvider from "../../../types/ProgramProvider.types";
import User from "../../../types/user.types";
import { PaddedPaper } from "../../Shared/components/PaddedPaper";
import { ProgramProviderAdminService } from "../services/ProgramProviderAdminService";
import { PendingProgramProvidersTable } from "./verification/PendingProgramProvidersTable";

const ProgramProvidersManagementPage: NextPage<Props> = (props) => {
  return (
    <AdminLayout>
      <Grid container spacing={2}>
        <Grid item xs={12} md={6}>
          <PaddedPaper fullWidth>
            <Grid container spacing={2} direction="column">
              <Grid item>
                <Typography gutterBottom variant="h6">
                  Program Providers Accounts to Verify
                </Typography>
              </Grid>

              <Grid item>
                <PendingProgramProvidersTable
                  programProviders={props.programProviders}
                  total={props.total}
                />
              </Grid>
            </Grid>
          </PaddedPaper>
        </Grid>
      </Grid>
    </AdminLayout>
  );
};

type Props = {
  user: User;
  programProviders: ProgramProvider[];
  total: number;
};

ProgramProvidersManagementPage.getInitialProps = async (ctx) => {
  const user = await getUserAndEnsureAdmin(ctx);
  const [
    programProviders,
    total,
  ] = await ProgramProviderAdminService.getAllPendingProgramProviders(ctx);

  return {
    user,
    programProviders,
    total,
  };
};

export const ProgramProvidersManagementPageWithError = withError(
  ProgramProvidersManagementPage
);

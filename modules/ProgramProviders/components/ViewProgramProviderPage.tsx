import React from "react";

import { filter, find } from "lodash";
import { NextPage } from "next";

import { Button, Divider, Grid, Typography } from "@material-ui/core";
import withError from "../../../components/HOCs/withError";
import AdminLayout from "../../../components/Layouts/AdminLayout";
import { getUserAndEnsureAdmin } from "../../../services/Auth.functions";
import {
  EProgramTypeEligibilityStatus,
  ProgramTypeEligibility,
} from "../../../types/ProgramEligiblity.types";
import ProgramProvider, {
  EProgramProviderStatus,
} from "../../../types/ProgramProvider.types";
import User from "../../../types/user.types";
import { countries } from "../../Countries/countries";
import { EProgramTypeLabels } from "../../Programs/types/Programs.types";
import { PaddedPaper } from "../../Shared/components/PaddedPaper";
import { ProgramProviderAdminService } from "../services/ProgramProviderAdminService";

const ViewProgramProviderPage: NextPage<Props> = (props) => {
  const [programProvider, setProgramProvider] = React.useState(
    props.programProvider
  );
  const [approving, setApproving] = React.useState(false);
  const [
    programTypeEligibilities,
    setProgramTypeEligibilities,
  ] = React.useState<ProgramTypeEligibility[]>(programProvider.programTypes);

  const registeredAddress = programProvider.business.address;
  const registeredAddressCountry = find(countries, {
    code: registeredAddress?.countryCode,
  });

  const approve = async () => {
    setApproving(true);
    const updatedProgramProvider = await ProgramProviderAdminService.markAsVerified(
      programProvider.id
    );

    // the reason why we cannot set the return object directly is because the relations are not loaded for that return
    // object above.
    // this way preserves the relations that are already loaded.
    setProgramProvider({
      ...programProvider,
      status: updatedProgramProvider.status,
    });
    setApproving(false);
  };

  const approveProgramType = async (id: string) => {
    try {
      const updated = await ProgramProviderAdminService.approveProgramType(
        programProvider.id,
        id
      );

      setProgramTypeEligibilities([
        ...filter(programTypeEligibilities, (pte) => pte.id !== id),
        updated,
      ]);
    } catch (e) {
      console.error(e);
    }
  };

  const markAsPending = async (id: string) => {
    try {
      const updated = await ProgramProviderAdminService.markProgramTypeAsPending(
        programProvider.id,
        id
      );

      setProgramTypeEligibilities([
        ...filter(programTypeEligibilities, (pte) => pte.id !== id),
        updated,
      ]);
    } catch (e) {
      console.error(e);
    }
  };

  const isVerified = programProvider.status === EProgramProviderStatus.VERIFIED;

  return (
    <AdminLayout>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <PaddedPaper fullWidth>
            <Grid item container justify="space-between" alignItems="center">
              <Grid item>
                <Typography variant="h4">{programProvider.name}</Typography>
              </Grid>
              <Grid item>
                {isVerified ? (
                  <Typography>Verified</Typography>
                ) : (
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={approve}
                    disabled={approving}
                  >
                    Approve
                  </Button>
                )}
              </Grid>
            </Grid>
          </PaddedPaper>
        </Grid>

        <Grid item md={6} container spacing={2}>
          <Grid item xs={12}>
            <PaddedPaper>
              <Grid container direction="column" spacing={1}>
                <Grid item xs={12}>
                  <Typography variant="h6">Registered Address</Typography>
                </Grid>

                <Grid item>{registeredAddress?.street}</Grid>

                <Grid item>{registeredAddress?.street2}</Grid>
                <Grid item>{registeredAddress?.postalCode}</Grid>

                <Grid item>{registeredAddressCountry?.label}</Grid>
              </Grid>
            </PaddedPaper>
          </Grid>

          <Grid item xs={12}>
            <PaddedPaper>
              <Grid container direction="column" spacing={2}>
                <Grid item xs={12}>
                  <Typography variant="h6">Program Type Eligibility</Typography>
                </Grid>

                <Grid item container spacing={2} direction="column">
                  {programTypeEligibilities.map((pte) => {
                    return (
                      <React.Fragment key={pte.id}>
                        <Grid
                          item
                          container
                          justify="space-between"
                          alignItems="center"
                        >
                          <Grid item>
                            <Typography>
                              {EProgramTypeLabels[pte.programType]}
                            </Typography>
                          </Grid>
                          <Grid item>
                            {pte.status ===
                            EProgramTypeEligibilityStatus.PENDING ? (
                              <Button
                                color="primary"
                                variant="outlined"
                                onClick={() => approveProgramType(pte.id)}
                              >
                                Approve
                              </Button>
                            ) : (
                              <Button
                                color="secondary"
                                variant="outlined"
                                onClick={() => markAsPending(pte.id)}
                              >
                                Un-approve
                              </Button>
                            )}
                          </Grid>
                        </Grid>
                        <Grid item>
                          <Divider variant="fullWidth" />
                        </Grid>
                      </React.Fragment>
                    );
                  })}
                </Grid>
              </Grid>
            </PaddedPaper>
          </Grid>
        </Grid>

        <Grid item md={6} container spacing={2}>
          <Grid item xs={12}>
            <PaddedPaper fullWidth>
              <Grid container spacing={1} direction="column">
                <Grid item xs={12}>
                  <Typography variant="h6">Documents</Typography>
                </Grid>

                <Grid item>
                  <Typography variant="body2">
                    UEN: {programProvider.business.uen}
                  </Typography>
                </Grid>
              </Grid>
            </PaddedPaper>
          </Grid>
        </Grid>
      </Grid>
    </AdminLayout>
  );
};

type Props = {
  user: User;
  programProvider: ProgramProvider;
};

ViewProgramProviderPage.getInitialProps = async (ctx) => {
  const user = await getUserAndEnsureAdmin(ctx);

  const programProviderId = ctx.query.id as string;

  const programProvider = await ProgramProviderAdminService.getOneAsAdmin(
    ctx,
    programProviderId
  );

  return {
    user,
    programProvider,
  };
};

export const ViewProgramProviderPageWithError = withError(
  ViewProgramProviderPage
);

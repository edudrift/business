import React from "react";

import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@material-ui/core";
import ButtonLink from "../../../../components/ButtonLink";
import { StyledTableCell } from "../../../../components/Tables/StyledTableCell";
import ProgramProvider from "../../../../types/ProgramProvider.types";
import { PaddedPaper } from "../../../Shared/components/PaddedPaper";

export const PendingProgramProvidersTable: React.FC<Props> = ({
  programProviders,
  total,
}) => {
  return (
    <TableContainer>
      <Table>
        <caption>You have a total of {total} verification(s).</caption>

        <TableHead>
          <TableRow>
            <StyledTableCell>Program Providers Name</StyledTableCell>
            <StyledTableCell>Action</StyledTableCell>
          </TableRow>
        </TableHead>

        <TableBody>
          {programProviders.length === 0 && (
            <PaddedPaper>No verifications needed.</PaddedPaper>
          )}

          {programProviders.map((pp) => {
            return (
              <TableRow key={pp.id} hover>
                <TableCell component="th" scope="row">
                  {pp.name}
                </TableCell>
                <TableCell>
                  <ButtonLink
                    color="primary"
                    href="/admin/program-providers/[id]"
                    as={`/admin/program-providers/${pp.id}`}
                  >
                    View
                  </ButtonLink>
                </TableCell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

type Props = {
  programProviders: ProgramProvider[];
  total: number;
};

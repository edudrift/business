import { red } from "@material-ui/core/colors";
import { createMuiTheme, responsiveFontSizes } from "@material-ui/core/styles";

export const themeOptions = {
  palette: {
    primary: {
      main: "#1A8FFF",
    },
    secondary: {
      main: "#f44336",
    },
    error: {
      main: red.A400,
    },
    background: {
      default: "#f5f5f5",
    },
    text: {
      // primary: "#131026",
      // secondary: "#266BF0"
    },
  },
  typography: {
    fontFamily: "source-han-sans-simplified-c, sans-serif",
    fontWeightRegular: 500,
  },
  mixins: {
    toolbar: {
      minHeight: 112,
      "@media (min-width:0px) and (orientation: landscape)": {
        minHeight: 96,
      },
      "@media (min-width:600px)": {
        minHeight: 96,
      },
    },
  },
};

// Create a theme instance.
const theme = createMuiTheme(themeOptions);

export default responsiveFontSizes(theme);

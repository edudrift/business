// tslint:disable: max-classes-per-file
export class NotLoggedIn extends Error {
  statusCode: number = 401;
}

export class UserNotAdmin extends Error {
  statusCode: number = 403;
}

export class UserCredentialsExpired extends Error {
  statusCode: number = 401;
}

import jwtDecode from "jwt-decode";
import { NextPageContext } from "next/types";

import cookie from "js-cookie";
import nextCookie from "next-cookies";

export interface IDecodedToken {
  id: string;
  firstName: string;
  lastName: string;
  email: string;
}

class TokenService {
  public getToken(ctx?: NextPageContext) {
    if (ctx) {
      const { token } = nextCookie(ctx);

      return token;
    } else {
      return cookie.get("token");
    }
  }

  public saveToken(token: string) {
    cookie.set("token", token);

    return Promise.resolve();
  }

  public removeToken() {
    cookie.remove("token");

    // to support logging out from all windows
    // window.localStorage.setItem("logout", Date.now());
  }

  public decodeToken(token: string): IDecodedToken {
    const decoded: IDecodedToken = jwtDecode(token);

    return {
      id: decoded.id,
      firstName: decoded.firstName,
      lastName: decoded.lastName,
      email: decoded.email,
    };
  }
}

export default new TokenService();

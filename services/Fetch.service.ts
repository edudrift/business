import fetch from "isomorphic-unfetch";
import EHttpMethods from "../utils/EHttpMethods";

class FetchService {
  public async isofetch(
    url: string,
    httpMethod: EHttpMethods,
    payload?: object
  ): Promise<any> {
    try {
      const rawResponse = await fetch(`${process.env.API_URL}${url}`, {
        body:
          httpMethod === EHttpMethods.GET
            ? null
            : JSON.stringify({ ...payload }),
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        method: httpMethod
      });
      const data = await rawResponse.json();

      return {
        status: rawResponse.status,
        data
      };
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  public async isofetchAuthed(
    url: string,
    httpMethod: EHttpMethods,
    token: string,
    payload?: object
  ): Promise<any> {
    try {
      const rawResponse = await fetch(`${process.env.API_URL}${url}`, {
        body:
          httpMethod === EHttpMethods.GET
            ? null
            : JSON.stringify({ ...payload }),
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`
        },
        method: httpMethod
      });
      const data = await rawResponse.json();

      return {
        status: rawResponse.status,
        data
      };
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  public handleErrors(response: string): string {
    if (response === "TypeError: Failed to fetch") {
      throw Error("Server error.");
    }
    return response;
  }
}

export default new FetchService();

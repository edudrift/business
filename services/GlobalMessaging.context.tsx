import React, { useContext, useReducer } from "react";

import {
  EGlobalMessageSeverity,
  IGlobalMessagePayload
} from "../types/global.types";

export const GlobalMessagingContext = React.createContext({});

const initialState: IGlobalMessagePayload = { message: null };

export enum GlobalMessageActionType {
  SetDetails = "setMessage",
  RemoveDetails = "removeMessage"
}

interface IAction {
  type: GlobalMessageActionType;
  payload: IGlobalMessagePayload;
}

const reducer: React.Reducer<{}, IAction> = (_state, action) => {
  switch (action.type) {
    case GlobalMessageActionType.SetDetails:
      const {
        severity = EGlobalMessageSeverity.INFO,
        message
      } = action.payload;

      return {
        message,
        severity
      };
    case GlobalMessageActionType.RemoveDetails:
      return {
        message: initialState.message
      };
    default:
      throw new Error(`Unhandled action type: ${action.type}`);
  }
};

export const GlobalMessagingProvider = ({ children }: any) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  return (
    <GlobalMessagingContext.Provider value={[state, dispatch]}>
      {children}
    </GlobalMessagingContext.Provider>
  );
};

// useContext hook - export here to keep code for global auth state
// together in this file, allowing user info to be accessed and updated
// in any functional component using the hook
export const useGlobalMessaging: any = () => useContext(GlobalMessagingContext);

import { NextPageContext } from "next";
import { FairPackagePurchase } from "../types/FairPackagePurchase.types";
import EHttpMethods from "../utils/EHttpMethods";
import isofetch from "../utils/isofetch";
import { EFairPackagePublishStatus } from "./../types/FairPackagePurchase.types";
import { NotLoggedIn } from "./Auth.errors";
import TokenService from "./Token.service";

export class PurchasesService {
  static async getAllPurchasesFromFairId(fairId: string) {
    const token = TokenService.getToken();
    if (token) {
      const response = await isofetch<FairPackagePurchase[]>(
        `/api/admin/fairs/${fairId}/purchases`,
        EHttpMethods.GET
      );

      if (response.status === 200 && response.data) {
        return response.data;
      } else {
        console.log(response);
        throw new Error("Error fetching purchases for fair");
      }
    } else {
      throw new NotLoggedIn();
    }
  }

  static async getAllPurchases(ctx: NextPageContext) {
    const token = TokenService.getToken(ctx);
    if (token) {
      const response = await isofetch<FairPackagePurchase[]>(
        "/api/fairs/purchases",
        EHttpMethods.GET,
        token
      );

      if (response.status === 200 && response.data) {
        return response.data;
      } else {
        console.log(response);
        throw new Error();
      }
    }
  }

  static async getById(ctx: NextPageContext, purchaseId: string) {
    const token = TokenService.getToken(ctx);
    if (token) {
      const url = `/api/fairs/purchases/${purchaseId}`;

      const response = await isofetch<FairPackagePurchase>(
        url,
        EHttpMethods.GET,
        token
      );

      if (response.status === 200 && response.data) {
        return response.data;
      } else {
        console.log(response);
        throw new Error();
      }
    }
  }

  static async requestToPublish(
    purchase: FairPackagePurchase
  ): Promise<FairPackagePurchase> {
    console.log("REQUESTING TO PUBLISH");
    const token = TokenService.getToken();
    if (token) {
      const url = `/api/fairs/purchases/${purchase.id}/publishStatus`;

      const response = await isofetch<FairPackagePurchase>(
        url,
        EHttpMethods.PUT,
        token,
        {
          publishStatus: EFairPackagePublishStatus.REQUESTED,
        }
      );

      if (response.status === 202 && response.data) {
        return response.data;
      } else {
        console.log(response);
        throw new Error("Error updating publish status for purchase");
      }
    } else {
      throw new NotLoggedIn();
    }
  }
}

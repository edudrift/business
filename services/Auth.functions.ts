import { NextPageContext } from "next";
import Router from "next/router";

import EHttpMethods from "../utils/EHttpMethods";
import isofetch from "../utils/isofetch";
import redirect from "../utils/redirect";
import { EUserRoles } from "./../types/user.types";
import {
  NotLoggedIn,
  UserCredentialsExpired,
  UserNotAdmin,
} from "./Auth.errors";
import FetchService from "./Fetch.service";
import TokenService from "./Token.service";

import User from "../types/user.types";

export async function getUserOrReturnUndefined(ctx: NextPageContext) {
  const token = TokenService.getToken(ctx);
  if (token) {
    const response = await isofetch<User>(
      "/api/user/me",
      EHttpMethods.GET,
      token
    );

    if (response.status === 200 && response.data) {
      return response.data;
    } else {
      return undefined;
    }
  } else {
    return undefined;
  }
}

export async function getUserAndEnsureAdmin(
  ctx: NextPageContext
): Promise<User> {
  const token = TokenService.getToken(ctx);

  if (token) {
    const response = await isofetch<User>(
      "/api/user/me",
      EHttpMethods.GET,
      token
    );
    if (response.status === 200 && response.data) {
      const user = response.data;
      if (user.role === EUserRoles.ADMIN) {
        return response.data;
      } else {
        throw new UserNotAdmin();
      }
    } else if (response.status === 401) {
      redirect(`/login?goTo=${ctx.asPath}`, 301, ctx);
      throw new NotLoggedIn();
    } else {
      console.log(response);
      throw new Error("Error fetching");
    }
  } else {
    throw new NotLoggedIn();
  }
}

export async function getUserProfileFromClient(): Promise<User> {
  const token = TokenService.getToken();
  if (token) {
    const response = await isofetch<User>(
      "/api/user/me",
      EHttpMethods.GET,
      token
    );
    if (response.status === 200) {
      if (response.data) {
        return response.data;
      } else {
        throw new Error("Response from API does not include data property.");
      }
    } else {
      throw new Error("Error fetching user profile");
    }
  } else {
    throw new NotLoggedIn();
  }
}

export async function getUserProfileOrRedirect(
  ctx: NextPageContext,
  requiredVerifiedEmail: boolean = true
): Promise<User> {
  const token = TokenService.getToken(ctx);

  if (token) {
    const response = await FetchService.isofetchAuthed(
      "/api/user/me",
      EHttpMethods.GET,
      token
    );

    if (response.status === 401) {
      redirect(`/login?goTo=${ctx.asPath}`, 301, ctx);
    }

    if (response.status === 200) {
      if (requiredVerifiedEmail && !response.data.isEmailVerified) {
        redirect("/verify/email/alert", 301, ctx);
      } else {
        return response.data;
      }
    }

    throw new Error("Something went wrong!");
  } else {
    redirect(`/login?goTo=${ctx.asPath}`, 301, ctx);
    throw new NotLoggedIn();
  }
}

export async function getUserProfileOrThrow(
  ctx: NextPageContext
): Promise<User> {
  const token = TokenService.getToken(ctx);
  if (token) {
    const response = await isofetch<User>(
      "/api/user/me",
      EHttpMethods.GET,
      token
    );
    if (response.status === 200 && response.data) {
      return response.data;
    } else if (response.status === 401) {
      throw new UserCredentialsExpired();
    } else {
      throw new Error(`Failed with ${response.status}`);
    }
  } else {
    throw new NotLoggedIn();
  }
}

export function ensureEmailVerifiedOrShowVerifyEmailPage(
  ctx: NextPageContext,
  user: User
) {
  if (!user.isEmailVerified) {
    redirect("/verify/email/alert", 301, ctx);
  }
}

export function logout() {
  TokenService.removeToken();
  Router.push("/");
}

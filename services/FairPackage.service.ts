import { NextPageContext } from "next";
import { FairPackagePurchase } from "../types/FairPackagePurchase.types";
import EHttpMethods from "../utils/EHttpMethods";
import isofetch from "../utils/isofetch";
import TokenService from "./Token.service";

export class FairPackageService {
  static async checkout(fairPackageId: string) {
    const response = await isofetch<{ sessionId: string }>(
      `/api/fairs/packages/${fairPackageId}/checkout`,
      EHttpMethods.POST,
      TokenService.getToken()
    );

    if (response.status === 201 && response.data) {
      return response.data.sessionId;
    }
  }

  static async getPurchase(
    ctx: NextPageContext,
    sessionId: string
  ): Promise<FairPackagePurchase> {
    const response = await isofetch<FairPackagePurchase>(
      `/api/fairs/purchases/session/${sessionId}`,
      EHttpMethods.GET,
      TokenService.getToken(ctx)
    );

    if (response.status === 200 && response.data) {
      return response.data;
    } else {
      console.log(response);
      throw new Error(
        "Did not get 200 when fetching purchase entity from Stripe session id"
      );
    }
  }

  static async cancelOrder(ctx: NextPageContext, sessionId: string) {
    const response = await isofetch<FairPackagePurchase>(
      `/api/fairs/purchases`,
      EHttpMethods.POST,
      TokenService.getToken(ctx),
      {
        stripeSessionId: sessionId,
        status: "CANCEL",
      }
    );

    if (response.status === 200 && response.data) {
      return response.data;
    } else {
      console.log(response);
      throw new Error();
    }
  }
}

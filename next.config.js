const path = require("path");
const webpack = require("webpack");
const withCSS = require("@zeit/next-css");
require("dotenv").config();

module.exports = withCSS({
  cssModules: true,
  env: {
    API_URL: process.env.API_URL,
  },
  webpack: (config) => {
    config.resolve.alias["~"] = path.resolve(__dirname);
    return config;
  },
  publicRuntimeConfig: {
    SENTRY_DSN: "https://ed12f71c54004ef1a097b51808438c15@sentry.io/5190802",
    STRIPE_KEY: "pk_test_HHQ609lyadDjnoCc2tZlqa1T00Yq7QTfcD",
  },
});

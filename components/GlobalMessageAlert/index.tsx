import { useTheme } from "@material-ui/core";
import { Alert, AlertTitle } from "@material-ui/lab";
import React from "react";
import {
  GlobalMessageActionType,
  useGlobalMessaging
} from "../../services/GlobalMessaging.context";
import {
  EGlobalMessageSeverity,
  IGlobalMessagePayload
} from "../../types/global.types";

function closeAlert(messageDispatch: any) {
  messageDispatch({
    type: GlobalMessageActionType.RemoveDetails
  });
}

function renderGlobalMessage(
  messageState: IGlobalMessagePayload,
  messageDispatch: any
) {
  const theme = useTheme();

  if (messageState.message) {
    return (
      <Alert
        style={{ marginBottom: theme.spacing(2) }}
        severity={getSeverity(messageState)}
        onClose={() => closeAlert(messageDispatch)}
      >
        {renderAlertTitle(messageState)}
        {messageState.message}
      </Alert>
    );
  }

  return <React.Fragment />;
}

function getSeverity(
  messageState: IGlobalMessagePayload
): EGlobalMessageSeverity {
  return messageState.severity
    ? messageState.severity
    : EGlobalMessageSeverity.INFO;
}

function renderAlertTitle(messageState: any) {
  switch (getSeverity(messageState)) {
    case EGlobalMessageSeverity.INFO:
      return <AlertTitle>Info</AlertTitle>;
    case EGlobalMessageSeverity.SUCCESS:
      return <AlertTitle>Success</AlertTitle>;
    case EGlobalMessageSeverity.WARNING:
      return <AlertTitle>Warning</AlertTitle>;
    case EGlobalMessageSeverity.ERROR:
      return <AlertTitle>Error</AlertTitle>;
  }
}

export default function GlobalMessageAlert() {
  const [messageState, messageDispatch] = useGlobalMessaging();

  return (
    <React.Fragment>
      {renderGlobalMessage(messageState, messageDispatch)}
    </React.Fragment>
  );
}

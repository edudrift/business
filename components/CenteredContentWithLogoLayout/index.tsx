import React from "react";

import { Grid, makeStyles } from "@material-ui/core";
import Link from "next/link";
import { PaddedPaper } from "../../modules/Shared/components/PaddedPaper";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: "#f9fafb",
    minHeight: "100vh",
    height: "100%",
    overflow: "hidden",
    // flexWrap: "nowrap",
  },
  logoWrapper: {
    display: "flex",
    justifyContent: "center",
    marginBottom: theme.spacing(2),
  },
  logo: {
    width: "210px",
    cursor: "pointer",
  },
  paperWrapper: {
    padding: theme.spacing(6),
  },
}));

const CenteredContentWithLogoLayout: React.FC = ({ children }) => {
  const classes = useStyles();

  return (
    <Grid
      className={classes.root}
      container
      justify="center"
      direction="column"
      spacing={2}
    >
      <Grid item container justify="center">
        <Grid item>
          <Link href="/">
            <img src="/logo@2x.png" className={classes.logo} />
          </Link>
        </Grid>
      </Grid>
      <Grid item container justify="center" spacing={2}>
        <PaddedPaper
          withElevation={3}
          className={classes.paperWrapper}
          fullWidth
        >
          <Grid item>{children}</Grid>
        </PaddedPaper>
      </Grid>
    </Grid>
  );
};

export default CenteredContentWithLogoLayout;

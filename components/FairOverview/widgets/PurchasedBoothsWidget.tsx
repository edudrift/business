import React from "react";

import Fair from "../../../types/Fairs.types";
import { PaddedPaper } from "../../../modules/Shared/components/PaddedPaper";

export const PurchasedBoothsWidget: React.FC<Props> = (props) => {
  console.log(props.fair);
  return (
    <PaddedPaper>
      <strong>Number of purchased booths</strong>
    </PaddedPaper>
  );
};

type Props = {
  fair: Fair;
};

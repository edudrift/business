import React from "react";

import { PurchasesService } from "../../../services/Purchases.service";
import Fair from "../../../types/Fairs.types";
import { formatDateWithFullDateAndTime } from "../../../utils/dateUtils";
import { PaddedPaper } from "../../../modules/Shared/components/PaddedPaper";
import { PurchasesReviewTable } from "../../Tables/PurchasesReviewTable/PurchasesReviewTable";
import { FairPackagePurchase } from "../../../types/FairPackagePurchase.types";

export const PurchaseReviewRequestTableWidget: React.FC<Props> = (props) => {
  const [fetched, setFetched] = React.useState<Date>();
  const [purchases, setPurchases] = React.useState<FairPackagePurchase[]>([]);

  const fetch = async () => {
    const response = await PurchasesService.getAllPurchasesFromFairId(
      props.fairId
    );

    console.log(response);

    setFetched(new Date());
    setPurchases(response);
  };

  React.useEffect(() => {
    try {
      fetch();
    } catch (e) {
      console.log(e);
    }
  }, []);

  const fetchedText = fetched
    ? formatDateWithFullDateAndTime(fetched)
    : "Not fetched";

  return (
    <PaddedPaper fullWidth>
      <PurchasesReviewTable
        purchases={purchases}
        caption={`Last fetched: ${fetchedText}`}
      />
    </PaddedPaper>
  );
};

type Props = {
  fairId: Fair["id"];
};

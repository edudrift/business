import { Chip, Divider, Grid, Tooltip, Typography } from "@material-ui/core";
import React from "react";
import Fair, { EFairStatus } from "../../types/Fairs.types";
import { PaddedPaper } from "../../modules/Shared/components/PaddedPaper";
import FairOverviewActions from "./FairOverviewActions";
import { PurchaseReviewRequestTableWidget } from "./widgets/PurchaseReviewRequestTableWidget";
import { formatDateWithFullDateAndTime } from "../../utils/dateUtils";

const FairOverview: React.FC<Props> = (props) => {
  const [fair, setFair] = React.useState(props.fair);

  return (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        <PaddedPaper>
          <Grid item container justify="space-between" alignItems="center">
            <Grid item>
              <Typography variant="h4">
                {fair.name}{" "}
                <Tooltip title="Publication status">
                  <Chip
                    size="small"
                    variant="outlined"
                    color={
                      fair.status === EFairStatus.DRAFT
                        ? "secondary"
                        : "primary"
                    }
                    label={fair.status}
                  />
                </Tooltip>
              </Typography>
            </Grid>

            <Grid item>
              <FairOverviewActions fair={fair} setFair={setFair} />
            </Grid>
          </Grid>
        </PaddedPaper>
      </Grid>

      <Grid item xs={10}>
        <PurchaseReviewRequestTableWidget fairId={fair.id} />
      </Grid>

      <Grid item xs={2}>
        <PaddedPaper>
          <Grid item container spacing={2}>
            <Grid item xs={12}>
              <Typography variant="h5" component="p">
                Starts
              </Typography>
              <Typography>
                {formatDateWithFullDateAndTime(new Date(fair.startDateTime))}
              </Typography>
            </Grid>

            <Grid item xs={12}>
              <Divider variant="middle" />
            </Grid>

            <Grid item xs={12}>
              <Typography variant="h5" component="p">
                Ends
              </Typography>
              <Typography>
                {formatDateWithFullDateAndTime(new Date(fair.endDateTime))}
              </Typography>
            </Grid>
          </Grid>
        </PaddedPaper>
      </Grid>
    </Grid>
  );
};

type Props = {
  fair: Fair;
};

export default FairOverview;

import { Button, Grid, Tooltip } from "@material-ui/core";
import InfoIcon from "@material-ui/icons/Info";
import React from "react";
import Fair, { EFairStatus } from "../../types/Fairs.types";
import FairsService from "../../utils/Fairs.service";
import ButtonLink from "../ButtonLink";

const FairOverviewActions: React.FC<Props> = ({ fair, setFair }) => {
  const publishFair = async () => {
    const updatedFair = await FairsService.publish(fair);

    if (updatedFair) {
      setFair(updatedFair);
    }
  };

  const unpublish = async () => {
    const updatedFair = await FairsService.unpublish(fair);

    if (updatedFair) {
      setFair(updatedFair);
    }
  };

  return (
    <Grid container spacing={1}>
      <Grid item xs>
        <ButtonLink
          color="primary"
          variant="outlined"
          href="/admin/fair/[id]"
          as={`/admin/fair/${fair.id}`}
        >
          Edit
        </ButtonLink>
      </Grid>

      {fair.status === EFairStatus.DRAFT && (
        <Grid item xs>
          <Tooltip title="Click to publish this fair">
            <Button
              startIcon={<InfoIcon />}
              variant="contained"
              color="primary"
              onClick={publishFair}
            >
              Publish
            </Button>
          </Tooltip>
        </Grid>
      )}

      {fair.status === EFairStatus.PUBLISHED && (
        <Grid item xs>
          <Tooltip title="Click to unpublish this fair">
            <Button
              startIcon={<InfoIcon />}
              variant="outlined"
              color="secondary"
              onClick={unpublish}
            >
              Unpublish
            </Button>
          </Tooltip>
        </Grid>
      )}
    </Grid>
  );
};

type Props = {
  fair: Fair;
  setFair: (v: any) => any;
};

export default FairOverviewActions;

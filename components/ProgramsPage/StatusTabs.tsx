import { Tab, Tabs } from "@material-ui/core";
import { useRouter } from "next/router";
import React from "react";

enum EProgramsPageStatusTabsLabel {
  APPROVED = "APPROVED PROGRAMS",
  PENDING = "PENDING PROGRAMS",
  PAST = "PAST PROGRAMS",
}

export enum EProgramsPageStatusTabKeys {
  APPROVED = 0,
  PENDING = 1,
  PAST = 2,
}

export type TabTypes = "APPROVED" | "PENDING" | "PAST";

const StatusTabs: React.FC<Props> = (props) => {
  const router = useRouter();
  const [value, setValue] = React.useState(
    EProgramsPageStatusTabKeys[props.tab]
  );

  const handleChange = (_: React.ChangeEvent<{}>, newValue: number) => {
    router.push({
      pathname: router.pathname,
      query: { tab: EProgramsPageStatusTabKeys[newValue] },
    });

    setValue(newValue);
    props.onChange();
  };

  return (
    <Tabs
      value={value}
      indicatorColor="primary"
      textColor="primary"
      onChange={handleChange}
      aria-label="disabled tabs example"
    >
      <Tab label={EProgramsPageStatusTabsLabel.APPROVED} />
      <Tab label={EProgramsPageStatusTabsLabel.PENDING} />
      <Tab label={EProgramsPageStatusTabsLabel.PAST} />
    </Tabs>
  );
};

type Props = {
  onChange: () => void;
  tab: TabTypes;
};

export default StatusTabs;

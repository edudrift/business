import React from "react";

import { Button, Container, Grid, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core";
import { Alert } from "@material-ui/lab";
import theme from "../../theme";
import ButtonLink from "../ButtonLink";

type Props = {
  renderTitle: () => React.ComponentElement<any, any>;
  renderSubtitle: () => React.ComponentElement<any, any>;
  IconElement: any;
  iconColor?: string;
  showSuccessAlert?: boolean;
  successAlertMessage?: string;

  buttonLess?: boolean;
  buttonText?: string;
  buttonLink?: string;
  buttonOnClick?: () => any;
};

const useStyles = makeStyles((incomingTheme) => ({
  iconWrapper: {
    marginBottom: incomingTheme.spacing(3),
  },
  icon: {
    width: 90,
    height: 90,
  },
  title: {
    textAlign: "center",
  },
  subtitle: {
    textAlign: "center",
  },
  buttonWrapper: {
    marginTop: incomingTheme.spacing(3),
  },
}));

const CallToActionWithIcon: React.FC<Props> = (props) => {
  const { IconElement, iconColor = theme.palette.primary.main } = props;
  const classes = useStyles();

  const conditionallyRenderButtonWithLink = () => {
    if (props.buttonLink) {
      return (
        <ButtonLink variant="outlined" color="primary" href="/biz">
          {props.buttonText}
        </ButtonLink>
      );
    }
  };

  const conditionallyRenderButtonWithOnClick = () => {
    if (props.buttonOnClick) {
      return (
        <Button
          onClick={props.buttonOnClick}
          color="primary"
          variant="outlined"
          disabled={props.showSuccessAlert}
        >
          {props.buttonText}
        </Button>
      );
    }
  };

  return (
    <Container maxWidth="md">
      <Grid container justify="center" className={classes.iconWrapper}>
        <Grid item>
          <IconElement className={classes.icon} style={{ color: iconColor }} />
        </Grid>
      </Grid>

      <Grid container justify="center" direction="column">
        <Grid item className={classes.title}>
          {props.renderTitle()}
        </Grid>

        <Grid item>
          <Typography className={classes.subtitle}>
            {props.renderSubtitle()}
          </Typography>
        </Grid>

        <Grid
          container
          item
          justify="center"
          direction="column"
          className={classes.buttonWrapper}
          spacing={1}
        >
          <Grid container item zeroMinWidth={true} justify="center">
            {conditionallyRenderButtonWithOnClick()}

            {conditionallyRenderButtonWithLink()}
          </Grid>

          {props.showSuccessAlert && (
            <Grid item>
              <Alert>{props.successAlertMessage}</Alert>
            </Grid>
          )}
        </Grid>
      </Grid>
    </Container>
  );
};

export default CallToActionWithIcon;

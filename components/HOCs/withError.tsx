import * as Sentry from "@sentry/browser";
import { NextPageContext } from "next";
import getConfig from "next/config";
import React from "react";
import { ErrorMessageFromCode } from "~/components/ErrorMessages/ErrorMessage";

const { SENTRY_DSN } = getConfig().publicRuntimeConfig;
Sentry.init({ dsn: SENTRY_DSN });

/**
 * Send an error event to Sentry.
 *
 * Server-side:
 * Next.js captures SSR errors and never passes them to the Sentry
 * middleware. It does, however, render the _error.js component, so
 * we can manually fire Sentry events here.
 *
 * Client-side:
 * Unhandled client exceptions will always bubble up to the _error.js
 * component, so we can manually fire events here.
 */
const notifySentry = (err: Error, statusCode: number, req?: any) => {
  Sentry.configureScope((scope) => {
    if (!req) {
      scope.setTag(`ssr`, "false");
    } else {
      scope.setTag(`ssr`, "true");
      scope.setExtra(`url`, req.url);
      scope.setExtra(`params`, req.params);
      scope.setExtra(`query`, req.query);
      scope.setExtra(`statusCode`, statusCode);
      scope.setExtra(`headers`, req.headers);

      if (req.user) {
        scope.setUser({ id: req.user.id, email: req.user.email });
      }
    }
  });

  Sentry.captureException(err);
};

export default (Component: any) => {
  return class WithError extends React.Component {
    props: any;

    static async getInitialProps(ctx: NextPageContext) {
      try {
        const props: any =
          (Component.getInitialProps
            ? await Component.getInitialProps(ctx)
            : null) || {};
        return props;
      } catch (e) {
        const { statusCode = 500 } = e;

        notifySentry(e, statusCode, ctx.req);

        return {
          statusCode,
        };
      }
    }

    render() {
      if (this.props.statusCode) {
        return <ErrorMessageFromCode statusCode={this.props.statusCode} />;
      }
      return <Component {...this.props} />;
    }
  };
};

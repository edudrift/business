import { Grid, makeStyles, TextField as MUITextField } from "@material-ui/core";
import { Field } from "formik";
import { Autocomplete } from "material-ui-formik-components";
import React from "react";
import { countries } from "../../modules/Countries/countries";
import { Country } from "../../modules/Countries/types/CountryType";

const useStyles = makeStyles({
  option: {
    fontSize: 15,
    "& > span": {
      marginRight: 10,
      fontSize: 18,
    },
  },
});

const buildFieldName = (name: string, namespace?: string) => {
  if (!namespace) return name;

  return `${namespace}.${name}`;
};

// ISO 3166-1 alpha-2
// ⚠️ No support for IE 11
function countryToFlag(isoCode: string) {
  return typeof String.fromCodePoint !== "undefined"
    ? isoCode
        .toUpperCase()
        .replace(/./g, (char) =>
          String.fromCodePoint(char.charCodeAt(0) + 127397)
        )
    : isoCode;
}

export const CountryField: React.FC<Props> = (props) => {
  const classes = useStyles();

  const { namespace, values, setValues } = props;

  return (
    <Grid item>
      <Field
        required
        value={namespace ? values[namespace].country : values.country}
        onChange={(_: any, selected: Country) => {
          if (namespace) {
            console.log({ namespace, values });
            const namespacedValue = values[namespace];
            namespacedValue.country = selected;

            setValues({ ...values, [namespace]: namespacedValue });
          } else {
            setValues({ ...values, country: selected });
          }
        }}
        component={Autocomplete}
        name={buildFieldName("countryCode", namespace)}
        options={countries as Country[]}
        classes={{
          option: classes.option,
        }}
        autoHighlight
        getOptionLabel={(option: Country) => option.label}
        renderOption={(option: Country) => (
          <React.Fragment>
            <span>{countryToFlag(option.code)}</span>
            {option.label} ({option.code}) +{option.phone}
          </React.Fragment>
        )}
        renderInput={(params: any) => {
          return (
            <MUITextField
              {...params}
              label="Choose a country"
              variant="outlined"
              inputProps={{
                ...params.inputProps,
                autoComplete: "new-password", // disable autocomplete and autofill
              }}
              fullWidth
              required
            />
          );
        }}
      />
    </Grid>
  );
};

type Props = {
  values: any;
  setValues: any;
  namespace?: string;
};

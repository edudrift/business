import React from "react";

import {
  Avatar,
  Grid,
  IconButton,
  makeStyles,
  Menu,
  MenuItem,
  Typography,
} from "@material-ui/core";
import Router from "next/router";

import TokenService from "../../services/Token.service";
import User, { EUserRoles } from "../../types/user.types";

const useStyles = makeStyles((theme) => ({
  avatar: {
    width: theme.spacing(4),
    height: theme.spacing(4),
  },
  name: {
    marginRight: theme.spacing(1),
    color: "#333333",
  },
}));

const conditionallyRenderAdminNav = (user: User) => {
  if (user.role === EUserRoles.ADMIN) {
    return (
      <MenuItem onClick={() => Router.push("/admin")}>Admin Panel</MenuItem>
    );
  }
};

const LoggedInNav: React.FC<Props> = (props) => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

  const handleProfileMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
  };

  const handleLogout = () => {
    TokenService.removeToken();
    Router.push("/");
    handleMenuClose();
  };

  const isMenuOpen = Boolean(anchorEl);
  return (
    <Grid item xs container justify="flex-end">
      <IconButton
        aria-label="account of current user"
        aria-controls="menu-appbar"
        aria-haspopup="true"
        color="inherit"
        style={{ borderRadius: 0 }}
        onClick={handleProfileMenuOpen}
      >
        <Typography variant="body2" className={classes.name}>
          {props.user.email}
        </Typography>
        <Avatar
          alt="Remy Sharp"
          src="https://randomuser.me/api/portraits/lego/2.jpg"
          className={classes.avatar}
        />
      </IconButton>
      <Menu
        anchorEl={anchorEl}
        anchorOrigin={{ vertical: "top", horizontal: "right" }}
        keepMounted
        transformOrigin={{ vertical: "top", horizontal: "right" }}
        open={isMenuOpen}
        onClose={handleMenuClose}
      >
        {conditionallyRenderAdminNav(props.user)}
        <MenuItem
          onClick={() => {
            handleMenuClose();
            Router.push("/biz");
          }}
        >
          Dashboard
        </MenuItem>
        <MenuItem onClick={handleLogout}>Log out</MenuItem>
      </Menu>
    </Grid>
  );
};

type Props = {
  user: User;
};

export default LoggedInNav;

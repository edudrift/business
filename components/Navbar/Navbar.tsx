import {
  AppBar,
  Container,
  Grid,
  makeStyles,
  Toolbar
} from "@material-ui/core";

import React from "react";

import User from "../../types/user.types";
import ButtonLink from "../ButtonLink";
import LoggedInNav from "./LoggedInNav";

const useStyles = makeStyles(theme => {
  return {
    title: {
      flexGrow: 1
    },
    appBar: {
      background: "#fff",
      boxShadow: "0px 2px 4px 0px rgba(0,0,0,0.1);",
      marginBottom: theme.spacing(2),
      zIndex: theme.zIndex.drawer + 1
    },
    logoButton: {
      justifyContent: "start",
      textDecoration: "none",
      color: theme.palette.text.primary,
      "&:hover": {
        backgroundColor: "transparent"
      },
      padding: 0
    }
  };
});

function LoggedOutNavigations() {
  return (
    <React.Fragment>
      <ButtonLink href="/login">Login</ButtonLink>
      <ButtonLink href="/register">Register</ButtonLink>
    </React.Fragment>
  );
}

const Navbar: React.FC<Props> = props => {
  const classes = useStyles();

  return (
    <AppBar position="static" color="default" className={classes.appBar}>
      <Container maxWidth="lg">
        <Toolbar disableGutters={true}>
          <Grid container justify="space-between">
            <Grid item xs container>
              <ButtonLink
                href="/"
                className={classes.logoButton}
                disableElevation={true}
                disableRipple={true}
              >
                <img
                  src="/logo@2x.png"
                  style={{
                    width: "50%",
                    justifySelf: "start",
                    paddingLeft: 0
                  }}
                />
              </ButtonLink>
            </Grid>

            {props.user ? (
              <LoggedInNav user={props.user} />
            ) : (
              <Grid item>
                <LoggedOutNavigations />
              </Grid>
            )}
          </Grid>
        </Toolbar>
      </Container>
    </AppBar>
  );
};

type Props = {
  user?: User;
};

export default Navbar;

import NextLink from "next/link";
import React from "react";

import { Breadcrumbs, Grid, Link, Paper, Typography } from "@material-ui/core";
import theme from "../../theme";

export type DashboardBreadcrumbCrumb = {
  title: string;
  link?: string;
};

type Props = {
  crumbs: DashboardBreadcrumbCrumb[];
};

const renderCrumb = (crumb: DashboardBreadcrumbCrumb) => {
  const { title, link } = crumb;

  if (link) {
    return (
      <NextLink href={link} key={title}>
        <Link href={link}>
          <Typography variant="subtitle2" style={{ color: "#999999" }}>
            {title}
          </Typography>
        </Link>
      </NextLink>
    );
  }

  return (
    <Typography
      color="textPrimary"
      key={title}
      variant="subtitle2"
      style={{ color: "#666666" }}
    >
      {title}
    </Typography>
  );
};

const DashboardBreadcrumbs: React.FC<Props> = (props) => {
  return (
    <Grid item>
      <Paper
        elevation={0}
        style={{
          padding: theme.spacing(1),
          paddingLeft: 0,
        }}
      >
        <Breadcrumbs aria-label="breadcrumb">
          {props.crumbs.map(renderCrumb)}
        </Breadcrumbs>
      </Paper>
    </Grid>
  );
};

export default DashboardBreadcrumbs;

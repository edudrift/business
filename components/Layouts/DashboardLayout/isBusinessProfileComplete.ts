import { EProgramProviderStatus } from "../../../types/ProgramProvider.types";
import User from "../../../types/user.types";

function isProgramProviderVerified(user: User): boolean {
  return user.programProvider.status === EProgramProviderStatus.VERIFIED;
}

export default isProgramProviderVerified;

import React, { FunctionComponent } from "react";

import {
  AppBar,
  Avatar,
  createStyles,
  Divider,
  Drawer,
  Grid,
  IconButton,
  List,
  // ListItemIcon,
  // ListItemText,
  ListItem,
  ListItemIcon,
  ListItemText,
  ListSubheader,
  makeStyles,
  Menu,
  MenuItem,
  Toolbar,
  Typography,
} from "@material-ui/core";

import clsx from "clsx";
// import Link from "next/link";

import AccountBalanceWalletIcon from "@material-ui/icons/AccountBalanceWallet";
import AccountBoxIcon from "@material-ui/icons/AccountBox";
import BusinessCenterIcon from "@material-ui/icons/BusinessCenter";
import ChatIcon from "@material-ui/icons/Chat";
import EqualizerIcon from "@material-ui/icons/Equalizer";
import HomeIcon from "@material-ui/icons/Home";
import LayersIcon from "@material-ui/icons/Layers";
import LoyaltyIcon from "@material-ui/icons/Loyalty";
import NotificationsIcon from "@material-ui/icons/Notifications";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";

import Router from "next/router";
import TokenService from "../../../services/Token.service";
import CompleteYourBusinessProfileAlert from "../../CompleteYourBusinessProfileAlert";
import isProgramProviderVerified from "./isBusinessProfileComplete";

import Link from "next/link";
import theme from "../../../theme";
import User from "../../../types/user.types";
import { PaddedPaper } from "../../../modules/Shared/components/PaddedPaper";

const drawerWidth = 240;

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      display: "flex",
    },
    appBar: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth,
      boxShadow: theme.shadows[0],
      backgroundColor: "white",
    },

    logoButton: {
      textDecoration: "none",
      color: theme.palette.text.primary,
      "&:hover": {
        backgroundColor: "transparent",
      },
    },
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
    },
    drawerPaper: {
      width: drawerWidth,
      boxShadow: theme.shadows[3],
    },
    content: {
      flexGrow: 1,
      backgroundColor: "#F0F2F5",
      minHeight: "100vh",
      // padding: theme.spacing(3),
      overflowY: "hidden",
      overflowX: "hidden",
    },
    childrenWrapper: {
      marginRight: theme.spacing(3),
      marginLeft: theme.spacing(3),
    },
    gutterTop: {
      marginTop: theme.spacing(3),
    },
    drawerListItemText: {
      textTransform: "uppercase",
    },
    selectedDrawerListItemText: {
      fontWeight: "bold",
      textTransform: "uppercase",
    },
    toolbarWithGutterBottom: {
      marginBottom: theme.spacing(1),
    },
    toolbarGutterWithBreadcrumb: {
      marginBottom: theme.spacing(10),
    },
    sidebarImage: {
      alignItems: "center",
      cursor: "pointer",
    },
    toolbar: {
      ...theme.mixins.toolbar,
      display: "flex",
      justifyContent: "center",
    },
    toolbarRoot: {
      flexDirection: "column",
      justifyContent: "center",
    },
    toolbarContainer: {
      display: "flex",
      alignItems: "center",
      minHeight: "64px",
    },
    selectedSidebarListItem: {
      backgroundColor: `${theme.palette.primary.main} !important`,
      color: `${theme.palette.primary.contrastText} !important`,
      "&:hover": {
        backgroundColor: theme.palette.primary.main,
      },
    },
    selectedIcon: {
      color: "white",
    },
    unselectedIcon: {
      color: theme.palette.primary.main,
    },
    name: {
      marginLeft: theme.spacing(2),
      color: "#333333",
    },
    avatar: {
      width: theme.spacing(4),
      height: theme.spacing(4),
    },
    notificationIcon: {
      width: theme.spacing(3),
      height: theme.spacing(3),
      cursor: "pointer",
    },
  })
);

export enum EDashboardLayoutSidebarNames {
  HOME = "Home",
  PROGRAMS = "Programs",
  FINANCES = "Finances",
  ANALYTICS = "Analytics",
  PROMOTIONS = "Promotions",
  BUSINESS_CENTER = "Business Center",
  PROFILE = "Profile",
  HELP_CENTER = "Help Center",
  PURCHASES = "Purchases",
}

const drawerItems = [
  {
    text: EDashboardLayoutSidebarNames.HOME,
    link: "/biz",
    IconComponent: HomeIcon,
  },
  {
    text: EDashboardLayoutSidebarNames.PURCHASES,
    link: "/biz/purchases",
    IconComponent: ShoppingCartIcon,
  },
  {
    text: EDashboardLayoutSidebarNames.PROGRAMS,
    link: "/biz/programs",
    IconComponent: LayersIcon,
  },
  {
    text: EDashboardLayoutSidebarNames.FINANCES,
    link: "/biz/finances",
    IconComponent: AccountBalanceWalletIcon,
  },
  {
    text: EDashboardLayoutSidebarNames.ANALYTICS,
    link: "/biz/analytics",
    IconComponent: EqualizerIcon,
  },
  {
    text: EDashboardLayoutSidebarNames.PROMOTIONS,
    link: "/biz/promotions",
    IconComponent: LoyaltyIcon,
  },
  {
    text: EDashboardLayoutSidebarNames.BUSINESS_CENTER,
    link: "/biz/business",
    IconComponent: BusinessCenterIcon,
  },
  {
    text: EDashboardLayoutSidebarNames.PROFILE,
    link: "/biz/profile",
    IconComponent: AccountBoxIcon,
  },
  {
    text: EDashboardLayoutSidebarNames.HELP_CENTER,
    link: "/biz/help",
    IconComponent: ChatIcon,
  },
];

function isSidebarItemSelected(
  activeSidebarName: EDashboardLayoutSidebarNames,
  currentSidebarName: EDashboardLayoutSidebarNames
) {
  return activeSidebarName === currentSidebarName;
}

function isSidebarItemDisabled(
  sidebarItemName: EDashboardLayoutSidebarNames,
  businessProfileIsComplete: boolean
) {
  if (businessProfileIsComplete) {
    switch (sidebarItemName) {
      case EDashboardLayoutSidebarNames.HOME:
        return false;
      case EDashboardLayoutSidebarNames.PROGRAMS:
        return false;
      case EDashboardLayoutSidebarNames.PURCHASES:
        return false;
      case EDashboardLayoutSidebarNames.BUSINESS_CENTER:
        return false;
      default:
        return true;
    }
  } else {
    switch (sidebarItemName) {
      case EDashboardLayoutSidebarNames.HOME:
        return false;
      case EDashboardLayoutSidebarNames.PROFILE:
        return false;
      case EDashboardLayoutSidebarNames.BUSINESS_CENTER:
        return false;
      default:
        return true;
    }
  }
}

const DashboardLayout: FunctionComponent<IProps> = (props) => {
  const { gutterTop = true } = props;
  const programProviderVerified = isProgramProviderVerified(props.user);
  const showBusinessProfileCompletionAlert =
    props.activeSidebarName !== EDashboardLayoutSidebarNames.PROFILE &&
    !programProviderVerified;
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

  const classes = useStyles();

  const handleProfileMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
  };
  const isMenuOpen = Boolean(anchorEl);
  const menuId = "profile-menu";

  const handleLogout = () => {
    TokenService.removeToken();
    Router.push("/");
    handleMenuClose();
  };

  return (
    <div className={classes.root}>
      <AppBar position="fixed" className={classes.appBar} color="transparent">
        <Toolbar classes={{ root: classes.toolbarRoot }}>
          <Grid container justify="space-between" alignItems="center">
            <Grid item xs className={classes.toolbarContainer}>
              <Typography
                component="p"
                style={{
                  fontWeight: "bold",
                  fontSize: "1.125rem",
                  color: "#333333",
                }}
              >
                {props.user.programProvider.name}
              </Typography>
            </Grid>
            <Grid
              item
              xs
              className={classes.toolbarContainer}
              container
              spacing={1}
              direction="row-reverse"
              alignItems="center"
            >
              <Grid item>
                <IconButton
                  aria-label="account of current user"
                  aria-controls="menu-appbar"
                  aria-haspopup="true"
                  color="inherit"
                  style={{ borderRadius: 0 }}
                  onClick={handleProfileMenuOpen}
                >
                  <Avatar
                    alt="Remy Sharp"
                    src="https://randomuser.me/api/portraits/lego/2.jpg"
                    className={classes.avatar}
                  />
                </IconButton>
              </Grid>

              <Grid item>
                <NotificationsIcon
                  color="primary"
                  className={classes.notificationIcon}
                />
              </Grid>
            </Grid>
          </Grid>
          {props.renderBreadcrumb && (
            <Grid
              item
              container
              direction="column"
              style={{ paddingBottom: theme.spacing(2) }}
            >
              <Grid item>{props.renderBreadcrumb()}</Grid>

              <Grid item container justify="space-between" alignItems="center">
                <Grid item>
                  <Typography
                    variant="h6"
                    component="p"
                    style={{ fontWeight: 500 }}
                  >
                    {props.headerTitle}
                  </Typography>
                </Grid>

                {props.renderCancelButton && (
                  <Grid item>{props.renderCancelButton()}</Grid>
                )}
              </Grid>
            </Grid>
          )}
        </Toolbar>
      </AppBar>
      <Menu
        anchorEl={anchorEl}
        anchorOrigin={{ vertical: "top", horizontal: "right" }}
        id={menuId}
        keepMounted
        transformOrigin={{ vertical: "top", horizontal: "right" }}
        open={isMenuOpen}
        onClose={handleMenuClose}
      >
        <MenuItem onClick={() => Router.push("/biz/profile")}>
          Business Profile
        </MenuItem>
        <MenuItem onClick={handleLogout}>Log out</MenuItem>
      </Menu>

      <Drawer
        className={classes.drawer}
        variant="permanent"
        classes={{
          paper: classes.drawerPaper,
        }}
        anchor="left"
        elevation={200}
      >
        <div
          className={clsx(classes.sidebarImage, classes.toolbar)}
          onClick={() => Router.push("/")}
        >
          <img
            src="https://ucarecdn.com/de5d4912-e98c-4ce9-9089-609d0d43c5cd/logoblackfontbluelogo.png"
            height="44px"
          />
        </div>
        <List style={{ paddingTop: 0 }}>
          {drawerItems.map((item, index) => {
            const isSelected = isSidebarItemSelected(
              props.activeSidebarName,
              item.text
            );
            const isDisabled = isSidebarItemDisabled(
              item.text,
              programProviderVerified
            );

            if (item.text === EDashboardLayoutSidebarNames.PROFILE) {
              return (
                <React.Fragment key={`fragment-${index}`}>
                  <Divider key={`divider-${index}`} />

                  <ListSubheader key={`subheader-${index}`}>
                    Account Management
                  </ListSubheader>

                  <ListItem
                    disabled={isDisabled}
                    button
                    key={index}
                    selected={isSelected}
                    classes={{
                      selected: classes.selectedSidebarListItem,
                    }}
                  >
                    <ListItemIcon>
                      <item.IconComponent
                        color="primary"
                        className={
                          isSelected
                            ? classes.selectedIcon
                            : classes.unselectedIcon
                        }
                      />
                    </ListItemIcon>
                    <Link href={item.link}>
                      <ListItemText
                        primary={item.text}
                        classes={{
                          primary: classes.drawerListItemText,
                        }}
                      />
                    </Link>
                  </ListItem>
                </React.Fragment>
              );
            }

            return (
              <ListItem
                disabled={isDisabled}
                button
                key={index}
                selected={isSelected}
                classes={{
                  selected: classes.selectedSidebarListItem,
                }}
              >
                <ListItemIcon>
                  <item.IconComponent
                    color="primary"
                    className={
                      isSelected ? classes.selectedIcon : classes.unselectedIcon
                    }
                  />
                </ListItemIcon>
                <Link href={item.link}>
                  <ListItemText
                    primary={item.text}
                    classes={{
                      primary: classes.drawerListItemText,
                    }}
                  />
                </Link>
              </ListItem>
            );
          })}
        </List>
      </Drawer>

      <main className={classes.content}>
        <div
          className={clsx(classes.toolbar, {
            [classes.toolbarWithGutterBottom]: gutterTop,
            [classes.toolbarGutterWithBreadcrumb]: !!props.renderBreadcrumb,
          })}
        />

        <Grid container direction="column" spacing={4}>
          {showBusinessProfileCompletionAlert && (
            <Grid item container direction="row">
              <PaddedPaper largerPadding fullWidth>
                <CompleteYourBusinessProfileAlert />
              </PaddedPaper>
            </Grid>
          )}
          {props.children}
        </Grid>
      </main>
    </div>
  );
};

interface IProps {
  activeSidebarName: EDashboardLayoutSidebarNames;
  user: User;
  gutterTop?: boolean;
  paddedContent?: boolean;
  renderBreadcrumb?: any;
  headerTitle: string;
  renderCancelButton?: any;
}

export default DashboardLayout;

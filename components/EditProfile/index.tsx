import React from "react";
import { PaddedPaper } from "../../modules/Shared/components/PaddedPaper";

const EditProfile = () => {
  return <PaddedPaper>Editing your profile</PaddedPaper>;
};

export default EditProfile;

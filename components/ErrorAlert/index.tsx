import React, { FunctionComponent } from "react";

import { Alert, AlertTitle } from "@material-ui/lab";

export interface IErrorAlertProps {
  title: string;
  message: string;
  className?: string;
}

const ErrorAlert: FunctionComponent<IErrorAlertProps> = props => (
  <Alert severity="error" className={props.className}>
    <AlertTitle>{props.title}</AlertTitle>
    {props.message}
  </Alert>
);

export default ErrorAlert;

import { createStyles, TableCell, withStyles } from "@material-ui/core";

export const StyledTableCell = withStyles(() =>
  createStyles({
    head: {
      backgroundColor: "rgba(245,245,245,1)",
      color: "rgba(0, 0, 0, 0.85)",
    },
    body: {
      fontSize: 14,
      fontWeight: "bold",
    },
  })
)(TableCell);

import React from "react";

import {
  makeStyles,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@material-ui/core";
import Fair from "../../../types/Fairs.types";
import ButtonLink from "../../ButtonLink";
import { PaddedPaper } from "../../../modules/Shared/components/PaddedPaper";
import { StyledTableCell } from "../StyledTableCell";

const useStyles = makeStyles(() => ({
  normal: {
    fontWeight: 500,
  },
  header: {
    fontWeight: 700,
  },
}));

const FairsTable: React.FC<Props> = (props) => {
  const classes = useStyles();

  return (
    <TableContainer>
      <Table stickyHeader>
        <TableHead>
          <TableRow>
            <StyledTableCell className={classes.header}>Name</StyledTableCell>
            <StyledTableCell className={classes.header}>Status</StyledTableCell>
            <StyledTableCell className={classes.header}>Action</StyledTableCell>
          </TableRow>
        </TableHead>

        <TableBody>
          {props.fairs.length === 0 && (
            <PaddedPaper>No fairs available.</PaddedPaper>
          )}

          {props.fairs.map((fair) => {
            return (
              <TableRow key={fair.id} hover>
                <TableCell
                  className={classes.normal}
                  component="th"
                  scope="row"
                >
                  {fair.name}
                </TableCell>
                <TableCell className={classes.normal}>{fair.status}</TableCell>
                <TableCell>
                  <ButtonLink
                    color="primary"
                    href="/admin/fair/[id]/overview"
                    as={`/admin/fair/${fair.id}/overview`}
                  >
                    Overview
                  </ButtonLink>
                  <ButtonLink
                    color="primary"
                    href="/admin/fair/[id]"
                    as={`/admin/fair/${fair.id}`}
                  >
                    Edit
                  </ButtonLink>
                </TableCell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

type Props = {
  fairs: Fair[];
};

export default FairsTable;

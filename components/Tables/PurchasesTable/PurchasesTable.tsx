import React from "react";

import {
  Grid,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@material-ui/core";
import { format } from "date-fns";
import { FairPackagePurchase } from "../../../types/FairPackagePurchase.types";
import ButtonLink from "../../ButtonLink";
import { StyledTableCell } from "../StyledTableCell";

const formatDate = (date: string) => {
  return format(new Date(date), "d MMMM yyyy hh:mm a");
};

const renderRows = (purchases: FairPackagePurchase[]) => {
  return purchases.map((purchase) => (
    <TableRow key={purchase.id}>
      <TableCell component="th" scope="row">
        {purchase.fairPackage.fair.name}
      </TableCell>
      <TableCell>
        {formatDate(purchase.fairPackage.fair.startDateTime)}
      </TableCell>
      <TableCell>{formatDate(purchase.fairPackage.fair.endDateTime)}</TableCell>
      <TableCell>{purchase.status}</TableCell>
      <TableCell>
        <Grid container spacing={1}>
          <Grid item>
            <ButtonLink
              variant="outlined"
              href="/fair/[slug]"
              as={`/fair/${purchase.fairPackage.fair.slug}`}
            >
              View Fair
            </ButtonLink>
          </Grid>

          <Grid item>
            <ButtonLink
              variant="outlined"
              href="/biz/purchases/[purchaseId]"
              as={`/biz/purchases/${purchase.id}`}
            >
              Manage
            </ButtonLink>
          </Grid>
        </Grid>
      </TableCell>
    </TableRow>
  ));
};

export const PurchasesTable: React.FC<Props> = (props) => {
  return (
    <TableContainer>
      <Table>
        <caption>You have a total of {props.total} purchase(s).</caption>
        <TableHead>
          <TableRow>
            <StyledTableCell>Name</StyledTableCell>
            <StyledTableCell>Start date</StyledTableCell>
            <StyledTableCell>End date</StyledTableCell>
            <StyledTableCell>Status</StyledTableCell>
            <StyledTableCell>Actions</StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>{renderRows(props.purchases)}</TableBody>
      </Table>
    </TableContainer>
  );
};

type Props = {
  purchases: FairPackagePurchase[];
  total: number;
};

import {
  Table,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TableBody,
} from "@material-ui/core";
import React from "react";
import { FairPackagePurchase } from "../../../types/FairPackagePurchase.types";
import { StyledTableCell } from "../StyledTableCell";

const renderRows = (purchases: FairPackagePurchase[]) => {
  return purchases.map((purchase) => (
    <TableRow key={purchase.id}>
      <TableCell component="th" scope="row">
        {purchase.status}
      </TableCell>
    </TableRow>
  ));
};

export const PurchasesReviewTable: React.FC<Props> = (props) => {
  return (
    <TableContainer>
      <Table>
        {props.caption && <caption>{props.caption}</caption>}
        <TableHead>
          <TableRow>
            <StyledTableCell>Status</StyledTableCell>
          </TableRow>
        </TableHead>

        <TableBody>{renderRows(props.purchases)}</TableBody>
      </Table>
    </TableContainer>
  );
};

type Props = {
  purchases: FairPackagePurchase[];
  caption?: string;
};

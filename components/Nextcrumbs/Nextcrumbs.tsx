import { Breadcrumbs, Link, Typography } from "@material-ui/core";
import NextLink from "next/link";
import React from "react";

export const Nextcrumbs: React.FC<Props> = (props) => {
  const renderCrumb = (crumb: Crumb) => {
    const { title, href, as } = crumb;
    if (!href) {
      return (
        <Typography color="textPrimary" key={title}>
          {title}
        </Typography>
      );
    }

    const nextLinkProps: { href: string; key: string; as?: string } = {
      href,
      key: title,
    };

    if (as) {
      nextLinkProps.as = as;
    }

    return (
      <NextLink {...nextLinkProps} passHref>
        <Link>{title}</Link>
      </NextLink>
    );
  };

  return (
    <Breadcrumbs aria-label="breadcrumb">
      {props.crumbs.map(renderCrumb)}
    </Breadcrumbs>
  );
};

type Crumb = {
  title: string;
  href?: string;
  as?: string;
};

type Props = {
  crumbs: Crumb[];
};

import React from "react";

import { Button, Grid, makeStyles } from "@material-ui/core";
import { Field, Form, Formik } from "formik";
import { TextField } from "material-ui-formik-components";
import { object, string } from "yup";

import {
  EProgramProviderTypeLabels,
  EProgramProviderTypes,
} from "../../types/ProgramProvider.types";

const validationSchema = object().shape({
  name: string().required("You need a name to be a Program Provider"),
  website: string()
    .url("This is not a valid URL. E.g https://edudrift.com")
    .required(
      "Program Provider have some form of online presence, we need that."
    ),
  managerTitle: string().required(
    "You are representing this Program Provider, what is your title?"
  ),
});

const useStyles = makeStyles((theme) => ({
  field: {
    marginTop: 0,
    marginBottom: 0,
  },
  submitButtonWrapper: {
    marginTop: theme.spacing(3),
  },
}));

const makeLabelFromType = (label: string, type: EProgramProviderTypes) => {
  if (label === "name") {
    if (type === EProgramProviderTypes.BUSINESS) {
      return "Name of Business";
    } else if (type === EProgramProviderTypes.STUDENT_ORGANISATION) {
      return "Name of Student Organisation";
    }
  } else if (label === "managerTitle") {
    if (type === EProgramProviderTypes.BUSINESS) {
      return "Your title in this business";
    } else if (type === EProgramProviderTypes.STUDENT_ORGANISATION) {
      return "Your title in this student organisation";
    }
  }
};

const RawProgramProviderKeyInformationForm: React.FC<Props> = (props) => {
  const classes = useStyles();

  return (
    <Formik
      initialValues={{ name: "", website: "", managerTitle: "" }}
      validationSchema={validationSchema}
      onSubmit={props.handleSubmit}
    >
      {({ isValid, dirty }) => {
        return (
          <Form autoComplete="off">
            <Grid container>
              <Grid item xs={5} container spacing={2}>
                <Grid item xs={12}>
                  <Field
                    component={TextField}
                    size="small"
                    name="name"
                    type="text"
                    label={makeLabelFromType("name", props.type)}
                    fullWidth
                    variant="outlined"
                    className={classes.field}
                    required
                  />
                </Grid>

                <Grid item xs={12}>
                  <Field
                    component={TextField}
                    size="small"
                    name="managerTitle"
                    type="text"
                    label={makeLabelFromType("managerTitle", props.type)}
                    fullWidth
                    variant="outlined"
                    className={classes.field}
                    required
                  />
                </Grid>

                <Grid item xs={12}>
                  <Field
                    component={TextField}
                    size="small"
                    name="website"
                    type="text"
                    label="Website"
                    fullWidth
                    variant="outlined"
                    className={classes.field}
                    required
                  />
                </Grid>
              </Grid>
            </Grid>
            <div className={classes.submitButtonWrapper}>
              <Button
                disabled={!isValid || !dirty}
                variant="contained"
                color="primary"
                type="submit"
              >
                Start creating this {EProgramProviderTypeLabels[props.type]}
              </Button>
            </div>
          </Form>
        );
      }}
    </Formik>
  );
};

type Props = {
  handleSubmit: (values: any) => Promise<void>;
  type: EProgramProviderTypes;
};

export default RawProgramProviderKeyInformationForm;

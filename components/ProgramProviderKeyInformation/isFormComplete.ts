import { isEmpty } from "lodash";

import ProgramProvider from "../../types/ProgramProvider.types";

export default function(programProvider: ProgramProvider) {
  if (!programProvider) return false;
  const { name, website, managerTitle } = programProvider;

  const required = [name, website, managerTitle];

  return required.every(value => !isEmpty(value));
}

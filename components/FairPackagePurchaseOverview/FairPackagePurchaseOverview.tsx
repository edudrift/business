import React from "react";

import { PaddedPaper } from "../../modules/Shared/components/PaddedPaper";

import { Grid, Typography } from "@material-ui/core";
import { Alert, AlertTitle } from "@material-ui/lab";
import { format } from "date-fns";
import FairPackage from "../../types/FairPackages.types";
import Fair from "../../types/Fairs.types";

export const FairPackagePurchaseOverview: React.FC<Props> = (props) => {
  const formatDate = (dateString: string) => {
    const date = new Date(dateString);
    return format(date, "d MMMM yyyy hh:mm a");
  };

  return (
    <PaddedPaper fullWidth>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Typography variant="h5">{props.package.title} Package</Typography>

          <Typography gutterBottom>{props.fair.name}</Typography>

          <Typography variant="subtitle2">
            {formatDate(props.fair.startDateTime)} -{" "}
            {formatDate(props.fair.endDateTime)}
          </Typography>
        </Grid>

        <Grid item xs={12}>
          <Typography variant="h4" color="primary">
            <strong>${props.package.price}</strong>
          </Typography>
        </Grid>

        {props.alertOptions && (
          <Grid item xs={12}>
            <Alert severity={props.alertOptions.severity}>
              <AlertTitle>{props.alertOptions.title}</AlertTitle>
              {props.alertOptions.body}
            </Alert>
          </Grid>
        )}
      </Grid>
    </PaddedPaper>
  );
};

type Props = {
  package: FairPackage;
  fair: Fair;
  alertOptions?: {
    title: string;
    body: string;
    severity: "error" | "success" | "info" | "warning";
  };
};

import React from "react";

import { Container } from "@material-ui/core";
import { withStyles } from "@material-ui/styles";

import User from "../../types/user.types";
import Navbar from "../Navbar";

const GenericLayout: React.FC<Props> = (props) => {
  if (props.whiteBackground) {
    const GlobalCss = withStyles({
      // @global is handled by jss-plugin-global.
      "@global": {
        // You should target [class*="MuiButton-root"] instead if you nest themes.
        body: {
          background: "#fff",
        },
      },
    })(() => null);

    return (
      <React.Fragment>
        <GlobalCss />
        <Navbar user={props.user} />
        <Container maxWidth="lg">{props.children}</Container>
      </React.Fragment>
    );
  }

  return (
    <React.Fragment>
      <Navbar user={props.user} />
      <Container maxWidth="lg">{props.children}</Container>
    </React.Fragment>
  );
};

type Props = {
  user?: User;
  whiteBackground?: boolean;
};

export default GenericLayout;

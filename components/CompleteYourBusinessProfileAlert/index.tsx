import React from "react";

import { Alert, AlertTitle } from "@material-ui/lab";

import ButtonLink from "../ButtonLink";

const CompleteYourBusinessProfileAlert = () => {
  return (
    <Alert
      variant="filled"
      severity="info"
      action={
        <ButtonLink href="/biz/profile" color="inherit" size="small">
          Complete business profile
        </ButtonLink>
      }
    >
      <AlertTitle>Notice on your Business Profile</AlertTitle>
      You have yet to submit your business profile for verification.
      <br /> Complete now to unlock the whole platform!
    </Alert>
  );
};

export default CompleteYourBusinessProfileAlert;

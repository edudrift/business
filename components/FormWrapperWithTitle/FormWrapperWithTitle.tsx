import { makeStyles, Typography } from "@material-ui/core";
import React from "react";

type Props = {
  title: string;
  renderSubtitle?: any;
};

const useStyles = makeStyles(theme => ({
  root: {},
  title: {
    fontWeight: "bold",
    fontSize: theme.typography.pxToRem(26)
  },
  childrenWrapper: {
    marginTop: theme.spacing(3)
  }
}));

const FormWrapperWithTitle: React.FC<Props> = props => {
  const classes = useStyles();

  const conditionallyRenderSubtitle = () => {
    if (props.renderSubtitle) {
      return props.renderSubtitle();
    }
  };

  return (
    <React.Fragment>
      <Typography
        component="p"
        variant="h6"
        className={classes.title}
        gutterBottom
      >
        {props.title}
      </Typography>

      {conditionallyRenderSubtitle()}

      <div className={classes.childrenWrapper}>{props.children}</div>
    </React.Fragment>
  );
};

export default FormWrapperWithTitle;

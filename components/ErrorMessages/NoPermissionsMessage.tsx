import React from "react";

import { Grid, Typography } from "@material-ui/core";
import ButtonLink from "../ButtonLink";

const NoPermissionsMessage = () => {
  return (
    <Grid item container spacing={2} alignContent="center">
      <Grid item xs={12} style={{ textAlign: "center" }}>
        <Typography>You are not authorised to see this.</Typography>
      </Grid>

      <Grid item xs={12} alignContent="center" style={{ textAlign: "center" }}>
        <ButtonLink variant="outlined" color="primary" href="/">
          Back to home
        </ButtonLink>
      </Grid>
    </Grid>
  );
};

export default NoPermissionsMessage;

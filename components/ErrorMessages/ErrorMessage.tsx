import React from "react";

import { Grid } from "@material-ui/core";
import Error from "next/error";
import NoPermissionsMessage from "./NoPermissionsMessage";
import ReloginMessage from "./ReloginMessage";

const renderComponentAccordingToCode = (code: number) => {
  if (code === 403) {
    return <NoPermissionsMessage />;
  } else if (code === 401) {
    return <ReloginMessage />;
  } else {
    return <Error statusCode={code} />;
  }
};

export const ErrorMessageFromCode: React.FC<Props> = props => {
  return (
    <Grid
      container
      justify="center"
      alignItems="center"
      style={{ height: "100vh" }}
    >
      <Grid item xs={4}>
        {renderComponentAccordingToCode(props.statusCode)}
      </Grid>
    </Grid>
  );
};

type Props = {
  statusCode: number;
};

export default ErrorMessageFromCode;

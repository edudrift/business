import React from "react";

import { Grid, Typography } from "@material-ui/core";
import ButtonLink from "../ButtonLink";

const ReloginMessage = () => {
  return (
    <Grid item container spacing={2} alignContent="center">
      <Grid item xs={12} style={{ textAlign: "center" }}>
        <Typography>
          Your login credentials are no longer working. Please log in again.
        </Typography>
      </Grid>

      <Grid item xs={12} alignContent="center" style={{ textAlign: "center" }}>
        <ButtonLink variant="outlined" color="primary" href="/login">
          Login
        </ButtonLink>
      </Grid>
    </Grid>
  );
};

export default ReloginMessage;

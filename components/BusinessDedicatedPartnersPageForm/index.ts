import BusinessDedicatedPartnersPageForm from "./BusinessDedicatedPartnersPageForm";
import isFormComplete from "./isFormComplete";

export {
  BusinessDedicatedPartnersPageForm,
  isFormComplete as isBusinessDedicatedPartnersPageComplete
};

import { Grid, Typography } from "@material-ui/core";
import React from "react";
import { IBusinessProfile } from "../../utils/IBusinessProfile";

const BusinessDedicatedPartnersPage: React.FC<Props> = () => {
  return (
    <Grid container>
      <Grid item>
        <Typography>Business Dedicated Partners Page</Typography>
      </Grid>
    </Grid>
  );
};

type Props = {
  business: IBusinessProfile;
};

export default BusinessDedicatedPartnersPage;

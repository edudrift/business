import React from "react";

import { Grid, TextField, OutlinedTextFieldProps } from "@material-ui/core";
import { red } from "@material-ui/core/colors";
import ErrorIcon from "@material-ui/icons/Error";
import { Field, FieldProps } from "formik";

import { TypographyWithPxFontAndColor } from "../../../modules/Shared/components/TypographyWithPxFontAndColor";
import { isUndefined, isNil } from "lodash";

const isEmptyString = (str: string) => str === "";
const isNumeric = (str: string) =>
  /^(-)?(([1-9][0-9]*)|(0))(?:\.[0-9]+)?$/.test(str);

export const FormikNumberTextField: React.FC<Props> = ({
  name,
  label,
  variant = "outlined",
  required,
  ...otherProps
}) => {
  return (
    <Grid container spacing={1} direction="column">
      <Field name={name}>
        {({ field, form, meta }: FieldProps) => {
          const fieldValue = field.value;
          const error = meta.error;
          const touched = meta.touched;

          const setFieldValue = form.setFieldValue;
          const setFieldTouched = form.setFieldTouched;

          const hasError = !isUndefined(error);

          const showError = touched && hasError;

          const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
            const newValue = e.target.value;
            if (isEmptyString(newValue) && !touched) {
              setFieldTouched(name, true);
            }

            if (isNumeric(newValue)) {
              setFieldValue(name, Number(newValue));
            } else {
              setFieldValue(name, newValue);
            }
          };

          const initialTextFieldValue = isNil(fieldValue) ? "" : fieldValue;

          return (
            <>
              <Grid item>
                <TextField
                  value={initialTextFieldValue}
                  label={label}
                  fullWidth
                  type="text"
                  error={showError}
                  style={{ margin: 0 }}
                  onChange={handleChange}
                  variant="outlined"
                  {...otherProps}
                />
              </Grid>
              <Grid item>
                {showError && (
                  <Grid item container spacing={1} alignItems="center">
                    {hasError && (
                      <Grid item container>
                        {!isEmptyString(fieldValue) && (
                          <Grid
                            item
                            style={{
                              display: "flex",
                              width: "20px",
                              padding: 0,
                            }}
                          >
                            <ErrorIcon
                              style={{ color: red.A400, fontSize: "1rem" }}
                            />
                          </Grid>
                        )}

                        <Grid
                          item
                          style={{
                            width: "calc(100% - 20px)",
                            wordBreak: "break-word",
                          }}
                        >
                          <TypographyWithPxFontAndColor
                            sizeInPx={11}
                            color={red.A400}
                          >
                            {error}
                          </TypographyWithPxFontAndColor>
                        </Grid>
                      </Grid>
                    )}
                  </Grid>
                )}
              </Grid>
            </>
          );
        }}
      </Field>
    </Grid>
  );
};

interface Props extends OutlinedTextFieldProps {
  name: string;
  label: string;
}

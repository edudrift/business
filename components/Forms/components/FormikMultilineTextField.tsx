import React from "react";

import { TextFieldProps } from "@material-ui/core";
import { Field } from "formik";
import { TextField } from "formik-material-ui";

export const FormikMultilineTextField: React.FC<Props> = ({
  name,
  label,
  placeholder,
  rows,
  required,
}) => {
  const otherProps: TextFieldProps = {};

  if (placeholder) otherProps.placeholder = placeholder;

  return (
    <Field
      component={TextField}
      name={name}
      label={label}
      fullWidth
      type="text"
      variant="outlined"
      required={required}
      style={{ margin: 0 }}
      InputLabelProps={{
        shrink: true,
      }}
      multiline
      rows={rows}
      {...otherProps}
    />
  );
};

type Props = {
  name: string;
  label: string;
  placeholder?: string;
  rows: number;
  required?: boolean;
};

import React from "react";

import {
  Grid,
  IconButton,
  InputAdornment,
  TextFieldProps,
} from "@material-ui/core";
import { red } from "@material-ui/core/colors";
import ErrorIcon from "@material-ui/icons/Error";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import { Field } from "formik";
import { TextField } from "formik-material-ui";
import { TypographyWithPxFontAndColor } from "../../../modules/Shared/components/TypographyWithPxFontAndColor";
import { get } from "lodash";

export const FormikTextField: React.FC<Props> = ({
  name,
  label,
  placeholder,
  required,
  disabled,
  type,
  errors,
  touched,
  ErrorMessageComponent,
}) => {
  const otherProps: TextFieldProps = {};
  const [showPassword, setShowPassword] = React.useState(false);
  const [controlledType, setControlledType] = React.useState(type || "text");

  const handleClickShowPassword = () => {
    if (showPassword) {
      setShowPassword(false);
      setControlledType("password");
    } else {
      setShowPassword(true);
      setControlledType("text");
    }
  };

  if (placeholder) otherProps.placeholder = placeholder;
  if (disabled) otherProps.disabled = disabled;

  if (!errors && !touched) {
    return (
      <Field
        component={TextField}
        name={name}
        label={label}
        fullWidth
        type={type || "text"}
        variant="outlined"
        required={required}
        style={{ margin: 0 }}
        InputLabelProps={{
          shrink: true,
        }}
        FormHelperTextProps={{
          component: () => {
            return null;
          },
        }}
        {...otherProps}
      />
    );
  }

  return (
    <Grid container spacing={1}>
      <Grid item xs>
        <Field
          component={TextField}
          name={name}
          label={label}
          fullWidth
          type={controlledType}
          variant="outlined"
          required={required}
          style={{ margin: 0 }}
          FormHelperTextProps={{
            component: () => {
              return null;
            },
          }}
          InputProps={{
            endAdornment:
              type === "password" ? (
                <InputAdornment position="end">
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={handleClickShowPassword}
                    edge="end"
                  >
                    {showPassword ? <Visibility /> : <VisibilityOff />}
                  </IconButton>
                </InputAdornment>
              ) : null,
          }}
          {...otherProps}
        />
      </Grid>

      <Grid
        item
        container
        spacing={1}
        alignItems="center"
        style={{
          visibility:
            get(errors, name) && get(touched, name) ? "visible" : "hidden",
        }}
      >
        <Grid item style={{ display: "flex", padding: 0 }}>
          <ErrorIcon style={{ color: red.A400, fontSize: "1rem" }} />
        </Grid>

        <Grid item>
          <TypographyWithPxFontAndColor sizeInPx={11} color={red.A400}>
            {(ErrorMessageComponent && <ErrorMessageComponent />) ||
              get(errors, name) ||
              "Clean"}
          </TypographyWithPxFontAndColor>
        </Grid>
      </Grid>
    </Grid>
  );
};

type Props = {
  name: string;
  label: string;
  placeholder?: string;
  required?: boolean;
  disabled?: boolean;
  type?: string;
  errors?: any;
  touched?: any;
  ErrorMessageComponent?: string | React.ComponentType;
};

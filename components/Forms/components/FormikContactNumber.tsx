import React from "react";

import { Grid, makeStyles, TextField as MUITextField } from "@material-ui/core";
import { Field } from "formik";
import { Autocomplete, TextField } from "material-ui-formik-components";
import * as Yup from "yup";

import { countries } from "../../../modules/Countries/countries";
import { Country } from "../../../modules/Countries/types/CountryType";

export const FormikContactNumberDefaultValues = {
  code: {
    label: "Singapore",
    code: "SG",
    phone: "65",
  },
  number: "91239123",
};

export const FormikContactNumberSchema = Yup.object().shape({
  code: Yup.object()
    .shape({
      label: Yup.string().required(),
      code: Yup.string().required(),
      phone: Yup.string().required(),
    })
    .nullable(),
  number: Yup.string()
    .required()
    .nullable(),
});

export type FormikContactNumberType = Yup.InferType<
  typeof FormikContactNumberSchema
>;

// ISO 3166-1 alpha-2
// ⚠️ No support for IE 11
function countryToFlag(isoCode: string) {
  return typeof String.fromCodePoint !== "undefined"
    ? isoCode
        .toUpperCase()
        .replace(/./g, (char) =>
          String.fromCodePoint(char.charCodeAt(0) + 127397)
        )
    : isoCode;
}

const useStyles = makeStyles({
  option: {
    fontSize: 15,
    "& > span": {
      marginRight: 10,
      fontSize: 18,
    },
  },
});

export const FormikContactNumber: React.FC<Props> = ({
  label,
  required,
  namespace,
  ...otherProps
}) => {
  const classes = useStyles();

  return (
    <Grid container spacing={1}>
      <Grid item xs={4}>
        <Field
          required
          component={Autocomplete}
          name={`${namespace}.code`}
          options={countries as Country[]}
          classes={{
            option: classes.option,
          }}
          autoHighlight
          getOptionLabel={(option: Country) => option.label}
          renderOption={(option: Country) => (
            <React.Fragment>
              <span>{countryToFlag(option.code)}</span>
              {option.label} ({option.code}) +{option.phone}
            </React.Fragment>
          )}
          renderInput={(params: any) => {
            return (
              <MUITextField
                {...params}
                label="Code"
                variant="outlined"
                inputProps={{
                  ...params.inputProps,
                  autoComplete: "new-password", // disable autocomplete and autofill
                }}
                fullWidth
                required={required}
              />
            );
          }}
        />
      </Grid>

      <Grid item xs>
        <Field
          component={TextField}
          name={`${namespace}.number`}
          label={label}
          fullWidth
          type="text"
          variant="outlined"
          required={required}
          style={{ margin: 0 }}
          FormHelperTextProps={{
            component: () => {
              return null;
            },
          }}
          {...otherProps}
        />
      </Grid>
    </Grid>
  );
};

type Props = {
  label: string;
  namespace: string;

  required?: boolean;
};

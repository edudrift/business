import React from "react";

import { Grid, FormControlLabel, Checkbox } from "@material-ui/core";
import { red } from "@material-ui/core/colors";
import ErrorIcon from "@material-ui/icons/Error";
import { Field } from "formik";
import { TextField } from "formik-material-ui";
import CheckBoxOutlineBlankIcon from "@material-ui/icons/CheckBoxOutlineBlank";
import CheckBoxIcon from "@material-ui/icons/CheckBox";

import { TypographyWithPxFontAndColor } from "../../../modules/Shared/components/TypographyWithPxFontAndColor";

export const FormikNullableTextField: React.FC<Props> = ({
  name,
  label,
  required,
  errors,
  touched,
  nullableLabel,
  setFieldValue,
  ...otherProps
}) => {
  const [nulled, setNulled] = React.useState(true);

  const showError = errors[name] && touched[name];
  return (
    <Grid container spacing={1} direction="column">
      <Grid item xs>
        <Field
          component={TextField}
          name={name}
          label={label}
          fullWidth
          type="text"
          variant="outlined"
          required={required}
          style={{ margin: 0 }}
          FormHelperTextProps={{
            component: () => {
              return null;
            },
          }}
          disabled={nulled}
          {...otherProps}
        />
      </Grid>

      {showError ? (
        <Grid
          item
          container
          spacing={1}
          alignItems="center"
          style={{
            visibility: showError ? "visible" : "hidden",
          }}
        >
          <Grid item style={{ display: "flex", padding: 0 }}>
            <ErrorIcon style={{ color: red.A400, fontSize: "1rem" }} />
          </Grid>

          <Grid item>
            <TypographyWithPxFontAndColor sizeInPx={11} color={red.A400}>
              {errors[name] || "Clean"}
            </TypographyWithPxFontAndColor>
          </Grid>
        </Grid>
      ) : (
        <Grid item>
          <FormControlLabel
            value="end"
            control={
              <Checkbox
                color="primary"
                checked={nulled}
                onChange={(e) => {
                  setFieldValue(name, "");

                  setNulled(e.target.checked);
                  setFieldValue(name, null);
                }}
                size="small"
                style={{ fontSize: "1rem" }}
                icon={<CheckBoxOutlineBlankIcon />}
                checkedIcon={<CheckBoxIcon />}
              />
            }
            label={nullableLabel}
            labelPlacement="end"
          />
        </Grid>
      )}
    </Grid>
  );
};

type Props = {
  name: string;
  label: string;
  errors: any;
  touched: any;
  setFieldValue: any;
  nullableLabel: string;
  required?: boolean;
};

import React from "react";

import {
  Grid,
  FormControlLabel,
  Checkbox,
  TextField,
  OutlinedTextFieldProps,
} from "@material-ui/core";
import { red } from "@material-ui/core/colors";
import ErrorIcon from "@material-ui/icons/Error";
import { Field, FieldProps } from "formik";
import CheckBoxOutlineBlankIcon from "@material-ui/icons/CheckBoxOutlineBlank";
import CheckBoxIcon from "@material-ui/icons/CheckBox";

import { TypographyWithPxFontAndColor } from "../../../modules/Shared/components/TypographyWithPxFontAndColor";
import { isNull, isUndefined, isEmpty, isNil } from "lodash";
import { requiredText } from "../../../utils/formikUtils";

const isEmptyString = (str: string) => str === "";
const isNumeric = (str: string) => /^[-]?[0-9]+$/.test(str);

export const FormikNullTextField: React.FC<Props> = ({
  name,
  label,
  required,
  nullableLabel,
  ...otherProps
}) => {
  return (
    <Grid container spacing={1} direction="column">
      <Field name={name}>
        {({ field, form, meta }: FieldProps) => {
          const fieldValue = field.value;
          const error = meta.error;
          const touched = meta.touched;

          const setFieldValue = form.setFieldValue;
          const setFieldTouched = form.setFieldTouched;

          const [nulled, setNulled] = React.useState(isNull(fieldValue));
          const [savedFieldValue, saveFieldValue] = React.useState();

          const hasError = !isUndefined(error);
          const isEmptyStringAndRequired =
            required && isEmptyString(fieldValue);

          const showError =
            touched && !nulled && (hasError || isEmptyStringAndRequired);

          const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
            const newValue = e.target.value;
            if (isEmptyString(newValue) && !touched) {
              setFieldTouched(name, true);
            }

            if (otherProps.type === "number" && isNumeric(newValue)) {
              setFieldValue(name, parseInt(newValue));
            } else {
              setFieldValue(name, newValue);
            }
          };

          const handleCheck = (e: React.ChangeEvent<HTMLInputElement>) => {
            const isChecked = e.target.checked;
            setNulled(isChecked);
            if (isChecked) {
              saveFieldValue(fieldValue);
              setFieldValue(name, null);
            } else {
              setFieldValue(name, savedFieldValue);
            }
          };
          const initialTextFieldValue = isNil(fieldValue) ? "" : fieldValue;

          return (
            <>
              <Grid item>
                <TextField
                  value={initialTextFieldValue}
                  label={label}
                  fullWidth
                  type="text"
                  required={!nulled && required}
                  error={showError}
                  style={{ margin: 0 }}
                  onChange={handleChange}
                  disabled={nulled}
                  {...otherProps}
                />
              </Grid>
              <Grid item>
                {showError && (
                  <Grid item container spacing={1} alignItems="center">
                    {isEmptyStringAndRequired && (
                      <Grid item>
                        <TypographyWithPxFontAndColor
                          sizeInPx={11}
                          color={red.A400}
                        >
                          {requiredText}
                        </TypographyWithPxFontAndColor>
                      </Grid>
                    )}
                    {hasError && (
                      <Grid item container>
                        <Grid
                          item
                          style={{
                            display: "flex",
                            width: "20px",
                            padding: 0,
                          }}
                        >
                          <ErrorIcon
                            style={{ color: red.A400, fontSize: "1rem" }}
                          />
                        </Grid>

                        <Grid
                          item
                          style={{
                            width: "calc(100% - 20px)",
                            wordBreak: "break-word",
                          }}
                        >
                          <TypographyWithPxFontAndColor
                            sizeInPx={11}
                            color={red.A400}
                          >
                            {error}
                          </TypographyWithPxFontAndColor>
                        </Grid>
                      </Grid>
                    )}
                  </Grid>
                )}
                {isEmpty(fieldValue) && (
                  <Grid item>
                    <FormControlLabel
                      value="end"
                      control={
                        <Checkbox
                          color="primary"
                          checked={nulled}
                          onChange={handleCheck}
                          size="small"
                          style={{ fontSize: "1rem" }}
                          icon={<CheckBoxOutlineBlankIcon />}
                          checkedIcon={<CheckBoxIcon />}
                        />
                      }
                      label={nullableLabel}
                      labelPlacement="end"
                    />
                  </Grid>
                )}
              </Grid>
            </>
          );
        }}
      </Field>
    </Grid>
  );
};

interface Props extends OutlinedTextFieldProps {
  name: string;
  label: string;
  nullableLabel: string;
}

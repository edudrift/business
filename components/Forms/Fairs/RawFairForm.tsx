import { Button, Grid, makeStyles } from "@material-ui/core";
import { Field, Form, Formik } from "formik";
import { TextField } from "formik-material-ui";
import React from "react";

import Fair from "~/types/Fairs.types";

import { DateTimePicker } from "material-ui-formik-components";
import FairDetailsSchema, { FairDetailsSchemaAsType } from "./schema";

const useStyles = makeStyles((theme) => ({
  root: {
    paddingTop: theme.spacing(2),
  },
  field: {
    // marginBottom: theme.spacing(1)
  },
}));

const RawFairForm: React.FC<Props> = (props) => {
  const classes = useStyles();

  const buildInitialValues = (fair?: Fair) => {
    if (fair) {
      return {
        name: fair.name,
        description: fair.description,
        startDateTime: new Date(fair.startDateTime),
        endDateTime: new Date(fair.endDateTime),
        slug: fair.slug,
      };
    }

    return {
      name: "",
      description: "",
      startDateTime: new Date(),
      endDateTime: new Date(),
      slug: "",
    };
  };

  return (
    <Formik
      initialValues={buildInitialValues(props.fair)}
      onSubmit={props.onSubmit}
      validationSchema={FairDetailsSchema}
    >
      {({ values }) => {
        return (
          <Form className={classes.root}>
            <Grid container spacing={2}>
              <Grid item container spacing={2} xs={6}>
                <Grid item xs={12}>
                  <Field
                    component={TextField}
                    size="small"
                    name="name"
                    type="text"
                    label="Name of Fair"
                    fullWidth
                    variant="outlined"
                    className={classes.field}
                  />
                </Grid>

                <Grid item xs={6}>
                  <Field
                    required
                    name="startDateTime"
                    component={DateTimePicker}
                    label="Start date and time"
                    format="d MMMM yyyy hh:mm a"
                    inputVariant="outlined"
                    style={{ margin: 0 }}
                    disablePast
                  />
                </Grid>

                <Grid item xs={6}>
                  <Field
                    required
                    name="endDateTime"
                    component={DateTimePicker}
                    label="End date and time"
                    format="d MMMM yyyy hh:mm a"
                    inputVariant="outlined"
                    style={{ margin: 0 }}
                    disablePast
                    minDate={values.startDateTime}
                  />
                </Grid>
              </Grid>
              <Grid item container spacing={2} xs={6}>
                <Grid item xs={12}>
                  <Field
                    component={TextField}
                    size="small"
                    name="slug"
                    type="text"
                    label="Slug"
                    fullWidth
                    variant="outlined"
                    className={classes.field}
                  />
                </Grid>
                <Grid item xs={12}>
                  <Field
                    component={TextField}
                    size="small"
                    name="description"
                    type="text"
                    label="Description"
                    fullWidth
                    variant="outlined"
                    className={classes.field}
                    multiline
                    rows={4}
                  />
                </Grid>
              </Grid>

              <Grid item xs={12}>
                <Button type="submit" variant="contained" color="primary">
                  {props.submitText}
                </Button>
              </Grid>
            </Grid>
          </Form>
        );
      }}
    </Formik>
  );
};

type Props = {
  onSubmit: (values: FairDetailsSchemaAsType) => Promise<void>;
  fair?: Fair;
  submitText: string;
};

export default RawFairForm;

import React from "react";

import { Button, Divider, Grid, Typography } from "@material-ui/core";

import FairPackage from "~/types/FairPackages.types";
import Fair from "~/types/Fairs.types";
import FairsService from "~/utils/Fairs.service";
import ButtonLink from "../../ButtonLink";
import { PaddedPaper } from "../../../modules/Shared/components/PaddedPaper";
import SingleFairPackageForm from "./SingleFairPackageForm";

const RawFairPackagesForm: React.FC<Props> = (props) => {
  const [packages, setPackages] = React.useState<FairPackage[]>(
    props.fair.packages || []
  );

  const addNewPackage = async () => {
    try {
      const fairPackage = await FairsService.initialiseNewPackage(props.fair);
      setPackages([...packages, fairPackage]);
    } catch (e) {
      alert(e);
    }
  };

  return (
    <Grid container spacing={2}>
      {packages.map((p, index) => (
        <Grid item xs={4} key={index}>
          <PaddedPaper withElevation={1}>
            <Typography
              variant="h5"
              style={{ fontWeight: "bold", marginBottom: "16px" }}
            >
              Package #{index + 1}
            </Typography>

            <SingleFairPackageForm fairPackage={p} />
          </PaddedPaper>
        </Grid>
      ))}

      <Grid item xs={12}>
        <Divider variant="middle" />
      </Grid>

      <Grid
        item
        container
        spacing={2}
        justify="space-between"
        alignItems="center"
      >
        <Grid item>
          <Button
            onClick={addNewPackage}
            size="small"
            variant="outlined"
            color="primary"
          >
            Initialise New Package
          </Button>
        </Grid>

        <Grid item>
          <ButtonLink
            href="/admin/fair/[id]/overview"
            as={`/admin/fair/${props.fair.id}/overview`}
            variant="contained"
            color="primary"
          >
            Proceed to overview
          </ButtonLink>
        </Grid>
      </Grid>
    </Grid>
  );
};

type Props = {
  fair: Fair;
};

export default RawFairPackagesForm;

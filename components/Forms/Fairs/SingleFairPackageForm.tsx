import { Field, Form, Formik } from "formik";
import { TextField } from "formik-material-ui";
import React from "react";

import { Button, Divider, Grid, InputAdornment } from "@material-ui/core";
import AttachMoneyIcon from "@material-ui/icons/AttachMoney";
import ConfirmationNumberIcon from "@material-ui/icons/ConfirmationNumber";
import PhotoSizeSelectActualIcon from "@material-ui/icons/PhotoSizeSelectActual";
import PictureAsPdfIcon from "@material-ui/icons/PictureAsPdf";
import RecordVoiceOverIcon from "@material-ui/icons/RecordVoiceOver";
import FairPackage from "~/types/FairPackages.types";
import FairsService from "../../../utils/Fairs.service";
import { FairPackageSchema, FairPackageSchemaAsType } from "./schema";

const SingleFairPackageForm: React.FC<Props> = (props) => {
  const [initialValues, setInitialValues] = React.useState<
    FairPackageSchemaAsType
  >({
    title: props.fairPackage.title,
    capacity: props.fairPackage.capacity,
    price: props.fairPackage.price,
    maxBrochures: props.fairPackage.maxBrochures,
    maxConsultants: props.fairPackage.maxConsultants,
    maxPhotos: props.fairPackage.maxPhotos,
    additionalBenefits: props.fairPackage.additionalBenefits,
  });

  const handleSubmit = async (values: FairPackageSchemaAsType) => {
    await FairsService.updateFairPackage(
      props.fairPackage.fairId,
      props.fairPackage.id,
      values
    );

    setInitialValues(values);
  };

  return (
    <Formik
      onSubmit={handleSubmit}
      initialValues={initialValues}
      validationSchema={FairPackageSchema}
    >
      {({ dirty }) => {
        return (
          <Form>
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <Field
                  component={TextField}
                  size="small"
                  name="title"
                  type="text"
                  label="Title of Package"
                  fullWidth
                  variant="outlined"
                />
              </Grid>

              <Grid item xs={12}>
                <Field
                  component={TextField}
                  size="small"
                  name="capacity"
                  type="number"
                  label="Number of purchasable packages"
                  fullWidth
                  variant="outlined"
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <ConfirmationNumberIcon />
                      </InputAdornment>
                    ),
                  }}
                />
              </Grid>

              <Grid item xs={12}>
                <Field
                  component={TextField}
                  size="small"
                  name="price"
                  type="number"
                  label="Price per package"
                  fullWidth
                  variant="outlined"
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <AttachMoneyIcon />
                      </InputAdornment>
                    ),
                  }}
                />
              </Grid>

              <Grid item xs={12}>
                <Divider variant="middle" />
              </Grid>

              <Grid item xs={12}>
                <Field
                  component={TextField}
                  size="small"
                  name="maxPhotos"
                  type="number"
                  label="Number of photos"
                  fullWidth
                  variant="outlined"
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <PhotoSizeSelectActualIcon />
                      </InputAdornment>
                    ),
                  }}
                />
              </Grid>

              <Grid item xs={12}>
                <Field
                  component={TextField}
                  size="small"
                  name="maxBrochures"
                  type="number"
                  label="Number of brochures"
                  fullWidth
                  variant="outlined"
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <PictureAsPdfIcon />
                      </InputAdornment>
                    ),
                  }}
                />
              </Grid>

              <Grid item xs={12}>
                <Field
                  component={TextField}
                  size="small"
                  name="maxConsultants"
                  type="number"
                  label="Number of consultants"
                  fullWidth
                  variant="outlined"
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <RecordVoiceOverIcon />
                      </InputAdornment>
                    ),
                  }}
                />
              </Grid>

              <Grid item xs={12}>
                <Divider variant="middle" />
              </Grid>

              <Grid item xs={12}>
                <Field
                  component={TextField}
                  size="small"
                  name="additionalBenefits"
                  type="text"
                  label="Additional benefits"
                  fullWidth
                  variant="outlined"
                  multiline
                  rows={4}
                />
              </Grid>

              {dirty && (
                <Grid item xs>
                  <Button type="submit" variant="contained" color="primary">
                    Save
                  </Button>
                </Grid>
              )}
            </Grid>
          </Form>
        );
      }}
    </Formik>
  );
};

type Props = {
  fairPackage: FairPackage;
};

export default SingleFairPackageForm;

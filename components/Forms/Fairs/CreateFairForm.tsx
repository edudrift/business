import React from "react";

import { useRouter } from "next/router";
import FairsService, { CreateFairsError } from "../../../utils/Fairs.service";
import { PaddedPaper } from "../../../modules/Shared/components/PaddedPaper";

import RawFairForm from "./RawFairForm";
import { FairDetailsSchemaAsType } from "./schema";
import StepperWithTitle from "../../../modules/Shared/components/Stepper/StepperWithTitle";

const CreateFairForm = () => {
  const [activeStep] = React.useState(0);
  const router = useRouter();

  const handleSubmit = async (values: FairDetailsSchemaAsType) => {
    try {
      const fair = await FairsService.tryToCreateFair(values);

      router.push("/admin/fair/:id", `/admin/fair/${fair.id}?startAtPackage=1`);
    } catch (e) {
      if (e instanceof CreateFairsError) {
        alert("Error creating the fair. Try again.");
      }
    }
  };

  return (
    <PaddedPaper>
      <StepperWithTitle
        activeStep={activeStep}
        steps={[
          {
            title: "Fair's Details",
            stepTitle: "Fair's Details",
            subtitle: "Details about the fair",
            isStepComplete: () => false,
          },
          {
            title: "Fair's Pricing Packages",
            stepTitle: "Fair's Pricing Packages",
            subtitle: "How are we charging our vendors?",
            isStepComplete: () => false,
          },
        ]}
      />

      <RawFairForm onSubmit={handleSubmit} submitText="Save and continue" />
    </PaddedPaper>
  );
};

export default CreateFairForm;

import React from "react";

import Fair from "../../../types/Fairs.types";
import FairsService, { CreateFairsError } from "../../../utils/Fairs.service";
import { PaddedPaper } from "../../../modules/Shared/components/PaddedPaper";
import StepperWithTitle from "../../../modules/Shared/components/Stepper/StepperWithTitle";
import RawFairForm from "./RawFairForm";
import RawFairPackagesForm from "./RawFairPackagesForm";
import { FairDetailsSchemaAsType } from "./schema";

const EditFairForm: React.FC<Props> = (props) => {
  const [activeStep, setActiveStep] = React.useState(
    props.startAtPackage ? 1 : 0
  );

  const handleFairsDetails = async (values: FairDetailsSchemaAsType) => {
    try {
      await FairsService.updateFairDetails(props.fair, values);
    } catch (e) {
      if (e instanceof CreateFairsError) {
        alert("Error creating the fair. Try again.");
      }
    }
  };

  return (
    <PaddedPaper>
      <StepperWithTitle
        activeStep={activeStep}
        onStepClick={setActiveStep}
        steps={[
          {
            title: "Fair's Details",
            stepTitle: "Fair's Details",
            subtitle: "Details about the fair",
            isStepComplete: () => false,
          },
          {
            title: "Fair's Pricing Packages",
            stepTitle: "Fair's Pricing Packages",
            subtitle: "How are we charging our vendors?",
            isStepComplete: () => false,
          },
        ]}
      />

      {activeStep === 0 && (
        <RawFairForm
          fair={props.fair}
          onSubmit={handleFairsDetails}
          submitText="Save and continue"
        />
      )}

      {activeStep === 1 && <RawFairPackagesForm fair={props.fair} />}
    </PaddedPaper>
  );
};

type Props = {
  fair: Fair;
  startAtPackage: boolean;
};

export default EditFairForm;

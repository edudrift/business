import * as Yup from "yup";
import { requiredText } from "../../../utils/formikUtils";

export const FairDetailsSchema = Yup.object().shape({
  name: Yup.string().required(requiredText),
  description: Yup.string().required(requiredText),
  startDateTime: Yup.date().required(requiredText),
  endDateTime: Yup.date().required(requiredText)
});

export const FairPackageSchema = Yup.object().shape({
  title: Yup.string().required(requiredText),
  capacity: Yup.number().required(requiredText),
  price: Yup.number().required(requiredText),
  maxPhotos: Yup.number().required(requiredText),
  maxBrochures: Yup.number().required(requiredText),
  maxConsultants: Yup.number().required(requiredText),
  additionalBenefits: Yup.string()
});

export type FairDetailsSchemaAsType = Yup.InferType<typeof FairDetailsSchema>;
export type FairPackageSchemaAsType = Yup.InferType<typeof FairPackageSchema>;

export default FairDetailsSchema;

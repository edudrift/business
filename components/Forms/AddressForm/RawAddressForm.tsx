import { Button, Grid } from "@material-ui/core";
import { Form, Formik } from "formik";
import { find } from "lodash";
import React from "react";

import { countries } from "../../../modules/Countries/countries";
import { AddressFormFields } from "./AddressFormFields";
import {
  AddressSchema,
  AddressSchemaFromDatabaseType,
  AddressSchemaType,
} from "./schema";

const buildInitialValues = (
  address?: AddressSchemaFromDatabaseType
): AddressSchemaType => {
  if (address) {
    const country = find(countries, { code: address.countryCode });

    if (country) {
      return {
        street: address.street,
        street2: address.street2,
        postalCode: address.postalCode,

        country,
        city: address.city,
        state: address.state,
      };
    }
  }

  return {
    street: "",
    street2: "",
    postalCode: "",
    country: {
      code: "",
      label: "",
      phone: "",
    },
    city: "",
    state: "",
  };
};

const RawAddressForm: React.FC<Props> = (props) => {
  const { address } = props;
  return (
    <Formik
      initialValues={buildInitialValues(address)}
      validationSchema={AddressSchema}
      onSubmit={props.onSubmit}
    >
      {({ values, setValues }) => {
        return (
          <Form>
            <AddressFormFields values={values} setValues={setValues} />
            <Grid item xs={12}>
              <Button variant="contained" color="primary" type="submit">
                {props.submitText}
              </Button>
            </Grid>
          </Form>
        );
      }}
    </Formik>
  );
};

type Props = {
  onSubmit: (values: AddressSchemaType) => Promise<void>;
  address?: AddressSchemaFromDatabaseType;
  submitText?: string;
};

RawAddressForm.defaultProps = {
  submitText: "Save",
};

export default RawAddressForm;

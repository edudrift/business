import * as yup from "yup";
import { requiredText } from "../../../utils/formikUtils";

export const AddressSchema = yup.object().shape({
  street: yup.string().required(requiredText),
  street2: yup.string().nullable(),
  postalCode: yup.string().required(),
  country: yup
    .object()
    .shape({
      label: yup.string().required(),
      code: yup.string().required(),
      phone: yup.string().required(),
    })
    .required(requiredText),
  city: yup.string().nullable(),
  state: yup.string().nullable(),
});

export const OptionalAddressSchema = yup.object().shape({
  street: yup.string(),
  street2: yup.string().nullable(),
  postalCode: yup.string(),
  country: yup
    .object()
    .shape({
      label: yup.string(),
      code: yup.string(),
      phone: yup.string(),
    })
    .required(requiredText),
  city: yup.string().nullable(),
  state: yup.string().nullable(),
});

export type AddressSchemaType = {
  street: string;
  street2?: string;
  postalCode: string;
  country: {
    label: string;
    code: string;
    phone: string;
  };
  city?: string;
  state?: string;
};

export type AddressSchemaFromDatabaseType = {
  id: string;
  street: string;
  street2?: string;
  postalCode: string;
  countryCode: string;
  city?: string;
  state?: string;
};

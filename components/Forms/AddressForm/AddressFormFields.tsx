import { Grid, makeStyles, TextField as MUITextField } from "@material-ui/core";
import { Field } from "formik";
import { Autocomplete, TextField } from "material-ui-formik-components";
import React from "react";
import { countries } from "../../../modules/Countries/countries";

// ISO 3166-1 alpha-2
// ⚠️ No support for IE 11
function countryToFlag(isoCode: string) {
  return typeof String.fromCodePoint !== "undefined"
    ? isoCode
        .toUpperCase()
        .replace(/./g, (char) =>
          String.fromCodePoint(char.charCodeAt(0) + 127397)
        )
    : isoCode;
}

const useStyles = makeStyles({
  option: {
    fontSize: 15,
    "& > span": {
      marginRight: 10,
      fontSize: 18,
    },
  },
});

const buildFieldName = (name: string, namespace?: string) => {
  if (!namespace) return name;

  return `${namespace}.${name}`;
};

export const AddressFormFields = ({
  values,
  setValues,
  namespace,
}: {
  values: any;
  setValues: any;
  namespace?: string;
}) => {
  const classes = useStyles();

  return (
    <React.Fragment>
      <Grid item container direction="column">
        <Grid item>
          <Field
            size="small"
            required
            value={namespace ? values[namespace].country : values.country}
            onChange={(_: any, selected: CountryType) => {
              if (namespace) {
                console.log({ namespace, values });
                const namespacedValue = values[namespace];
                namespacedValue.country = selected;

                setValues({ ...values, [namespace]: namespacedValue });
              } else {
                setValues({ ...values, country: selected });
              }
            }}
            component={Autocomplete}
            name={buildFieldName("countryCode", namespace)}
            options={countries as CountryType[]}
            classes={{
              option: classes.option,
            }}
            autoHighlight
            getOptionLabel={(option: CountryType) => option.label}
            renderOption={(option: CountryType) => (
              <React.Fragment>
                <span>{countryToFlag(option.code)}</span>
                {option.label} ({option.code}) +{option.phone}
              </React.Fragment>
            )}
            renderInput={(params: any) => {
              return (
                <MUITextField
                  {...params}
                  label="Choose a country"
                  variant="outlined"
                  inputProps={{
                    ...params.inputProps,
                    autoComplete: "new-password", // disable autocomplete and autofill
                  }}
                  fullWidth
                  required
                />
              );
            }}
          />
        </Grid>

        <Grid item>
          <Field
            required
            component={TextField}
            size="small"
            name={buildFieldName("street", namespace)}
            type="text"
            label="Street"
            fullWidth
            variant="outlined"
          />
        </Grid>

        <Grid item>
          <Field
            component={TextField}
            size="small"
            name={buildFieldName("street2", namespace)}
            type="text"
            label="Street Line 2"
            fullWidth
            variant="outlined"
            placeholder="Street here"
            InputLabelProps={{
              shrink: true,
            }}
          />
        </Grid>

        <Grid item>
          <Field
            required
            component={TextField}
            size="small"
            name={buildFieldName("postalCode", namespace)}
            type="text"
            label="Postal Code"
            fullWidth
            variant="outlined"
          />
        </Grid>

        <Grid item>
          <Field
            component={TextField}
            size="small"
            name={buildFieldName("city", namespace)}
            type="text"
            label="City"
            fullWidth
            variant="outlined"
          />
        </Grid>

        <Grid item>
          <Field
            component={TextField}
            size="small"
            name={buildFieldName("state", namespace)}
            type="text"
            label="State"
            fullWidth
            variant="outlined"
          />
        </Grid>
      </Grid>
    </React.Fragment>
  );
};

interface CountryType {
  code: string;
  label: string;
  phone: string;
}

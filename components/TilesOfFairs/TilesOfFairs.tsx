import React from "react";

import {
  Card,
  CardActions,
  CardContent,
  Grid,
  makeStyles,
  Typography
} from "@material-ui/core";
import Fair from "../../types/Fairs.types";
import ButtonLink from "../ButtonLink";

const useStyles = makeStyles({
  root: {
    minWidth: 275
  },
  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)"
  },
  title: {
    fontSize: 14
  },
  pos: {
    marginBottom: 12
  }
});

const TilesOfFairs: React.FC<Props> = props => {
  const classes = useStyles();
  return (
    <Grid container spacing={2}>
      {props.fairs.map(fair => (
        <Grid item key={fair.id}>
          <Card className={classes.root}>
            <CardContent>
              <Typography
                className={classes.title}
                color="textSecondary"
                gutterBottom
              >
                Word of the Day
              </Typography>
              <Typography variant="h5" component="h2">
                {fair.name}
              </Typography>
              <Typography className={classes.pos} color="textSecondary">
                adjective
              </Typography>
              <Typography variant="body2" component="p">
                well meaning and kindly.
                <br />
                {'"a benevolent smile"'}
              </Typography>
            </CardContent>
            <CardActions>
              <ButtonLink
                size="small"
                href="/fair/[slug]"
                as={`/fair/${fair.slug}`}
              >
                Learn More
              </ButtonLink>
            </CardActions>
          </Card>
        </Grid>
      ))}
    </Grid>
  );
};

type Props = {
  fairs: Fair[];
};

export default TilesOfFairs;
